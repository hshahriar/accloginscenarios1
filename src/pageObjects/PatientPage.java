package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;


public class PatientPage {
	
	private static WebElement element =null;
	private static Select oSelection =null;
	
	public WebElement patientPageMethod (WebDriver driver, String type){
		
		if (type.equals("Patient -- Add & Search"))
		{element = driver.findElement(By.partialLinkText("Patient -- Add & Search"));}
		//2000
		else if (type.equals("LastName"))
		{element = driver.findElement(By.id("LastName"));}
		
		//2010
		else if (type.equals("FirstName"))
		{element = driver.findElement(By.id("FirstName"));}
		
		//2020
		else if (type.equals("MidName"))
		{element = driver.findElement(By.id("MidName"));}
		
		//2050
		else if (type.equals("DOB"))
		{element = driver.findElement(By.id("DOB"));}
		
		//2030
		else if (type.equals("SSN"))
		{element = driver.findElement(By.id("SSN"));}
		
		//2031
		else if (type.equals("SSNNA"))
		{element = driver.findElement(By.id("SSNNA"));}
		
		//2040
		else if (type.equals("Pid"))
		{element = driver.findElement(By.id("Pid"));}
		
		//ICD they coded as "pid" for ICD only
		else if (type.equals("pid"))
		{element = driver.findElement(By.id("pid"));}
		
		//2045
		else if (type.equals("OtherID"))
		{element = driver.findElement(By.id("OtherID"));}
		
		//2060
		else if (type.equals("Sex"))
		{element = driver.findElement(By.id("Sex"));}
				
		//2076
		else if (type.equals("HispOrig"))
		{element = driver.findElement(By.id("HispOrig"));}
		
		//2070
		else if (type.equals("RaceWhite"))
		{element = driver.findElement(By.id("RaceWhite"));}
		
		//2071
		else if (type.equals("RaceBlack"))
		{element = driver.findElement(By.id("RaceBlack"));}
		
		//2073
		else if (type.equals("RaceAmIndian"))
		{element = driver.findElement(By.id("RaceAmIndian"));}
		
		//2072
		else if (type.equals("RaceAsian"))
		{element = driver.findElement(By.id("RaceAsian"));}
		
		//2080
		else if (type.equals("RaceAsianIndian"))
		{element = driver.findElement(By.id("RaceAsianIndian"));}
		
		//2081
		else if (type.equals("RaceChinese"))
		{element = driver.findElement(By.id("RaceChinese"));}
		
		//2082
		else if (type.equals("RaceFilipino"))
		{element = driver.findElement(By.id("RaceFilipino"));}
		
		//2083
		else if (type.equals("RaceJapanese"))
		{element = driver.findElement(By.id("RaceJapanese"));}
		
		//2084
		else if (type.equals("RaceKorean"))
		{element = driver.findElement(By.id("RaceKorean"));}
		
		//2085
		else if (type.equals("RaceVietnamese"))
		{element = driver.findElement(By.id("RaceVietnamese"));}
		
		//2086
		else if (type.equals("RaceAsianOther"))
		{element = driver.findElement(By.id("RaceAsianOther"));}
		
		//2074
		else if (type.equals("RaceNatHaw"))
		{element = driver.findElement(By.id("RaceNatHaw"));}
		
		//2090
		else if (type.equals("RaceNativeHawaii"))
		{element = driver.findElement(By.id("RaceNativeHawaii"));}
		
		//2091
		else if (type.equals("RaceGuamChamorro"))
		{element = driver.findElement(By.id("RaceGuamChamorro"));}
		
		//2092
		else if (type.equals("RaceSamoan"))
		{element = driver.findElement(By.id("RaceSamoan"));}
		
		//2093
		else if (type.equals("RacePacificIslandOther"))
		{element = driver.findElement(By.id("RacePacificIslandOther"));}
		
		//2100
		else if (type.equals("HispEthnicityMexican"))
		{element = driver.findElement(By.id("HispEthnicityMexican"));}
		
		//2101
		else if (type.equals("HispEthnicityPuertoRico"))
		{element = driver.findElement(By.id("HispEthnicityPuertoRico"));}
		
		//2102
		else if (type.equals("HispEthnicityCuban"))
		{element = driver.findElement(By.id("HispEthnicityCuban"));}
		
		//2103
		else if (type.equals("HispEthnicityOtherOrigin"))
		{element = driver.findElement(By.id("HispEthnicityOtherOrigin"));}
		
		//2500
		else if (type.equals("Aux1"))
		{element = driver.findElement(By.id("Aux1"));}
		
		//2501
		else if (type.equals("Aux2"))
		{element = driver.findElement(By.id("Aux2"));}
		
		//Patient search button 
		else if (type.equals("btnSearch3"))
		{element = driver.findElement(By.id("btnSearch3"));}
		//Patient search button for ICD only
		else if (type.equals("btnSearch2"))
		{element = driver.findElement(By.id("btnSearch2"));}
		
		
		//Patient Save
		else if (type.equals("btnSave1"))
		{element = driver.findElement(By.id("btnSave1"));}
		
		//Patient Save only for ICD
		else if (type.equals("btnSave"))
		{element = driver.findElement(By.id("btnSave"));}
		
		//Patient view Episode in the grid
		else if (type.equals("ViewEpisode"))
		{element = driver.findElement(By.xpath(".//*[@id='tblSearchResults']/tbody/tr/td[1]/a[3]/img"));}
		
		//Edit patient in the grid
		else if (type.equals("EditPatient"))
		{element = driver.findElement(By.xpath(".//*[@id='tblSearchResults']/tbody/tr/td[1]/a[1]/img"));}
		
		
		return element;
	}//Method
	
	//Method for drop down selection elements
	public Select PatientSelectMethod(WebDriver driver, String type){ 
		if (type.equals("SelectSex"))
    	{oSelection = new Select(driver.findElement(By.id("Sex")));} 
		else if (type.equals("HispOrig"))
		{oSelection = new Select(driver.findElement(By.id("HispOrig")));} 
		
         return oSelection; 
        }//Select Method end.
	
	
	
	
	
	
	
	
	
	

}
