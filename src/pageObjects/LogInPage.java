package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LogInPage {

	public  static WebElement element = null;
    
    String RegistrayLogIn="content_2_lnkActivateNowLink";
    String ParticipantId="MainContent_C018_login_PartID";
    String UserName ="MainContent_C018_login_username";
    String PassWord ="MainContent_C018_login_password";
    String BtnLogIn ="MainContent_C018_btnLogon";
    
    //String RegDropDown ="dropdown-toggle";
    
    public  WebElement LogInMethod(WebDriver driver, String type){
    	
    	if (type.equals("RegistrayLogIn"))
    	{element = driver.findElement(By.id(RegistrayLogIn));}
    	
    	else if (type.equals("ParticipantId"))
    	{element =driver.findElement(By.id(ParticipantId));}
    	else if (type.equals("UserName"))
    	{element =driver.findElement(By.id(UserName));}
    	else if (type.equals("PassWord"))
    	{element =driver.findElement(By.id(PassWord));}
    	else if (type.equals("BtnLogIn"))
    	{element =driver.findElement(By.id(BtnLogIn));}
		else if (type.equals("WelcomeNCDRWebsite"))
		{element =driver.findElement(By.xpath("//*[@id='BreadcrumbNav']/ul[1]/li[2]/a"/*".//*[@id='left']/div[1]/div/h2"*//*".//*[@id='left']/div[1]/h2[1]/p"*/));}
		else if (type.equals("iecer"))
		{element =driver.findElement(By.linkText("Continue to this website (not recommended)."));}
    	

    	//else if (type.equals("RegDropDown"))
    	//{element =driver.findElement(By.id(RegDropDown));}
    	
    	return element;}

    
      /* 
     //Impact Registry
     public static WebElement RegImpact(WebDriver driver){ 
         element = driver.findElement(By.xpath(".//*[@id='MainFormTag']/div[5]/div/div/div[1]/div[2]/a[4]/img"));
         System.out.println(element);
         return element; 
         }
     
	
	
*/
}
