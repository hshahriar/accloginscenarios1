package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdministrationPage {
	
	private static WebElement element = null;
	
	public WebElement adminPageMethod (WebDriver driver, String type){
		//Administration left menu
		if (type.equals("Administration"))
		{element =driver.findElement(By.linkText("Administration"));}
		//Action Site profile
		else if (type.equals("SiteProfile"))
		{element =driver.findElement(By.partialLinkText("Site Profile"));}
		
		else if (type.equals("NCDRHomeAdmin"))
		//{element =driver.findElement(By.xpath(".//*[@id='MainFormTag']/div[3]/h1/a/span"));}
		{element =driver.findElement(By.className("ncdr_logo"));}
		
		
		//-------------Action Registry------------------------------------//
		//Action Administration text in admin page.
		else if (type.equals("ActionAdminText"))
		{element =driver.findElement(By.xpath(".//*[@id='content']/div[1]/div/h2"));} 
		//Pariticipant ID value (999999)
		else if (type.equals("Action999999"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_lblParticipantID"));}
		
		//Encryption Key: value 
		else if (type.equals("ActionEncryptionKey"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_lblEncryptionKey"));}
		
		//Action Site Name
		else if (type.equals("ActionSiteName"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbNCDRPartName"));}
		
		//Action Brand Name
		else if (type.equals("ActionBrandName"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbFacilityBrandName"));}
		
		//Action National Provider Identification (NPI) Number 
		else if (type.equals("ActionNPI"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbNPINumber"));}
		
		//Action Medicare Provider Number (MPN) 
		else if (type.equals("ActionMPN"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbMPNumber"));}
		
		//Action American Hospital Association (AHA) Number 
		else if (type.equals("ActionAHA"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbAHANumber"));}
		
		//Action Site Profile Submit button 
		else if (type.equals("ActionSubmit"))
		{element =driver.findElement(By.id("MainContent_C002_btnSubmit"));}
		

		
		
		//-------------Cath PCI------------------------------------//
		//Cath Administration text in admin page.
		else if (type.equals("CathAdminText"))
		{element =driver.findElement(By.xpath(".//*[@id='content']/div[1]/h2"));} 
		
		//Pariticipant ID value (999999)
		else if (type.equals("Cath999999"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_lblParticipantID"));}
				
		//Encryption Key: value 
		else if (type.equals("CathEncryptionKey"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_lblEncryptionKey"));}
				
		//Cath Site Name
		else if (type.equals("CathSiteName"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbNCDRPartName"));}
				
		//Cath Brand Name
		else if (type.equals("CathBrandName"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbFacilityBrandName"));}
				
		//Cath National Provider Identification (NPI) Number 
		else if (type.equals("CathNPI"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbNPINumber"));}
				
		//Cath Medicare Provider Number (MPN) 
		else if (type.equals("CathMPN"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbMPNumber"));}
				
		//Cath American Hospital Association (AHA) Number 
		else if (type.equals("CathAHA"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbAHANumber"));}
				
		//Cath Site Profile Submit button 
		else if (type.equals("CathSubmit"))
		{element =driver.findElement(By.id("MainContent_C004_btnSubmit"));}
		
		//----------------------ICD Reg---------------------------//
		//ICD Administration text in admin page.
		else if (type.equals("ICDAdminText"))
		{element =driver.findElement(By.xpath(".//*[@id='content']/div[1]/h2"));} 

		//Pariticipant ID value (999999)
		else if (type.equals("ICD999999"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_lblParticipantID"));}
				
		//Encryption Key: value 
		else if (type.equals("ICDEncryptionKey"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_lblEncryptionKey"));}
				
		//ICD Site Name
		else if (type.equals("ICDSiteName"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbNCDRPartName"));}
				
		//ICD Brand Name
		else if (type.equals("ICDBrandName"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbFacilityBrandName"));}
				
		//ICD National Provider Identification (NPI) Number 
		else if (type.equals("ICDNPI"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbNPINumber"));}
				
		//ICD Medicare Provider Number (MPN) 
		else if (type.equals("ICDMPN"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbMPNumber"));}
				
		//ICD American Hospital Association (AHA) Number 
		else if (type.equals("ICDAHA"))
		{element =driver.findElement(By.id("MainContent_C002_ucSiteProfile_tbAHANumber"));}
				
		//ICD Site Profile Submit button 
		else if (type.equals("ICDSubmit"))
		{element =driver.findElement(By.id("MainContent_C002_btnSubmit"));}
		
		
		//----------------------Impact Registry-------------------------//
		//Action Administration text in admin page.
		else if (type.equals("ImpactAdminText"))
		{element =driver.findElement(By.xpath(".//*[@id='content']/div[1]/h2"));} 
		//Pariticipant ID value (999999)
		else if (type.equals("Impact999999"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_lblParticipantID"));}
				
		//Encryption Key: value 
		else if (type.equals("ImpactEncryptionKey"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_lblEncryptionKey"));}
				
		//Impact Site Name
		else if (type.equals("ImpactSiteName"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_tbNCDRPartName"));}
				
		//Impact Brand Name
		else if (type.equals("ImpactBrandName"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_tbFacilityBrandName"));}
				
		//Impact National Provider Identification (NPI) Number 
		else if (type.equals("ImpactNPI"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_tbNPINumber"));}
				
		//Impact Medicare Provider Number (MPN) 
		else if (type.equals("ImpactMPN"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_tbMPNumber"));}
				
		//Impact American Hospital Association (AHA) Number 
		else if (type.equals("ImpactAHA"))
		{element =driver.findElement(By.id("MainContent_C003_ucSiteProfile_tbAHANumber"));}
				
		//Impact Site Profile Submit button 
		else if (type.equals("ImpactSubmit"))
		{element =driver.findElement(By.id("MainContent_C003_btnSubmit"));}
		
		
		
		//-------------PVI ------------------------------------//
		//PVI Administration text in admin page.
		else if (type.equals("PVIAdminText"))
		{element =driver.findElement(By.xpath(".//*[@id='content']/div[1]/h2"));} 
		//Pariticipant ID value (999999)
		else if (type.equals("PVI999999"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_lblParticipantID"));}
				
		//Encryption Key: value 
		else if (type.equals("PVIEncryptionKey"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_lblEncryptionKey"));}
				
		//PVI Site Name
		else if (type.equals("PVISiteName"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_tbNCDRPartName"));}
				
		//PVI Brand Name
		else if (type.equals("PVIBrandName"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_tbFacilityBrandName"));}
				
		//PVI National Provider Identification (NPI) Number 
		else if (type.equals("PVINPI"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_tbNPINumber"));}
				
		//PVI Medicare Provider Number (MPN) 
		else if (type.equals("PVIMPN"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_tbMPNumber"));}
				
		//PVI American Hospital Association (AHA) Number 
		else if (type.equals("PVIAHA"))
		{element =driver.findElement(By.id("MainContent_C006_ucSiteProfile_tbAHANumber"));}
				
		//PVI Site Profile Submit button 
		else if (type.equals("PVISubmit"))
		{element =driver.findElement(By.id("MainContent_C006_btnSubmit"));}
		
		
		
		//-------------TVT ------------------------------------//
		//TVT Administration text in admin page.
		else if (type.equals("TVTAdminText"))
		{element =driver.findElement(By.xpath(".//*[@id='content']/div[1]/h2"));} 
		//Pariticipant ID value (999999)
		else if (type.equals("TVT999999"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_lblParticipantID"));}
				
		//Encryption Key: value 
		else if (type.equals("TVTEncryptionKey"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_lblEncryptionKey"));}
				
		//Cath Site Name
		else if (type.equals("TVTSiteName"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbNCDRPartName"));}
				
		//Cath Brand Name
		else if (type.equals("TVTBrandName"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbFacilityBrandName"));}
				
		//Cath National Provider Identification (NPI) Number 
		else if (type.equals("TVTNPI"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbNPINumber"));}
				
		//Cath Medicare Provider Number (MPN) 
		else if (type.equals("TVTMPN"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbMPNumber"));}
				
		//Cath American Hospital Association (AHA) Number 
		else if (type.equals("TVTAHA"))
		{element =driver.findElement(By.id("MainContent_C004_ucSiteProfile_tbAHANumber"));}
				
		//Cath Site Profile Submit button 
		else if (type.equals("TVTSubmit"))
		{element =driver.findElement(By.id("MainContent_C004_btnSubmit"));}
		
		
		return element;
		
		
		
	}//Method

}
