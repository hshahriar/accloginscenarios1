package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DiabetesRegPage {

	private static WebElement element = null;

	public WebElement ActionRegPageMethod(WebDriver driver, String type) {
		System.out.println("type:"+type);
		// Action registry
		if (type.equalsIgnoreCase("DiabetesRegistry")) {
			element = driver
					.findElement(By
							.xpath("//*[@id='left']/div[1]/div/div/div[3]/ul/li[1]/a"));
		}
		// Action registry data
		else if (type.equals("Data")) {
			element = driver.findElement(By.linkText("Data"));
		}
		
		else if (type.equals("DQR")) {
			element = driver.findElement(By.partialLinkText("DQR (v2.4)"));
		}
		
		
		// Data Collection Tool (v2.4)
		else if (type.equals("Data Collection Tool")) {
			element = driver.findElement(By
					.partialLinkText("Data Collection Tool (v2.4)"));
		}

		// Upload Data (v2.4)
		else if (type.equals("Upload Data")) {
			element = driver.findElement(By
					.partialLinkText("Upload Data (v2.4)"));
		}

		// Welcome Page(Welcome ACTION Registry-GWTG Participants)
		else if (type.equals("ActionWelcomePage")) {
			element = driver.findElement(By.className("pageheader"));
		}

		// ActionAdmin
		else if (type.equals("ActionAdmin")) {
			element = driver.findElement(By.linkText("Administration"));
		}

		// Dashboard
		else if (type.equals("ActionDashboard")) {
			element = driver.findElement(By.linkText("Dashboard"));
		}

		// Reports
		else if (type.equals("ActionReports")) {
			element = driver.findElement(By.linkText("Reports"));
		}

		// Resources
		else if (type.equals("ActionResources")) {
			element = driver.findElement(By.linkText("Resources"));
		}

		// Control
		else if (type.equals("ActionControl")) {
			element = driver.findElement(By.linkText("Control"));
		}

		else if (type.equals("NCDRHome")) {
			element = driver.findElement(By
					.xpath("//*[@id='header']/div/h1/a"));
		}
		
		else if (type.equals("Home")) {
			element = driver.findElement(By
					.xpath("//*[@id='MainFormTag']/div[3]/h1/a/span"));
		}

		// Browse the file
		else if (type.equals("Browse")) {
			element = driver.findElement(By
					.id("MainContent_C003_fileUploadDQRData"));
		}

		// Attach file
		else if (type.equals("AttachFile")) {
			element = driver.findElement(By
					.id("MainContent_C003_btnAttachFile"));
		}
		// // {element
		// =driver.findElement(By.xpath("/html/body/form/div[5]/div/div[2]/div[2]/div[2]/div[2]/input[2]"));}

		// By clicking on the submit button.
		else if (type.equals("ComplianceCheck")) {
			element = driver.findElement(By
					.id("MainContent_C003_chkComplianceCheck"));
		}

		// DQR Submit
		else if (type.equals("DQRSubmit")) {
			element = driver.findElement(By
					.id("MainContent_C003_btnSubmitData"));
		}

		// Remove File
		else if (type.equals("RemoveFile")) {
			element = driver.findElement(By
					.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"));
		}

		// 1st download in upload submission Status
		else if (type.equals("Download_0")) {
			element = driver.findElement(By
					.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0"));
		}

		// 1st submission
		else if (type.equals("1stSubmission")) {
			element = driver
					.findElement(By
							.xpath(".//*[@id='tblSubmissionStatus']/tbody/tr[1]/td[2]"));
		}

		// Submission History
		else if (type.equals("SubmissionHistory")) {
			element = driver
					.findElement(By
							.xpath(".//*[@id='MainContent_C004_divSubmissionHistoryToggle1']/a"));
		}

		// Data elements

		else if (type.equalsIgnoreCase("call For Data Schedule")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_5_d0_body']/div/ul/li[1]/a"));
		} else if (type.equalsIgnoreCase("data Collection Tool")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_5_d0_body']/div/ul/li[2]/a"));
		} else if (type.equalsIgnoreCase("upload Data")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_5_d0_body']/div/ul/li[3]/a"));
		} else if (type.equalsIgnoreCase("Data Migration")) {
			element = driver.findElement(By
					.xpath("//*[@id='node_5_d0_body']/div/ul/li[4]/a"));
		} else if (type.equalsIgnoreCase("dqr")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_5_d0_body']/div/ul/li[5]/a"));
		}

		// Administartion elements
		/*else if (type.equalsIgnoreCase("individual Profile")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[1]/a"));
		}*/ else if (type.equalsIgnoreCase("site Profile")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[2]/a"));
		} else if (type.equalsIgnoreCase("site User Administration")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[3]/a"));
		} else if (type.equalsIgnoreCase("vendor Profile")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[5]/a"));
		} else if (type.equalsIgnoreCase("vendor User Management")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[6]/a"));
		} else if (type.equalsIgnoreCase("partner User Administration")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[8]/a"));
		} else if (type.equalsIgnoreCase("vendor Participants")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[9]/a"));
		} else if (type.equalsIgnoreCase("corporate Profile")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[10]/a"));
		} else if (type.equalsIgnoreCase("corporate User Administration")) {
			element = driver.findElement(By
					.xpath(".//*[@id='node_1_d0_body']/div/ul/li[11]/a"));
		}

		// Dashboard elements
		else if (type.equalsIgnoreCase("eReports")) {
			element = driver.findElement(By
					.xpath(".//*[@id='MainContent_C003_ultabs']/li[1]/a/span"));
		} else if (type.equalsIgnoreCase("comparator")) {
			element = driver.findElement(By
					.xpath(".//*[@id='MainContent_C003_ultabs']/li[2]/a/span"));
		}

		else if (type.equalsIgnoreCase("file Delivery")) {
			element = driver.findElement(By
					.xpath("//input[contains(text(), 'File Delivery')]"));
		} else if (type.equalsIgnoreCase("outComesReport")) {
			element = driver.findElement(By
					.xpath("//input[contains(text(), 'Outcomes Report')]"));
		} else if (type.equalsIgnoreCase("additionalReports")) {
			element = driver.findElement(By
					.xpath("//input[contains(text(), 'Additional Reports')]"));
		} else if (type.equalsIgnoreCase("filterPanel")) {
			element = driver.findElement(By
					.xpath("//input[contains(text(), 'Filter Panel')]"));
		}

		// Resources elements
		else if (type.equalsIgnoreCase("Documents")) {
			element = driver.findElement(By
					.partialLinkText("Documents (v2)"));
		} else if (type.equalsIgnoreCase("FAQ")) {
			element = driver.findElement(By
					.partialLinkText("FAQ (v2.0)"));
		} else if (type.equalsIgnoreCase("participant Directory")) {
			element = driver.findElement(By
					.partialLinkText("Participant Directory"));
		} else if (type.equalsIgnoreCase("technology Downloads")) {
			element = driver.findElement(By
					.partialLinkText("Technology Downloads"));
		}
		
		
		else if (type.equals("Administration")) {
			element = driver.findElement(By.linkText("Administration"));
		}
		
		else {
			try {
				element = driver.findElement(By.linkText(type));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
				element = driver.findElement(By.partialLinkText(type));
			}
		}

		return element;

	}// Method ends

	/*
	 * 
	 * public static WebElement DataTitle(WebDriver driver){ element =
	 * driver.findElement(By.linkText("Data")); return element;}
	 * 
	 * public static WebElement DataCollwctionTools(WebDriver driver){ element =
	 * driver.findElement(By.partialLinkText("Data Collection Tool (v1)"));
	 * System.out.println(element); return element; }
	 */

}// Class
