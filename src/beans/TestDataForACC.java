package beans;

public class TestDataForACC
{
	private String TestScenario;
	private String TestCase;
	private String UserName;
	private String Password;
	private String Credential;
	
	
	
	public String getCredential() {
		return Credential;
	}
	public void setCredential(String credential) {
		Credential = credential;
	}
	public String getTestScenario() {
		return TestScenario;
	}
	public void setTestScenario(String testScenario) {
		TestScenario = testScenario;
	}
	public String getTestCase() {
		return TestCase;
	}
	public void setTestCase(String testCase) {
		TestCase = testCase;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	@Override
	public String toString() {
		return "TestDataForACC [TestScenario=" + TestScenario + ", TestCase="
				+ TestCase + ", UserName=" + UserName + ", Password="
				+ Password + ", Credential=" + Credential + "]";
	}
	
	
}
