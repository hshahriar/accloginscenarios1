package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.PatientPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;

public class Patient {
	static int x1 = 1;
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");

	private static PatientPage PatientPageObj = new PatientPage();

	public static void Execute(WebDriver driver, com.gallop.Logger logger,ExtentTest et)
			throws Exception {

		try {

			PatientPageObj.patientPageMethod(driver, "Patient -- Add & Search")
					.click();
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// setting patient tab in the spread sheet in excel sheet.
			XSSFSheet ExcelWSheet=ExcelUtils.setExcelFile2("patient");

			// This is to get the values from Excel sheet, passing parameters
			// (Row num &amp; Col num)to getCellData method

			// /Passing the excel data to UI
			// 2040
			// passing patient id to UI
			String patient_id = ExcelUtils.getCellData(x1, 4,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "Pid").clear();
			PatientPageObj.patientPageMethod(driver, "Pid")
					.sendKeys(patient_id);
			System.out.println(patient_id);
			PatientPageObj.patientPageMethod(driver, "btnSearch3").click();
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			PatientPageObj.patientPageMethod(driver, "EditPatient").click();
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// 2000
			String last_name = ExcelUtils.getCellData(x1, 1,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "LastName").clear();
			PatientPageObj.patientPageMethod(driver, "LastName").sendKeys(
					last_name);

			// 2010
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			String first_name = ExcelUtils.getCellData(x1, 2,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "FirstName").clear();
			PatientPageObj.patientPageMethod(driver, "FirstName").sendKeys(
					first_name);

			// 2020
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			String Middle_Name = ExcelUtils.getCellData(x1, 3,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "MidName").clear();
			PatientPageObj.patientPageMethod(driver, "MidName").sendKeys(
					Middle_Name);

			// 2050
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			String DOB = ExcelUtils.getCellData(x1, 5,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "DOB").clear();
			PatientPageObj.patientPageMethod(driver, "DOB").sendKeys(DOB);

			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// Unchecking SSN N/A2031 if it is checked.
			if (!(PatientPageObj.patientPageMethod(driver, "SSNNA")
					.getAttribute("checked") == null)) {
				System.out.println(PatientPageObj.patientPageMethod(driver,
						"SSNNA").getAttribute("checked"));
				PatientPageObj.patientPageMethod(driver, "SSNNA").click();
			}

			// 2030
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			String SSN = ExcelUtils.getCellData(x1, 6,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "SSN").clear();
			PatientPageObj.patientPageMethod(driver, "SSN").sendKeys(SSN);

			// 2045
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			String OtherID = ExcelUtils.getCellData(x1, 7,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "OtherID").clear();
			PatientPageObj.patientPageMethod(driver, "OtherID").sendKeys(
					OtherID);

			// 2060
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// Getting Sex value from spread sheet.
			String Patient_Sex = ExcelUtils.getCellData(x1, 8,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "Sex").click();
			// Passing the value to UI drop down list
			PatientPageObj.patientPageMethod(driver, "Sex").sendKeys(
					Patient_Sex);
			PatientPageObj.patientPageMethod(driver, "Sex").click();
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// 2076
			String HispOrig = ExcelUtils.getCellData(x1, 9,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "HispOrig").click();
			// Passing the value to UI drop down list
			PatientPageObj.patientPageMethod(driver, "HispOrig").sendKeys(
					HispOrig);
			PatientPageObj.patientPageMethod(driver, "HispOrig").click();

			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// Getting the attribute for "CHECKED" value.
			// Action_Patient_Page.RaceWhite(driver).getAttribute("checked");
			// Checking the button is already checked or not. if it is checked I
			// am not clicking the button

			// 2070
			if (PatientPageObj.patientPageMethod(driver, "RaceWhite")
					.getAttribute("checked") == null) {
				System.out.println(PatientPageObj.patientPageMethod(driver,
						"RaceWhite").getAttribute("checked"));
				PatientPageObj.patientPageMethod(driver, "RaceWhite").click();
			}
			//Thread.sleep(1000);
			//Utils.page_wait();
			Utils.page_wait(driver);
			// 2071
			if (PatientPageObj.patientPageMethod(driver, "RaceBlack")
					.getAttribute("checked") == null) {
				System.out.println(PatientPageObj.patientPageMethod(driver,
						"RaceBlack").getAttribute("checked"));
				PatientPageObj.patientPageMethod(driver, "RaceBlack").click();
			}
			/*Thread.sleep(1000);*/	Utils.page_wait();
			// 2073
			if (PatientPageObj.patientPageMethod(driver, "RaceAmIndian")
					.getAttribute("checked") == null) {
				System.out.println(PatientPageObj.patientPageMethod(driver,
						"RaceAmIndian").getAttribute("checked"));
				PatientPageObj.patientPageMethod(driver, "RaceAmIndian")
						.click();
			}
			/*Thread.sleep(1000);*/	//Utils.page_wait();
			Utils.page_wait(driver);
			// 2072
			if (PatientPageObj.patientPageMethod(driver, "RaceAsian")
					.getAttribute("checked") == null) {
				System.out.println(PatientPageObj.patientPageMethod(driver,
						"RaceAsian").getAttribute("checked"));
				PatientPageObj.patientPageMethod(driver, "RaceAsian").click();
			}
			/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
			// 2080
			try {
				if (PatientPageObj.patientPageMethod(driver, "RaceAsianIndian")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceAsianIndian").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceAsianIndian")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2081
				if (PatientPageObj.patientPageMethod(driver, "RaceChinese")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceChinese").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceChinese").click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2082
				if (PatientPageObj.patientPageMethod(driver, "RaceFilipino")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceFilipino").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceFilipino")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2083
				if (PatientPageObj.patientPageMethod(driver, "RaceJapanese")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceJapanese").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceJapanese")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2084
				if (PatientPageObj.patientPageMethod(driver, "RaceKorean")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceKorean").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceKorean").click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2085
				if (PatientPageObj.patientPageMethod(driver, "RaceVietnamese")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceVietnamese").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceVietnamese")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2086
				if (PatientPageObj.patientPageMethod(driver, "RaceAsianOther")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceAsianOther").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceAsianOther")
							.click();
				}
				/*Thread.sleep(1000);*//*	Utils.page_wait();*/Utils.page_wait(driver);
				// 2074
				if (PatientPageObj.patientPageMethod(driver, "RaceNatHaw")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceNatHaw").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceNatHaw").click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2090
				if (PatientPageObj.patientPageMethod(driver, "RaceNativeHawaii")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceNativeHawaii").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceNativeHawaii")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2091
				if (PatientPageObj.patientPageMethod(driver, "RaceGuamChamorro")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceGuamChamorro").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceGuamChamorro")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2092
				if (PatientPageObj.patientPageMethod(driver, "RaceSamoan")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RaceSamoan").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "RaceSamoan").click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2093
				if (PatientPageObj.patientPageMethod(driver,
						"RacePacificIslandOther").getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"RacePacificIslandOther").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver,
							"RacePacificIslandOther").click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2100
				if (PatientPageObj
						.patientPageMethod(driver, "HispEthnicityMexican")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"HispEthnicityMexican").getAttribute("checked"));
					PatientPageObj
							.patientPageMethod(driver, "HispEthnicityMexican")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2101
				if (PatientPageObj.patientPageMethod(driver,
						"HispEthnicityPuertoRico").getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"HispEthnicityPuertoRico").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver,
							"HispEthnicityPuertoRico").click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2102
				if (PatientPageObj.patientPageMethod(driver, "HispEthnicityCuban")
						.getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"HispEthnicityCuban").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver, "HispEthnicityCuban")
							.click();
				}
				/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
				// 2103
				if (PatientPageObj.patientPageMethod(driver,
						"HispEthnicityOtherOrigin").getAttribute("checked") == null) {
					System.out.println(PatientPageObj.patientPageMethod(driver,
							"HispEthnicityOtherOrigin").getAttribute("checked"));
					PatientPageObj.patientPageMethod(driver,
							"HispEthnicityOtherOrigin").click();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Entering data to Aux1 from spread sheet
			/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
			String Patient_Aux1 = ExcelUtils.getCellData(x1, 10,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "Aux1").clear();
			PatientPageObj.patientPageMethod(driver, "Aux1").sendKeys(
					Patient_Aux1);

			// Entering data to Aux2 from spread sheet
			/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
			String Patient_Aux2 = ExcelUtils.getCellData(x1, 11,ExcelWSheet);
			PatientPageObj.patientPageMethod(driver, "Aux2").clear();
			PatientPageObj.patientPageMethod(driver, "Aux2").sendKeys(
					Patient_Aux2);

			// Saving
			/*Thread.sleep(1000);*/	/*Utils.page_wait();*/Utils.page_wait(driver);
			PatientPageObj.patientPageMethod(driver, "btnSave1").click();
			Thread.sleep(3000);
			logger.log("pass", "Patient:"+patient_id+" creation/modification success", false, driver, "",et);

			Log.info("Patient class executed successfull!!!");
		} catch (Exception exp) {
			exp.printStackTrace();
			logger.log("fail", "Patient creation failed", true, driver,
					"Q:\\QA Stuff\\Syam pithani\\testData\\",et);
			LoggerParent.warn(
					"Following exception was raised in Patient Class", exp);
		}

	}// end Method

}// Class

