package appModule;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.ExtentTest;

import beans.Registry;
import beans.Tab;
import pageObjects.CathPCIRegPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Reporter;
import utility.Utils;
//import Frame.Frame;

public class CathPCIReg {
	static int x1 = 16;
	static int x2 = 17;
	private static Logger Log = Logger.getLogger(CathPCIReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static CathPCIPatientVerification CathPCIPatientVerificationObj = new CathPCIPatientVerification();

	private static CathPCIRegPage CathPCIRegPageObj = new CathPCIRegPage();

	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");

	public static void Execute(WebDriver driver, Reporter reporter,
			com.gallop.Logger logger, String registry, Map<String, Registry> registryMap, List<Registry> registries) throws Exception {
		try {
			/*
			 * logger.createTest("ActionRegistry Test",
			 * "Welcome ACTION Registry-GWTG Participants");
			 */
			String registryName = "cathPCIRegistry";
			//Map<String, Registry> registryMap = ExcelUtils.getRegistryMap();
			Registry registry2 = registryMap.get(registry);
			// Action registry
			/*if (registry.contains("RegistryLink") || registry.contains("Data")
					|| registry.contains("PatientNavigatorProgram"))*/
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, registryName)
					.click();

			if (registry.contains("RegistryLink")) {
				ExtentTest et=logger.createTest(registryName + " RegistryLink Test",
						"RegistryLink Test");
				Utils.checkForUsernameAndLogoutLink(driver, logger,et);
				Utils.checkForRegistriesHeaderAndAnnouncementsHeader(driver,
						logger,et);
				try {
					if (registry2.getLinks() != null
							&& !registry2.getLinks().isEmpty())
						Utils.checkForLinks(driver, logger,
								registry2.getLinks(),et);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.flush();
			}
			/* logger.createTest(" Menu items Test", "Menu items Test"); */

			// Process each headers and capture log
			//List<Registry> registries = ExcelUtils.getRegisteries();
			Map<String, List<Tab>> registryHeadersMap = Utils
					.getTabsAndChildrenFromRegistry(registries);

			List<Tab> headers = registryHeadersMap.get(registry);

			if (headers != null && !headers.isEmpty()) {
				for (Tab header : headers) {
					ExtentTest et=logger.createTest(registryName + " " + header.getId()
							+ "  Test", header.getId() + "  Test");
					WebElement parentTab = Utils.getWebElement(driver,
							header.getId(), registryName);
					/*
					 * if (parentTab != null) { logger.log("pass",
					 * header.getTabName() + " exists", false, driver, "");
					 * 
					 * } else { logger.log("fail", header.getTabName() +
					 * " doesn't exists", false, driver,
					 * "Q:\\QA Stuff\\Syam pithani\\testData\\"); }
					 */

					List<Tab> childrenTabs = header.getChildTabs();
					if (childrenTabs != null && !childrenTabs.isEmpty()) {
						for (Tab tab : childrenTabs) {/*
							WebElement childTab = Utils.getWebElement(driver,
									tab.getId(), registryName);
							// click parent element

							if (childTab != null) {
								try {
									childTab.click();
									if (driver
											.getPageSource()
											.contains(
													"Server Error in '/WebNCDR' Application")
											|| driver
													.getPageSource()
													.contains(
															"An unexpected error has occurred")) {
										logger.log("fail",
												"Server Error in '/WebNCDR' Application after clicking "
														+ tab.getTabName()
														+ ".", true, driver,
												reportingPath,et);
										driver.navigate().back();
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								
								 * logger.log("pass", tab.getTabName() +
								 * " exists", false, driver, "");
								 

								
								 * if (tab.getTabName().equalsIgnoreCase(
								 * "Site Profile")) {
								 * Utils.checkIfTextExists(driver,
								 * "Registry Information", logger); }
								 * 
								 * else
								 if (tab.getTabName().equalsIgnoreCase(
										"eReports")
										&& registry.contains("Dashboard")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									Utils.checkIfTextExistsInsideMenu(driver,
											"eReports", logger, "Dashboard",et);
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									driver.switchTo().frame("ifreport0");
									Utils.checkIfTextExistsInsideMenu(driver,
											"File Delivery", logger,
											"Dashboard",et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Outcomes Report", logger,
											"Dashboard",et);
									
									 * Utils.checkIfTextExists(driver,
									 * "Additional Reports", logger);
									 

									Utils.checkIfTextExistsInsideMenu(driver,
											"Additional Reports", logger,
											"Dashboard",et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"comparator")
										&& registry.contains("Dashboard")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									Utils.checkIfTextExistsInsideMenu(driver,
											"Comparator", logger, "Dashboard",et);
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Thread.sleep(2000);
									
									 * Utils.checkIfTextExists(driver,
									 * "Filter Panel", logger);
									 
									driver.switchTo().frame("ifreport1");
									Utils.checkIfTextExistsInsideMenu(driver,
											"Filter Panel", logger, "Dashboard",et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Data Collection Tool")
										&& registry.contains("Data")) {

									try {
										Utils.getWebElement(driver,
												registryName, registryName)
												.click();
									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									
									 * Utils.getWebElement(driver,
									 * tab.getTabName(), registry).click();
									 
									PatientLess.Execute(driver, logger,et);
									EpisodeLess.Execute(driver, logger,et);
									Utils.getWebElement(driver, "Maintenance",
											registryName).click();
									Utils.getWebElement(driver,
											"Submit to DQR", registryName)
											.click();

									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Status", logger,
											"Data Collection Tool",et);
									Utils.getWebElement(driver, "Data Extract",
											registryName).click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Download Extract", logger,
											"Data Collection Tool",et);
									Utils.getWebElement(driver, "NCDRHome",
											registryName).click();
									Utils.getWebElement(driver, registryName,
											registryName).click();
									Utils.getWebElement(driver, "Data",
											registryName).click();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Data Migration")
										&& registry.contains("Data")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 * Utils.getWebElement(driver, "Data",
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger,et);

									Utils.checkIfTextExistsInsideMenu(driver,
											"Operator Remediation", logger,
											"Data Migration",et);

									
									 * Utils.checkIfTextExists(driver,
									 * "Submission Status", logger);
									 

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"DQR")
										&& registry.contains("Data")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 * Utils.getWebElement(driver, "Data",
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger,et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Results", logger, "DQR",et);

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Upload Data")
										&& registry.contains("Data")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 * Utils.getWebElement(driver, "Data",
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											"Upload Data", logger,et);

									Utils.uploadData(driver, registryName,
											logger,et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Site Profile")
										&& registry.contains("Admin")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											"Administration", registryName)
											.click();
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Registry Information", logger,
											"Site Profile",et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Site User Administration")
										&& registry.contains("Admin")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											"Administration", registryName)
											.click();
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											"Site User Administration", logger,et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Site User Administration", logger,
											"Site User Administration",et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Documents")
										&& registry.contains("Resources")) {
									try {
										Utils.getWebElement(driver,
												registryName, registryName)
												.click();

									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger,et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Survey Collection Tool")
										&& registry.contains("Navigator")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									
									 * Utils.getWebElement(driver,
									 * "Patient Navigator Program",
									 * registryName).click();
									 * Utils.getWebElement(driver,
									 * tab.getTabName(), registryName) .click();
									 
									Utils.checkIfTextExistsInsideMenu(driver,
											"New Surveys", logger,
											"Survey Collection Tool",et);
									
									 * Utils.getWebElement(driver,
									 * "ACTION Registry - GWTG Home",
									 * registryName) .click();
									 

									driver.navigate().back();

								} else if (tab.getTabName().equalsIgnoreCase(
										"AHA Benchmarks")
										&& registry.contains("Navigator")) {
									
									 * Utils.getWebElement(driver,
									 * "Patient Navigator Program",
									 * registryName).click();
									 * Utils.getWebElement(driver,
									 * tab.getTabName(), registryName) .click();
									 
									Utils.checkIfTextExistsInsideMenu(driver,
											"Timeframe", logger,
											"AHA Benchmarks",et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Pending Hospital Surveys")
										&& registry.contains("Navigator")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									
									 * Utils.getWebElement(driver,
									 * "Patient Navigator Program",
									 * registryName).click();
									 
									
									 * Utils.getWebElement(driver,
									 * tab.getTabName(), registryName) .click();
									 
									Utils.checkIfTextExistsInsideMenu(driver,
											"Pending Hospital Surveys", logger,
											"Patient Navigator Program",et);

								} else {
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger,et);
								}

							} else {
								logger.log("fail", tab.getTabName()
										+ " doesn't exists.", true, driver,
										reportingPath,et);
							}
						*/

							WebElement childTab = Utils.getWebElement(driver, tab.getId(),
									registryName);
							// click parent element

							if (childTab != null) {
								try {
									childTab.click();
									if (driver.getPageSource().contains(
											"Server Error in '/WebNCDR' Application")
											|| driver.getPageSource().contains(
													"An unexpected error has occurred")) {
										logger.log("fail",
												"Server Error in '/WebNCDR' Application after clicking "
														+ tab.getTabName() + ".", true,
												driver, reportingPath, et);
										driver.navigate().back();
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								/*
								 * logger.log("pass", tab.getTabName() + " exists", false,
								 * driver, "");
								 */

								/*
								 * if (tab.getTabName().equalsIgnoreCase( "Site Profile")) {
								 * Utils.checkIfTextExists(driver, "Registry Information",
								 * logger); }
								 * 
								 * else
								 */if (tab.getTabName().equalsIgnoreCase("eReports")
										&& registry.contains("Dashboard")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									Utils.checkIfTextExistsInsideMenu(driver, "eReports",
											logger, "Dashboard", et);
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									driver.switchTo().frame("ifreport0");
									Utils.checkIfTextExistsInsideMenu(driver,
											"File Delivery", logger, "Dashboard", et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Outcomes Report", logger, "Dashboard", et);
									/*
									 * Utils.checkIfTextExists(driver, "Additional Reports",
									 * logger);
									 */

									Utils.checkIfTextExistsInsideMenu(driver,
											"Additional Reports", logger, "Dashboard", et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase("comparator")
										&& registry.contains("Dashboard")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									Utils.checkIfTextExistsInsideMenu(driver, "Comparator",
											logger, "Dashboard", et);
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									try {
										Thread.sleep(2000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									/*
									 * Utils.checkIfTextExists(driver, "Filter Panel",
									 * logger);
									 */
									driver.switchTo().frame("ifreport1");
									Utils.checkIfTextExistsInsideMenu(driver,
											"Filter Panel", logger, "Dashboard", et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Data Collection Tool")
										&& registry.contains("Data")) {

									try {
										Utils.getWebElement(driver, registryName,
												registryName).click();
									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									/*
									 * Utils.getWebElement(driver, tab.getTabName(),
									 * registry).click();
									 */
									try {
										PatientLess.Execute(driver, logger, et);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									try {
										EpisodeLess.Execute(driver, logger, et);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									Utils.getWebElement(driver, "Maintenance", registryName)
											.click();
									Utils.getWebElement(driver, "Submit to DQR",
											registryName).click();

									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Status", logger,
											"Data Collection Tool", et);
									Utils.getWebElement(driver, "Data Extract",
											registryName).click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Download Extract", logger,
											"Data Collection Tool", et);
									Utils.getWebElement(driver, "NCDRHome", registryName)
											.click();
									Utils.getWebElement(driver, registryName, registryName)
											.click();
									Utils.getWebElement(driver, "Data", registryName)
											.click();

								}

								else if (tab.getTabName()
										.equalsIgnoreCase("Data Migration")
										&& registry.contains("Data")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 * Utils.getWebElement(driver, "Data", registry)
									 * .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);

									Utils.checkIfTextExistsInsideMenu(driver,
											"Operator Remediation", logger,
											"Data Migration", et);

									/*
									 * Utils.checkIfTextExists(driver, "Submission Status",
									 * logger);
									 */

								}

								else if (tab.getTabName().equalsIgnoreCase("DQR")
										&& registry.contains("Data")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 * Utils.getWebElement(driver, "Data", registry)
									 * .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Results", logger, "DQR", et);

								}

								else if (tab.getTabName().equalsIgnoreCase("Upload Data")
										&& registry.contains("Data")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 * Utils.getWebElement(driver, "Data", registry)
									 * .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, "Upload Data", logger,
											et);

									Utils.uploadData(driver, registryName, logger, et);

								} else if (tab.getTabName()
										.equalsIgnoreCase("Site Profile")
										&& registry.contains("Admin")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									Utils.getWebElement(driver, "Administration",
											registryName).click();
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.page_wait_DQR(driver, 2l);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Registry Information", logger, "Site Profile",
											et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Site User Administration")
										&& registry.contains("Admin")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									Utils.getWebElement(driver, "Administration",
											registryName).click();
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver,
											"Site User Administration", logger, et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Site User Administration", logger,
											"Site User Administration", et);

								} else if (tab.getTabName().equalsIgnoreCase("Documents")
										&& registry.contains("Resources")) {
									try {
										Utils.getWebElement(driver, registryName,
												registryName).click();

									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Survey Collection Tool")
										&& registry.contains("Navigator")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									/*
									 * Utils.getWebElement(driver,
									 * "Patient Navigator Program", registryName).click();
									 * Utils.getWebElement(driver, tab.getTabName(),
									 * registryName) .click();
									 */
									Utils.checkIfTextExistsInsideMenu(driver,
											"New Surveys", logger,
											"Survey Collection Tool", et);
									/*
									 * Utils.getWebElement(driver,
									 * "ACTION Registry - GWTG Home", registryName)
									 * .click();
									 */

									driver.navigate().back();

								} else if (tab.getTabName().equalsIgnoreCase(
										"AHA Benchmarks")
										&& registry.contains("Navigator")) {
									/*
									 * Utils.getWebElement(driver,
									 * "Patient Navigator Program", registryName).click();
									 * Utils.getWebElement(driver, tab.getTabName(),
									 * registryName) .click();
									 */
									Utils.checkIfTextExistsInsideMenu(driver, "Timeframe",
											logger, "AHA Benchmarks", et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Pending Hospital Surveys")
										&& registry.contains("Navigator")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									/*
									 * Utils.getWebElement(driver,
									 * "Patient Navigator Program", registryName).click();
									 */
									/*
									 * Utils.getWebElement(driver, tab.getTabName(),
									 * registryName) .click();
									 */
									Utils.checkIfTextExistsInsideMenu(driver,
											"Pending Hospital Surveys", logger,
											"Patient Navigator Program", et);

								} else {
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);
								}

							} else {
								logger.log("fail", tab.getTabName() + " doesn't exists.",
										true, driver, reportingPath, et);
							}
							
						
						}
					}
					logger.flush();

				}
			}
			if (registry.contains("SwitchRegistry")) {
				ExtentTest et=logger.createTest(registryName + " SwitchRegistry Test",
						"SwitchRegistry Test");
				Utils.checkSwithRegisteries(driver, logger,et);
				logger.flush();
				// Action "Welcome ACTION Registry-GWTG Participants" page
				// verification
			}
			// Thread.sleep(2000);
			Utils.page_wait();
			/*if (registry.contains("RegistryLink") || registry.contains("Data")
					|| registry.contains("PatientNavigatorProgram"))*/
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Home").click();
			// Thread.sleep(2000);
			Utils.page_wait();
			/*Frame.title = " CathPCIRegistry in...";
			Frame.lebel = "CathPCIRegistry successfull!!!";
			Frame.createAndShowGUIBlue();*/
			Thread.sleep(2000);
			Log.info("PVIRegistry successfull!!!");

			logger.flush();
		} catch (Exception exp) {
			LoggerParent.warn("Following exception was raised in Action Class",
					exp);
		}

	}

	public static void Execute(WebDriver driver, Reporter rr,
			com.gallop.Logger logger, String registry,
			Map<String, Registry> registryMap, List<Registry> registries,
			Configuration configuration,ThreadGroup tg,List<Thread> threadList) throws Exception {
		try {
			/*
			 * logger.createTest("ActionRegistry Test",
			 * "Welcome ACTION Registry-GWTG Participants");
			 */
			String registryName = "cathPCIRegistry";
			// Map<String, Registry> registryMap = ExcelUtils.getRegistryMap();
			Registry registry2 = registryMap.get(registry);
			// Action registry
			try {
				/*if (registry.contains("RegistryLink") || registry.contains("Data")
						|| registry.contains("PatientNavigatorProgram"))*/
				CathPCIRegPageObj.CathPCIRegPageMethod(driver, registryName)
						.click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (registry.contains("RegistryLink")) {
				ExtentTest et = logger.createTest(registryName
						+ " RegistryLink Test", "RegistryLink Test");
				Utils.checkForUsernameAndLogoutLink(driver, logger, et);
				Utils.checkForRegistriesHeaderAndAnnouncementsHeader(driver,
						logger, et);
				try {
					if (registry2.getLinks() != null
							&& !registry2.getLinks().isEmpty())
						Utils.checkForLinks(driver, logger,
								registry2.getLinks(), et);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.flush();
			}
			/* logger.createTest(" Menu items Test", "Menu items Test"); */

			// Process each headers and capture log
			// List<Registry> registries = ExcelUtils.getRegisteries();
			Map<String, List<Tab>> registryHeadersMap = Utils
					.getTabsAndChildrenFromRegistry(registries);

			List<Tab> headers = registryHeadersMap.get(registry);
			ExecutorService executor = Executors.newFixedThreadPool(6);
			Long timeStart = System.currentTimeMillis();
			if (headers != null && !headers.isEmpty()) {
				for (Tab header : headers) {
					Runnable exec = new CathPCIMultiExecutor(configuration,
							driver, rr, registryMap, logger, registry,
							registries, registryName, header,tg,threadList);
					executor.execute(exec);
				}
			}
			executor.shutdown();
			
			if (registry.contains("SwitchRegistry")) {
				ExtentTest et = logger.createTest(registryName
						+ " SwitchRegistry Test", "SwitchRegistry Test");
				Utils.checkSwithRegisteries(driver, logger, et);
				logger.flush();
				// Action "Welcome ACTION Registry-GWTG Participants" page
				// verification
			}
			// Thread.sleep(2000);
			//Utils.page_wait();
			/*if (registry.contains("RegistryLink") || registry.contains("Data")
					|| registry.contains("PatientNavigatorProgram"))*/
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Home").click();
			// Thread.sleep(2000);
			//Utils.page_wait();
			/*Frame.title = " CathPCIRegistry in...";
			Frame.lebel = "CathPCIRegistry successfull!!!";
			Frame.createAndShowGUIBlue();*/
			Thread.sleep(2000);
			Log.info("CathPCIRegistry successfull!!!");

			logger.flush();
		} catch (Exception exp) {
			LoggerParent.warn("Following exception was raised in Action Class",
					exp);
		}

	}
	public static void executeHeader(Reporter rr, com.gallop.Logger logger,
			String registry, Map<String, Registry> registryMap,
			List<Registry> registries, Configuration configuration,
			String registryName, Tab header) {
		WebDriver driver = null;
		String run = configuration.getBrowser();
		try {
			if (run.trim().equalsIgnoreCase("ie")) {
				// Open IE
				driver = Utils.OpenIEBrowser(driver, configuration);
			}

			else if (run.trim().equalsIgnoreCase("firefox"))
			// Open Firefox
			{
				driver = Utils.OpenFFBrowser(driver, configuration);
			} else if (run.trim().equalsIgnoreCase("Chrome"))
			// Open Chrome
			{
				driver = Utils.OpenChrmBrowser(driver, configuration);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ExtentTest et = logger.createTest(registryName + " " + header.getId()
				+ "  Test", header.getId() + "  Test");
		Boolean loggedIn = LogIn.Execute(driver, rr, logger, configuration, et);

		CathPCIRegPageObj.CathPCIRegPageMethod(driver, registryName).click();

		WebElement parentTab = Utils.getWebElement(driver, header.getId(),
				registryName);
		/*
		 * if (parentTab != null) { logger.log("pass", header.getTabName() +
		 * " exists", false, driver, "");
		 * 
		 * } else { logger.log("fail", header.getTabName() + " doesn't exists",
		 * false, driver, "Q:\\QA Stuff\\Syam pithani\\testData\\"); }
		 */

		List<Tab> childrenTabs = header.getChildTabs();
		if (childrenTabs != null && !childrenTabs.isEmpty()) {
			for (Tab tab : childrenTabs) {
				WebElement childTab = Utils.getWebElement(driver, tab.getId(),
						registryName);
				// click parent element

				if (childTab != null) {
					try {
						childTab.click();
						if (driver.getPageSource().contains(
								"Server Error in '/WebNCDR' Application")
								|| driver.getPageSource().contains(
										"An unexpected error has occurred")) {
							logger.log("fail",
									"Server Error in '/WebNCDR' Application after clicking "
											+ tab.getTabName() + ".", true,
									driver, reportingPath, et);
							driver.navigate().back();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					/*
					 * logger.log("pass", tab.getTabName() + " exists", false,
					 * driver, "");
					 */

					/*
					 * if (tab.getTabName().equalsIgnoreCase( "Site Profile")) {
					 * Utils.checkIfTextExists(driver, "Registry Information",
					 * logger); }
					 * 
					 * else
					 */if (tab.getTabName().equalsIgnoreCase("eReports")
							&& registry.contains("Dashboard")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						Utils.checkIfTextExistsInsideMenu(driver, "eReports",
								logger, "Dashboard", et);
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						driver.switchTo().frame("ifreport0");
						Utils.checkIfTextExistsInsideMenu(driver,
								"File Delivery", logger, "Dashboard", et);
						Utils.checkIfTextExistsInsideMenu(driver,
								"Outcomes Report", logger, "Dashboard", et);
						/*
						 * Utils.checkIfTextExists(driver, "Additional Reports",
						 * logger);
						 */

						Utils.checkIfTextExistsInsideMenu(driver,
								"Additional Reports", logger, "Dashboard", et);
						driver.switchTo().defaultContent();

					}

					else if (tab.getTabName().equalsIgnoreCase("comparator")
							&& registry.contains("Dashboard")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						Utils.checkIfTextExistsInsideMenu(driver, "Comparator",
								logger, "Dashboard", et);
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						/*
						 * Utils.checkIfTextExists(driver, "Filter Panel",
						 * logger);
						 */
						driver.switchTo().frame("ifreport1");
						Utils.checkIfTextExistsInsideMenu(driver,
								"Filter Panel", logger, "Dashboard", et);
						driver.switchTo().defaultContent();

					}

					else if (tab.getTabName().equalsIgnoreCase(
							"Data Collection Tool")
							&& registry.contains("Data")) {

						try {
							Utils.getWebElement(driver, registryName,
									registryName).click();
						} catch (Exception e) {
							// TODO: handle exception
						}
						try {
							childTab.click();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						/*
						 * Utils.getWebElement(driver, tab.getTabName(),
						 * registry).click();
						 */
						try {
							PatientLess.Execute(driver, logger, et);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							EpisodeLess.Execute(driver, logger, et);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Utils.getWebElement(driver, "Maintenance", registryName)
								.click();
						Utils.getWebElement(driver, "Submit to DQR",
								registryName).click();

						Utils.checkIfTextExistsInsideMenu(driver,
								"Submission Status", logger,
								"Data Collection Tool", et);
						Utils.getWebElement(driver, "Data Extract",
								registryName).click();
						Utils.checkIfTextExistsInsideMenu(driver,
								"Download Extract", logger,
								"Data Collection Tool", et);
						Utils.getWebElement(driver, "NCDRHome", registryName)
								.click();
						Utils.getWebElement(driver, registryName, registryName)
								.click();
						Utils.getWebElement(driver, "Data", registryName)
								.click();

					}

					else if (tab.getTabName()
							.equalsIgnoreCase("Data Migration")
							&& registry.contains("Data")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 * Utils.getWebElement(driver, "Data", registry)
						 * .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);

						Utils.checkIfTextExistsInsideMenu(driver,
								"Operator Remediation", logger,
								"Data Migration", et);

						/*
						 * Utils.checkIfTextExists(driver, "Submission Status",
						 * logger);
						 */

					}

					else if (tab.getTabName().equalsIgnoreCase("DQR")
							&& registry.contains("Data")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 * Utils.getWebElement(driver, "Data", registry)
						 * .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);
						Utils.checkIfTextExistsInsideMenu(driver,
								"Submission Results", logger, "DQR", et);

					}

					else if (tab.getTabName().equalsIgnoreCase("Upload Data")
							&& registry.contains("Data")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 * Utils.getWebElement(driver, "Data", registry)
						 * .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, "Upload Data", logger,
								et);

						Utils.uploadData(driver, registryName, logger, et);

					} else if (tab.getTabName()
							.equalsIgnoreCase("Site Profile")
							&& registry.contains("Admin")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						Utils.getWebElement(driver, "Administration",
								registryName).click();
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.page_explicit_wait(driver, 2l,"Registry Information");
						Utils.checkIfTextExistsInsideMenu(driver,
								"Registry Information", logger, "Site Profile",
								et);

					} else if (tab.getTabName().equalsIgnoreCase(
							"Site User Administration")
							&& registry.contains("Admin")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						Utils.getWebElement(driver, "Administration",
								registryName).click();
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver,
								"Site User Administration", logger, et);
						Utils.checkIfTextExistsInsideMenu(driver,
								"Site User Administration", logger,
								"Site User Administration", et);

					} else if (tab.getTabName().equalsIgnoreCase("Documents")
							&& registry.contains("Resources")) {
						try {
							Utils.getWebElement(driver, registryName,
									registryName).click();

						} catch (Exception e) {
							// TODO: handle exception
						}
						try {
							childTab.click();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);

					} else if (tab.getTabName().equalsIgnoreCase(
							"Survey Collection Tool")
							&& registry.contains("Navigator")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						/*
						 * Utils.getWebElement(driver,
						 * "Patient Navigator Program", registryName).click();
						 * Utils.getWebElement(driver, tab.getTabName(),
						 * registryName) .click();
						 */
						Utils.checkIfTextExistsInsideMenu(driver,
								"New Surveys", logger,
								"Survey Collection Tool", et);
						/*
						 * Utils.getWebElement(driver,
						 * "ACTION Registry - GWTG Home", registryName)
						 * .click();
						 */

						driver.navigate().back();

					} else if (tab.getTabName().equalsIgnoreCase(
							"AHA Benchmarks")
							&& registry.contains("Navigator")) {
						/*
						 * Utils.getWebElement(driver,
						 * "Patient Navigator Program", registryName).click();
						 * Utils.getWebElement(driver, tab.getTabName(),
						 * registryName) .click();
						 */
						Utils.checkIfTextExistsInsideMenu(driver, "Timeframe",
								logger, "AHA Benchmarks", et);

					} else if (tab.getTabName().equalsIgnoreCase(
							"Pending Hospital Surveys")
							&& registry.contains("Navigator")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						/*
						 * Utils.getWebElement(driver,
						 * "Patient Navigator Program", registryName).click();
						 */
						/*
						 * Utils.getWebElement(driver, tab.getTabName(),
						 * registryName) .click();
						 */
						Utils.checkIfTextExistsInsideMenu(driver,
								"Pending Hospital Surveys", logger,
								"Patient Navigator Program", et);

					} else {
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);
					}

				} else {
					logger.log("fail", tab.getTabName() + " doesn't exists.",
							true, driver, reportingPath, et);
				}
			}
		}
		// logger.flush();
		logger.endTest();
		Utils.close_Browser(driver);

	}

	public static void Execute(WebDriver driver) throws Exception {

		try {
			// CathPCI registry
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "CathPCIRegistry")
					.click();

			// CathPCI "Welcome CathPCI Registry-GWTG Participants" page
			// verification
			// Thread.sleep(1000);
			if (CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "CathPCIWelcomePage")
					.getText().equals("Welcome CathPCI Registry Participants")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass(
						"Welcome CathPCI Registry-GWTG Participants is passed",
						x1, 1);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail(
						"Welcome CathPCI Registry-GWTG Participants is failed",
						x1, 1);
			}
			// CathPCIAdmin
			// Thread.sleep(1000);
			if (CathPCIRegPageObj.CathPCIRegPageMethod(driver, "CathPCIAdmin")
					.getText().equals("Administration")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("CathPCIAdmin", x1, 2);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("CathPCIAdmin", x1, 2);
			}

			// Dashboard
			// Thread.sleep(1000);
			if (CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "CathPCIDashboard").getText()
					.equals("Dashboard")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Dashboard", x1, 3);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Dashboard", x1, 3);
			}
			// Reports
			// Thread.sleep(1000);
			if (CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "CathPCIReports").getText()
					.equals("Reports")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Reports", x1, 4);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Reports", x1, 4);
			}

			// Data
			// Thread.sleep(1000);
			if (CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Data")
					.getText().equals("Data")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Data", x1, 5);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Data", x1, 5);
			}

			// Resources
			// Thread.sleep(1000);
			if (CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "CathPCIResources").getText()
					.equals("Resources")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Resources", x1, 6);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Resources", x1, 6);
			}

			// Control
			// Thread.sleep(1000);
			if (CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "CathPCIControl").getText()
					.equals("Control")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Control", x2, 1);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Control", x2, 1);
			}

			// CathPCI registry data
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Data").click();
			// Thread.sleep(1000);
			// Data Collection Tool (v2.4)
			CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "DataCollectionTool").click();
			Thread.sleep(2000);

			// Patient module
			// PatientLess.Execute(driver,Log);
			Thread.sleep(2000);

			// Returning to CathPCI registry again for verification.
			// CathPCI registry
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "NCDRHome").click();
			Thread.sleep(2000);
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "CathPCIRegistry")
					.click();
			Thread.sleep(2000);
			// CathPCI registry data
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Data").click();
			Thread.sleep(1000);
			// Data Collection Tool (v2.4)
			CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "DataCollectionTool").click();
			Thread.sleep(2000);

			// Patient Verification module
			CathPCIPatientVerificationObj.Execute(driver);

			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "NCDRHome").click();

			Thread.sleep(2000);
			/*Frame.title = " CathPCI Reg in...";
			Frame.lebel = "CathPCI Registry successfull!!!";
			Frame.createAndShowGUIBlue();*/
			Thread.sleep(2000);

			Log.info("CathPCI Registry executed successfully!!!");

			Thread.sleep(2000);
		} catch

		(Exception exp) {
			LoggerParent.warn(
					"Following exception was raised in Cath PCI registry", exp);
		}

	}// end Method

}// Class
