package appModule;

//import java.util.Date;
import java.util.concurrent.TimeUnit;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObjects.ActionRegPage;
import pageObjects.AdministrationPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;
//import Frame.Frame;

import com.relevantcodes.extentreports.ExtentTest;

public class ActionDQR {
	static int x1 = 14;
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static ActionRegPage ActionRegPageObj = new ActionRegPage();

	public static void Execute(WebDriver driver, com.gallop.Logger logger,ExtentTest et)
			throws Exception {

		try {
			
			/*ActionRegPageObj.ActionRegPageMethod(driver,
					"Submission History").click();
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data",et);
			driver.navigate().back();*/
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);

			// Action registry
			/*
			 * ActionRegPageObj.ActionRegPageMethod(driver,
			 * "ActionRegistry").click(); Thread.sleep(1000); //Action registry
			 * data ActionRegPageObj.ActionRegPageMethod(driver,
			 * "Data").click(); Thread.sleep(1000); //Upload Data (v2.4)
			 * ActionRegPageObj.ActionRegPageMethod(driver,
			 * "UploadData").click(); Thread.sleep(1000);
			 */

			if (Constant.isElementPresent(driver,
					By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0")) == true) {
				ActionRegPageObj.ActionRegPageMethod(driver, "RemoveFile")
						.click();
			}

			String MainWindow = driver.getWindowHandle();
			System.out.println("Main Window handle is: " + MainWindow);

			// Browse to attach file.
			// ActionRegPageObj.ActionRegPageMethod(driver, "Browse").click();

			try {
				/*
				 * ExcelUtils.setExcelFile(Constant.Path_TestData +
				 * Constant.File_TestData,"Config"); String ASCFile =
				 * ExcelUtils.getCellData(6, 1); StringSelection stringSelection
				 * = new StringSelection(Constant.ActionDQRFile+ASCFile);
				 * 
				 * System.out.println(stringSelection); Thread.sleep(1000);
				 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
				 * stringSelection, null);
				 * 
				 * //native key strokes for CTRL, V and ENTER keys Robot robot =
				 * new Robot(); robot.delay(1000);
				 * robot.keyPress(KeyEvent.VK_ENTER);
				 * robot.keyRelease(KeyEvent.VK_ENTER); robot.delay(500);
				 * robot.keyPress(KeyEvent.VK_CONTROL);
				 * robot.keyPress(KeyEvent.VK_V);
				 * robot.keyRelease(KeyEvent.VK_V);
				 * robot.keyRelease(KeyEvent.VK_CONTROL);
				 * robot.keyPress(KeyEvent.VK_ENTER);
				 * robot.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(1000);
				 * robot.keyPress(KeyEvent.VK_ENTER);
				 * robot.keyRelease(KeyEvent.VK_ENTER);
				 * ActionRegPageObj.ActionRegPageMethod(driver,
				 * "AttachFile").click();
				 */
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "Config");
				String ASCFile = ExcelUtils.getCellData(6, 1);
				driver.findElement(By.id("MainContent_C003_fileUploadDQRData"))
						.sendKeys(Constant.ActionDQRFile + ASCFile);
				ActionRegPageObj.ActionRegPageMethod(driver, "AttachFile")
						.click();
				//Thread.sleep(1000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				logger.log("pass", "File attachment success", false, driver, "",et);
				// CHeck for submission status text search

			} catch (Exception e) {
				e.printStackTrace();
				logger.log("fail", "File attaching failed", true, driver,
						"Q:\\QA Stuff\\Syam pithani\\testData\\",et);
				System.out.println("Error when attaching file:\n"
						+ e.toString());
			}

			///Thread.sleep(1000);
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);

			try {
				if (ActionRegPageObj.ActionRegPageMethod(driver,
						"ComplianceCheck").getAttribute("checked") == null) {
					ActionRegPageObj.ActionRegPageMethod(driver,
							"ComplianceCheck").click();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//Thread.sleep(1000);
			ActionRegPageObj.ActionRegPageMethod(driver, "DQRSubmit").click();
			//Thread.sleep(2000);
			//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 20l);

			// ActionRegPageObj.ActionRegPageMethod(driver, "Download_0")
			// /while(ActionRegPageObj.ActionRegPageMethod(driver,
			// "Download_0").size()==0)

			// Keep on refreshing till I see 1st download link.
			// while(driver.findElements(By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0")).size()
			// == 0 )
			//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			while (driver.findElements(
					By.id("MainContent_C004_lvSubmissions_img1_0")).size() != 0) {
				//Thread.sleep(5000);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 5l);
				driver.navigate().refresh();
			}
			
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data",et);

			// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// driver.navigate().refresh();
			// ActionRegPageObj.ActionRegPageMethod(driver,
			// "Download_0").getSize().equals(0);

			// if
			// (driver.findElements(By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0")).size()
			// == 0 )
			// {driver.navigate().refresh();}

			if (driver.findElements(
					By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0"))
					.size() == 0) {
				ActionRegPageObj.ActionRegPageMethod(driver,
						"Submission History").click();
				Utils.checkIfTextExistsInsideMenu(driver,
						"Submission Status", logger,
						"Upload Data",et);
			}

			if (driver.findElements(
					By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0"))
					.size() != 0)
			// if (ActionRegPageObj.ActionRegPageMethod(driver,
			// "Download_0").isDisplayed())
			{
				ActionRegPageObj.ActionRegPageMethod(driver, "1stSubmission")
						.getText();
				String SubMissionDate = ActionRegPageObj.ActionRegPageMethod(
						driver, "1stSubmission").getText();
				/*System.out.println(SubMissionDate);

				SimpleDateFormat format = new SimpleDateFormat(
						"MM/dd/yyyy hh:mm:ss aa");
				Date dt = new Date();
				Date d1 = format.parse(SubMissionDate);
				System.out.println(d1);
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Action upload date/time" + d1, 14,
						2);
				System.out.println(dt);
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Current date/time" + dt, 14, 3);
				if (d1.compareTo(dt) <= 0) {
					System.out.println("Pass message");
				} else {
					System.out.println("Failed message");
				}
*/
				// CHeck for submission status text search
				/*ActionRegPageObj.ActionRegPageMethod(driver,
						"SubmissionHistory").click();
				Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
						logger, "Upload Data");*/

				/*Frame.title = "Upload Data Submission...";
				Frame.lebel = "Upload Data Submission successfull!!!";
				Frame.createAndShowGUIBlue();*/
				//Thread.sleep(2000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				Log.info("Upload Data Submission successfull!!!");
				/*ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Action DQR passed", x1, 1);*/
			} else {

				// CHeck for submission status text search
				/*ActionRegPageObj.ActionRegPageMethod(driver,
						"SubmissionHistory").click();
				Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
						logger, "Upload Data");*/
			/*	Frame.title = "Upload Data Submission...";
				Frame.lebel = "Upload Data Submission failed!!!";
				Frame.createAndShowGUIRed();*/
				/*ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Action DQR failed", x1, 1);*/
			}

			//Thread.sleep(2000);
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);
			AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin")
					.click();
			Log.info("ActionDQR executed successfully!!!!!!");

		} catch (Exception exp) {
			exp.printStackTrace();
			LoggerParent.warn(
					"Following exception was raised in ActionDQR Class", exp);
		}

	}// end Method

}
