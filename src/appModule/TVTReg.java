package appModule;

import java.util.List;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.TVTRegPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Reporter;
import utility.Utils;
/*import Frame.Frame;*/
import beans.Registry;
import beans.Tab;

public class TVTReg {
	static int x1 = 60;
	static int x2 = 61;
	private static Logger Log = Logger.getLogger(TVTReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static TVTPatientVerification TVTPatientVerificationObj = new TVTPatientVerification();
	private static TVTRegPage TVTRegPageObj = new TVTRegPage();

	public static void Execute(WebDriver driver) throws Exception {

		try {
			// TVT registry
			TVTRegPageObj.TVTRegPageMethod(driver, "TVTRegistry").click();
			Thread.sleep(1000);
			/*
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"Login"); Thread.sleep(1000); String
			 * ParticipantID = ExcelUtils.getCellData(1, 1); String sUserName =
			 * ExcelUtils.getCellData(1, 2); System.out.println(sUserName);
			 * String PassWord = ExcelUtils.getCellData(1, 3);
			 * System.out.println(PassWord);
			 * 
			 * TVTRegPageObj.TVTRegPageMethod(driver, "ParticipantID").clear();
			 * TVTRegPageObj.TVTRegPageMethod(driver,
			 * "ParticipantID").sendKeys(ParticipantID);
			 * TVTRegPageObj.TVTRegPageMethod(driver, "Username").clear();
			 * TVTRegPageObj.TVTRegPageMethod(driver,
			 * "Username").sendKeys(sUserName);
			 * TVTRegPageObj.TVTRegPageMethod(driver, "Password").clear();
			 * TVTRegPageObj.TVTRegPageMethod(driver,
			 * "Password").sendKeys(PassWord); Thread.sleep(1000);
			 * TVTRegPageObj.TVTRegPageMethod(driver, "TVTButton").click();
			 * Thread.sleep(2000);
			 */
			// System.out.println(TVTRegPageObj.TVTRegPageMethod(driver,
			// "Announcements").getText());

			if (TVTRegPageObj.TVTRegPageMethod(driver, "TVTWelcomePage")
					.getText()
					.equals("Welcome STS/ACC TVT Registry Participants")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass(
						"Welcome TVT Registry-GWTG Participants is visable",
						x1, 1);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils
						.setCellDataFail(
								"Welcome TVT Registry-GWTG Participants is not visable",
								x1, 1);
			}

			// ActionAdmin
			// Thread.sleep(1000);
			if (TVTRegPageObj.TVTRegPageMethod(driver, "TVTAdmin").getText()
					.equals("Administration")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("ActionAdmin", x1, 2);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("ActionAdmin", x1, 2);
			}

			// Reports
			// Thread.sleep(1000);
			if (TVTRegPageObj.TVTRegPageMethod(driver, "TVTReports").getText()
					.equals("Reports")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Reports", x1, 3);
			}

			else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Reports", x1, 3);
			}

			// Data
			// Thread.sleep(1000);
			if (TVTRegPageObj.TVTRegPageMethod(driver, "Data").getText()
					.equals("Data")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Data", x1, 4);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Data", x1, 4);
			}

			// Resources
			// Thread.sleep(1000);
			if (TVTRegPageObj.TVTRegPageMethod(driver, "TVTResources")
					.getText().equals("Resources")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Resources", x1, 5);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Resources", x1, 5);
			}

			// Control
			// Thread.sleep(1000);
			if (TVTRegPageObj.TVTRegPageMethod(driver, "TVTControl").getText()
					.equals("Control")) {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Control", x1, 6);
			} else {
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Control", x1, 6);
			}

			// Data
			TVTRegPageObj.TVTRegPageMethod(driver, "Data").click();
			Thread.sleep(1000);

			// Data Collection Tool
			TVTRegPageObj.TVTRegPageMethod(driver, "DataCollectionTool")
					.click();
			Thread.sleep(1000);

			// Patient module
			// PatientLess.Execute(driver);
			Thread.sleep(2000);

			TVTPatientVerificationObj.Execute(driver);
			Thread.sleep(1000);

			Thread.sleep(1000);
		/*	Frame.title = " TVT Reg in...";
			Frame.lebel = "TVT Registry successfull!!!";
			Frame.createAndShowGUIBlue();*/
			Thread.sleep(2000);

			TVTRegPageObj.TVTRegPageMethod(driver, "NCDRHome").click();

			Log.info("TVT Registry executed successfull!!!");
			Thread.sleep(2000);
		} catch (Exception exp) {
			LoggerParent.warn("Following exception was raised in TVT Registry",
					exp);
		}

	}// end Method

	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");

	public static void Execute(WebDriver driver, Reporter reporter,
			com.gallop.Logger logger, String registry,
			Map<String, Registry> registryMap, List<Registry> registries)
			throws Exception {
		try {
			/*
			 * logger.createTest("ActionRegistry Test",
			 * "Welcome ACTION Registry-GWTG Participants");
			 */
			String registryName = "TVTRegistry";
			// Map<String, Registry> registryMap = ExcelUtils.getRegistryMap();
			Registry registry2 = registryMap.get(registry);
			// Action registry
			TVTRegPageObj.TVTRegPageMethod(driver, registryName).click();

			if (registry.contains("RegistryLink")) {
				ExtentTest et = logger.createTest(registryName
						+ " RegistryLink Test", "RegistryLink Test");
				Utils.checkForUsernameAndLogoutLink(driver, logger, et);
				Utils.checkForRegistriesHeaderAndAnnouncementsHeader(driver,
						logger, et);
				try {
					if (registry2.getLinks() != null
							&& !registry2.getLinks().isEmpty())
						Utils.checkForLinks(driver, logger,
								registry2.getLinks(), et);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.flush();
			}
			/* logger.createTest(" Menu items Test", "Menu items Test"); */

			// Process each headers and capture log
			// List<Registry> registries = ExcelUtils.getRegisteries();
			Map<String, List<Tab>> registryHeadersMap = Utils
					.getTabsAndChildrenFromRegistry(registries);

			List<Tab> headers = registryHeadersMap.get(registry);

			if (headers != null && !headers.isEmpty()) {
				for (Tab header : headers) {
					ExtentTest et = logger.createTest(registryName + " "
							+ header.getId() + "  Test", header.getId()
							+ "  Test");
					WebElement parentTab = Utils.getWebElement(driver,
							header.getId(), registryName);
					/*
					 * if (parentTab != null) { logger.log("pass",
					 * header.getTabName() + " exists", false, driver, "");
					 * 
					 * } else { logger.log("fail", header.getTabName() +
					 * " doesn't exists", false, driver,
					 * "Q:\\QA Stuff\\Syam pithani\\testData\\"); }
					 */

					List<Tab> childrenTabs = header.getChildTabs();
					if (childrenTabs != null && !childrenTabs.isEmpty()) {
						for (Tab tab : childrenTabs) {/*
							WebElement childTab = Utils.getWebElement(driver,
									tab.getId(), registryName);
							// click parent element

							if (childTab != null) {
								try {
									childTab.click();
									if (driver
											.getPageSource()
											.contains(
													"Server Error in '/WebNCDR' Application")
											|| driver
													.getPageSource()
													.contains(
															"An unexpected error has occurred")) {
										logger.log("fail",
												"Server Error in '/WebNCDR' Application after clicking "
														+ tab.getTabName()
														+ ".", true, driver,
												reportingPath, et);
										driver.navigate().back();
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								
								 * logger.log("pass", tab.getTabName() +
								 * " exists", false, driver, "");
								 

								
								 * if (tab.getTabName().equalsIgnoreCase(
								 * "Site Profile")) {
								 * Utils.checkIfTextExists(driver,
								 * "Registry Information", logger); }
								 * 
								 * else
								 if (tab.getTabName().equalsIgnoreCase(
										"eReports")
										&& registry.contains("Dashboard")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									Thread.sleep(1000);
									Utils.checkIfTextExistsInsideMenu(driver,
											"eReports", logger, "Dashboard", et);
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									driver.switchTo().frame("ifreport0");
									Utils.checkIfTextExistsInsideMenu(driver,
											"File Delivery", logger,
											"Dashboard", et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Outcomes Report", logger,
											"Dashboard", et);
									
									 * Utils.checkIfTextExists(driver,
									 * "Additional Reports", logger);
									 

									Utils.checkIfTextExistsInsideMenu(driver,
											"Additional Reports", logger,
											"Dashboard", et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"comparator")
										&& registry.contains("Dashboard")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									Thread.sleep(1000);
									Utils.checkIfTextExists(driver,
											"Comparator", logger, et);
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Thread.sleep(2000);
									
									 * Utils.checkIfTextExists(driver,
									 * "Filter Panel", logger);
									 
									driver.switchTo().frame("ifreport1");
									Utils.checkIfTextExistsInsideMenu(driver,
											"Filter Panel", logger,
											"Dashboard", et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Data Collection Tool")
										&& registry.contains("Data")) {

									try {
										Utils.getWebElement(driver,
												registryName, registryName)
												.click();
									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									
									 * Utils.getWebElement(driver,
									 * tab.getTabName(), registry).click();
									 
									PatientLess.Execute(driver, logger, et);
									EpisodeLess.Execute(driver, logger, et);
									Utils.getWebElement(driver, "Maintenance",
											registryName).click();
									Utils.getWebElement(driver,
											"Submit to DQR", registryName)
											.click();

									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Status", logger,
											"Data Collection Tool", et);
									try {
										Utils.getWebElement(driver,
												"Data Extract", registryName)
												.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									Utils.checkIfTextExistsInsideMenu(driver,
											"Download Extract", logger,
											"Data Collection Tool", et);
									Utils.getWebElement(driver, "NCDRHome",
											registryName).click();
									Utils.getWebElement(driver, registryName,
											registryName).click();
									Utils.getWebElement(driver, "Data",
											registryName).click();

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Data Migration")
										&& registry.contains("Data")) {

									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger, et);
									
									 * Utils.checkIfTextExists(driver,
									 * "Operator Remediation", logger);
									 * 
									 * Utils.checkIfTextExists(driver,
									 * "Submission Status", logger);
									 

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"DQR")
										&& registry.contains("Data")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 * Utils.getWebElement(driver, "Data",
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger, et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Results", logger,
											"DQR", et);

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Upload Data")
										&& registry.contains("Data")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 * Utils.getWebElement(driver, "Data",
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Upload Data", logger, "Data", et);

									Utils.uploadData(driver, registryName,
											logger, et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Site Profile")
										&& registry.contains("Admin")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											"Administration", registryName)
											.click();
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Registry Information", logger,
											"Site Profile", et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Site User Administration")
										&& registry.contains("Admin")) {
									
									 * try { childTab.click(); } catch
									 * (Exception e) { // TODO Auto-generated
									 * catch block //e.printStackTrace(); }
									 
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											"Administration", registryName)
											.click();
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Site User Administration", logger,
											"Site User Administration", et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Documents")
										&& registry.contains("Resources")) {
									try {
										Utils.getWebElement(driver,
												registryName, registryName)
												.click();

									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click();
									 * Utils.getWebElement(driver, registry,
									 * registry) .click();
									 
									Utils.getWebElement(driver,
											tab.getTabName(), registryName)
											.click();
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger, et);

								} else {
									Utils.checkIfTextExists(driver,
											tab.getTabName(), logger, et);
									Utils.checkIfTextExistsInsideMenu(driver,
											tab.getTabName(), logger,
											tab.getTabName(), et);
								}

							} else {
								logger.log("fail", tab.getTabName()
										+ " doesn't exists.", true, driver,
										reportingPath, et);
							}
						*/

							WebElement childTab = Utils.getWebElement(driver, tab.getId(),
									registryName);
							// click parent element

							if (childTab != null) {
								try {
									childTab.click();
									if (driver.getPageSource().contains(
											"Server Error in '/WebNCDR' Application")
											|| driver.getPageSource().contains(
													"An unexpected error has occurred")) {
										logger.log("fail",
												"Server Error in '/WebNCDR' Application after clicking "
														+ tab.getTabName() + ".", true,
												driver, reportingPath, et);
										driver.navigate().back();
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									// e.printStackTrace();
								}
								/*
								 * logger.log("pass", tab.getTabName() + " exists", false,
								 * driver, "");
								 */

								/*
								 * if (tab.getTabName().equalsIgnoreCase( "Site Profile")) {
								 * Utils.checkIfTextExists(driver, "Registry Information",
								 * logger); }
								 * 
								 * else
								 */if (tab.getTabName().equalsIgnoreCase("eReports")
										&& registry.contains("Dashboard")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									Utils.checkIfTextExistsInsideMenu(driver, "eReports",
											logger, "Dashboard", et);
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									driver.switchTo().frame("ifreport0");
									Utils.checkIfTextExistsInsideMenu(driver,
											"File Delivery", logger, "Dashboard", et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Outcomes Report", logger, "Dashboard", et);
									/*
									 * Utils.checkIfTextExists(driver, "Additional Reports",
									 * logger);
									 */

									Utils.checkIfTextExistsInsideMenu(driver,
											"Additional Reports", logger, "Dashboard", et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase("comparator")
										&& registry.contains("Dashboard")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									Utils.checkIfTextExists(driver, "Comparator", logger,
											et);
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									try {
										Thread.sleep(2000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									/*
									 * Utils.checkIfTextExists(driver, "Filter Panel",
									 * logger);
									 */
									driver.switchTo().frame("ifreport1");
									Utils.checkIfTextExistsInsideMenu(driver,
											"Filter Panel", logger, "Dashboard", et);
									driver.switchTo().defaultContent();

								}

								else if (tab.getTabName().equalsIgnoreCase("Data Extract")
										&& registry.contains("Data")) {

									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);
									Utils.selectValue(driver,
											"MainContent_C002_ddlExtract", "Base");
									Utils.checkIfTextExistsInsideMenu(driver,
											"Download Extract", logger, "Data Extract", et);

								}

								else if (tab.getTabName().equalsIgnoreCase(
										"Data Collection Tool")
										&& registry.contains("Data")) {

									try {
										Utils.getWebElement(driver, registryName,
												registryName).click();
									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									/*
									 * Utils.getWebElement(driver, tab.getTabName(),
									 * registry).click();
									 */
									try {
										PatientLess.Execute(driver, logger, et);
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									try {
										EpisodeLess.Execute(driver, logger, et);
									} catch (Exception e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									Utils.getWebElement(driver, "Maintenance", registryName)
											.click();
									Utils.getWebElement(driver, "Submit to DQR",
											registryName).click();

									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Status", logger,
											"Data Collection Tool", et);
									/*
									 * try { Utils.getWebElement(driver, "Data Extract",
									 * registryName).click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block e.printStackTrace();
									 * } Utils.checkIfTextExistsInsideMenu(driver,
									 * "Download Extract",
									 * logger,"Data Collection Tool",et);
									 */
									Utils.getWebElement(driver, "NCDRHome", registryName)
											.click();
									Utils.getWebElement(driver, registryName, registryName)
											.click();
									Utils.getWebElement(driver, "Data", registryName)
											.click();

								}

								/*
								 * else if (tab.getTabName().equalsIgnoreCase(
								 * "Data Migration") && registry.contains("Data")) {
								 * 
								 * Utils.getWebElement(driver, tab.getTabName(),
								 * registryName) .click(); Utils.checkIfTextExists(driver,
								 * tab.getTabName(), logger,et);
								 * 
								 * 
								 * }
								 */

								else if (tab.getTabName().equalsIgnoreCase("DQR")
										&& registry.contains("Data")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 * Utils.getWebElement(driver, "Data", registry)
									 * .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Submission Results", logger, "DQR", et);

								}

								else if (tab.getTabName().equalsIgnoreCase("Upload Data")
										&& registry.contains("Data")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 * Utils.getWebElement(driver, "Data", registry)
									 * .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Upload Data", logger, "Data", et);

									Utils.uploadData(driver, registryName, logger, et);

								} else if (tab.getTabName()
										.equalsIgnoreCase("Site Profile")
										&& registry.contains("Admin")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									Utils.getWebElement(driver, "Administration",
											registryName).click();
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.page_wait_DQR(driver, 2l);
									Utils.checkIfTextExistsInsideMenu(driver,
											"Registry Information", logger, "Site Profile",
											et);

								} else if (tab.getTabName().equalsIgnoreCase(
										"Site User Administration")
										&& registry.contains("Admin")) {
									/*
									 * try { childTab.click(); } catch (Exception e) { //
									 * TODO Auto-generated catch block
									 * //e.printStackTrace(); }
									 */
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									Utils.getWebElement(driver, "Administration",
											registryName).click();
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExistsInsideMenu(driver,
											"Site User Administration", logger,
											"Site User Administration", et);

								} else if (tab.getTabName().equalsIgnoreCase("Documents")
										&& registry.contains("Resources")) {
									try {
										Utils.getWebElement(driver, registryName,
												registryName).click();

									} catch (Exception e) {
										// TODO: handle exception
									}
									try {
										childTab.click();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										// e.printStackTrace();
									}
									/*
									 * Utils.getWebElement(driver, "NCDRHome",
									 * registry).click(); Utils.getWebElement(driver,
									 * registry, registry) .click();
									 */
									Utils.getWebElement(driver, tab.getTabName(),
											registryName).click();
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);

								} else {
									Utils.checkIfTextExists(driver, tab.getTabName(),
											logger, et);
									Utils.checkIfTextExistsInsideMenu(driver,
											tab.getTabName(), logger, tab.getTabName(), et);
								}

							} else {
								logger.log("fail", tab.getTabName() + " doesn't exists.",
										true, driver, reportingPath, et);
							}
							
						}
					}
					logger.flush();

				}
			}
			if (registry.contains("SwitchRegistry")) {
				ExtentTest et = logger.createTest(registryName
						+ " SwitchRegistry Test", "SwitchRegistry Test");
				Utils.checkSwithRegisteries(driver, logger, et);
				logger.flush();
				// Action "Welcome ACTION Registry-GWTG Participants" page
				// verification
			}
			// Thread.sleep(2000);
			Utils.page_wait();
			TVTRegPageObj.TVTRegPageMethod(driver, "Home").click();
			// Thread.sleep(2000);
			Utils.page_wait();
			/*Frame.title = " TVTRegistry in...";
			Frame.lebel = "TVTRegistry successfull!!!";
			Frame.createAndShowGUIBlue();*/
			// Thread.sleep(2000);
			Utils.page_wait();
			Log.info("TVTRegistry successfull!!!");

			logger.flush();
		} catch (Exception exp) {
			LoggerParent.warn("Following exception was raised in Action Class",
					exp);
		}

	}// end Method

	public static void Execute(WebDriver driver, Reporter rr,
			com.gallop.Logger logger, String registry,
			Map<String, Registry> registryMap, List<Registry> registries,
			Configuration configuration, List<Thread> threadList) {
		// TODO Auto-generated method stub

		System.out.println("Scenario:" + registry);
		try {
			/*
			 * logger.createTest("ActionRegistry Test",
			 * "Welcome ACTION Registry-GWTG Participants");
			 */
			String registryName = "TVTRegistry";
			// Map<String, Registry> registryMap = ExcelUtils.getRegistryMap();
			Registry registry2 = registryMap.get(registry);
			// Action registry
			// if(registry.contains("RegistryLink") ||
			// registry.contains("Data"))
			// Boolean loggedIn = LogIn.Execute(driver, rr, logger,
			// configuration);

			try {
				/*
				 * if (registry.contains("RegistryLink") ||
				 * registry.contains("Data") ||
				 * registry.contains("PatientNavigatorProgram"))
				 */
				TVTRegPageObj.TVTRegPageMethod(driver, registryName).click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (registry.contains("RegistryLink")) {
				ExtentTest et = logger.createTest(registryName
						+ " RegistryLink Test", "RegistryLink Test");
				Utils.checkForUsernameAndLogoutLink(driver, logger, et);
				Utils.checkForRegistriesHeaderAndAnnouncementsHeader(driver,
						logger, et);
				try {
					if (registry2.getLinks() != null
							&& !registry2.getLinks().isEmpty())
						Utils.checkForLinks(driver, logger,
								registry2.getLinks(), et);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				logger.flush();
			}
			/* logger.createTest(" Menu items Test", "Menu items Test"); */

			// Process each headers and capture log
			// List<Registry> registries = ExcelUtils.getRegisteries();
			Map<String, List<Tab>> registryHeadersMap = Utils
					.getTabsAndChildrenFromRegistry(registries);

			List<Tab> headers = registryHeadersMap.get(registry);
			ExecutorService executor = Executors.newFixedThreadPool(6);
			Long timeStart = System.currentTimeMillis();
			if (headers != null && !headers.isEmpty()) {
				for (Tab header : headers) {
					/*
					 * Boolean loggedIn = LogIn.Execute(driver, rr, logger,
					 * configuration);
					 */
					Runnable exec = new TVTMultiExecutor(configuration, driver,
							rr, registryMap, logger, registry, registries,
							registryName, header, threadList);
					executor.execute(exec);
					/* logger.flush(); */
				}
			}
			executor.shutdown();
			/*
			 * executor.shutdown();
			 * 
			 * while (!executor.isTerminated()) { //
			 * System.out.println("Still running threads!! "
			 * +inputWorkbook.getParentFile().getName()); }
			 */
			long timeEnd = System.currentTimeMillis();
			System.out.println("Finished all threads " + (timeEnd - timeStart)
					+ " millis");
			if (registry.contains("SwitchRegistry")) {
				ExtentTest et = logger.createTest(registryName
						+ " SwitchRegistry Test", "SwitchRegistry Test");
				Utils.checkSwithRegisteries(driver, logger, et);
				logger.flush();
				// Action "Welcome ACTION Registry-GWTG Participants" page
				// verification
			}
			// Thread.sleep(2000);
			// Utils.page_wait();
			/* if (registry.contains("Dashboard") || registry.contains("Data")) */
			TVTRegPageObj.TVTRegPageMethod(driver, "Home").click();
			/* Thread.sleep(2000); */
			/*Frame.title = " TVT Reg in...";
			Frame.lebel = "TVT Registry successfull!!!";
			Frame.createAndShowGUIBlue();*/
			// Thread.sleep(2000);
			// Utils.page_wait();
			Log.info("TVT Registry successfull!!!");

			logger.flush();
		} catch (Exception exp) {
			LoggerParent.warn("Following exception was raised in Action Class",
					exp);
		}
	}

	public static void executeHeader(Reporter rr, com.gallop.Logger logger,
			String registry, Map<String, Registry> registryMap,
			List<Registry> registries, Configuration configuration,
			String registryName, Tab header) {
		WebDriver driver = null;
		String run = configuration.getBrowser();
		try {
			if (run.trim().equalsIgnoreCase("ie")) {
				// Open IE
				driver = Utils.OpenIEBrowser(driver, configuration);
			}

			else if (run.trim().equalsIgnoreCase("firefox"))
			// Open Firefox
			{
				driver = Utils.OpenFFBrowser(driver, configuration);
			} else if (run.trim().equalsIgnoreCase("Chrome"))
			// Open Chrome
			{
				driver = Utils.OpenChrmBrowser(driver, configuration);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ExtentTest et = logger.createTest(registryName + " " + header.getId()
				+ "  Test", header.getId() + "  Test");
		Boolean loggedIn = LogIn.Execute(driver, rr, logger, configuration, et);

		TVTRegPageObj.TVTRegPageMethod(driver, registryName).click();

		WebElement parentTab = Utils.getWebElement(driver, header.getId(),
				registryName);
		/*
		 * if (parentTab != null) { logger.log("pass", header.getTabName() +
		 * " exists", false, driver, "");
		 * 
		 * } else { logger.log("fail", header.getTabName() + " doesn't exists",
		 * false, driver, "Q:\\QA Stuff\\Syam pithani\\testData\\"); }
		 */

		List<Tab> childrenTabs = header.getChildTabs();
		if (childrenTabs != null && !childrenTabs.isEmpty()) {
			for (Tab tab : childrenTabs) {
				WebElement childTab = Utils.getWebElement(driver, tab.getId(),
						registryName);
				// click parent element

				if (childTab != null) {
					try {
						childTab.click();
						if (driver.getPageSource().contains(
								"Server Error in '/WebNCDR' Application")
								|| driver.getPageSource().contains(
										"An unexpected error has occurred")) {
							logger.log("fail",
									"Server Error in '/WebNCDR' Application after clicking "
											+ tab.getTabName() + ".", true,
									driver, reportingPath, et);
							driver.navigate().back();
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
					}
					/*
					 * logger.log("pass", tab.getTabName() + " exists", false,
					 * driver, "");
					 */

					/*
					 * if (tab.getTabName().equalsIgnoreCase( "Site Profile")) {
					 * Utils.checkIfTextExists(driver, "Registry Information",
					 * logger); }
					 * 
					 * else
					 */if (tab.getTabName().equalsIgnoreCase("eReports")
							&& registry.contains("Dashboard")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Utils.checkIfTextExistsInsideMenu(driver, "eReports",
								logger, "Dashboard", et);
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						driver.switchTo().frame("ifreport0");
						Utils.checkIfTextExistsInsideMenu(driver,
								"File Delivery", logger, "Dashboard", et);
						Utils.checkIfTextExistsInsideMenu(driver,
								"Outcomes Report", logger, "Dashboard", et);
						/*
						 * Utils.checkIfTextExists(driver, "Additional Reports",
						 * logger);
						 */

						Utils.checkIfTextExistsInsideMenu(driver,
								"Additional Reports", logger, "Dashboard", et);
						driver.switchTo().defaultContent();

					}

					else if (tab.getTabName().equalsIgnoreCase("comparator")
							&& registry.contains("Dashboard")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						Utils.checkIfTextExists(driver, "Comparator", logger,
								et);
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						/*
						 * Utils.checkIfTextExists(driver, "Filter Panel",
						 * logger);
						 */
						driver.switchTo().frame("ifreport1");
						Utils.checkIfTextExistsInsideMenu(driver,
								"Filter Panel", logger, "Dashboard", et);
						driver.switchTo().defaultContent();

					}

					else if (tab.getTabName().equalsIgnoreCase("Data Extract")
							&& registry.contains("Data")) {

						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);
						Utils.selectValue(driver,
								"MainContent_C002_ddlExtract", "Base");
						Utils.checkIfTextExistsInsideMenu(driver,
								"Download Extract", logger, "Data Extract", et);

					}

					else if (tab.getTabName().equalsIgnoreCase(
							"Data Collection Tool")
							&& registry.contains("Data")) {

						try {
							Utils.getWebElement(driver, registryName,
									registryName).click();
						} catch (Exception e) {
							// TODO: handle exception
						}
						try {
							childTab.click();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						/*
						 * Utils.getWebElement(driver, tab.getTabName(),
						 * registry).click();
						 */
						try {
							PatientLess.Execute(driver, logger, et);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						try {
							EpisodeLess.Execute(driver, logger, et);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						Utils.getWebElement(driver, "Maintenance", registryName)
								.click();
						Utils.getWebElement(driver, "Submit to DQR",
								registryName).click();

						Utils.checkIfTextExistsInsideMenu(driver,
								"Submission Status", logger,
								"Data Collection Tool", et);
						/*
						 * try { Utils.getWebElement(driver, "Data Extract",
						 * registryName).click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block e.printStackTrace();
						 * } Utils.checkIfTextExistsInsideMenu(driver,
						 * "Download Extract",
						 * logger,"Data Collection Tool",et);
						 */
						Utils.getWebElement(driver, "NCDRHome", registryName)
								.click();
						Utils.getWebElement(driver, registryName, registryName)
								.click();
						Utils.getWebElement(driver, "Data", registryName)
								.click();

					}

					/*
					 * else if (tab.getTabName().equalsIgnoreCase(
					 * "Data Migration") && registry.contains("Data")) {
					 * 
					 * Utils.getWebElement(driver, tab.getTabName(),
					 * registryName) .click(); Utils.checkIfTextExists(driver,
					 * tab.getTabName(), logger,et);
					 * 
					 * 
					 * }
					 */

					else if (tab.getTabName().equalsIgnoreCase("DQR")
							&& registry.contains("Data")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 * Utils.getWebElement(driver, "Data", registry)
						 * .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);
						Utils.checkIfTextExistsInsideMenu(driver,
								"Submission Results", logger, "DQR", et);

					}

					else if (tab.getTabName().equalsIgnoreCase("Upload Data")
							&& registry.contains("Data")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 * Utils.getWebElement(driver, "Data", registry)
						 * .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExistsInsideMenu(driver,
								"Upload Data", logger, "Data", et);

						Utils.uploadData(driver, registryName, logger, et);

					} else if (tab.getTabName()
							.equalsIgnoreCase("Site Profile")
							&& registry.contains("Admin")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						Utils.getWebElement(driver, "Administration",
								registryName).click();
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.page_explicit_wait(driver, 2l,"Registry Information");
						Utils.checkIfTextExistsInsideMenu(driver,
								"Registry Information", logger, "Site Profile",
								et);

					} else if (tab.getTabName().equalsIgnoreCase(
							"Site User Administration")
							&& registry.contains("Admin")) {
						/*
						 * try { childTab.click(); } catch (Exception e) { //
						 * TODO Auto-generated catch block
						 * //e.printStackTrace(); }
						 */
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						Utils.getWebElement(driver, "Administration",
								registryName).click();
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExistsInsideMenu(driver,
								"Site User Administration", logger,
								"Site User Administration", et);

					} else if (tab.getTabName().equalsIgnoreCase("Documents")
							&& registry.contains("Resources")) {
						try {
							Utils.getWebElement(driver, registryName,
									registryName).click();

						} catch (Exception e) {
							// TODO: handle exception
						}
						try {
							childTab.click();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							// e.printStackTrace();
						}
						/*
						 * Utils.getWebElement(driver, "NCDRHome",
						 * registry).click(); Utils.getWebElement(driver,
						 * registry, registry) .click();
						 */
						Utils.getWebElement(driver, tab.getTabName(),
								registryName).click();
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);

					} else {
						Utils.checkIfTextExists(driver, tab.getTabName(),
								logger, et);
						Utils.checkIfTextExistsInsideMenu(driver,
								tab.getTabName(), logger, tab.getTabName(), et);
					}

				} else {
					logger.log("fail", tab.getTabName() + " doesn't exists.",
							true, driver, reportingPath, et);
				}
			}
		}
		// logger.flush();
		logger.endTest();
		Utils.close_Browser(driver);

	}

}
