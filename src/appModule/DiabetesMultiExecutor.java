package appModule;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import beans.Registry;
import beans.Tab;
import utility.Constant;
import utility.ExcelUtils;
import utility.Reporter;
import utility.Utils;
import appModule.ActionReg;
import appModule.Configuration;

public class DiabetesMultiExecutor implements Runnable {

	Configuration configuration;
	WebDriver driver;
	Reporter rr;
	Map<String, Registry> registryMap;
	com.gallop.Logger logger;
	String registry;
	List<Registry> registries;
	String registryName;
	Tab header;
	List<Thread> threadList;
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");

	public DiabetesMultiExecutor(Configuration configuration, WebDriver driver,
			Reporter rr, Map<String, Registry> registryMap,
			com.gallop.Logger logger, String registry,
			List<Registry> registries, String registryName, Tab header, List<Thread> threadList) {
		this.configuration = configuration;
		this.driver = driver;
		this.rr = rr;
		this.registryMap = registryMap;
		this.logger = logger;
		this.registry = registry;
		this.registries = registries;
		this.registryName = registryName;
		this.header = header;
		this.threadList=threadList;
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		long timeStart = System.currentTimeMillis();
		
		processThread();
		long timeEnd = System.currentTimeMillis();
		System.out.println("Time taken for thread: " + (timeEnd - timeStart)
				+ " millis");

	}

	private void processThread() {
		try {
			Thread.currentThread().setName(registry);
			threadList.add(Thread.currentThread());
			DiabetesReg.executeHeader(rr, logger, registry, registryMap,
					registries, configuration, registryName, header);
		} catch (Exception e) {
		//	throw new RuntimeException();
			e.printStackTrace();
		}
	}
	

}
