package appModule;

import org.apache.log4j.LogManager;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.ActionRegPage;
import pageObjects.AdministrationPage;
import utility.Constant;
import utility.ExcelUtils;
//import Frame.Frame;

public class ActionAdmin {
	static int x1 =12;
	static int x2 =13;
	static int g1 =1;
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static ActionRegPage ActionRegPageObj = new ActionRegPage();
	public static void Execute(WebDriver driver) throws Exception{
		
		try{
			//Action registry  
			ActionRegPageObj.ActionRegPageMethod(driver, "ActionRegistry").click();
			Thread.sleep(2000);
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
						
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(1000);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			
			//SiteName
			//AdministrationPageObj.adminPageMethod(driver, "ActionSiteName").clear();
			//String SiteName = ExcelUtils.getCellData(g1, 3);
			//AdministrationPageObj.adminPageMethod(driver, "ActionSiteName").sendKeys(SiteName);
			Thread.sleep(1000);
			
			//Brand Name
			AdministrationPageObj.adminPageMethod(driver, "ActionBrandName").clear();
			String BrandName = ExcelUtils.getCellData(g1, 4);
			AdministrationPageObj.adminPageMethod(driver, "ActionBrandName").sendKeys(BrandName);
			Thread.sleep(1000);
			
			//National Provider Identification (NPI) Number
			AdministrationPageObj.adminPageMethod(driver, "ActionNPI").clear();
			String NPI = ExcelUtils.getCellData(g1, 5);
			AdministrationPageObj.adminPageMethod(driver, "ActionNPI").sendKeys(NPI);
			Thread.sleep(1000);
			
			// Medicare Provider Number (MPN)
			AdministrationPageObj.adminPageMethod(driver, "ActionMPN").clear();
			String MPN = ExcelUtils.getCellData(g1, 6);
			AdministrationPageObj.adminPageMethod(driver, "ActionMPN").sendKeys(MPN);
			Thread.sleep(1000);
			
			//American Hospital Association (AHA) Number
			AdministrationPageObj.adminPageMethod(driver, "ActionAHA").clear();
			String AHA = ExcelUtils.getCellData(g1, 7);
			AdministrationPageObj.adminPageMethod(driver, "ActionAHA").sendKeys(AHA);
			Thread.sleep(1000);
			
			//Submit 
			AdministrationPageObj.adminPageMethod(driver, "ActionSubmit").click();
			Thread.sleep(3000);
			
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
			Thread.sleep(2000);
			// Admin page verification
			String AdminTitle = AdministrationPageObj.adminPageMethod(driver, "ActionAdminText").getText();
			if (AdminTitle.equals("Administration"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Administration page is visable", x1, 1);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Administration page is not visable", x1, 1);}
			
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(2000);
			
			
			//******************Verification******************//
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");	
			System.out.println(ExcelUtils.getCellData(g1, 1));
			//Participant Id verification
			String ParticipantId = AdministrationPageObj.adminPageMethod(driver, "Action999999").getText();
			System.out.println(ParticipantId);
			if (ParticipantId.equals(ExcelUtils.getCellData(g1, 1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action Participant Id is visable", x1, 2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action Participant Id is not visable", x1, 2);}
			
			
			//Encryption Key verification
			String EncryptionKey = AdministrationPageObj.adminPageMethod(driver, "ActionEncryptionKey").getText();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			String str =ExcelUtils.getCellData(g1, 2);
			if (EncryptionKey.equals(str.trim()))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action Encryption Key is visable", x1, 3);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action Encryption Key is not visable", x1, 3);}

			//Site Name
			String SiteNameV = AdministrationPageObj.adminPageMethod(driver, "ActionSiteName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (SiteNameV.equals(ExcelUtils.getCellData(g1, 3)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action Site Name is visable", x1, 4);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action Site Name is not visable", x1, 4);}
			
			
			//Brand Name
			String BrandNameV = AdministrationPageObj.adminPageMethod(driver, "ActionBrandName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (BrandNameV.equals(ExcelUtils.getCellData(g1, 4)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action Brand Name is visable", x1, 5);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action Brand Name is not visable", x1, 5);}
			
			//National Provider Identification (NPI) Number 
			String NPIV = AdministrationPageObj.adminPageMethod(driver, "ActionNPI").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (NPIV.equals(ExcelUtils.getCellData(g1, 5)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action NPI is visable", x1, 6);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action NPI is not visable", x1, 6);}
			
			//Medicare Provider Number (MPN) 
			String MPNV = AdministrationPageObj.adminPageMethod(driver, "ActionMPN").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (MPNV.equals(ExcelUtils.getCellData(g1, 6)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action MPN is visable", x2, 1);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action MPN is not visable", x2, 1);}
			
			//American Hospital Association (AHA) Number 
			String AHAV = AdministrationPageObj.adminPageMethod(driver, "ActionAHA").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (AHAV.equals(ExcelUtils.getCellData(g1, 7)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Action AHA is visable", x2, 2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Action AHA is not visable", x2, 2);}
			
		/*    Frame.title=" Action administration in...";
		    Frame.lebel="Action administration successfull!!!";
		    Frame.createAndShowGUIBlue();*/
		    Thread.sleep(2000); 
		    
		    
		    AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
		    Log.info("Action administration successfull!!!");
			
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in Action administration Class", exp);
	    	}
	
    
}//end Method
		
	

}
