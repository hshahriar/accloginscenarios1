package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.AdministrationPage;
import pageObjects.TVTRegPage;
import utility.Constant;
import utility.ExcelUtils;
//import Frame.Frame;

public class TVTAdmin {
	static int x1=65;
	static int x2=66;
	static int g1=21;
	private static Logger Log = Logger.getLogger(TVTReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static TVTRegPage TVTRegPageObj = new TVTRegPage();
	public static void Execute(WebDriver driver) throws Exception{
		
		try{
			//TVT registry  
			TVTRegPageObj.TVTRegPageMethod(driver, "TVTRegistry").click();
			Thread.sleep(2000);
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
						
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(1000);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			
			//SiteName
			AdministrationPageObj.adminPageMethod(driver, "TVTSiteName").clear();
			String SiteName = ExcelUtils.getCellData(g1, 3);
			AdministrationPageObj.adminPageMethod(driver, "TVTSiteName").sendKeys(SiteName);
			Thread.sleep(1000);
			
			//Brand Name
			AdministrationPageObj.adminPageMethod(driver, "TVTBrandName").clear();
			String BrandName = ExcelUtils.getCellData(g1, 4);
			AdministrationPageObj.adminPageMethod(driver, "TVTBrandName").sendKeys(BrandName);
			Thread.sleep(1000);
			
			//National Provider Identification (NPI) Number
			AdministrationPageObj.adminPageMethod(driver, "TVTNPI").clear();
			String NPI = ExcelUtils.getCellData(g1, 5);
			AdministrationPageObj.adminPageMethod(driver, "TVTNPI").sendKeys(NPI);
			Thread.sleep(1000);
			
			// Medicare Provider Number (MPN)
			AdministrationPageObj.adminPageMethod(driver, "TVTMPN").clear();
			String MPN = ExcelUtils.getCellData(g1, 6);
			AdministrationPageObj.adminPageMethod(driver, "TVTMPN").sendKeys(MPN);
			Thread.sleep(1000);
			
			//American Hospital Association (AHA) Number
			AdministrationPageObj.adminPageMethod(driver, "TVTAHA").clear();
			String AHA = ExcelUtils.getCellData(g1, 7);
			AdministrationPageObj.adminPageMethod(driver, "TVTAHA").sendKeys(AHA);
			Thread.sleep(1000);
			
			//Submit 
			AdministrationPageObj.adminPageMethod(driver, "TVTSubmit").click();
			Thread.sleep(3000);
			
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
			Thread.sleep(2000);
			
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(1000);
			// Admin page verification
			//String AdminTitle = AdministrationPageObj.AdminPageMethod(driver, "TVTAdminText").getText();
			//if (AdminTitle.equals("Administration"))
			//{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			//ExcelUtils.setCellDataPass("Administration page is visable", 50, 1);}
			//else
			//{ExcelUtils.setCellDataFail("Administration page is not visable", 50, 1);}
			
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(2000);
			
			
			//******************Verification******************//
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");	
			//Participant Id verification
			String ParticipantId = AdministrationPageObj.adminPageMethod(driver, "TVT999999").getText();
			System.out.println(ParticipantId);
			if (ParticipantId.equals(ExcelUtils.getCellData(g1, 1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT Participant Id is visable", x1, 2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("TVT Participant Id is not visable", x1, 2);}
						
			//Encryption Key verification
			String EncryptionKey = AdministrationPageObj.adminPageMethod(driver, "TVTEncryptionKey").getText();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			String str =ExcelUtils.getCellData(g1, 2);
			
			if (EncryptionKey.equals(str.trim()))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT Encryption Key is visable", x1, 3);}
			else
			{ExcelUtils.setCellDataFail("TVT Encryption Key is not visable", x1, 3);}

			//Site Name
			String SiteNameV = AdministrationPageObj.adminPageMethod(driver, "TVTSiteName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (SiteNameV.equals(ExcelUtils.getCellData(g1, 3)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT Site Name is visable", x1, 4);}
			else
			{ExcelUtils.setCellDataFail("TVT Site Name is not visable",x1, 4);}
			
			
			//Brand Name
			String BrandNameV = AdministrationPageObj.adminPageMethod(driver, "TVTBrandName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (BrandNameV.equals(ExcelUtils.getCellData(g1, 4)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT Brand Name is visable", x1, 5);}
			else
			{ExcelUtils.setCellDataFail("TVT Brand Name is not visable", x1, 5);}
			
			//National Provider Identification (NPI) Number 
			String NPIV = AdministrationPageObj.adminPageMethod(driver, "TVTNPI").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (NPIV.equals(ExcelUtils.getCellData(g1, 5)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT NPI is visable", x1, 6);}
			else
			{ExcelUtils.setCellDataFail("TVT NPI is not visable", x1, 6);}
			
			//Medicare Provider Number (MPN) 
			String MPNV = AdministrationPageObj.adminPageMethod(driver, "TVTMPN").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (MPNV.equals(ExcelUtils.getCellData(g1, 6)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT MPN is visable", x2, 1);}
			else
			{ExcelUtils.setCellDataFail("TVT MPN is not visable", x2, 1);}
			
			//American Hospital Association (AHA) Number 
			String AHAV = AdministrationPageObj.adminPageMethod(driver, "TVTAHA").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (AHAV.equals(ExcelUtils.getCellData(g1, 7)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("TVT AHA is visable", x2, 2);}
			else
			{ExcelUtils.setCellDataFail("TVT AHA is not visable", x2, 2);}
			
		   /* Frame.title=" TVT administration in...";
		    Frame.lebel="TVT administration successfull!!!";
		    Frame.createAndShowGUIBlue();*/
		    Thread.sleep(2000); 
		    Log.info("TVT administration successfull!!!");
		    
		    Thread.sleep(2000);
		    AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
	
		    Log.info("TVT administration executed successfully!!!");
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in TVT administration Class", exp);
	    	}
	
    
}//end Method
		
	

}
