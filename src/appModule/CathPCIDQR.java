package appModule;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.AdministrationPage;
import pageObjects.CathPCIRegPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;
//import Frame.Frame;

public class CathPCIDQR {
	static int x1 = 23;
	private static Logger Log = Logger.getLogger(CathPCIReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static CathPCIRegPage CathPCIRegPageObj = new CathPCIRegPage();

	public static void Execute(WebDriver driver, com.gallop.Logger logger,
			ExtentTest et) throws Exception {

		try {

			// CathPCI registry
			/*
			 * CathPCIRegPageObj.CathPCIRegPageMethod(driver,
			 * "CathPCIRegistry").click(); Thread.sleep(1000); //CathPCI
			 * registry data CathPCIRegPageObj.CathPCIRegPageMethod(driver,
			 * "Data").click(); Thread.sleep(1000); //Upload Data (v2.4)
			 * CathPCIRegPageObj.CathPCIRegPageMethod(driver,
			 * "UploadData").click(); Thread.sleep(1000);
			 */

			/*CathPCIRegPageObj
					.CathPCIRegPageMethod(driver, "Submission History").click();
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data", et);
			driver.navigate().back();*/
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 2l);

			if (Constant.isElementPresent(driver,
					By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0")) == true) {
				CathPCIRegPageObj.CathPCIRegPageMethod(driver, "RemoveFile")
						.click();
			}

			String MainWindow = driver.getWindowHandle();
			System.out.println("Main Window handle is: " + MainWindow);
			/*
			 * if (Constant.isElementPresent(driver,
			 * By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"
			 * ))==true) {CathPCIRegPageObj.CathPCIRegPageMethod(driver,
			 * "RemoveFile").click();}
			 */

			// Browse to attach file.
			/* CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Browse").click(); */
			try {
				// Do not put any Thread.sleep
				/*
				 * ExcelUtils.setExcelFile(Constant.Path_TestData +
				 * Constant.File_TestData,"Config"); String PCIFile =
				 * ExcelUtils.getCellData(7, 1); StringSelection stringSelection
				 * = new StringSelection(Constant.CathPCIDQRFile+PCIFile);
				 * Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
				 * stringSelection, null); //native key strokes for CTRL, V and
				 * ENTER keys Robot robot = new Robot();
				 * robot.keyPress(KeyEvent.VK_ENTER);
				 * robot.keyRelease(KeyEvent.VK_ENTER); robot.delay(500);
				 * robot.keyPress(KeyEvent.VK_CONTROL);
				 * robot.keyPress(KeyEvent.VK_V);
				 * robot.keyRelease(KeyEvent.VK_V);
				 * robot.keyRelease(KeyEvent.VK_CONTROL);
				 * robot.keyPress(KeyEvent.VK_ENTER);
				 * robot.keyRelease(KeyEvent.VK_ENTER); Thread.sleep(1000);
				 * robot.keyPress(KeyEvent.VK_ENTER);
				 * robot.keyRelease(KeyEvent.VK_ENTER);
				 * CathPCIRegPageObj.CathPCIRegPageMethod(driver,
				 * "AttachFile").click();
				 */
				ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "Config");
				String ASCFile = ExcelUtils.getCellData(7, 1);
				driver.findElement(By.id("MainContent_C003_fileUploadDQRData"))
						.sendKeys(Constant.ActionDQRFile + ASCFile);
				CathPCIRegPageObj.CathPCIRegPageMethod(driver, "AttachFile")
						.click();
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				logger.log("pass", "File attachment success", false, driver,
						"", et);
			} catch (AWTException e) {
				e.printStackTrace();
				logger.log("fail", "File attaching failed", true, driver,
						"Q:\\QA Stuff\\Syam pithani\\testData\\", et);
				System.out.println(e);
			}
			// Thread.sleep(1000);
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);

			try {
				if (CathPCIRegPageObj.CathPCIRegPageMethod(driver,
						"ComplianceCheck").getAttribute("checked") == null) {
					CathPCIRegPageObj.CathPCIRegPageMethod(driver,
							"ComplianceCheck").click();
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Thread.sleep(1000);
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "DQRSubmit").click();
			// Thread.sleep(1000);
			//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 20l);

			// CathPCIRegPageObj.CathPCIRegPageMethod(driver, "Download_0")
			// /while(CathPCIRegPageObj.CathPCIRegPageMethod(driver,
			// "Download_0").size()==0)

			// Keep on refreshing till I see 1st download link.
			// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			while (driver.findElements(
					By.id("MainContent_C004_lvSubmissions_img1_0")).size() != 0) {// Thread.sleep(5000);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 5l);
				driver.navigate().refresh();
			}
			
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data", et);

			// driver.navigate().refresh();
			// ActionRegPageObj.ActionRegPageMethod(driver,
			// "Download_0").getSize().equals(0);

			if (driver.findElements(
					By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0"))
					.size() == 0) {
				CathPCIRegPageObj.CathPCIRegPageMethod(driver,
						"SubmissionHistory").click();
			}

			if (driver.findElements(
					By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0"))
					.size() != 0)
			// if (ActionRegPageObj.ActionRegPageMethod(driver,
			// "Download_0").isDisplayed())
			{
				CathPCIRegPageObj.CathPCIRegPageMethod(driver, "1stSubmission")
						.getText();
				/*
				 * String SubMissionDate
				 * =CathPCIRegPageObj.CathPCIRegPageMethod(driver,
				 * "1stSubmission").getText();
				 * System.out.println(SubMissionDate);
				 * 
				 * SimpleDateFormat format = new
				 * SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa"); Date dt =new
				 * Date(); Date d1=format.parse(SubMissionDate);
				 * System.out.println(d1); System.out.println(dt);
				 * if(d1.compareTo(dt)<=0) {System.out.println("Pass message");
				 * }else {System.out.println("Failed message");}
				 */

				/*Frame.title = "Upload Data Submission...";
				Frame.lebel = "Upload Data Submission successfull!!!";
				Frame.createAndShowGUIBlue();*/
				// Thread.sleep(2000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				Log.info("Upload Data Submission successfull!!!");
			/*	ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("CathPCI DQR passed", x1, 1);*/
			} else {
			/*	Frame.title = "Upload Data Submission...";
				Frame.lebel = "Upload Data Submission failed!!!";
				Frame.createAndShowGUIRed();*/
			/*	ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("CathPCI DQR failed", x1, 1);*/
			}

			// Thread.sleep(2000);
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);
			AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin")
					.click();

			Log.info("CathPCIDQR executed successfully!!!");

		} catch (Exception exp) {

			LoggerParent.warn(
					"Following exception was raised in CathPCIDQR Class", exp);
		}

	}// end Method

}
