package appModule;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObjects.AdministrationPage;
import pageObjects.PVIRegPage;
import pageObjects.PinnacleRegPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;
//import Frame.Frame;


public class PinnacleDQR {
	static int x1=58;
	private static Logger Log = Logger.getLogger(PVIReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static PinnacleRegPage pinnaclePageObj = new PinnacleRegPage();
	public static void Execute(WebDriver driver, com.gallop.Logger logger) throws Exception{
		
		try{
			
			//PVI registry  
		/*	PVIRegPageObj.ActionRegPageMethod(driver, "PVIRegistry").click();
			Thread.sleep(1000);
			//PVI registry data
			PVIRegPageObj.ActionRegPageMethod(driver, "Data").click();
			Thread.sleep(1000);
			//Upload Data (v2.4)
			PVIRegPageObj.ActionRegPageMethod(driver, "UploadData").click();
			Thread.sleep(1000);*/
			
			if (Constant.isElementPresent(driver, By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"))==true)
			{pinnaclePageObj.ActionRegPageMethod(driver, "RemoveFile").click();}
			
			
			
			//Browse to attach file.
		/*	PVIRegPageObj.ActionRegPageMethod(driver, "Browse").click();*/
			//Do not put any Thread.sleep	
			try{
			/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
			String PVIFile = ExcelUtils.getCellData(10, 1);	
			StringSelection stringSelection = new StringSelection(Constant.PVIDQRFile+PVIFile);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			//native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.delay(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			PVIRegPageObj.ActionRegPageMethod(driver, "AttachFile").click();*/
			if (Constant.isElementPresent(driver, By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"))==true)
			{pinnaclePageObj.ActionRegPageMethod(driver, "RemoveFile").click();}
			
			
			String MainWindow=driver.getWindowHandle();
			System.out.println("Main Window handle is: "+MainWindow);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
			String ASCFile = ExcelUtils.getCellData(10, 1);
			driver.findElement(By.id("MainContent_C003_fileUploadDQRData")).sendKeys(Constant.ActionDQRFile+ASCFile);
			pinnaclePageObj.ActionRegPageMethod(driver, "AttachFile").click();
			/*Thread.sleep(1000);*/	Utils.page_wait();
		  //  logger.log("pass", "File attachment success", false, driver, "");
			
			} catch (AWTException e) {
				e.printStackTrace();}
			
			/*Thread.sleep(1000);*/	Utils.page_wait();
			
			try {
				if (pinnaclePageObj.ActionRegPageMethod(driver, "ComplianceCheck").getAttribute("checked")==null)
				   {pinnaclePageObj.ActionRegPageMethod(driver, "ComplianceCheck").click();}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		 
			/*Thread.sleep(1000);*/	Utils.page_wait();
			pinnaclePageObj.ActionRegPageMethod(driver, "DQRSubmit").click();
			Thread.sleep(3000);
					
			//PVIRegPageObj.ActionRegPageMethod(driver, "Download_0")
			///while(PVIRegPageObj.ActionRegPageMethod(driver, "Download_0").size()==0)
						
			//Keep on refreshing till I see 1st download link.
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			while(driver.findElements(By.id("MainContent_C004_lvSubmissions_img1_0")).size() != 0 )	
			{Thread.sleep(5000);
			driver.navigate().refresh();}

			 //driver.navigate().refresh();
			 //ActionRegPageObj.ActionRegPageMethod(driver, "Download_0").getSize().equals(0);
			 
			if (driver.findElements(By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0")).size() == 0 )
			{pinnaclePageObj.ActionRegPageMethod(driver, "SubmissionHistory").click();}
			 
			 if (driver.findElements(By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0")).size()!=0)
				//if (ActionRegPageObj.ActionRegPageMethod(driver, "Download_0").isDisplayed())
				{
				 pinnaclePageObj.ActionRegPageMethod(driver, "1stSubmission").getText();
			        String SubMissionDate =pinnaclePageObj.ActionRegPageMethod(driver, "1stSubmission").getText();
		            System.out.println(SubMissionDate);
		            
		            SimpleDateFormat format  = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
		            Date dt =new Date();
		        	Date d1=format.parse(SubMissionDate);
					System.out.println(d1);
					System.out.println(dt);
					if(d1.compareTo(dt)<=0) 
					{System.out.println("Pass message");
				}else {System.out.println("Failed message");}
				 			 
				/*Frame.title="Upload Data Submission...";
				Frame.lebel="Upload Data Submission successfull!!!";
				Frame.createAndShowGUIBlue();*/
				Thread.sleep(2000); 
				Log.info("Upload Data Submission successfull!!!");
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
				ExcelUtils.setCellDataPass("PVI DQR passed", x1, 1);
				}
				else
				{/*Frame.title="Upload Data Submission...";
				Frame.lebel="Upload Data Submission failed!!!";
				Frame.createAndShowGUIRed();*/
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
				ExcelUtils.setCellDataFail("PVI DQR failed", x1, 1);
		        Log.info("Upload Data Submission failed!!!");}
			
			 Thread.sleep(2000); 
			 AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
			
			 Log.info("PVI DQR executed successfull!!!");
			
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in PVIDQR Class", exp);
	    	}
	
    
}//end Method

}

