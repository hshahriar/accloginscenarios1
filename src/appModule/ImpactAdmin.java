package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.AdministrationPage;
import pageObjects.ImpactRegPage;
import utility.Constant;
import utility.ExcelUtils;
//import Frame.Frame;

public class ImpactAdmin {
	
	static int x1=43;
	static int x2=44;
	static int g1=13;
	private static Logger Log = Logger.getLogger(ImpactReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static ImpactRegPage ImpactRegPageObj = new ImpactRegPage();
	public static void Execute(WebDriver driver) throws Exception{
		
		try{
			//Impact registry  
			ImpactRegPageObj.ImpactRegPageMethod(driver, "ImpactRegistry").click();
			Thread.sleep(2000);
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
						
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(1000);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			
			//SiteName
			AdministrationPageObj.adminPageMethod(driver, "ImpactSiteName").clear();
			String SiteName = ExcelUtils.getCellData(g1, 3);
			AdministrationPageObj.adminPageMethod(driver, "ImpactSiteName").sendKeys(SiteName);
			Thread.sleep(1000);
			
			//Brand Name
			AdministrationPageObj.adminPageMethod(driver, "ImpactBrandName").clear();
			String BrandName = ExcelUtils.getCellData(g1, 4);
			AdministrationPageObj.adminPageMethod(driver, "ImpactBrandName").sendKeys(BrandName);
			Thread.sleep(1000);
			
			//National Provider Identification (NPI) Number
			AdministrationPageObj.adminPageMethod(driver, "ImpactNPI").clear();
			String NPI = ExcelUtils.getCellData(g1, 5);
			AdministrationPageObj.adminPageMethod(driver, "ImpactNPI").sendKeys(NPI);
			Thread.sleep(1000);
			
			// Medicare Provider Number (MPN)
			AdministrationPageObj.adminPageMethod(driver, "ImpactMPN").clear();
			String MPN = ExcelUtils.getCellData(g1, 6);
			AdministrationPageObj.adminPageMethod(driver, "ImpactMPN").sendKeys(MPN);
			Thread.sleep(1000);
			
			//American Hospital Association (AHA) Number
			AdministrationPageObj.adminPageMethod(driver, "ImpactAHA").clear();
			String AHA = ExcelUtils.getCellData(g1, 7);
			AdministrationPageObj.adminPageMethod(driver, "ImpactAHA").sendKeys(AHA);
			Thread.sleep(1000);
			
			//Submit 
			AdministrationPageObj.adminPageMethod(driver, "ImpactSubmit").click();
			Thread.sleep(3000);
			
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
			Thread.sleep(2000);
			// Admin page verification
			String AdminTitle = AdministrationPageObj.adminPageMethod(driver, "ImpactAdminText").getText();
			if (AdminTitle.equals("Administration"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Administration page is visable", x1, 1);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Administration page is not visable", x1, 1);}
			
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(2000);
			
			
			//******************Verification******************//
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");	
	
			//Participant Id verification
			String ParticipantId = AdministrationPageObj.adminPageMethod(driver, "Impact999999").getText();
			System.out.println(ParticipantId);
			if (ParticipantId.equals(ExcelUtils.getCellData(g1, 1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact Participant Id is visable", x1, 2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact Participant Id is not visable", x1, 2);}
			
		
			//Encryption Key verification
			String EncryptionKey = AdministrationPageObj.adminPageMethod(driver, "ImpactEncryptionKey").getText();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			String str =ExcelUtils.getCellData(g1, 2);
			System.out.println(ExcelUtils.getCellData(g1, 2));
			if (EncryptionKey.equals(str.trim()))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact Encryption Key is visable", x1,3);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact Encryption Key is not visable", x1, 3);}

			//Site Name
			String SiteNameV = AdministrationPageObj.adminPageMethod(driver, "ImpactSiteName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (SiteNameV.equals(ExcelUtils.getCellData(g1, 3)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact Site Name is visable", x1, 4);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact Site Name is not visable", x1, 4);}
			
			
			//Brand Name
			String BrandNameV = AdministrationPageObj.adminPageMethod(driver, "ImpactBrandName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (BrandNameV.equals(ExcelUtils.getCellData(g1, 4)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact Brand Name is visable", x1, 5);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact Brand Name is not visable", x1, 5);}
			
			//National Provider Identification (NPI) Number 
			String NPIV = AdministrationPageObj.adminPageMethod(driver, "ImpactNPI").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (NPIV.equals(ExcelUtils.getCellData(g1, 5)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact NPI is visable", x1, 6);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact NPI is not visable", x1, 6);}
			
			//Medicare Provider Number (MPN) 
			String MPNV = AdministrationPageObj.adminPageMethod(driver, "ImpactMPN").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (MPNV.equals(ExcelUtils.getCellData(g1, 6)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact MPN is visable", x2, 1);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact MPN is not visable", x2, 1);}
			
			//American Hospital Association (AHA) Number 
			String AHAV = AdministrationPageObj.adminPageMethod(driver, "ImpactAHA").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (AHAV.equals(ExcelUtils.getCellData(g1, 7)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Impact AHA is visable", x2, 2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Impact AHA is not visable", x2, 2);}
			
		   /* Frame.title=" Impact administration in...";
		    Frame.lebel="Impact administration successfull!!!";
		    Frame.createAndShowGUIBlue();*/
		    Thread.sleep(2000); 
		    
		    
		    AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
	
		    Log.info("Impact administration executed successfully!!!");
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in Impact administration Class", exp);
	    	}
	
    
}//end Method
		
	

}

