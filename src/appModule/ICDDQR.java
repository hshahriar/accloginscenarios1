package appModule;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.AdministrationPage;
import pageObjects.ICDRegPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;
//import Frame.Frame;


public class ICDDQR {
	static int x1 =32;
	private static Logger Log = Logger.getLogger(ICDReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static ICDRegPage ICDRegPageObj = new ICDRegPage();
	public static void Execute(WebDriver driver, com.gallop.Logger logger, ExtentTest et) throws Exception{
		
		try{
			
			//ICD registry  
			/*ICDRegPageObj.ICDRegPageMethod(driver, "ICDRegistry").click();
			Thread.sleep(1000);
			//ICD registry data
			ICDRegPageObj.ICDRegPageMethod(driver, "Data").click();
			Thread.sleep(1000);
			//Upload Data (v2.4)
			ICDRegPageObj.ICDRegPageMethod(driver, "UploadData").click();
			Thread.sleep(1000);*/
			/*
			if (Constant.isElementPresent(driver, By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"))==true)
			{ICDRegPageObj.ICDRegPageMethod(driver, "RemoveFile").click();}
			*/
			/*ICDRegPageObj.ICDRegPageMethod(driver,
					"Submission History").click();
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data",et);
			driver.navigate().back();*/
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);
			if (Constant.isElementPresent(driver, By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"))==true)
			{ICDRegPageObj.ICDRegPageMethod(driver, "RemoveFile").click();}
			
			
			String MainWindow=driver.getWindowHandle();
			System.out.println("Main Window handle is: "+MainWindow);
			
			//Browse to attach file.
			/*ICDRegPageObj.ICDRegPageMethod(driver, "Browse").click();*/
			try{
			//Do not put any Thread.sleep	
		/*	ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
			String ICDFile = ExcelUtils.getCellData(8, 1);		
			StringSelection stringSelection = new StringSelection(Constant.ICDDQRFile+ICDFile);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			//native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.delay(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			ICDRegPageObj.ICDRegPageMethod(driver, "AttachFile").click();*/
				ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
				String ASCFile = ExcelUtils.getCellData(8, 1);
				driver.findElement(By.id("MainContent_C003_fileUploadDQRData")).sendKeys(Constant.ActionDQRFile+ASCFile);
				ICDRegPageObj.ICDRegPageMethod(driver, "AttachFile").click();
				//Thread.sleep(1000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				logger.log("pass", "File attachment success", false, driver, "",et);
			} catch (AWTException e) {
				e.printStackTrace();
				logger.log("fail", "File attaching failed", true, driver,
						"Q:\\QA Stuff\\Syam pithani\\testData\\",et);	
			}
			
					
			//if (ICDRegPageObj.ICDRegPageMethod(driver, "ComplianceCheck").getAttribute("checked")==null)
			   //{ICDRegPageObj.ICDRegPageMethod(driver, "ComplianceCheck").click();}  
		 
			//Thread.sleep(1000);
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);
			ICDRegPageObj.ICDRegPageMethod(driver, "DQRSubmit").click();
			//Thread.sleep(3000);
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 30l);
					
			//ICDRegPageObj.ICDRegPageMethod(driver, "Download_0")
			///while(ICDRegPageObj.ICDRegPageMethod(driver, "Download_0").size()==0)
						
			//Keep on refreshing till I see 1st download link.
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			while(driver.findElements(By.id("MainContent_C003_lvSubmissions_img1_0")).size() != 0 )	
			{//Thread.sleep(5000);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 5l);
			driver.navigate().refresh();}
			
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data",et);
			 //driver.navigate().refresh();
			 //ActionRegPageObj.ActionRegPageMethod(driver, "Download_0").getSize().equals(0);
			
			//System.out.println(ICDRegPageObj.ICDRegPageMethod(driver, "SubmissionHistory").getText());
			//ICDRegPageObj.ICDRegPageMethod(driver, "SubmissionHistory").click();
			
			
			if (driver.findElements(By.id("MainContent_C003_lvHistoryQueue_lnkbtnDownload_0")).size() == 0 )
			{ICDRegPageObj.ICDRegPageMethod(driver, "SubmissionHistory").click();}
			 
			//Thread.sleep(2000);
			 if (driver.findElements(By.id("MainContent_C003_lvHistoryQueue_lnkbtnDownload_0")).size()!=0)
				//if (ActionRegPageObj.ActionRegPageMethod(driver, "Download_0").isDisplayed())
				{
				 ICDRegPageObj.ICDRegPageMethod(driver, "1stSubmission").getText();
			       /* String SubMissionDate =ICDRegPageObj.ICDRegPageMethod(driver, "1stSubmission").getText();
		            System.out.println(SubMissionDate);
		            
		            SimpleDateFormat format  = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
		            Date dt =new Date();
		        	Date d1=format.parse(SubMissionDate);
					System.out.println(d1);
					System.out.println(dt);
					if(d1.compareTo(dt)<=0) 
					{System.out.println("Pass message");
				}else {System.out.println("Failed message");}*/
				 			 
			/*	Frame.title="Upload Data Submission...";
				Frame.lebel="Upload Data Submission successfull!!!";
				Frame.createAndShowGUIBlue();*/
				//Thread.sleep(2000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				Log.info("Upload Data Submission successfull!!!");
				/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
				ExcelUtils.setCellDataPass("ICD DQR passed", x1, 1);*/
				}
				else
				{/*Frame.title="Upload Data Submission...";
				Frame.lebel="Upload Data Submission failed!!!";
				Frame.createAndShowGUIRed();*/
				/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
				ExcelUtils.setCellDataFail("ICD DQR failed", x1, 1);*/}
			
			// Thread.sleep(2000);
			// driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			 Utils.page_wait_DQR(driver, 1l);
			 AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();	
			 Log.info("ICD DQR executed successfully!!!");
			
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in ICD DQR Class", exp);
	    	}
	
    
}//end Method

}

