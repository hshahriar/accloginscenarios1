package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.PatientPage;
import utility.ExcelUtils;

public class DiabetesPatientVerification extends PatientParent {
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private  PatientPage PatientPageObj = new PatientPage();
	static int x1=6;
	static int x2=7;
	static int x3=8;
	static int x4=9;
	static int x5=10;
	static int x6=11;
	public void Execute(WebDriver driver) throws Exception {
		try {

			String patient_id = ExcelUtils.getCellData(1, 4);
			 PatientPageObj.patientPageMethod(driver, "Pid").clear();
			 PatientPageObj.patientPageMethod(driver, "Pid").sendKeys(patient_id);
			 PatientPageObj.patientPageMethod(driver, "btnSearch3").click();
			 Thread.sleep(1000);
			 PatientPageObj.patientPageMethod(driver, "EditPatient").click();
			 Thread.sleep(1000);
			super.LastNameCheck(driver, PatientPageObj, 1,1,x1,1);
			super.FirstNameCheck(driver, PatientPageObj, 1,2,x1,2);
			super.MiddleNameCheck(driver, PatientPageObj, 1, 3, x1, 3);
			super.PatientIDcheck(driver, PatientPageObj, 1, 4, x1, 4);
			super.DOBheck(driver, PatientPageObj,1, 5, x1, 5);
			super.SSNcheck(driver, PatientPageObj, 1, 6,x1,6);
			super.OtherIDcheck(driver, PatientPageObj, 1,7,x2,1);
			super.SexCheck2060(driver, PatientPageObj, 1,8,x2,2);
			super.HispOrigCheck2076(driver, PatientPageObj, 1,9,x2,3);
			super.RaceWhiteCheck2070(driver, PatientPageObj, 0,0, x2,4);
			super.RaceBlackCheck2071(driver, PatientPageObj, 0,0, x2,5);
			super.RaceAmIndianCheck2073(driver, PatientPageObj, 0,0, x2,6);
			super.RaceAsianCheck2072(driver, PatientPageObj, 0,0, x3,1);
			super.RaceAsianIndian2080(driver, PatientPageObj, 0,0, x3,2);
			super.RaceChinese2081(driver, PatientPageObj, 0,0, x3,3);
			super.RaceFilipino2082(driver, PatientPageObj, 0,0, x3,4);
			super.RaceJapanese2083(driver, PatientPageObj, 0,0, x3,5);
			super.RaceKorean2084(driver, PatientPageObj, 0,0, x3,6);
			super.RaceVietnamese2085(driver, PatientPageObj, 0,0, x4,1);
			super.RaceAsianOther2086(driver, PatientPageObj, 0,0, x4,2);
			super.RaceNatHaw2074(driver, PatientPageObj, 0,0, x4,3);
			super.RaceNativeHawaii2090(driver, PatientPageObj, 0,0, x4,4);
			super.RaceGuamChamorro2091(driver, PatientPageObj, 0,0, x4,5);
			super.RaceSamoan2092(driver, PatientPageObj, 0,0, x4,6);
			super.RacePacificIslandOther2093(driver, PatientPageObj, 0,0, x5,1);
			super.HispEthnicityMexican2100(driver, PatientPageObj, 0,0, x5,2);
			super.HispEthnicityPuertoRico2101(driver, PatientPageObj, 0,0, x5,3);
			super.HispEthnicityCuban2102(driver, PatientPageObj,0,0, x5,4);
			super.HispEthnicityOtherOrigin2103(driver, PatientPageObj,0,0, x5,5);
			super.Aux1(driver, PatientPageObj, 1, 10, x5, 6);
			super.Aux2(driver, PatientPageObj, 1, 11, x6, 1);
			
			
			
		Log.info("Action Patient verification executed successfully!!!");
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerParent.warn("Following exception was raised in Action Patient verification Class", e);
		}
	}
	
	
}

