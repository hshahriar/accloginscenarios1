package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.AdministrationPage;
import pageObjects.PVIRegPage;
import utility.Constant;
import utility.ExcelUtils;
//import Frame.Frame;

public class PVIAdmin {
	static int x1= 56;
	static int x2= 57;
	static int g1= 17;		
	private static Logger Log = Logger.getLogger(PVIReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static PVIRegPage PVIRegPageObj = new PVIRegPage();
	public static void Execute(WebDriver driver) throws Exception{
		
		try{
			//PVI registry  
			PVIRegPageObj.PVIRegPageMethod(driver, "PVIRegistry").click();
			Thread.sleep(2000);
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
						
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(1000);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			
			//SiteName
			AdministrationPageObj.adminPageMethod(driver, "PVISiteName").clear();
			String SiteName = ExcelUtils.getCellData(g1, 3);
			AdministrationPageObj.adminPageMethod(driver, "PVISiteName").sendKeys(SiteName);
			Thread.sleep(1000);
			
			//Brand Name
			AdministrationPageObj.adminPageMethod(driver, "PVIBrandName").clear();
			String BrandName = ExcelUtils.getCellData(g1, 4);
			AdministrationPageObj.adminPageMethod(driver, "PVIBrandName").sendKeys(BrandName);
			Thread.sleep(1000);
			
			//National Provider Identification (NPI) Number
			AdministrationPageObj.adminPageMethod(driver, "PVINPI").clear();
			String NPI = ExcelUtils.getCellData(g1, 5);
			AdministrationPageObj.adminPageMethod(driver, "PVINPI").sendKeys(NPI);
			Thread.sleep(1000);
			
			// Medicare Provider Number (MPN)
			AdministrationPageObj.adminPageMethod(driver, "PVIMPN").clear();
			String MPN = ExcelUtils.getCellData(g1, 6);
			AdministrationPageObj.adminPageMethod(driver, "PVIMPN").sendKeys(MPN);
			Thread.sleep(1000);
			
			//American Hospital Association (AHA) Number
			AdministrationPageObj.adminPageMethod(driver, "PVIAHA").clear();
			String AHA = ExcelUtils.getCellData(g1, 7);
			AdministrationPageObj.adminPageMethod(driver, "PVIAHA").sendKeys(AHA);
			Thread.sleep(1000);
			
			//Submit 
			AdministrationPageObj.adminPageMethod(driver, "PVISubmit").click();
			Thread.sleep(3000);
			
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
			Thread.sleep(2000);
			// Admin page verification
			String AdminTitle = AdministrationPageObj.adminPageMethod(driver, "PVIAdminText").getText();
			if (AdminTitle.equals("Administration"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Administration page is visable", x1, 1);}
			else
			{ExcelUtils.setCellDataFail("Administration page is not visable", x1, 1);}
			
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(2000);
			
			
			//******************Verification******************//
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");	
			System.out.println(ExcelUtils.getCellData(g1, 1));
			//Participant Id verification
			String ParticipantId = AdministrationPageObj.adminPageMethod(driver, "PVI999999").getText();
			System.out.println(ParticipantId);
			if (ParticipantId.equals(ExcelUtils.getCellData(g1, 1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI Participant Id is visable", x1, 2);}
			else
			{ExcelUtils.setCellDataFail("PVI Participant Id is not visable", x1, 2);}
			
			
			//Encryption Key verification
			String EncryptionKey = AdministrationPageObj.adminPageMethod(driver, "PVIEncryptionKey").getText();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			String str =ExcelUtils.getCellData(g1, 2);
			if (EncryptionKey.equals(str.trim()))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI Encryption Key is visable", x1, 3);}
			else
			{ExcelUtils.setCellDataFail("PVI Encryption Key is not visable", x1, 3);}

			//Site Name
			String SiteNameV = AdministrationPageObj.adminPageMethod(driver, "PVISiteName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (SiteNameV.equals(ExcelUtils.getCellData(g1, 3)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI Site Name is visable", x1, 4);}
			else
			{ExcelUtils.setCellDataFail("PVI Site Name is not visable", x1, 4);}
			
			
			//Brand Name
			String BrandNameV = AdministrationPageObj.adminPageMethod(driver, "PVIBrandName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (BrandNameV.equals(ExcelUtils.getCellData(g1, 4)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI Brand Name is visable", x1, 5);}
			else
			{ExcelUtils.setCellDataFail("PVI Brand Name is not visable", x1, 5);}
			
			//National Provider Identification (NPI) Number 
			String NPIV = AdministrationPageObj.adminPageMethod(driver, "PVINPI").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (NPIV.equals(ExcelUtils.getCellData(g1, 5)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI NPI is visable", x1, 6);}
			else
			{ExcelUtils.setCellDataFail("PVI NPI is not visable", x1, 6);}
			
			//Medicare Provider Number (MPN) 
			String MPNV = AdministrationPageObj.adminPageMethod(driver, "PVIMPN").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (MPNV.equals(ExcelUtils.getCellData(g1, 6)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI MPN is visable", x2, 1);}
			else
			{ExcelUtils.setCellDataFail("PVI MPN is not visable", x2, 1);}
			
			//American Hospital Association (AHA) Number 
			String AHAV = AdministrationPageObj.adminPageMethod(driver, "PVIAHA").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (AHAV.equals(ExcelUtils.getCellData(g1, 7)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("PVI AHA is visable", x2, 2);}
			else
			{ExcelUtils.setCellDataFail("PVI AHA is not visable", x2, 2);}
			
		   /* Frame.title=" PVI administration in...";
		    Frame.lebel="PVI administration successfull!!!";
		    Frame.createAndShowGUIBlue();*/
		    Thread.sleep(2000); 
		    
		    
		    AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
	
		    Log.info("PVI administration successfull!!!");
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in PVI administration Class", exp);
	    	}
	
    
}//end Method
		
	

}

