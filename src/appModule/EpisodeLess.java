package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.EpisodePage;
import pageObjects.PatientPage;
import utility.Constant;
import utility.ExcelUtils;

public class EpisodeLess {
	static int x1=1;
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	
	private static EpisodePage episodePageObj = new EpisodePage();
	public static void Execute(WebDriver driver, com.gallop.Logger logger,ExtentTest et) throws Exception{
		
		try{
			
			episodePageObj.episodePageMethod(driver, "Episode -- Search & Edit").click();
			Thread.sleep(1000);
			//setting patient tab in the spread sheet in excel sheet.
			 ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			
			    //This is to get the values from Excel sheet, passing parameters (Row num &amp; Col num)to getCellData method
			 
			 
			 ///Passing the excel data to UI
			   //2040
			 //passing patient id to UI
			 String patient_id = ExcelUtils.getCellData(x1, 4);
			
		    //Saving 
		    Thread.sleep(1000);
		    episodePageObj.episodePageMethod(driver, "search").click();
		    Thread.sleep(3000);
		    
		    episodePageObj.episodePageMethod(driver, "edit").click();
		    Thread.sleep(3000);
		    
		    String episodeId="";
		    
			   try {
				String url= driver.getCurrentUrl();
				   String[] buffer=url.split("/");
					
					System.out.println(buffer[buffer.length-1]);
					episodeId=buffer[buffer.length-1];
				} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
		    try {
				episodePageObj.episodePageMethod(driver, "btnSaveForCath").click();
			} catch (Exception e) {
				// TODO Auto-generated catch block
			//	e.printStackTrace();
				try {
					episodePageObj.episodePageMethod(driver, "save").click();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					episodePageObj.episodePageMethod(driver, "saveByValue").click();
				}
			}
		    Thread.sleep(3000);
		    logger.log("pass", "Episode:"+episodeId+" creation/modification success", false, driver, "",et);
		    Log.info("Episode class executed successfull!!!");
		 }catch		
		    (Exception exp){	 
			 exp.printStackTrace();
			 logger.log("fail", "Episode creation failed", true, driver, "Q:\\QA Stuff\\Syam pithani\\testData\\",et);
			LoggerParent.warn("Following exception was raised in Episode Class", exp);
		    	}
		
	    
	}//end Method
	
	
		}//Class

	
	
	
	

