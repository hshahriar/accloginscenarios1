package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.PatientPage;
import utility.Constant;
import utility.ExcelUtils;

public class PatientParent {

	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");

	//private static PatientPage PatientPageObj = new PatientPage();

	public  void LastNameCheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {

		//***********Last Name check************//
		//Verify saved LastName matched with spread sheet LastName value.
		//Setting patient tab in the spread sheet in excel sheet.
		ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
		String last_Name_Value = PatientPageObj.patientPageMethod(driver, "LastName").getAttribute("value");
		if (last_Name_Value.equals(ExcelUtils.getCellData(x1, y1))) {
			//This is to send the PASS value to the Excel sheet in the result column.
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Last Name Matched", x2, y2);
		} else {
			//This is to send the PASS value to the Excel sheet in the result column.
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Last Name didn't Match", x2, y2);
		}

	}

	public  void FirstNameCheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {			   
			//***********First Name check************//
			//Setting patient tab in the spread sheet in excel sheet.
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			//Verify saved FistName matched with spread sheet FirstName value.
			String First_Name_Value = PatientPageObj.patientPageMethod(driver, "FirstName").getAttribute("value");
			if (First_Name_Value.equals(ExcelUtils.getCellData(x1, y1)))
				//This is to send the PASS value to the Excel sheet in the result column.
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Fist Name Matched", x2, y2);}
			else
				//This is to send the Failed value to the Excel sheet in the result column.
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("First Name didn't Match", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//***********Middle Name check************//
	public  void MiddleNameCheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String Middle_Name_Value = PatientPageObj.patientPageMethod(driver, "MidName").getAttribute("value");
			if (Middle_Name_Value.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Middle Name Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Middle Name didn't Match", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//***********Patient ID check************//
	public  void PatientIDcheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String Patient_id_Value = PatientPageObj.patientPageMethod(driver, "Pid").getAttribute("value");
			if (Patient_id_Value.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Patient ID Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Patient ID didn't Match", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//***********Date Of Birth check************//
	public  void DOBheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String DOB_Value = PatientPageObj.patientPageMethod(driver, "DOB").getAttribute("value");
			if (DOB_Value.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Patient ID Matched",x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Patient ID didn't Match",x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//*********** SSN check************//
	public  void SSNcheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String SSN_Value = PatientPageObj.patientPageMethod(driver, "SSN").getAttribute("value");
			if (SSN_Value.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("SSN Matched",x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("SSN didn't Match",x2, y2);}
		} catch (Exception e) {	}

	}//Method
	//*********** Other ID check************//
	public  void OtherIDcheck(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String  Other_ID_Value = PatientPageObj.patientPageMethod(driver, "OtherID").getAttribute("value");
			if (Other_ID_Value.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Other ID Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Other ID didn't Match", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//*********** Element Sex 2060 check************//
	public  void SexCheck2060(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String  SexValue = PatientPageObj.PatientSelectMethod(driver, "SelectSex").getFirstSelectedOption().getText();
			if (SexValue.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("SexCheck2060 Matched",x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("SexCheck2060 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//*********** Hispanic or Latino Ethnicity 2076 check************//
	public  void HispOrigCheck2076(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			String  HispOrigValue = PatientPageObj.PatientSelectMethod(driver, "HispOrig").getFirstSelectedOption().getText();
			if (HispOrigValue.equals(ExcelUtils.getCellData(x1, y1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("HispOrigCheck2076 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("HispOrigCheck2076 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//*********** //White 2070 check************//
	public  void RaceWhiteCheck2070(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceWhiteValue=PatientPageObj.patientPageMethod(driver, "RaceWhite").getAttribute("checked");
			if (RaceWhiteValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("RaceWhiteCheck2070 Matched",x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("RaceWhiteCheck2070 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Black/African American 2071
	public  void RaceBlackCheck2071(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceBlackValue=PatientPageObj.patientPageMethod(driver, "RaceBlack").getAttribute("checked");
			if (RaceBlackValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Black/African American 2071 Sex Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Black/African American 2071 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//American Indian/Alaskan Native 2073
	public  void RaceAmIndianCheck2073(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceAmIndianValue=PatientPageObj.patientPageMethod(driver, "RaceAmIndian").getAttribute("checked");
			if (RaceAmIndianValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("American Indian/Alaskan Native 2073 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("American Indian/Alaskan Native 2073 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Asian 2072
	public  void RaceAsianCheck2072(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceAsianValue=PatientPageObj.patientPageMethod(driver, "RaceAsian").getAttribute("checked");
			if (RaceAsianValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Asian 2072 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Asian 2072 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	////Asian Indian 2080
	public  void RaceAsianIndian2080(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceAsianIndianValue=PatientPageObj.patientPageMethod(driver, "RaceAsianIndian").getAttribute("checked");
			if (RaceAsianIndianValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Asian Indian 2080 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Asian Indian 2080 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Chinese 2081
	public  void RaceChinese2081(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceChineseValue=PatientPageObj.patientPageMethod(driver, "RaceChinese").getAttribute("checked");
			if (RaceChineseValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Chinese 2081 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Chinese 2081 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Filipino 2082 
	public  void RaceFilipino2082(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceFilipinoValue=PatientPageObj.patientPageMethod(driver, "RaceFilipino").getAttribute("checked");
			if (RaceFilipinoValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Filipino 2082  Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Filipino 2082  didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Japanese 2083  
	public  void RaceJapanese2083(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceJapaneseValue=PatientPageObj.patientPageMethod(driver, "RaceJapanese").getAttribute("checked");
			if (RaceJapaneseValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Japanese 2083  Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Japanese2083 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Korean 2084  
	public  void RaceKorean2084(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceKoreanValue=PatientPageObj.patientPageMethod(driver, "RaceKorean").getAttribute("checked");
			if (RaceKoreanValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Korean 2084  Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Korean 2084 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//Vietnamese 2085  
	public  void RaceVietnamese2085(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceVietnameseValue=PatientPageObj.patientPageMethod(driver, "RaceVietnamese").getAttribute("checked");
			if (RaceVietnameseValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Vietnamese 2085  Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Vietnamese 2085 didn't Matched", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	// Other 2086  
	public  void RaceAsianOther2086(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceAsianOtherValue=PatientPageObj.patientPageMethod(driver, "RaceAsianOther").getAttribute("checked");
			if (RaceAsianOtherValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Other 2086  Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail(" Other 2086 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	// Native Hawaiian/Pacific Islander2074   
	public  void RaceNatHaw2074(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceNatHawValue=PatientPageObj.patientPageMethod(driver, "RaceNatHaw").getAttribute("checked");
			if (RaceNatHawValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Native Hawaiian/Pacific Islander 2074  Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Native Hawaiian/Pacific Islander 2074 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//Native Hawaiian 2090   
	public  void RaceNativeHawaii2090(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceNativeHawaiiValue=PatientPageObj.patientPageMethod(driver, "RaceNativeHawaii").getAttribute("checked");
			if (RaceNativeHawaiiValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Native Hawaiian 2090 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Native Hawaiian 2090 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//Guamanian or Chamorro 2091   
	public  void RaceGuamChamorro2091(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceGuamChamorroValue=PatientPageObj.patientPageMethod(driver, "RaceGuamChamorro").getAttribute("checked");
			if (RaceGuamChamorroValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Guamanian or Chamorro 2091 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Guamanian or Chamorro 2091 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//Samoan 2092   
	public  void RaceSamoan2092(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RaceSamoanValue=PatientPageObj.patientPageMethod(driver, "RaceSamoan").getAttribute("checked");
			if (RaceSamoanValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Samoan 2092 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Samoan 2092 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//Other Island 2093 
	public  void RacePacificIslandOther2093(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String RacePacificIslandOtherValue=PatientPageObj.patientPageMethod(driver, "RacePacificIslandOther").getAttribute("checked");
			if (RacePacificIslandOtherValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Other Island 2093 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Other Island 2093 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//HispEthnicityMexican 2100  
	public  void HispEthnicityMexican2100(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String HispEthnicityMexicanValue=PatientPageObj.patientPageMethod(driver, "HispEthnicityMexican").getAttribute("checked");
			if (HispEthnicityMexicanValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("HispEthnicityMexican 2100 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("HispEthnicityMexican 2100 didn't Matched", x2, y2);} 
		} catch (Exception e) {	}

	}//Method

	//Puerto Rican 2101  
	public  void HispEthnicityPuertoRico2101(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String HispEthnicityPuertoRicoValue=PatientPageObj.patientPageMethod(driver, "HispEthnicityPuertoRico").getAttribute("checked");
			if (HispEthnicityPuertoRicoValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass(" Puerto Rican 2101 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail(" Puerto Rican 2101 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//Cuban 2102  
	public  void HispEthnicityCuban2102(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String HispEthnicityCubanValue=PatientPageObj.patientPageMethod(driver, "HispEthnicityCuban").getAttribute("checked");
			if (HispEthnicityCubanValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cuban 2102 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Cuban 2102 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//Other Hispanic, Latino or Spanish Origin 2103   
	public  void HispEthnicityOtherOrigin2103(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			String HispEthnicityOtherOriginValue=PatientPageObj.patientPageMethod(driver, "HispEthnicityOtherOrigin").getAttribute("checked");
			if (HispEthnicityOtherOriginValue.contentEquals("true"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Other Hispanic, Latino or Spanish Origin 2103 Matched", x2, y2);}
			else
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Other Hispanic, Latino or Spanish Origin 2103 didn't Matched", x2, y2);}  
		} catch (Exception e) {	}

	}//Method

	//*********** Patient_AUX1 check************// 
	public  void Aux1(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			//setting patient tab in the spread sheet in excel sheet.
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			//Verify saved  HispOrig_Value matched with spread sheet HispOrig Value.
			String PatientAUX1Value = PatientPageObj.patientPageMethod(driver, "Aux1").getAttribute("value");
			if (PatientAUX1Value.equals(ExcelUtils.getCellData(x1, y1)))
				//This is to send the PASS value to the Excel sheet in the result column.
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Patient_AUX1 Matched", x2, y2);}
			else
				//This is to send the Failed value to the Excel sheet in the result column.
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Patient_AUX1 didn't Match", x2, y2);}
		} catch (Exception e) {	}

	}//Method

	//*********** Patient_AUX2 check************//  
	public  void Aux2(WebDriver driver, PatientPage PatientPageObj, int x1, int y1,
			int x2, int y2) throws Exception {
		try {
			//setting patient tab in the spread sheet in excel sheet.
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"patient");
			//Verify saved  HispOrig_Value matched with spread sheet HispOrig Value.
			String PatientAUX2Value = PatientPageObj.patientPageMethod(driver, "Aux2").getAttribute("value");
			if (PatientAUX2Value.equals(ExcelUtils.getCellData(x1, y1)))
				//This is to send the PASS value to the Excel sheet in the result column.
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Patient_AUX2 Matched", x2, y2);}
			else
				//This is to send the Failed value to the Excel sheet in the result column.
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataFail("Patient_AUX2 didn't Match", x2, y2);} 
		} catch (Exception e) {	}	
		
	}//Method

	
}//Class
