package appModule;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Choice;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Label;
import java.awt.List;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class AppletExample extends Frame implements ActionListener {
	String msg;
	Button b1 = new Button("save");
	Label l11 = new Label("NCDR Automation", Label.CENTER);
	Label l1 = new Label("Url:", Label.LEFT);
	Label l2 = new Label("Browser:", Label.LEFT);
	Label l3 = new Label("ParticipantId:", Label.LEFT);
	Label l4 = new Label("UserName:", Label.LEFT);
	Label l5 = new Label("Password:", Label.LEFT);
	Label l6 = new Label("Scenario:", Label.LEFT);
	Label l7 = new Label("Email Report:", Label.RIGHT);
	
	Choice c = new Choice();
	CheckboxGroup cbg1 = new CheckboxGroup();
	Checkbox ck11 = new Checkbox("Stage", false, cbg1);
	Checkbox ck21 = new Checkbox("Production", false, cbg1);
	
	Choice c12 = new Choice();
	CheckboxGroup cbg2 = new CheckboxGroup();
	Checkbox ck12 = new Checkbox("IE", false, cbg2);
	Checkbox ck22 = new Checkbox("Firefox", false, cbg2);
	Checkbox ck23 = new Checkbox("Chrome", false, cbg2);
		
	TextField t1 = new TextField();
	TextField t2 = new TextField();
	TextField t3= new TextField();
	
	List list1=new List(6,true);
	
	
/*	CheckboxGroup cbg = new CheckboxGroup();
	Checkbox ck1 = new Checkbox("Male", false, cbg);
	Checkbox ck2 = new Checkbox("Female", false, cbg);
	TextArea t2 = new TextArea("", 180, 90, TextArea.SCROLLBARS_VERTICAL_ONLY);*/
	/*Choice course = new Choice();
	Choice sem = new Choice();
	Choice age = new Choice();
*/
	public AppletExample() {
		addWindowListener(new myWindowAdapter());
		setBackground(Color.cyan);
		setForeground(Color.black);
		setLayout(null);
		add(l11);
		add(l1);
		add(l2);
		add(l3);
		add(l4);
		add(l5);
		add(l6);
		add(l7);
		add(t1);
		add(t2);
		/*add(ck1);
		add(ck2);
		add(course);
		add(sem);
		add(age);*/
		add(b1);
		b1.addActionListener(this);
		add(b1);
		/*course.add("BSc c.s");
		course.add("BSc maths");
		course.add("BSc physics");
		course.add("BA English");
		course.add("BCOM");
		sem.add("1");
		sem.add("2");
		sem.add("3");
		sem.add("4");
		sem.add("5");
		sem.add("6");
		age.add("17");
		age.add("18");
		age.add("19");
		age.add("20");
		age.add("21");*/
		l1.setBounds(25, 65, 90, 20);
		l2.setBounds(25, 90, 90, 20);
		l3.setBounds(25, 120, 90, 20);
		l4.setBounds(25, 185, 90, 20);
		l5.setBounds(25, 260, 90, 20);
		l6.setBounds(25, 290, 90, 20);
		l7.setBounds(25, 260, 90, 20);
		l11.setBounds(10, 40, 280, 20);
		t1.setBounds(120, 65, 170, 20);
		t2.setBounds(120, 185, 170, 60);
		/*ck1.setBounds(120, 120, 50, 20);
		ck2.setBounds(170, 120, 60, 20);
		course.setBounds(120, 260, 100, 20);
		sem.setBounds(120, 290, 50, 20);
		age.setBounds(120, 90, 50, 20);*/
		b1.setBounds(120, 350, 50, 30);
	}

	public void paint(Graphics g) {
		g.drawString(msg, 200, 450);
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getActionCommand().equals("save")) {
			msg = "Student details saved!";
			setForeground(Color.red);
		}
	}

	public static void main(String g[]) {
		AppletExample stu = new AppletExample();	
		stu.setSize(new Dimension(500, 500));
		stu.setTitle("student registration");
		stu.setVisible(true);
	}
}

class myWindowAdapter extends WindowAdapter {
	public void windowClosing(WindowEvent we) {
		System.exit(0);
	}
}