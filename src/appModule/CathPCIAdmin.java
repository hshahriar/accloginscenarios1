package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.AdministrationPage;
import pageObjects.CathPCIRegPage;
import utility.Constant;
import utility.ExcelUtils;
//import Frame.Frame;

public class CathPCIAdmin {
	static int x1 = 21;
	static int x2 = 22;
	static int g1 =5;
	private static Logger Log = Logger.getLogger(CathPCIAdmin.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static CathPCIRegPage CathPCIRegPageObj = new CathPCIRegPage();
	public static void Execute(WebDriver driver) throws Exception{
		
		try{
			//Cath registry  
			CathPCIRegPageObj.CathPCIRegPageMethod(driver, "CathPCIRegistry").click();
			Thread.sleep(2000);
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
						
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(1000);
			
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			
			//SiteName
			AdministrationPageObj.adminPageMethod(driver, "CathSiteName").clear();
			String SiteName = ExcelUtils.getCellData(g1, 3);
			AdministrationPageObj.adminPageMethod(driver, "CathSiteName").sendKeys(SiteName);
			Thread.sleep(1000);
			
			//Brand Name
			AdministrationPageObj.adminPageMethod(driver, "CathBrandName").clear();
			String BrandName = ExcelUtils.getCellData(g1, 4);
			AdministrationPageObj.adminPageMethod(driver, "CathBrandName").sendKeys(BrandName);
			Thread.sleep(1000);
			
			//National Provider Identification (NPI) Number
			AdministrationPageObj.adminPageMethod(driver, "CathNPI").clear();
			String NPI = ExcelUtils.getCellData(g1, 5);
			AdministrationPageObj.adminPageMethod(driver, "CathNPI").sendKeys(NPI);
			Thread.sleep(1000);
			
			// Medicare Provider Number (MPN)
			AdministrationPageObj.adminPageMethod(driver, "CathMPN").clear();
			String MPN = ExcelUtils.getCellData(g1, 6);
			AdministrationPageObj.adminPageMethod(driver, "CathMPN").sendKeys(MPN);
			Thread.sleep(1000);
			
			//American Hospital Association (AHA) Number
			AdministrationPageObj.adminPageMethod(driver, "CathAHA").clear();
			String AHA = ExcelUtils.getCellData(g1, 7);
			AdministrationPageObj.adminPageMethod(driver, "CathAHA").sendKeys(AHA);
			Thread.sleep(1000);
			
			//Submit 
			AdministrationPageObj.adminPageMethod(driver, "CathSubmit").click();
			Thread.sleep(3000);
			
			//Clicking on Administration left tab.
			AdministrationPageObj.adminPageMethod(driver, "Administration").click();
			Thread.sleep(2000);
			// Admin page verification
			String AdminTitle = AdministrationPageObj.adminPageMethod(driver, "CathAdminText").getText();
			if (AdminTitle.equals("Administration"))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Administration page is visable", x1, 1);}
			else
			{ExcelUtils.setCellDataFail("Administration page is not visable", x1, 1);}
			
			//Clicking in site profile
			AdministrationPageObj.adminPageMethod(driver, "SiteProfile").click();
			Thread.sleep(2000);
			
			
			//******************Verification******************//
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");	
			//Participant Id verification
			String ParticipantId = AdministrationPageObj.adminPageMethod(driver, "Cath999999").getText();
			System.out.println(ParticipantId);
			if (ParticipantId.equals(ExcelUtils.getCellData(g1, 1)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath Participant Id is visable", x1, 2);}
			else
			{ExcelUtils.setCellDataFail("Cath Participant Id is not visable", x1, 2);}
			
			
			//Encryption Key verification
			String EncryptionKey = AdministrationPageObj.adminPageMethod(driver, "CathEncryptionKey").getText();
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			String str =ExcelUtils.getCellData(g1, 2);
			if (EncryptionKey.equals(str.trim()))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath Encryption Key is visable", x1, 3);}
			else
			{ExcelUtils.setCellDataFail("Cath Encryption Key is not visable", x1, 3);}

			//Site Name
			String SiteNameV = AdministrationPageObj.adminPageMethod(driver, "CathSiteName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (SiteNameV.equals(ExcelUtils.getCellData(g1, 3)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath Site Name is visable", x1, 4);}
			else
			{ExcelUtils.setCellDataFail("Cath Site Name is not visable", x1, 4);}
			
			
			//Brand Name
			String BrandNameV = AdministrationPageObj.adminPageMethod(driver, "CathBrandName").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (BrandNameV.equals(ExcelUtils.getCellData(g1, 4)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath Brand Name is visable", x1, 5);}
			else
			{ExcelUtils.setCellDataFail("Cath Brand Name is not visable", x1, 5);}
			
			//National Provider Identification (NPI) Number 
			String NPIV = AdministrationPageObj.adminPageMethod(driver, "CathNPI").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (NPIV.equals(ExcelUtils.getCellData(g1, 5)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath NPI is visable", x1, 6);}
			else
			{ExcelUtils.setCellDataFail("Cath NPI is not visable", x1, 6);}
			
			//Medicare Provider Number (MPN) 
			String MPNV = AdministrationPageObj.adminPageMethod(driver, "CathMPN").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (MPNV.equals(ExcelUtils.getCellData(g1, 6)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath MPN is visable", x2, 1);}
			else
			{ExcelUtils.setCellDataFail("Cath MPN is not visable", x2, 1);}
			
			//American Hospital Association (AHA) Number 
			String AHAV = AdministrationPageObj.adminPageMethod(driver, "CathAHA").getAttribute("Value");
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"SiteProfile");
			if (AHAV.equals(ExcelUtils.getCellData(g1, 7)))
			{ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
			ExcelUtils.setCellDataPass("Cath AHA is visable", x2, 2);}
			else
			{ExcelUtils.setCellDataFail("Cath AHA is not visable", x2, 2);}
						
			AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
			
			Thread.sleep(2000); 
			
		   /* Frame.title=" Cath PCI administration in...";
		    Frame.lebel="Cath PCI administration successfull!!!";
		    Frame.createAndShowGUIBlue();*/
		    Thread.sleep(2000); 
		    
		    Log.info("Cath PCI administration successfull!!!");
	
			
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in Cath PCI administration Class", exp);
	    	}
	
    
}//end Method
	
	

}//Class
