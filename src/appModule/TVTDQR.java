package appModule;

import java.awt.AWTException;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.AdministrationPage;
import pageObjects.TVTRegPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;
//import Frame.Frame;



public class TVTDQR {
	static int x1=67;
	private static Logger Log = Logger.getLogger(TVTReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static AdministrationPage AdministrationPageObj = new AdministrationPage();
	private static TVTRegPage TVTRegPageObj = new TVTRegPage();
	public static void Execute(WebDriver driver, com.gallop.Logger logger,ExtentTest et) throws Exception{
		
		try{
			
			/*TVTRegPageObj.TVTRegPageMethod(driver,
					"Submission History").click();
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data",et);
			driver.navigate().back();*/
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);
			//TVT registry  
		/*	TVTRegPageObj.TVTRegPageMethod(driver, "TVTRegistry").click();
			Thread.sleep(1000);
			//TVT registry data
			TVTRegPageObj.TVTRegPageMethod(driver, "Data").click();
			Thread.sleep(1000);
			//Upload Data (v2.4)
			TVTRegPageObj.TVTRegPageMethod(driver, "UploadData").click();
			Thread.sleep(1000);
			
			if (Constant.isElementPresent(driver, By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"))==true)
			{TVTRegPageObj.TVTRegPageMethod(driver, "RemoveFile").click();}
			
			
			
			//Browse to attach file.
			TVTRegPageObj.TVTRegPageMethod(driver, "Browse").click();*/
			try{
			//Do not put any Thread.sleep
			/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
			String TVTFile = ExcelUtils.getCellData(11, 1);	
			StringSelection stringSelection = new StringSelection(Constant.TVTDQRFile+TVTFile);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			//native key strokes for CTRL, V and ENTER keys
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.delay(500);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);   
			TVTRegPageObj.TVTRegPageMethod(driver, "AttachFile").click();*/
			
			if (Constant.isElementPresent(driver, By.id("MainContent_C003_uploadedFiles_lnkbtnRemoveFile_0"))==true)
			{TVTRegPageObj.TVTRegPageMethod(driver, "RemoveFile").click();}
			
			
			String MainWindow=driver.getWindowHandle();
			System.out.println("Main Window handle is: "+MainWindow);
			ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
			String ASCFile = ExcelUtils.getCellData(11, 1);
			driver.findElement(By.id("MainContent_C003_fileUploadDQRData")).sendKeys(Constant.ActionDQRFile+ASCFile);
			TVTRegPageObj.TVTRegPageMethod(driver, "AttachFile").click();
			//Thread.sleep(1000);
			//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 1l);
		logger.log("pass", "File attachment success", false, driver, "",et);
			} catch (AWTException e) {
				e.printStackTrace();
				logger.log("fail", "File attaching failed", true, driver,
						"Q:\\QA Stuff\\Syam pithani\\testData\\",et);	
			}
			//Thread.sleep(1000);
			driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			
//			if (TVTRegPageObj.TVTRegPageMethod(driver, "ComplianceCheck").getAttribute("checked")==null)
//			   {TVTRegPageObj.TVTRegPageMethod(driver, "ComplianceCheck").click();}  
		 
			//Thread.sleep(1000);
			TVTRegPageObj.TVTRegPageMethod(driver, "DQRSubmit").click();
			//Thread.sleep(3000);
			//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 3l);
					
			//TVTRegPageObj.TVTRegPageMethod(driver, "Download_0")
			///while(TVTRegPageObj.TVTRegPageMethod(driver, "Download_0").size()==0)
						
			//Keep on refreshing till I see 1st download link.
			//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Utils.page_wait_DQR(driver, 30l);
			while(driver.findElements(By.id("MainContent_C004_lvSubmissions_img1_0")).size() != 0 )	
			{//Thread.sleep(5000);
				//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 5l);
			driver.navigate().refresh();}
			
			Utils.checkIfTextExistsInsideMenu(driver, "Submission Status",
					logger, "Upload Data",et);

			 //driver.navigate().refresh();
			 //ActionRegPageObj.ActionRegPageMethod(driver, "Download_0").getSize().equals(0);
			 
			if (driver.findElements(By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0")).size() == 0 )
			{TVTRegPageObj.TVTRegPageMethod(driver, "SubmissionHistory").click();}
			 
			 if (driver.findElements(By.id("MainContent_C004_lvHistoryQueue_lnkbtnDownload_0")).size()!=0)
				//if (ActionRegPageObj.ActionRegPageMethod(driver, "Download_0").isDisplayed())
				{
				 TVTRegPageObj.TVTRegPageMethod(driver, "1stSubmission").getText();
			        String SubMissionDate =TVTRegPageObj.TVTRegPageMethod(driver, "1stSubmission").getText();
		            /*System.out.println(SubMissionDate);
		            
		            SimpleDateFormat format  = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
		            Date dt =new Date();
		        	Date d1=format.parse(SubMissionDate);
					System.out.println(d1);
					System.out.println(dt);
					if(d1.compareTo(dt)<=0) 
					{System.out.println("Pass message");
				}else {System.out.println("Failed message");}*/
				 			 
			/*	Frame.title="Upload Data Submission...";
				Frame.lebel="Upload Data Submission successfull!!!";
				Frame.createAndShowGUIBlue();*/
				//Thread.sleep(2000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
				Utils.page_wait_DQR(driver, 1l);
				Log.info("Upload Data Submission successfull!!!");
				/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
				ExcelUtils.setCellDataPass("TVT DQR passed", x1, 1);*/
				}
				else
				{/*Frame.title="Upload Data Submission...";
				Frame.lebel="Upload Data Submission failed!!!";
				Frame.createAndShowGUIRed();*/
				/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"TestReport");
				ExcelUtils.setCellDataFail("TVT DQR failed", x1, 1);*/}
			
			// Thread.sleep(2000);
				//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
			 Utils.page_wait_DQR(driver, 1l);
			 AdministrationPageObj.adminPageMethod(driver, "NCDRHomeAdmin").click();
			
			 Log.info("TVTDQR executed successfull!!!");
		}catch		
	    (Exception exp){	 

		LoggerParent.warn("Following exception was raised in TVTDQR Class", exp);
	    	}
	
    
}//end Method

}

