package appModule;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import pageObjects.PatientPage;
import utility.ExcelUtils;

public class ICDPatientVerification extends PatientParent {
	private  PatientPage PatientPageObj = new PatientPage();
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	static int x1=27;
	static int x2=28;
	static int x3=29;
	//static int x4=6;
	public void Execute(WebDriver driver) {
		try {

			String patient_id = ExcelUtils.getCellData(1, 4);
			 PatientPageObj.patientPageMethod(driver, "pid").clear();
			 PatientPageObj.patientPageMethod(driver, "pid").sendKeys(patient_id);
			 PatientPageObj.patientPageMethod(driver, "btnSearch2").click();
			 Thread.sleep(1000);
			 PatientPageObj.patientPageMethod(driver, "EditPatient").click();
			 Thread.sleep(1000);
			super.LastNameCheck(driver, PatientPageObj, 1,1,x1,1);
			super.FirstNameCheck(driver, PatientPageObj, 1,2,x1,2);
			super.MiddleNameCheck(driver, PatientPageObj, 1, 3, x1, 3);
			super.PatientIDcheck(driver, PatientPageObj, 1, 4, x1, 4);
			super.DOBheck(driver, PatientPageObj,1, 5, x1, 5);
			super.SSNcheck(driver, PatientPageObj, 1, 6,x1,6);
			super.OtherIDcheck(driver, PatientPageObj, 1,7,x2,1);
			super.SexCheck2060(driver, PatientPageObj, 1,8,x2,2);
			super.HispOrigCheck2076(driver, PatientPageObj, 1,9,x2,3);
			super.RaceWhiteCheck2070(driver, PatientPageObj, 0,0, x2,4);
			super.RaceBlackCheck2071(driver, PatientPageObj, 0,0, x2,5);
			super.RaceAmIndianCheck2073(driver, PatientPageObj, 0,0, x2,6);
			super.RaceAsianCheck2072(driver, PatientPageObj, 0,0, x3,1);
			/*
			super.RaceAsianIndian2080(driver, PatientPageObj, 0,0, x2,6);
			super.RaceChinese2081(driver, PatientPageObj, 0,0, x2,7);
			super.RaceFilipino2082(driver, PatientPageObj, 0,0, x2,8);
			super.RaceJapanese2083(driver, PatientPageObj, 0,0, x3,1);
			super.RaceKorean2084(driver, PatientPageObj, 0,0, x3,2);
			super.RaceVietnamese2085(driver, PatientPageObj, 0,0, x3,3);
			super.RaceAsianOther2086(driver, PatientPageObj, 0,0, x3,4);
			*/
			super.RaceNatHaw2074(driver, PatientPageObj, 0,0, x3,2);
			/*
			super.RaceNativeHawaii2090(driver, PatientPageObj, 0,0, x3,6);
			super.RaceGuamChamorro2091(driver, PatientPageObj, 0,0, x3,7);
			super.RaceSamoan2092(driver, PatientPageObj, 0,0, x3,8);
			super.RacePacificIslandOther2093(driver, PatientPageObj, 0,0, x4,1);
			super.HispEthnicityMexican2100(driver, PatientPageObj, 0,0, x4,2);
			super.HispEthnicityPuertoRico2101(driver, PatientPageObj, 0,0, x4,3);
			super.HispEthnicityCuban2102(driver, PatientPageObj,0,0, x4,4);
			super.HispEthnicityOtherOrigin2103(driver, PatientPageObj,0,0, x4,5);
			*/
			super.Aux1(driver, PatientPageObj, 1, 10, x3, 3);
			super.Aux2(driver, PatientPageObj, 1, 11, x3, 4);
			
			
			
			Log.info("ICD Patient Verification executed successfully!!!");
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerParent.warn("Following exception was raised in ICDPatientVerification Class", e);
		}
	}
	
	
}


