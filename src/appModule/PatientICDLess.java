package appModule;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.PatientPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Utils;

public class PatientICDLess {
	static int x1 = 1;
	private static Logger Log = Logger.getLogger(ActionReg.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	
	private static PatientPage PatientPageObj = new PatientPage();
	public static void Execute(WebDriver driver, com.gallop.Logger logger,ExtentTest et) throws Exception{
		
		try{
			
			PatientPageObj.patientPageMethod(driver, "Patient -- Add & Search").click();
			/*Thread.sleep(1000);*/
			Utils.page_wait();
			//setting patient tab in the spread sheet in excel sheet.
			XSSFSheet ExcelWSheet=ExcelUtils.setExcelFile2("patient");
			
			    //This is to get the values from Excel sheet, passing parameters (Row num &amp; Col num)to getCellData method
			 
			 
			 ///Passing the excel data to UI
			   //2040
			 //passing patient id to UI
			 String patient_id = ExcelUtils.getCellData(x1, 4,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "pid").click();
			 PatientPageObj.patientPageMethod(driver, "pid").clear();
			 PatientPageObj.patientPageMethod(driver, "pid").sendKeys(patient_id);
			 System.out.println(patient_id);
			 PatientPageObj.patientPageMethod(driver, "btnSearch2").click();
			Utils.page_wait();
			 PatientPageObj.patientPageMethod(driver, "EditPatient").click();
			Utils.page_wait();
			 //2000
			 String last_name = ExcelUtils.getCellData(x1, 1,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "LastName").clear();
			 PatientPageObj.patientPageMethod(driver, "LastName").sendKeys(last_name);
			 
			 //2010
			Utils.page_wait();
			 String first_name = ExcelUtils.getCellData(x1, 2,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "FirstName").clear();
			 PatientPageObj.patientPageMethod(driver, "FirstName").sendKeys(first_name);  
			 
			 //2020
			Utils.page_wait();
			 String Middle_Name = ExcelUtils.getCellData(x1, 3,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "MidName").clear();
			 PatientPageObj.patientPageMethod(driver, "MidName").sendKeys(Middle_Name);  
			
			 //2050
			Utils.page_wait();
			 String DOB = ExcelUtils.getCellData(x1, 5,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "DOB").clear();
			 PatientPageObj.patientPageMethod(driver, "DOB").sendKeys(DOB);  
			 			 
			Utils.page_wait();
			    //Unchecking SSN N/A2031 if it is checked.
			 if  (!(PatientPageObj.patientPageMethod(driver, "SSNNA").getAttribute("checked")==null))
			 {System.out.println(PatientPageObj.patientPageMethod(driver, "SSNNA").getAttribute("checked"));
			 PatientPageObj.patientPageMethod(driver, "SSNNA").click();}  
		
			 //2030
			Utils.page_wait();
			 String SSN = ExcelUtils.getCellData(x1, 6,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "SSN").clear();
			 PatientPageObj.patientPageMethod(driver, "SSN").sendKeys(SSN); 
			 
			//2045
			Utils.page_wait();
			 String OtherID = ExcelUtils.getCellData(x1, 7,ExcelWSheet);
			 PatientPageObj.patientPageMethod(driver, "OtherID").clear();
			 PatientPageObj.patientPageMethod(driver, "OtherID").sendKeys(OtherID); 		 
			 
			 //2060
			Utils.page_wait();
			 //Getting Sex value from spread sheet.
			 try {
				String Patient_Sex = ExcelUtils.getCellData(x1, 8,ExcelWSheet);
				 PatientPageObj.patientPageMethod(driver, "Sex").click();
				 //Passing the value to UI drop down list
				 PatientPageObj.patientPageMethod(driver, "Sex").sendKeys(Patient_Sex);
				 PatientPageObj.patientPageMethod(driver, "Sex").click();
				Utils.page_wait();
				 //2076  
				 String HispOrig = ExcelUtils.getCellData(x1, 9,ExcelWSheet);
				 PatientPageObj.patientPageMethod(driver, "HispOrig").click();
				 //Passing the value to UI drop down list
				 PatientPageObj.patientPageMethod(driver, "HispOrig").sendKeys(HispOrig);
				 PatientPageObj.patientPageMethod(driver, "HispOrig").click();
					 
				   Utils.page_wait();
				 //Getting the attribute for "CHECKED" value.
				 //Action_Patient_Page.RaceWhite(driver).getAttribute("checked");
				 //Checking the button is already checked or not. if it is checked I am not clicking the button
				    
				 //2070
				 if (PatientPageObj.patientPageMethod(driver, "RaceWhite").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.patientPageMethod(driver, "RaceWhite").getAttribute("checked"));
				 PatientPageObj.patientPageMethod(driver, "RaceWhite").click();}  
				Utils.page_wait();
				 //2071
				 if (PatientPageObj.patientPageMethod(driver, "RaceBlack").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.patientPageMethod(driver, "RaceBlack").getAttribute("checked"));
				 PatientPageObj.patientPageMethod(driver, "RaceBlack").click();}  
				Utils.page_wait();
				 //2073
				 if (PatientPageObj.patientPageMethod(driver, "RaceAmIndian").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.patientPageMethod(driver, "RaceAmIndian").getAttribute("checked"));
				 PatientPageObj.patientPageMethod(driver, "RaceAmIndian").click();}  
				Utils.page_wait();
				 //2072
				 if (PatientPageObj.patientPageMethod(driver, "RaceAsian").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.patientPageMethod(driver, "RaceAsian").getAttribute("checked"));
				 PatientPageObj.patientPageMethod(driver, "RaceAsian").click();}
				Utils.page_wait();
				 /*----
				 //2080
				 if (PatientPageObj.PatientPageMethod(driver, "RaceAsianIndian").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceAsianIndian").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceAsianIndian").click();}
				Utils.page_wait();
				 //2081
				 if (PatientPageObj.PatientPageMethod(driver, "RaceChinese").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceChinese").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceChinese").click();}
				 Thread.sleep(1000);
				 //2082
				 if (PatientPageObj.PatientPageMethod(driver, "RaceFilipino").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceFilipino").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceFilipino").click();}
				 Thread.sleep(1000);
				 //2083
				 if (PatientPageObj.PatientPageMethod(driver, "RaceJapanese").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceJapanese").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceJapanese").click();}
				 Thread.sleep(1000);
				 //2084
				 if (PatientPageObj.PatientPageMethod(driver, "RaceKorean").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceKorean").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceKorean").click();}
				 Thread.sleep(1000);
				 //2085
				 if (PatientPageObj.PatientPageMethod(driver, "RaceVietnamese").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceVietnamese").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceVietnamese").click();}
				 Thread.sleep(1000);
				 //2086
				 if (PatientPageObj.PatientPageMethod(driver, "RaceAsianOther").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceAsianOther").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceAsianOther").click();}
				 Thread.sleep(1000);
				 
				 ---*/
				 //2074
				 if (PatientPageObj.patientPageMethod(driver, "RaceNatHaw").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.patientPageMethod(driver, "RaceNatHaw").getAttribute("checked"));
				 PatientPageObj.patientPageMethod(driver, "RaceNatHaw").click();}
				Utils.page_wait();
				 
				 /*---
				 //2090
				 if (PatientPageObj.PatientPageMethod(driver, "RaceNativeHawaii").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceNativeHawaii").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceNativeHawaii").click();}
				 Thread.sleep(1000); 
				 //2091
				 if (PatientPageObj.PatientPageMethod(driver, "RaceGuamChamorro").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceGuamChamorro").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceGuamChamorro").click();}
				 Thread.sleep(1000);
				 //2092
				 if (PatientPageObj.PatientPageMethod(driver, "RaceSamoan").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RaceSamoan").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RaceSamoan").click();}
				 Thread.sleep(1000);
				 //2093
				 if (PatientPageObj.PatientPageMethod(driver, "RacePacificIslandOther").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "RacePacificIslandOther").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "RacePacificIslandOther").click();}
				 Thread.sleep(1000);
				 //2100
				 if (PatientPageObj.PatientPageMethod(driver, "HispEthnicityMexican").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "HispEthnicityMexican").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "HispEthnicityMexican").click();}
				 Thread.sleep(1000);
				 //2101
				 if (PatientPageObj.PatientPageMethod(driver, "HispEthnicityPuertoRico").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "HispEthnicityPuertoRico").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "HispEthnicityPuertoRico").click();}
				 Thread.sleep(1000);
				 //2102
				 if (PatientPageObj.PatientPageMethod(driver, "HispEthnicityCuban").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "HispEthnicityCuban").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "HispEthnicityCuban").click();}
				 Thread.sleep(1000);
				 //2103
				 if (PatientPageObj.PatientPageMethod(driver, "HispEthnicityOtherOrigin").getAttribute("checked")==null)
				 {System.out.println( PatientPageObj.PatientPageMethod(driver, "HispEthnicityOtherOrigin").getAttribute("checked"));
				 PatientPageObj.PatientPageMethod(driver, "HispEthnicityOtherOrigin").click();}
				 ---*/
				 
				 //Entering data to Aux1 from spread sheet
				Thread.sleep(1000);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    String Patient_Aux1 = ExcelUtils.getCellData(x1, 10,ExcelWSheet);
		    PatientPageObj.patientPageMethod(driver, "Aux1").clear();
		    PatientPageObj.patientPageMethod(driver, "Aux1").sendKeys(Patient_Aux1);
		    
            //Entering data to Aux2 from spread sheet
		   Utils.page_wait();
		    String Patient_Aux2 = ExcelUtils.getCellData(x1, 11,ExcelWSheet);
		    PatientPageObj.patientPageMethod(driver, "Aux2").clear();
		    PatientPageObj.patientPageMethod(driver, "Aux2").sendKeys(Patient_Aux2);
			
		    //Saving 
		   Utils.page_wait();
		    PatientPageObj.patientPageMethod(driver, "btnSave").click();
		    Thread.sleep(3000);
			logger.log("pass", "Patient:" + patient_id
					+ " creation/modification success", false, driver, "",et);
		    Log.info("PatientICDLess executed successfull!!!");
		 }catch		
		    (Exception exp){	 
			 logger.log("fail", "Patient creation failed", true, driver,
						"Q:\\QA Stuff\\Syam pithani\\testData\\",et);

			LoggerParent.warn("Following exception was raised in PatientICDLess  Class", exp);
		    	}
		
	    
	}//end Method
	
	
		}//Class

	
	
	
	

