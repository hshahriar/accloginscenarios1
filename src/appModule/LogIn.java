package appModule;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.relevantcodes.extentreports.ExtentTest;

import beans.Registry;
import pageObjects.ActionRegPage;
import pageObjects.LogInPage;
import utility.Constant;
import utility.ExcelUtils;
import utility.Reporter;
import utility.Utils;
//import Frame.Frame;

public class LogIn {
	static int x1 = 1;
	static int x2 = 2;
	private static Logger Log = Logger.getLogger(LogIn.class.getName());
	static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	private static LogInPage loginPageObj = new LogInPage();
	private static ActionRegPage ActionRegPageObj = new ActionRegPage();
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");

	public static Boolean Execute(WebDriver driver, Reporter reporter,com.gallop.Logger logger, Configuration configuration,ExtentTest et)
			 {

		try {
			// DOMConfigurator.configure("log4j.xml"); ///This configuration
			// creating log file in root project.

			/*ExcelUtils.setExcelFile(Constant.Path_TestData
					+ Constant.File_TestData, "Config");*/
			Thread.sleep(1000);
			// System.out.println(loginPageObj.LogInMethod(driver,
			// "iecer").getText());

			try {
				if (driver.findElements(
						By.linkText("Continue to this website (not recommended)."))
						.size() != 0) {

					// it will click if IE has certificate error.
					if (driver instanceof InternetExplorerDriver) {
						driver.navigate()
								.to("javascript:document.getElementById('overridelink').click()");
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// else{
			// This is to get the values from Excel sheet, passing parameters
			
		// (Row num &amp; Col num)to getCellData method

		    		
			String ParticipantId = configuration.getParticipantId();//ExcelUtils.getCellData(1, 1);
			String sUserName = configuration.getUserName();//ExcelUtils.getCellData(1, 2);
			System.out.println(sUserName);
			String PassWord = configuration.getPassword();//ExcelUtils.getCellData(1, 3);
			System.out.println(PassWord);

			// loginPageObj.LogInMethod(driver, "RegistrayLogIn").click();
			loginPageObj.LogInMethod(driver, "ParticipantId").clear();
			loginPageObj.LogInMethod(driver, "ParticipantId").sendKeys(
					ParticipantId);
			loginPageObj.LogInMethod(driver, "UserName").clear();
			loginPageObj.LogInMethod(driver, "UserName").sendKeys(sUserName);
			loginPageObj.LogInMethod(driver, "PassWord").clear();
			loginPageObj.LogInMethod(driver, "PassWord").sendKeys(PassWord);
			Thread.sleep(1000);
			loginPageObj.LogInMethod(driver, "BtnLogIn").click();

			Thread.sleep(2000);

			System.out.println(loginPageObj.LogInMethod(driver,
					"WelcomeNCDRWebsite").isDisplayed());
			// Login
			if (loginPageObj.LogInMethod(driver, "WelcomeNCDRWebsite")
					.isDisplayed() == true) {
				/*ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataPass("Login successfull!!!", x1, 1);
				reporter.report("Login", "Login successfull!!!", "Passed");*/
				logger.log("pass", "Login successfull!!!", false, driver, null,et);
/*
				Frame.title = " Sign in...";
				Frame.lebel = "Login successfull!!!";
				Frame.createAndShowGUIBlue();*/
			}

			else {
				/*ExcelUtils.setExcelFile(Constant.Path_TestData
						+ Constant.File_TestData, "TestReport");
				ExcelUtils.setCellDataFail("Login is failed", x1, 1);
				reporter.report("Login", "Action Registry-GWTG visible",
						"Failed");*/
			/*	Frame.title = "Sign in...";
				Frame.lebel = "Login is failed";
				logger.log("fail", "Login failed!!!", true, driver, reportingPath,et);
				Frame.createAndShowGUIRed();*/
				return false;

			}
             logger.flush();
			// Iterate through registries and check for registry exisits or not
            

			// Action
			/*
			 * if (ActionRegPageObj.ActionRegPageMethod(driver,
			 * "ActionRegistry").isDisplayed() == true) {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataPass("Action Registry-GWTG is visible ",
			 * x1, 2); reporter.report("Login",
			 * "Action Registry-GWTG is visible", "Passed"); } else {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataFail("Action Registry-GWTG is not visible "
			 * , x1, 2); reporter.report("Login",
			 * "Action Registry-GWTG is not visible", "Failed"); } //Cath PCI if
			 * (ActionRegPageObj.ActionRegPageMethod(driver,
			 * "CathPCIRegistry").isDisplayed() == true) {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataPass("Cath PCI Registry is visible ", x1,
			 * 3); reporter.report("Login", "Cath PCI Registry is visible",
			 * "Passed"); } else {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataFail("Cath PCI Registry is not visible ",
			 * x1, 3); reporter.report("Login",
			 * "Cath PCI Registry is not visible", "Failed"); }
			 * 
			 * //ICD if (ActionRegPageObj.ActionRegPageMethod(driver,
			 * "ICDRegistry").isDisplayed() == true) {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataPass("ICD Registry is visible ", x1, 4);
			 * reporter.report("Login", "ICD Registry is visible", "Passed"); }
			 * else { ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataFail("ICD Registry is not visible ", x1,
			 * 4); reporter.report("Login", "ICD Registry is not visible",
			 * "Failed"); }
			 * 
			 * //Impact if (ActionRegPageObj.ActionRegPageMethod(driver,
			 * "ImpactRegistry").isDisplayed() == true) {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataPass("Impact Registry is visible ", x1, 5);
			 * reporter.report("Login", "Impact Registry is visible", "Passed");
			 * } else { ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataFail("Impact Registry is not visible ", x1,
			 * 5); reporter.report("Login", "Impact Registry is not visible",
			 * "Failed"); }
			 * 
			 * //PVI if (ActionRegPageObj.ActionRegPageMethod(driver,
			 * "PVIRegistry").isDisplayed() == true) {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataPass("PVI Registry is visible ", x1, 6);
			 * reporter.report("Login", "PVI Registry is visible", "Passed"); }
			 * else { ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataFail("PVI Registry is not visible ", x1,
			 * 6); reporter.report("Login", "PVI Registry is not visible",
			 * "Failed"); }
			 * 
			 * //TVT if (ActionRegPageObj.ActionRegPageMethod(driver,
			 * "TVTRegistry").isDisplayed() == true) {
			 * ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataPass("TVT Registry is visible ", x2, 1);
			 * reporter.report("Login", "TVT Registry is visible", "Passed"); }
			 * else { ExcelUtils.setExcelFile(Constant.Path_TestData +
			 * Constant.File_TestData,"TestReport");
			 * ExcelUtils.setCellDataFail("TVT Registry is not visible ", x2,
			 * 1); reporter.report("Login", "TVT Registry is not visible",
			 * "Failed"); }
			 */

			// }//else
			Log.info("Login class executed successfully!!!");
			Thread.sleep(1000);
			return true;
			
		} catch

		(Exception exp) {
			exp.printStackTrace();

			/*LoggerParent.warn("Following exception was raised in SignIn Class",
					exp);*/
			try {
				logger.log("fail", "Login failed!!!", true, driver, reportingPath,et);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			logger.flush();
            return false;
		}
	}// end Method
	
	
	public static void reisteriesTest(WebDriver driver, com.gallop.Logger logger){
		ExtentTest et= logger.createTest("Registries Test",
					"Registries test");
			List<Registry> registries = Utils.getRegistries();//ExcelUtils.getRegisteries();

			for (Registry registry : registries) {
				WebElement element = Utils.getWebElement(driver, registry.getRegistryWelcomeLabel(), registry.getRegistryName());/*ActionRegPageObj.ActionRegPageMethod(
						driver, registry.getRegistryName());*/
				if (element != null) {
					if (element.isDisplayed() == true) {
						/*ExcelUtils.setExcelFile(Constant.Path_TestData
								+ Constant.File_TestData, "TestReport");
						ExcelUtils.setCellDataPass(
								registry.getRegistryWelcomeLabel()
										+ " is visible ", x1, 2);
						reporter.report("Login",
								registry.getRegistryWelcomeLabel()
										+ " is visible", "Passed");*/
						logger.log("pass", "Registry Name \"" + registry.getRegistryWelcomeLabel() + "\"  exists!!!", false, driver, null,et);
					} else {
						/*ExcelUtils.setExcelFile(Constant.Path_TestData
								+ Constant.File_TestData, "TestReport");
						ExcelUtils.setCellDataFail(
								registry.getRegistryWelcomeLabel()
										+ " is not visible ", x1, 2);
						reporter.report("Login",
								registry.getRegistryWelcomeLabel()
										+ " is not visible", "Failed");*/
						logger.log("fail", "Registry Name \"" + registry.getRegistryWelcomeLabel() + "\" is not visible", true, driver, null,et);
					}
				}
			}
			logger.flush();
	}

}// Class
