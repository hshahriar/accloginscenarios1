

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.junit.runner.JUnitCore;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.Select;

import utility.Reporter;
import utility.Utils;
import appModule.Configuration;
import beans.TestDataForACC;

import com.relevantcodes.extentreports.ExtentTest;

public class ScenarioForAcc {
	static Properties properties = loadProperties();
	static WebDriver dRDriver;
	String baseUrl = "http://www.acc.org/";
	static String BrowserType = null;
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");
	private static StringBuffer verificationErrors = new StringBuffer();

	public static void main(String[] args) {
		System.out.println("In main method");
		JUnitCore jCore = new JUnitCore();
		//jCore.run(ACC_LOGIN_LOGOUT.class);
	}

	public static WebDriver OpenFFBrowser() {

		dRDriver = new FirefoxDriver();
		// baseUrl = "http://www.acc.org/";
		dRDriver.get("https://www.acc.org");
		// baseUrl = "http://author.acc.org";

		// baseUrl = "https://prodcd1.acc.org";
		// baseUrl = "https://prodcd2.acc.org";
		// baseUrl = "https://prodcd3.acc.org";
		dRDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return dRDriver;
	}

	public static WebDriver OpenIEBrowser() {
		System.setProperty(
				"webdriver.ie.driver",
				"Q:\\PS Script\\selenium\\APRIL2015Workspace\\ACC.ORG_Regression\\src\\Library\\IEDriverServer.exe");
		dRDriver = new InternetExplorerDriver();
		dRDriver.get("https://www.acc.org");
		// baseUrl = "http://www.acc.org/";

		// baseUrl = "https://prodcd1.acc.org";
		// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		return dRDriver;
	}

	public static WebDriver OpenChromeBrowser() {

		System.setProperty(
				"webdriver.chrome.driver",
				"Q:\\PS Script\\selenium\\APRIL2015Workspace\\ACC.ORG_Regression\\src\\Library\\chromedriver.exe");
		dRDriver = new ChromeDriver();
		dRDriver.get("https://www.acc.org");
		
		
		return dRDriver;

	}

	public static void MemberLogin(String reportPath, WebDriver dRDriver,
			String userName, String password, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		try {
			et = logger.createTest("ACCMemberLogin", "Login Test");
			Thread.sleep(4000);

			dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			try
			{
				WebElement LoginPage = dRDriver.findElement(
						By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
				
				if (LoginPage.isDisplayed()) {
					logger.log("pass", "Home page displayed for acc", false,
							dRDriver, reportingPath, et);
				} 
			}catch(Exception e) {
					logger.log("fail", "Home page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
				logger.flush();
			page_Wait1();
			
			

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			try
			{
			dRDriver.findElement(
					By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")).click();
			page_Wait();
			String login = dRDriver.findElement(
					By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

			if (login.contains("Login")) {
				logger.log("pass", "Login page displayed for acc", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			page_Wait1();
			System.out.println("login screen captured");
			
			try
			{
			dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
			// driver.findElement(By.id("UserName")).clear();

			// driver.findElement(By.id("UserName")).sendKeys(vUsername);

			TestDataForACC testdataforacc = new TestDataForACC();

			// String UserNameForMembership= testdataforacc.getUserName();
			
			
			String UserNameForMembership = userName;

			dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
					UserNameForMembership);

			String username = dRDriver.findElement(
					By.xpath("//*[@id='UserName']")).getAttribute("value");

			System.out.println(username);
			page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "UserName" + '"' + username + '"'
						+ "successfully entered", false, dRDriver,
						reportingPath, et);
			}
			}catch(Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			page_Wait1();
			
			try
			{
			dRDriver.findElement(By.id("Password")).click();
			page_Wait();
			// String PasswordForMembership= testdataforacc.getPassword();
			String PasswordForMembership = password;

			page_Wait1();

			// driver.findElement(By.id("Password")).sendKeys(vPassword);

			dRDriver.findElement(By.id("Password")).sendKeys(
					PasswordForMembership);

			String Password = dRDriver.findElement(By.id("Password"))
					.getAttribute("value");
			if (!Password.isEmpty()) {
				logger.log("pass", "Password" + '"' + Password + '"'
						+ " successfully entered", false, dRDriver,
						reportingPath, et);
			} }
			catch(Exception e){
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			// et=logger.createTest("password", "password entered");

		page_Wait1();
			
			try
			{
			dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"))
					.click();

			// et=logger.createTest("Login", "Login successful");

			System.out.println("Successfully logged in to ACC site");

			// page_Wait();

			// CapturingPopup(dRDriver);
			WebElement logoforacc = dRDriver.findElement(By
					.xpath(".//*[@id='nav-myacc']/li[1]/span"));
			// String
			// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
			if (logoforacc.isDisplayed()) {
				logger.log("pass", "Successfully logged into ACC", false,
						dRDriver, reportingPath, et);
			} 
			}
			catch(Exception e){
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			page_Wait();
			page_Wait1();
			
			try
			{

			String logoutLink = dRDriver.findElement(
					By.xpath(".//*[@id='header-personal']/p/a")).getText();

			if (logoutLink.contains("Log Out")) {
				logger.log("pass", "The" + '"' + logoutLink + '"'
						+ " Link is displayed", false, dRDriver, reportingPath,
						et);
			} 
			}catch(Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
		page_Wait1();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void CapturingPopup(WebDriver dRDriver) {
		try {
			dRDriver.findElement(
					By.xpath(".//*[@id='fsrOverlay']/div/div/div/div/div/div[2]/div[1]/a"))
					.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static WebDriver LoginMethoCalling(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et, String userName, String password,
			String testscenarios, String run, Configuration configuration,
			String selectedRegistries) throws Exception {
		CloseBrowser(dRDriver);
		if (run.trim().equalsIgnoreCase("ie")) {
			// Open IE
			dRDriver = OpenIEBrowser(dRDriver, configuration);
		}

		else if (run.trim().equalsIgnoreCase("firefox"))
		// Open Firefox
		{
			dRDriver = OpenFFBrowser(dRDriver, configuration);
		} else if (run.trim().equalsIgnoreCase("Chrome"))
		// Open Chrome
		{
			dRDriver = OpenChrmBrowser(dRDriver, configuration);
		} else if (run.trim().equalsIgnoreCase("Safari"))
		// Open Chrome
		{
			dRDriver = OpenSafariBrowser(dRDriver, configuration);
		}

		if (testscenarios.contains("Membership")
				|| selectedRegistries.contains("MemberLogin"))

		{

			ScenarioForAcc.MemberLogin(reportPath, dRDriver, userName,
					password, logger, reportingPath, et);
		} else if (testscenarios.contains("Nonmembership")) {
			ScenarioForAcc.NonmemberLogin(reportPath, dRDriver, userName,
					password, logger, reportingPath, et);
		} else if (testscenarios.contains("Sitecore")
				|| selectedRegistries.contains("VerifySitecoreLogin")) {
			ScenarioForAcc.SiteCoreLogin(reportPath, dRDriver, userName,
					password, logger, reportingPath, et);
		} else if (testscenarios.contains("EducationSiteCore")
				|| selectedRegistries.contains("VerifySiteCoreEducationLogin")) {
			ScenarioForAcc.EducationLoginInSitecore(reportPath, dRDriver,
					userName, password, logger, reportingPath, et);
		}
		return dRDriver;
	}

	public static void NonmemberLogin(String reportPath, WebDriver dRDriver,
			String userName, String password, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		try {
			et = logger.createTest("NonmemberLogin", "NonmemberLogin Test");
			page_Wait1();

			try
			{
			WebElement HomePage = dRDriver.findElement(
					By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
			if (HomePage.isDisplayed()) {
				logger.log("pass", "Home page displayed for acc", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "homepage displaying is failed" + "\"" + "\" doesn't exists.",
						true, dRDriver, reportingPath, et);
			}
			logger.flush();
			page_Wait1();
			
			try
			{
			dRDriver.findElement(
					By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")).click();

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );

			page_Wait();
			String login = dRDriver.findElement(
					By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

			if (login.contains("Login")) {
				logger.log("pass", "login page displayed for acc", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "login fail" + "\"" + "\" doesn't exists.",
						true, dRDriver, reportingPath, et);
			}
			logger.flush();

			page_Wait1();
			System.out.println("login screen captured");
			
			try
			{
			dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
			// driver.findElement(By.id("UserName")).clear();

			// driver.findElement(By.id("UserName")).sendKeys(vUsername);

			dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
					userName);

			String username = dRDriver.findElement(
					By.xpath("//*[@id='UserName']")).getAttribute("value");

			System.out.println(username);
			page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "username" + '"' + username + '"'
						+ "is entered successfully", false, dRDriver,
						reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			page_Wait1();
			
			try
			{
			dRDriver.findElement(By.id("Password")).click();
			page_Wait();

			dRDriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

			// driver.findElement(By.id("Password")).sendKeys(vPassword);

			dRDriver.findElement(By.id("Password")).sendKeys(password);

			String Password = dRDriver.findElement(By.id("Password"))
					.getAttribute("value");
			if (!Password.isEmpty()) {
				logger.log("pass", "password " + '"' + Password + '"'
						+ " entered successfully", false, dRDriver,
						reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			page_Wait1();
			// et=logger.createTest("password", "password entered");

			try
			{
			dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"))
					.click();

			// et=logger.createTest("Login", "Login successful");

			System.out.println("Successfully logged in ACC site");

			page_Wait();
			// CapturingPopup(dRDriver);

			WebElement logoforacc = dRDriver.findElement(By
					.xpath("//*[@id='header-personal']/p/a"));
			// String
			// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
			if (logoforacc.isDisplayed()) {
				logger.log("pass", "login succesfull for acc", false, dRDriver,
						reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "login fail" + "\"" + "\" doesn't exists.",
						true, dRDriver, reportingPath, et);

			}
			logger.flush();
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).
			 * click(); dRDriver.close();
			 */
			page_Wait();
			
			try
			{
			String logoutLink = dRDriver.findElement(
					By.xpath(".//*[@id='header-personal']/p/a")).getText();
			page_Wait();

			if (logoutLink.contains("Log Out")) {
				logger.log("pass", "The" + '"' + logoutLink + '"'
						+ " Link is displayed", false, dRDriver, reportingPath,
						et);
			} 
			}catch(Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			page_Wait1();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void VerifyTestSearchInACC(Configuration configuration,
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		try {
			et = logger.createTest("VerifyTestSearchInACC",
					"TestSearchInACC Test");
			page_Wait();

			LoggerForHomepageDisplay(logger, dRDriver, et);

			try
			{
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.click();

			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.clear();

			// driver.findElement(By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input")).sendKeys(vSearchCriteria);

			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.sendKeys("afib");
			
			String Searchele = dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input")).getAttribute("value");
			
			
			
			if (Searchele.contains("Search")) {
				logger.log("pass", "text is entered in search box", false,
						dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			logger.log("fail", "text is not entered in search box" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);
		}
		logger.flush();
		page_Wait();
		page_Wait1();
		
		String searchresults=null;
			try {
				dRDriver.findElement(
						By.xpath("//html/body/div/header/div[2]/div/div/div[2]/button"))
						.click();
				page_Wait();
				
				searchresults = dRDriver
						.findElement(
								By.xpath("//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.getText();

				String search = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				if (search.contains("Search")) {
					logger.log("pass", "Search page displayed for acc and "+ '"' + searchresults+ '"' + " ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "search page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			System.out.println("Validating Search Query");

			dRDriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

			System.out.println("Validating Search Results Page");

			dRDriver.manage().timeouts().implicitlyWait(150, TimeUnit.SECONDS);

			System.out.println("Found my search topics");

			
			// driver.findElement(By.xpath("//html/body/div/header/div[3]/div[1]/div/ul/li[1]/div/ul/li[1]/a")).click();
			/*System.out.println("Text captured from Search Results page :"
					+ searchresults);*/

			// String destFilePath = null;

			System.out.println("Found my search topics");

			dRDriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

			/*
			 * dRDriver.findElement(By.xpath("//ul[@id='nav-myacc']/li")).click()
			 * ;
			 * 
			 * System.out.println("Back to MyACC Page");
			 */
			page_Wait();

			/*try {
				WebElement searchresult = dRDriver.findElement(By
						.xpath(".//*[@id='lvl2-masthead']/h1"));
				if (searchresult.isDisplayed()) {
					logger.log("pass", "Search results are displayed", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "Search results are displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();*/

			

			try {
				dRDriver.findElement(
						By.xpath("//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.click();

				page_Wait();
				String ResultDisplayed = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				if (searchresults.equalsIgnoreCase(ResultDisplayed)) {
					logger.log("pass", "Search results are displayed For AFIB",
							false, dRDriver, reportingPath, et);
				}

			} catch (Exception e) {
				logger.log("fail", "Search results are not displayed for AFIB"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			// destFilePath = Utils.createReportWithTimeStamp(run);
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
			// redirectToHomePage(logger, et);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void EditingPopup(WebDriver dRDriver) {
		try {

			/*
			 * dRDriver.switchTo().frame(dRDriver.findElement(By.id(
			 * "//gateway.answerscloud.com/acc-org/production/foresee/frameWorker.html?v=R03709DA381"
			 * )));
			 * 
			 * String header= dRDriver.findElement(By.xpath(
			 * ".//*[@id='ng-app']/div[4]/div/div/form/h1")).getText();
			 * System.out.println(header);
			 */
			dRDriver.findElement(
					By.xpath(".//*[@id='ng-app']/div[4]/div/div/form/ul/li[1]/button"))
					.click();
			page_Wait();
			dRDriver.findElement(
					By.xpath(".//*[@id='ng-app']/div[3]/div/div/section/form/ul[2]/li[1]/button"))
					.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void TestEditProfile(String reportPath, WebDriver dRDriver,
			String userNameForACC, String passwordForACC,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("MyProfileEdit", "edit Profile Test");

			LoggerForHomepageDisplayWithLogin(logger,dRDriver, et);

			
			try {
				
				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
				.click();

		 page_Wait(); 
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();

			

			try {
				
				dRDriver.findElement(
						By.xpath("//*[@id='nav-myacc']/li[1]/div/ul/li[1]/a"))
						.click();

				page_Wait();
				String profile = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-masthead']/h1")).getText();

				if (profile.contains("My Profile")) {
					logger.log("pass", "Navigated to My Profile page and"
							+ profile + "header exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "My profile page is not opened" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			/*
			 * textfrommyrofile =
			 * dRDriver.findElement(By.id("myacc-masthead")).getText();
			 * //driver.findElement(By.xpath(
			 * "//html/body/div/header/div[3]/div[1]/div/ul/li[1]/div/ul/li[1]/a"
			 * )).click();
			 * System.out.println("Text captured from Profile page :" +
			 * textfrommyrofile );
			 */
			System.out.println("Optained My Profile");

			page_Wait();
			/*
			 * JavascriptExecutor jse = (JavascriptExecutor)dRDriver;
			 * jse.executeScript("window.scrollBy(0,4000)", "");
			 */

			try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[1]/h4/a/span"))
						.click();

				System.out.println("Expanding Account Preferences");

				page_Wait();

				String AccPreference = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[1]/h4/a/span"))
						.getText();
				page_Wait();

				if (AccPreference.contains("Account Preferences ")) {
					logger.log("pass",
							"Account Preferences tab opened", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Account Preferences tab  not opened"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			
			// dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			// dRDriver.findElement(By.xpath("//*[@id='main-content']/div/div/accordion/div/div[4]/div[2]/div/div/dl/dd[3]/button")).click();

			// Thread.sleep(2500);

			

			try {
				
				dRDriver.findElement(
						By.xpath("//*[@id='main-content']/div/div/accordion/div/div[4]/div[2]/div/div/dl/dd[2]/button"))
						.click();
				page_Wait();
				String editcontact = dRDriver
						.findElement(
								By.xpath(".//*[@id='ng-app']/div[3]/div/div/section/form/h1"))
						.getText();
				page_Wait();

				if (editcontact.contains("Edit Contact")) {
					logger.log("pass",
							"Edit contact  information popup opened", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Edit contact  information popup not opened"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			System.out.println("Planning to edit my Profile");

			page_Wait();
			try
			{

			dRDriver.findElement(By.id("contact-information-email")).click();

			page_Wait();
			dRDriver.findElement(By.id("contact-information-email")).clear();

			dRDriver.findElement(By.id("contact-information-email")).sendKeys(
					"cs1@yahoo.com");

			page_Wait();
			
			 String emaiID = dRDriver.findElement(By.id("contact-information-email")).getAttribute("value");
			
			if (!emaiID.isEmpty()) {
				logger.log("pass",
						"User Email id is entered successfully "
								, false,
						dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log("fail", "User Email id is not entered  successfully" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}

		logger.flush();

			dRDriver.findElement(
					By.xpath("//*[@id='ng-app']/div[3]/div/div/section/form/ul[2]/li[1]/button"))
					.click();

			page_Wait();

			EditingPopup(dRDriver);
			// dRDriver.findElement(By.xpath("//*[@id='ng-app']/div[4]/div/div/form/ul/li[1]/button")).click();

			page_Wait();

			// String emailId=
			// dRDriver.findElement(By.id("contact-information-email")).getAttribute("value");

			// page_Wait();
			System.out.println("Successfully edited profile");

			try {
				String newemailID = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[2]/div/div/dl/dd[2]"))
						.getText();
				if (newemailID.contains("cs1@yahoo.com")) {
					logger.log("pass",
							"User Email id matched and the changed email id is "
									+ '"' + newemailID + '"' + " ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "email id is not changed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).
			 * click(); dRDriver.close();
			 */
			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait(); dRDriver.findElement(
			 * By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")) .click();
			 */
			page_Wait();

			redirectToHomePage(logger, et, dRDriver);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void TestChangePassword(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyChangePasswordfuntionality",
					"VerifyChangePasswordfuntionality Test");
			page_Wait();

			 LoggerForHomepageDisplayWithLogin(logger,dRDriver, et);

			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
				.click();

		page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();

			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/div/ul/li[1]/a"))
						.click();

				page_Wait();
				String profile = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-masthead']/h1")).getText();

				if (profile.contains("My Profile")) {
					logger.log("pass", "Navigated to My Profile page and"
							+ '"'+ profile + '"'+ "header exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "My profile page is not opened" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			page_Wait();
			
                 try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[1]/h4/a/span"))
						.click();

				System.out.println("Expanding Account Preferences");

				page_Wait();

				String AccPreference = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[1]/h4/a/span"))
						.getText();
				page_Wait();

				if (AccPreference.contains("Account Preferences ")) {
					logger.log("pass",
							"Account Preferences tab opened", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Account Preferences tab  not opened"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			

			String EditPassword;

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[2]/div/div/dl/dd[3]/button"))
						.click();
				EditPassword = dRDriver
						.findElement(
								By.xpath(".//*[@id='ng-app']/div[3]/div/div/section/form/h1"))
						.getText();
				// driver.findElement(By.xpath("//html/body/div/header/div[3]/div[1]/div/ul/li[1]/div/ul/li[1]/a")).click();

				if (EditPassword.contains("Edit Password")) {
					logger.log("pass", "Edit Password page opened", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Edit Password page not opened" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			/*
			 * System.out.println("Text captured from Profile page :" +
			 * EditPassword);
			 */
			System.out.println("on Edit Password Window");

			// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

			page_Wait();
			try
			{

			dRDriver.findElement(By.id("existing-password")).click();

			// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

			// driver.findElement(By.id("existing-password")).sendKeys(vPassword);

			dRDriver.findElement(By.id("existing-password")).sendKeys("a123456789");

			page_Wait();

			dRDriver.findElement(By.id("new-password")).click();

			dRDriver.findElement(By.id("new-password")).sendKeys("a123456789");
			dRDriver.findElement(By.id("re-enter-password")).click();

			dRDriver.findElement(By.id("re-enter-password")).sendKeys(
					"a123456789");

			page_Wait();

                   String newpassword = dRDriver.findElement(By.id("new-password")).getAttribute("value");
                   String reenterpassword = dRDriver.findElement(By.id("new-password")).getAttribute("value");
                   
			
			if (!newpassword.isEmpty() && !reenterpassword.isEmpty()) {
				logger.log("pass",
						"User password id is entered successfully "
								, false,
						dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.log("fail", "User password id is not entered  successfully" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}

		logger.flush();
			
			page_Wait();
			

			try {
				dRDriver.findElement(By.id("password-save")).click();

				page_Wait();

				// driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

				System.out.println("Successfully change the passsword");

				page_Wait();
				String newpassword = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/div/accordion/div/div[4]/div[2]/div/div/dl/dt[3]"))
						.getText();
				if (newpassword.equalsIgnoreCase("Password:")) {
					logger.log("pass", "Password changed successfully", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Password is not changed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			/*
			 * JavascriptExecutor jse = (JavascriptExecutor)dRDriver;
			 * jse.executeScript("window.scrollBy(0,-900)", "");
			 * 
			 * dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).
			 * click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } dRDriver.close();
			 */
			page_Wait();
			redirectToHomePage(logger, et, dRDriver);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void VerifyForgotPasswordfunctionality(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyForgotPasswordfunctionality",
					"VerifyForgotPasswordfunctionality Test");

			// driver.findElement(By.xpath(".//*[@id='formLogin']/p[3]/a")).click();
			
			LoggerForHomepageDisplay(logger, dRDriver, et);
			page_Wait();
			try
			{
			dRDriver.findElement(
					By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
					.click();
			page_Wait();
			
			String login = dRDriver.findElement(
					By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

			if (login.contains("Login")) {
				logger.log("pass", "Login page displayed for acc", false,
						dRDriver, reportingPath, et);
			}
			} catch(Exception e) {
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			page_Wait();

			String ForgotPassword;
			try {
				
				dRDriver.findElement(By.linkText("I forgot my password")).click();
				ForgotPassword = dRDriver.findElement(
						By.xpath("//*[@id='forgot-password']/h1")).getText();

				if (ForgotPassword.contains("Forgot")) {
					logger.log("pass",
							"Forgot password page displayed for acc", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Forgot password page not displayed for acc"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			/*
			 * }
			 * 
			 * System.out.println("Text captured from Profile page :" +
			 * ForgotPassword);
			 */

			System.out.println("On Forgot Password Window");

			String usernameValue="";
			try
			{
				
			
			dRDriver.findElement(By.id("UserName")).click();

			// driver.findElement(By.id("UserName")).sendKeys(vUsername);

			dRDriver.findElement(By.id("UserName")).sendKeys("cs1@yahoo.com");

			usernameValue = dRDriver.findElement(By.id("UserName"))
					.getAttribute("value");

			if (usernameValue.contains("Create an Account")
					) { 
				logger.log("pass", "email id is entered successfully "
						+ '"' + usernameValue + '"'
						+ " is entered successfully", false, dRDriver,
						reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "email id  is not entered " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
			logger.flush();
			
			
			try {
				
				dRDriver.findElement(By.id("btnEnter")).click();
				System.out.println("password changed");

				page_Wait();
				String passwordchanged = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				String verifyPage = dRDriver.findElement(
						By.xpath(".//*[@id='forgot-password']/h1")).getText();
				page_Wait();
				if (passwordchanged.contains("Create an Account")
						&& verifyPage.contains("Verify Your Identity")) {
					logger.log("pass", "Verify page displayed and email id "
							+ '"' + usernameValue + '"'
							+ " is entered successfully", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Verify page is not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			redirectToHomePage(logger, et, dRDriver);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void MyMembership(String reportPath, WebDriver dRDriver,
			String userName, String password, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		try {
			et = logger.createTest("VerifyMyMembershipfunctionality",
					"membership Test");

			LoggerForHomepageDisplayWithLogin(logger,dRDriver, et);

			
			
			try {
				dRDriver.findElement(By.xpath("//*[@id='nav-myacc']/li[1]/span"))
				.click();
		page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
				/*
			 * try {
			 * assertEquals("My Membership - American College of Cardiology",
			 * driver.getTitle()); } catch (Error e) {
			 * verificationErrors.append(e.toString()); }
			 */
			
//			dRDriver.get("http://www.acc.org/my-acc/my-membership");

			System.out.println("Validating My Membership Page");

			try {
				dRDriver.findElement(
						By.xpath("//html/body/div/header/div[3]/div[1]/div/ul/li[1]/div/ul/li[2]/a"))
						.click();
			
				page_Wait();
				String membership = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-masthead']/h1")).getText();

				if (membership.contains("My Membership")) {
					logger.log("pass", "Navigated to My membership page and "
							+ '"' + membership + '"' + " header exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Navigation to My Membership link failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

			page_Wait();
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/div/accordion/div/div/div[1]/h4/a/span"))
						.click();

				dRDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			
				page_Wait();
				String disclosure = dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/div/accordion/div/div/div[2]/div/a")).getText();

				if (disclosure.contains("Annual Disclosures ")) {
					logger.log("pass", "Navigated to Annual Disclosures  page and "
							+ '"' + disclosure + '"' + " link exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Navigation to disclosure link failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

		

			

			/*
			 * try { assertEquals("Home | ACC Disclosure System",
			 * driver.getTitle()); } catch (Error e) {
			 * verificationErrors.append(e.toString()); }
			 */
			/*
			 * dRDriver.get("http://www.acc.org/");
			 * 
			 * 
			 * 
			 * dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img")).
			 * click();
			 */
			try {
				
				dRDriver.findElement(By.xpath(".//*[@id='main-content']/div/div/accordion/div/div/div[2]/div/a"))
				.click();

		System.out.println("Validating Disclosure link");
		page_Wait();
				String ManageYourDisclosures = dRDriver.findElement(
						By.xpath("html/body/div[1]/div/section/div[1]/div/a"))
						.getText();

				String MergeAccounts = dRDriver.findElement(
						By.xpath("html/body/div[1]/div/section/div[2]/div/a"))
						.getText();

				String Logout = dRDriver.findElement(
						By.xpath("html/body/header/div/div[2]/span/a[2]/span"))
						.getText();

				if (ManageYourDisclosures.contains("Manage Your Disclosures")
						&& MergeAccounts.contains("Merge Accounts")
						&& Logout.contains("Logout")) {
					logger.log("pass",
							"Navigated to annual disclosure page and " + '"' + Logout
							+ '"' +" " + ManageYourDisclosures+ '"' + " "
							+ '"' + MergeAccounts + '"' + " exists", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				logger.log("fail", "Navigation to annual diclosure page failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait();
			 */
		    page_Wait();

			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void SsoWithSilverChair(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {
		try {
			/*
			 * driver.get("http://www.acc.org/");
			 * 
			 * 
			 * 
			 * driver.findElement(By.xpath("//html/body/div/header/h1/a/img")).click
			 * ();
			 */

			et = logger.createTest("VerifySSOwithSilverchairNavigation",
					"SSO With SIlver Chair Test");
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * String windowHandle = dRDriver.getWindowHandle();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			page_Wait();
			dRDriver.get("http://content.onlinejacc.org/article.aspx?articleID=2108911");
			page_Wait();
			page_Wait1();
			CapturingPopup(dRDriver);
			// dRDriver.findElement(By.xpath(".//*[@id='fsrOverlay']/div/div/div/div/div/div[2]/div[2]/a")).click();
			System.out.println("Validating JACC Journal link");
			page_Wait1();

			try {
				 WebElement Logo = dRDriver.findElement(
						By.xpath(".//*[@id='logo']/img"));
						

				WebElement Search = dRDriver
						.findElement(
								By.xpath(".//*[@id='nice-menu-1']/li[2]/a"));
						
				if (Search.isDisplayed()) {
					logger.log("pass",
							"Successfully navigated to JACC page from ACC ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", "ACC page is not navigated to JACC journal page" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			// dRDriver.switchTo().window(ParentwindowHandle);

			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait();
			 */
            page_Wait();
			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/*
	 * public static void SsoWithCoeTruman(WebDriver driver) { driver.get(
	 * "http://auth.acc.org/accfederatedlogin/postsp?SP=CTT&src=accmeeting&target=pp816"
	 * ); driver.switchTo().alert().accept();
	 * driver.findElement(By.id("appendedInputButton")).click();
	 * driver.findElement(By.id("appendedInputButton")).sendKeys("Coe Truman");
	 * driver
	 * .findElement(By.xpath(".//*[@id='body']/div/div[2]/div[1]/div/div/button"
	 * )).click();
	 * 
	 * }
	 */
	public static void RP(String reportPath, WebDriver dRDriver,
			String userName, String password, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		try {
			et = logger.createTest("VerifyRP", "RP Test");
			dRDriver.get("http://www.acc.org" + "/");
			WebElement RandPresults;
			RandPresults = dRDriver.findElement(By.id("WebPartWPQ6"));
			// driver.findElement(By.xpath("//html/body/div/header/div[3]/div[1]/div/ul/li[1]/div/ul/li[1]/a")).click();
			System.out.println("Text captured from Search R and P page page :"
					+ RandPresults);

			if (RandPresults.isDisplayed()) {
				logger.log("pass", "silverchair result for acc", false,
						dRDriver, reportingPath, et);
			} else {
				logger.log("fail", "silverchaqir fail" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			dRDriver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);

			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();

			Thread.sleep(3000);

			dRDriver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);

			Thread.sleep(6000);

			dRDriver.get("http://www.rp.acc.org");
			WebElement RandPresults1;
			RandPresults1 = dRDriver.findElement(By.id("WebPartWPQ6"));
			// driver.findElement(By.xpath("//html/body/div/header/div[3]/div[1]/div/ul/li[1]/div/ul/li[1]/a")).click();
			System.out.println("Text captured from Search R and P page page :"
					+ RandPresults);

			if (RandPresults.isDisplayed()) {
				logger.log("pass", "silverchair result for acc", false,
						dRDriver, reportingPath, et);
			} else {
				logger.log("fail", "silverchaqir fail" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			System.out.println("Validating R&P link");

			/*
			 * if (BrowserType == "ff") {
			 * 
			 * dRDriver.switchTo().alert().accept() ;
			 * 
			 * }
			 * 
			 * else
			 * 
			 * 
			 * Thread.sleep(700);
			 */

			Thread.sleep(1000);
			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			page_Wait();
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	/*
	 * public static void SSOWithDDcForMembership(String reportPath, WebDriver
	 * dRDriver, String userName, String password, com.gallop.Logger logger,
	 * String reportingPath, ExtentTest et) throws Exception {
	 * 
	 * et=logger.createTest("VerifySSOWithDDCMembership",
	 * "SSOWithDDcForMembership Test");
	 * 
	 * dRDriver.get("http://auth.acc.org/accfederatedlogin/postsp?SP=DDC");
	 * page_Wait(); WebElement
	 * WelcomeString=dRDriver.findElement(By.linkText("Your ACCPAC"));
	 * 
	 * if (WelcomeString.isDisplayed()) { logger.log("pass",
	 * "DDC page displayed for acc" , false, dRDriver, reportingPath, et); }
	 * else { logger.log("fail", "DDC page display fail" + "\"" +
	 * "\" doesn't exists.", true, dRDriver, reportingPath, et); }
	 * logger.flush(); /*
	 * dRDriver.findElement(By.xpath(".//*[@id='UserInfo']/a")).click();
	 * Thread.sleep(3000);
	 * 
	 * WebElement loginpage=dRDriver.findElement(By.xpath(
	 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if (loginpage.isDisplayed())
	 * { logger.log("pass", "Logged out " , false, dRDriver, reportingPath, et);
	 * } else { logger.log("fail", "Log Out  failed" + "\"" +
	 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
	 * 
	 * } logger.flush(); dRDriver.get(
	 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
	 * page_Wait();
	 * dRDriver.findElement(By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"
	 * )).click();
	 * 
	 * }
	 */
	public static void SSOWithDDcForNonmembership(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {
		try {
			et = logger.createTest("VerifySSOWithDDCMembership",
					"SSOWithDDcForMembership Test");
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * String windowHandle = dRDriver.getWindowHandle();
			 * 
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */

			LoggerForHomepageDisplayWithLogin(logger,dRDriver, et);
			
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");

			page_Wait1();
			dRDriver.get("http://auth.acc.org/accfederatedlogin/postsp?SP=DDC");
			page_Wait();
			try {
				WebElement WelcomeString = dRDriver.findElement(By
						.xpath(".//*[@id='Logomark']/a"));
				String Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='NavWrapper']/ul/li[1]/a")).getText();


				if (WelcomeString.isDisplayed() && Homepage.contains("Your ACCPAC")) {
					logger.log("pass", "successfully navigated to DDC page from acc and " + '"' +Homepage + '"' +" tab exists ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Navigation failed for DDC link from ACC home page" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			page_Wait1();
			// dRDriver.switchTo().window(ParentwindowHandle);
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			page_Wait();
           redirectToHomePage(logger, et, dRDriver);
           
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void ValidatingSearchForACCSAP8(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyAccesstoACCSAP8",
					"VerifyAccesstoACCSAP8 Test");
			dRDriver.get("http://www.acc.org/" + "/");
			dRDriver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);

			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();

			LoggerForHomepageDisplay(logger, dRDriver, et);
			
			try
			{

			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.click();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.clear();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.sendKeys("ACCSAP8");
			
			String SearchText = dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input")).getAttribute("value");
			
			if (!SearchText.isEmpty()) {
				logger.log("pass",
						"Search text is entered successfully",

						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "Search text is entered successfully" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
		logger.flush();
		page_Wait();
			
			try {
				dRDriver.findElement(
						By.xpath("//html/body/div/header/div[2]/div/div/div[2]/button"))
						.click();
				page_Wait();
				String search = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				String Logintab = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();

				if (search.contains("Search")
						&& Logintab.contains("Log in to MyACC")) {
					logger.log("pass",
							"Search page displayed for acc Without Login and "
									+'"'+ Logintab +'"'+ " tab exists",

							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Search page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			page_Wait();
			page_Wait1();
			System.out.println("Validating Product: ACCSAP8 ");
			

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.click();
				page_Wait();
				String ACCSAP8 = dRDriver
						.findElement(
								By.xpath(".//*[@id='lvl2-masthead']/h1"))
						.getText();
				String StartProgram = dRDriver
						.findElement(
								By.xpath(".//*[@id='test']"))
						.getText();
				page_Wait();
				if (ACCSAP8.contains("ACCSAP 8")
						&& StartProgram.contains("Launch")) {
					logger.log("pass",
							"Search result for ACCSAP 8 displayed and"
									+ '"'+StartProgram +'"'+ " button exists", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Search results fo ACCSAP 8 is not displayed" + "\""
								+ "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait1();
			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait(); dRDriver.findElement(
			 * By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")) .click();
			 */
			redirectToHomePage(logger, et, dRDriver);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void ValidatingSearchForECHOSap7(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyAccesstoECHOSAP7",
					"EducationEchoSap8 Test");
			dRDriver.get("http://www.acc.org/" + "/");
			dRDriver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);
			LoggerForHomepageDisplay(logger, dRDriver, et);
			
			try
			{
			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.click();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.clear();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.sendKeys("ECHOSap 7");
			
			String SearchText = dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input")).getAttribute("value");
			
			if (!SearchText.isEmpty()) {
				logger.log("pass",
						"Search text is entered successfully",

						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "Search text is entered successfully" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
		logger.flush();
		page_Wait();
		page_Wait1();
			
			try {
				dRDriver.findElement(
						By.xpath("//html/body/div/header/div[2]/div/div/div[2]/button"))
						.click();
				page_Wait();

				String search = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				page_Wait();
				String Logintab = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				page_Wait();
				if (search.contains("Search")
						&& Logintab.contains("Log in to MyACC")) {
					logger.log("pass",
							"Search page displayed for acc Without Login and "
									+'"'+ Logintab+'"'+ " tab exists",

							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Search page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			page_Wait();
			page_Wait1();
			System.out.println("Validating Product: ECHOSap7 ");
			

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.click();
				page_Wait();
				String EchoSap7 = dRDriver
						.findElement(
								By.xpath(".//*[@id='lvl2-masthead']/h1"))
						.getText();
				/*String PurchaseNow = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
						.getText();*/
				page_Wait();
				if (EchoSap7.contains("ECHOSAP 7")) {
					logger.log("pass",
							"Search result for EchoSap7  displayed and"
									+'"'+ EchoSap7 +'"'+ " button exists", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

				logger.log("fail",
						"Search results fo Echo Sap7 is not displayed" + "\""
								+ "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			page_Wait();
			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait(); dRDriver.findElement(
			 * By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")) .click();
			 */

			redirectToHomePage(logger, et, dRDriver);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void ValidatingSearchForHeartSongs(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyAccesstoHeartSongs",
					"EducationHeartSongs Test");
			dRDriver.get("http://www.acc.org/" + "/");
			dRDriver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);

			LoggerForHomepageDisplay(logger, dRDriver, et);

			try
			{
			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.click();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.clear();
			dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input"))
					.sendKeys("Heart Songs");
			
			String SearchText = dRDriver.findElement(
					By.xpath("//html/body/div/header/div[2]/div/div/div[1]/input")).getAttribute("value");
			
			if (!SearchText.isEmpty()) {
				logger.log("pass",
						"Search text is entered successfully",

						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "Search text is entered successfully" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
		logger.flush();
		page_Wait();
			
			
			

			try {
				dRDriver.findElement(
						By.xpath("//html/body/div/header/div[2]/div/div/div[2]/button"))
						.click();

				System.out.println("Validating Product: HeartSongs ");
				WebElement searchResult = dRDriver
						.findElement(By
								.xpath("//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"));

				if (searchResult.isDisplayed()) {
					logger.log(
							"pass",
							"Search page displayed for acc Without Login for heart songs  ",

							false, dRDriver, reportingPath, et);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.log("fail", "Search page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			
			page_Wait();
			try {
				dRDriver.findElement(
						By.xpath("//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.click();

				String search = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				page_Wait();

				String Logintab = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				page_Wait();
				if (search.contains("Heart")
						&& Logintab.contains("Log in to MyACC")) {
					logger.log("pass",
							"Search page displayed for acc Without Login and "
									+'"'+ Logintab+'"' + " tab exists",

							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

				logger.log("fail", "Search page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			page_Wait();

			/*
			 * String HeartSongs=dRDriver.findElement(By.xpath(
			 * ".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"
			 * )).getText();
			 * 
			 * page_Wait(); if (HeartSongs.contains("Heart Songs")) {
			 * logger.log("pass", "Search result for EchoSap7  displayed and " +
			 * HeartSongs +" header exists" , false, dRDriver, reportingPath,
			 * et); } else { logger.log("fail",
			 * "Search results fo Heart Songs is not displayed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 */

			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait(); dRDriver.findElement(
			 * By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")) .click();
			 */
			redirectToHomePage(logger, et, dRDriver);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void VerifyingExperientLink(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyExperientSSOLogin", "experient Test");

			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();

			dRDriver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);

			/*
			 * WebElement Homepage = dRDriver.findElement(By.xpath(
			 * ".//*[@id='nav-primary-holder']/div[3]/a")); Thread.sleep(2000);
			 * if (Homepage.isDisplayed()) { logger.log("pass",
			 * "Entered SSOLOGIN URL & Navigated to SSO login page" , false,
			 * dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "HomePage not displayed fail" + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 */
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * String windowHandle = dRDriver.getWindowHandle();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */

			// LoggerForHomepageDisplay(logger,dRDriver, et);
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			page_Wait();
			dRDriver.get("https://auth.acc.org/accfederatedlogin/postsp?SP=Experient");

			page_Wait();
			System.out.println("Validating Experiment link");

			try {
				String LogOut = dRDriver
						.findElement(
								By.xpath(".//*[@id='Form1']/div[4]/div/nav/section/ul/li[2]/a"))
						.getText();

				 WebElement Profile = dRDriver.findElement(
						By.xpath(".//*[@id='MainContent']/div[2]/h1/span"));
						

				Thread.sleep(2000);
				if (LogOut.contains("Logout") && Profile.isDisplayed()) {
					logger.log("pass", "The" + '"' + LogOut
							+ '"' + " link exists and " + '"' + Profile + '"'+ "Headers exists and successfully navigated from ACC home page",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "does not navigated to Experient link" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			/*
			 * dRDriver.switchTo().window(ParentwindowHandle); page_Wait();
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * );
			 */
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			page_Wait();

			/*
			 * dRDriver.findElement(
			 * By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")) .click();
			 */
			redirectToHomePage(logger, et, dRDriver);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void ValidatingMediaAsBrightCove(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {
		try {
			/*
			 * dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img")).
			 * click();
			 * 
			 * Thread.sleep(3000);
			 */

			et = logger.createTest("VerifyBrightCoveNavigation",
					"VerifyBrightCoveNavigation Test");
			page_Wait();
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * String windowHandle = dRDriver.getWindowHandle();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */

			LoggerForHomepageDisplay(logger, dRDriver, et);
			
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			dRDriver.get("http://www.acc.org/education-and-meetings/image-and-slide-gallery/media-detail?id=54f80fa104ca3d049b2bbd7b8b076552");

			System.out.println("Validating Bright Cove");

			page_Wait();
			String TestBrightCove;
			try {
				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				TestBrightCove = dRDriver.findElement(
						By.xpath(".//*[@id='main-content']/div/article/h1"))
						.getText();

				if (TestBrightCove.contains("cv_summit_2014")) {
					logger.log("pass",
							"Bright cove page is displayed without login and "
									+'"'+ Logout + '"'+ " tab exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Bright cove page not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();

			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).
			 * click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */
			/*
			 * dRDriver.switchTo().window(ParentwindowHandle); page_Wait();
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * );
			 */
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			page_Wait();

			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void LoginToCM(String reportPath, WebDriver dRDriver,
			String userName, String password, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {
		try {
			/*
			 * dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img")).
			 * click();
			 * 
			 * Thread.sleep(3000);
			 */

			et = logger.createTest("Verifying CM Login", "Verifying CM Login");
			page_Wait();
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * String windowHandle = dRDriver.getWindowHandle();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */

			LoggerForHomepageDisplay(logger, dRDriver, et);
			
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			dRDriver.get("http://www.acc.org/coveo/rest");

			System.out.println("Verifying CM Login");

			page_Wait();

			try {
				String TotalCount = dRDriver.findElement(
						By.xpath("xhtml:html/xhtml:body/xhtml:pre")).getText();
				// String TestBrightCove =
				// dRDriver.findElement(By.xpath(".//*[@id='main-content']/div/article/h1")).getText();

				if (TotalCount.contains("totalCount")) {
					logger.log("pass", "Verifying JSON page is displayed",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Verifying JSON page is not displayed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();

			// System.out.println("Text captured from BrightCove page :" +
			// TestBrightCove );
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).
			 * click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */
			/*
			 * dRDriver.switchTo().window(ParentwindowHandle); page_Wait();
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * );
			 */
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			page_Wait();

			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
			page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void SiteCoreLogin(String reportPath, WebDriver dRDriver,
			String userName, String password, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) throws Exception {

		try {
			et = logger.createTest("VerifySitecoreLogin",
					"VerifySitecoreLogin Test");

			System.out.println("Lets check Sitecore 7.2 now");

			dRDriver.get("http://author.acc.org/sitecore/login");
			page_Wait();
			try {
				WebElement sitecorelogo = dRDriver.findElement(By
						.xpath(".//*[@id='BannerLogo']"));

				if (sitecorelogo.isDisplayed()) {
					logger.log("pass", "Login page displayed for SiteCore",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			page_Wait();

			System.out.println("Validating Author 7.2 Sitecore");

			// driver.findElement(By.id("Login_UserName")).sendKeys(vSitecoreusername);

			String username = properties.getProperty("SiteCoreUserNameV.7.2");
			dRDriver.findElement(By.id("Login_UserName")).sendKeys(username);
			page_Wait();
			try {
				String usernameForSiteCore = dRDriver.findElement(
						By.id("Login_UserName")).getAttribute("value");

				System.out.println(usernameForSiteCore);
				page_Wait();
				if (!usernameForSiteCore.isEmpty()) {
					logger.log("pass", "UserName"+ '"' +usernameForSiteCore + '"' + "successfully entered", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			String SiteCorePassword = properties
					.getProperty("SiteCorePasswordV.7.2");
			// driver.findElement(By.id("Login_Password")).sendKeys(vSitecorepassword);

			dRDriver.findElement(By.id("Login_Password")).sendKeys(
					SiteCorePassword);
			try {
				String passwordForSiteCore = dRDriver.findElement(
						By.id("Login_Password")).getAttribute("value");

				if (!passwordForSiteCore.isEmpty()) {
					logger.log("pass", "Password" + '"' + passwordForSiteCore+ '"' + "successfully entered", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			dRDriver.findElement(By.id("Login_Login")).click();
			page_Wait();

			System.out
					.println("Successfully logged into Author Sitecore and validating Content Editor ");

			try {
				String ContentEditor = dRDriver.findElement(
						By.linkText("Content Editor")).getText();

				if (ContentEditor.contains("Content Editor")) {
					logger.log("pass", "Successfully logged into SiteCore and "
							+ '"' + ContentEditor + '"' + " tab exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			page_Wait();
			try {
				dRDriver.findElement(By.linkText("Content Editor")).click();
				page_Wait();

				WebElement contenteditor = dRDriver.findElement(By
						.xpath(".//*[@id='BContent']/span/span"));

				if (contenteditor.isDisplayed()) {
					logger.log("pass",
							"Login Successfull and Content editor page opened",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Contenteditor page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void SearchForSiteCoreContentarea(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifySearchInContentArea",
					"VerifySearchInContentArea Test");

			/*
			 * try { String ContentEditor = dRDriver.findElement(
			 * By.linkText("Content Editor")).getText();
			 * 
			 * if (ContentEditor.contains("Content Editor")) {
			 * logger.log("pass", "Successfully logged into SiteCore and " +
			 * ContentEditor + " tab exists", false, dRDriver, reportingPath,
			 * et); } } catch (Exception e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); logger.log("fail",
			 * "Login is not successfull" + "\"" + "\" doesn't exists.", true,
			 * dRDriver, reportingPath, et); }
			 * 
			 * 
			 * logger.flush();
			 */
			try
			{
			dRDriver.findElement(
					By.xpath(".//*[@id='BTAddNewSearch']/span/img")).click();

			System.out.println("Selecting Option");
			page_Wait();

			dRDriver.switchTo()
					.frame(dRDriver.findElement(By
							.xpath(".//iFrame[contains(@src,'/sitecore/shell/Applications/Buckets/ShowResult.aspx?')]")));

			dRDriver.findElement(By.id("raw-user-input")).click();

			dRDriver.findElement(By.id("raw-user-input")).sendKeys(
					"Heart Songs");

			String searchTesxt=dRDriver.findElement(By.id("raw-user-input")).getAttribute("value");
//			page_Wait();
			if (!searchTesxt.isEmpty())
					{
				logger.log(
						"pass",
						"SearchText is entered successfully",
						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail",
					"SearchText is not entered "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
					reportingPath, et);

		}

		logger.flush();

		page_Wait();
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='ui_element']/div[1]/div[3]/div[2]"))
						.click();
				page_Wait();
				String HeartSongs = dRDriver
						.findElement(
								By.xpath(".//*[@id='ui_element']/div[1]/div[2]/div/div/div[4]/div"))
						.getText();

				String SubItemstab = dRDriver
						.findElement(
								By.xpath(".//*[@id='ui_element']/div[1]/div[2]/div/div/div[4]/div"))
						.getText();

				if (HeartSongs.equals("Heart Songs")
						&& SubItemstab.contains("Heart")) {
					logger.log(
							"pass",
							"Search for Heartsongs in content area of Sitecore is displayed",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Search for Heartsongs in content area of Sitecore is not  displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			dRDriver.switchTo().defaultContent();
			Thread.sleep(2000);
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='SystemMenu']")).click();
			 * Thread.sleep(1000);
			 * dRDriver.findElement(By.xpath(".//*[text()='Exit']")).click();
			 * dRDriver.close();
			 */
			dRDriver.findElement(By.linkText("Content Editor")).click();
			page_Wait();
			try {
				String ContentEditor = dRDriver.findElement(
						By.linkText("Content Editor")).getText();

				if (ContentEditor.contains("Content Editor")) {
					logger.log("pass", "Successfully logged into SiteCore and "
							+ '"' + ContentEditor + '"' + " tab exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void SearchForSiteCoreLeftSearchBox(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifySearchwithHeartSongs",
					"VerifySearchwithHeartSongs Test");
			// dRDriver.findElement(By.xpath(".//*[@id='BTAddNewSearch']/span/img")).click();

			System.out.println("Selecting Option");
			try
			{

			dRDriver.findElement(By.xpath(".//*[@id='TreeSearch']")).click();

			dRDriver.findElement(By.xpath(".//*[@id='TreeSearch']")).sendKeys(
					"Heart Songs");
			page_Wait();
			String SearchText = dRDriver.findElement(By.xpath(".//*[@id='TreeSearch']")).getAttribute("value");
			
			if (!SearchText.isEmpty())
					 {
				logger.log("pass",
						"search text is entered successfully", false,
						dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			logger.log("fail",
					"Search text is not entered successfully"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
					reportingPath, et);

		}
		logger.flush();
			
			page_Wait();
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='SearchPanel']/table[1]/tbody/tr/td[2]/a/img"))
						.click();
				page_Wait();
				String HeartSongs = dRDriver.findElement(
						By.xpath(".//*[@id='EditorPanel']/div[1]/div/a"))
						.getText();

				String Content = dRDriver.findElement(
						By.xpath(".//*[@id='BContent']/span/span")).getText();

				if (HeartSongs.equals("Heart-Songs")
						&& Content.contains("Content")) {
					logger.log("pass",
							"Search results Heartsongs in Sitecore is displayed and "
									+ '"' + Content + '"' + " tab is highlighted", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail",
						"Search for Heartsongs in Sitecore is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='SystemMenu']")).click();
			 * Thread.sleep(1000);
			 * dRDriver.findElement(By.xpath(".//*[text()='Exit']")).click();
			 * dRDriver.close();
			 */
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void EducationLoginInSitecore(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("Sitecore education Login", "Login Test");

			System.out.println("Lets check Sitecore 6.5 now");
			dRDriver.get("http://author.education.acc.org/sitecore/login");
			page_Wait();
			try {
				WebElement sitecorelogo = dRDriver.findElement(By
						.xpath(".//*[@id='BannerLogo']"));

				if (sitecorelogo.isDisplayed()) {
					logger.log("pass",
							"Login page displayed for EducationSiteCore",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			page_Wait();

			System.out.println("Validating Education Author 6.5 Sitecore");

			// driver.findElement(By.id("Login_UserName")).sendKeys(vSitecoreusername);

			String username = properties.getProperty("SiteCoreUserNameV.6.5");
			dRDriver.findElement(By.id("Login_UserName")).sendKeys(username);

			page_Wait();
			try {
				String usernameForSiteCore = dRDriver.findElement(
						By.xpath(".//*[@id='Login_UserName']")).getAttribute(
						"value");

				System.out.println(usernameForSiteCore);
				page_Wait();
				if (!usernameForSiteCore.isEmpty()) {
					logger.log("pass", "UserName" + '"' +usernameForSiteCore + '"' + "successfully entered", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			String SiteCorePassword = properties
					.getProperty("SiteCorePasswordV.6.5");
			// driver.findElement(By.id("Login_Password")).sendKeys(vSitecorepassword);

			dRDriver.findElement(By.id("Login_Password")).sendKeys(
					SiteCorePassword);

			page_Wait();
			try {
				String passwordForSiteCore = dRDriver.findElement(
						By.id("Login_Password")).getAttribute("value");

				if (!passwordForSiteCore.isEmpty()) {
					logger.log("pass", "Password"+ '"' + passwordForSiteCore+ '"' + " successfully entered", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			dRDriver.findElement(By.id("Login_Login")).click();
			page_Wait();
			try {
				String ContentEditor = dRDriver.findElement(
						By.linkText("Content Editor")).getText();

				if (ContentEditor.contains("Content Editor")) {
					logger.log("pass", "Successfully logged into SiteCore and "
							+ '"' + ContentEditor + '"' + " tab exists", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();

			System.out
					.println("Successfully logged into Author Sitecore and validating Content Editor ");

			page_Wait();

			/*
			 * dRDriver.findElement(By.linkText("Content Editor")).click();
			 * page_Wait();
			 */
			try {
				WebElement contenteditor = dRDriver.findElement(By
						.xpath(".//*[@id='EditorPanel']/div[1]/div/a"));

				if (contenteditor.isDisplayed()) {
					logger.log(
							"pass",
							"Contenteditor for Sitecore education page is displayed",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Contenteditor page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void SearchACCSAP8ThroughSiteCore(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifySearchACCSAP8",
					"VerifySearchACCSAP8 Test");
			// dRDriver.findElement(By.xpath(".//*[@id='BTAddNewSearch']/span/img")).click();

			System.out.println("Selecting Option");

			dRDriver.findElement(By.xpath(".//*[@id='TreeSearch']")).click();

			Thread.sleep(2000);
			dRDriver.findElement(By.xpath(".//*[@id='TreeSearch']")).sendKeys(
					"ACCSAP8");

			try {
				String search = dRDriver.findElement(
						By.xpath(".//*[@id='TreeSearch']")).getAttribute(
						"value");
				page_Wait();

				if (!search.isEmpty()) {
					logger.log("pass", "Search Keyword entered ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Search Keyword not entered" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			dRDriver.findElement(
					By.xpath(".//*[@id='SearchPanel']/td/table[1]/tbody/tr/td[2]/a/img"))
					.click();
			page_Wait();
			try {
				String ACCSAP8 = dRDriver
						.findElement(
								By.xpath(".//*[@id='SearchResult']/table/tbody/tr[1]/td[2]/a[1]"))
						.getText();

				String Content = dRDriver.findElement(
						By.xpath(".//*[@id='BContent']/span/span")).getText();

				if (ACCSAP8.contains("ACCSAP8") && Content.contains("Content")) {
					logger.log("pass",
							"Search results ACCSAP8 in Sitecore is displayed and "
									+ '"' + Content+ '"' +" tab is highlighted", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail",
						"Search for ACCSAP8 in Sitecore is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			dRDriver.findElement(By.xpath(".//*[@id='SystemMenu']")).click();
			page_Wait();
			dRDriver.findElement(By.xpath(".//*[text()='Exit']")).click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void AlertForCoeTruman(WebDriver dRDriver,
			Configuration configuration) throws InterruptedException

	{
		try {
			String run = configuration.getBrowser();
			if (run.equalsIgnoreCase("firefox")) {

				dRDriver.switchTo().alert().accept();
				Thread.sleep(2000);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void ValidatingSearchForCoeTruman(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et,
			Configuration configuration) throws Exception {
		try {
			// dRDriver.get(baseUrl + "/");

			et = logger.createTest("VerifySSOwithCoeTrumanNavigation",
					"CoeTruman Test");

			
			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * String windowHandle = dRDriver.getWindowHandle();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */
			 LoggerForHomepageDisplayWithLogin(logger,dRDriver, et);
			 
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");

			dRDriver.get("https://auth.acc.org/accfederatedlogin/postsp?SP=CTT&src=accmeeting&target=pp814");

			page_Wait();

			page_Wait1();
			AlertForCoeTruman(dRDriver, configuration);
			page_Wait();
			page_Wait1();

			try {
				String Itinerary = dRDriver
						.findElement(
								By.xpath(".//*[@id='header']/div[1]/div/div/div/ul[1]/li[2]/a"))
						.getText();

				if (Itinerary.equals("My Itinerary")) {
					logger.log("pass",
							"Navigated to SSO with Coe Truman and "
									+ '"' + Itinerary + '"' + " Link displayed", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "not navigated to Coe Truman " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			System.out.println("Validating Test SSO with Coe Truman");
			try
			{
			dRDriver.findElement(By.xpath("//*[@id='appendedInputButton']"))
					.click();

			dRDriver.findElement(By.xpath("//*[@id='appendedInputButton']"))
					.sendKeys("Coe Truman");

			String SearchEle = dRDriver.findElement(By.xpath("//*[@id='appendedInputButton']")).getAttribute("value");
			
			if (!SearchEle.isEmpty()) {
				logger.log("pass",
						"Search text for Coe Truman is entered successfully", false,
						dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "Search text for Coe Truman is not entered" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}

		logger.flush();
		page_Wait();
			

			try {
				dRDriver.findElement(
						By.xpath("//*[@id='body']/div/div[2]/div[1]/div/div/button"))
						.click();
				page_Wait();
				page_Wait();
				String CoeTruman = dRDriver.findElement(
						By.xpath(".//*[@id='params']/h6/span")).getText();
				page_Wait();
				if (CoeTruman.equalsIgnoreCase("Coe Truman")) {
					logger.log("pass",
							"Search for Coe Truman displayed for acc", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Search for Coe Truman fail" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();

			/*
			 * dRDriver.findElement(By.xpath(
			 * "//*[@id='header']/div[1]/div/div/div/ul[1]/li[4]/a")).click();
			 * dRDriver.findElement(By.xpath(
			 * ".//*[@id='header']/div[1]/div/div/div/ul[1]/li[4]/ul/li/a"
			 * )).click(); dRDriver.close();
			 */
			// *[@id='header']/div[1]/div/div/div/ul[1]/li[4]/a
			/*
			 * dRDriver.switchTo().window(ParentwindowHandle); dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * );
			 */
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			page_Wait();

			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void VerifyingEbizPage(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			System.out.println("Executing VerifyingEbizPage");
			et = logger.createTest("Verify Ebiz functionality ", "Ebiz Test");

			dRDriver.findElement(By.xpath("//html/body/div/header/h1/a/img"))
					.click();

			page_Wait();

			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			
			

			try {
				dRDriver.findElement(By.xpath("//*[@id='nav-myacc']/li[3]/a"))
				.click();

		System.out.println("Validating Ebiz functionality");

		page_Wait();
				String continueShopping = dRDriver
						.findElement(
								By.xpath("//*[@id='main_0_ctl00_ButtonContinueShopping']"))
						.getAttribute("value");

				System.out.println("Text captured from Ebiz Page :"
						+ continueShopping);

				if (continueShopping.contains("Continue Shopping")) {
					logger.log(
							"pass",
							"Clicked on Shopping cart button and navigated to Shopping cart page successfully "
									+ '"' + continueShopping + '"' + " button exists",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

				logger.log("fail", "Shopping cart page is not displayed" + "\"" + "\" doesn't exists.", true,
						dRDriver, reportingPath, et);

			}
			logger.flush();
			page_Wait();

			System.out.println("validating continue shopping");

			

			try {
				
				dRDriver.findElement(
						By.xpath("//*[@id='main_0_ctl00_ButtonContinueShopping']"))
						.click();
				page_Wait();
				String MyACCLink = dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/span")).getText();
				String LogoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();

				System.out
						.println("Text captured from Ebiz Page :" + MyACCLink);

				if (MyACCLink.contains("My ACC")
						&& LogoutLink.contains("Log Out")) {
					logger.log("pass",
							"Clicked on continue shopping button and navigated to the home page and "
									+ '"' + MyACCLink + '"' + " and " + '"' + LogoutLink
									+ '"' + " is displayed", false, dRDriver,
							reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block

				logger.log("fail",
						"continue Shopping button did not redirect to home page"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

			page_Wait();
			/*
			 * dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a"
			 * )).click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush();
			 */
			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void AddingFITToExistingUser(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger
					.createTest("AddingFITToExistingUser", "Adding FIT test");
			
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			page_Wait();
			try {
				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
				.click();
		page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			page_Wait();

			try {

				dRDriver.findElement(
						By.xpath("//*[@id='nav-myacc']/li[1]/div/ul/li[2]/a"))
						.click();
				page_Wait();
				WebElement Mytrainingpgm = dRDriver.findElement(By
						.xpath(".//*[@id='ExamManage_FITManButton']"));
				page_Wait();

				String AddFITTextBox = dRDriver
						.findElement(
								By.xpath(".//*[@id='divApplication']/div[3]/div/div[1]/div[2]/div/div[1]/h3/label"))
						.getText();

				if (Mytrainingpgm.isDisplayed()) {
					logger.log("pass", "Clicked on My Training program and "
							+ '"' + AddFITTextBox
							+ '"' + " email field is available on trainings page  ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "My Training Program page display  failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			page_Wait();
			
			String emailid="";

			try {
				dRDriver.findElement(By.xpath("//*[@id='FITEmail']")).click();

				dRDriver.findElement(By.xpath("//*[@id='FITEmail']")).sendKeys(
						userName);
				emailid = dRDriver.findElement(
						By.xpath("//*[@id='FITEmail']")).getAttribute("value");
				dRDriver.findElement(By.xpath("//*[@id='btnFITEmail']")).click();
				page_Wait();
				AlertForFIT(dRDriver);
				page_Wait();
				String verifyemail = dRDriver.findElement(
						By.xpath(".//*[@id='TrainingProgram']")).getText();
				if (verifyemail.contains(emailid)) {
					logger.log("pass","The" + '"' + emailid + '"' + " is entered  ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", emailid + " is not added" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			page_Wait();
			System.out.println("validating exam reigstration");

			
			try {
				dRDriver.findElement(
						By.xpath("//*[@id='ExamManage_ExamRegButton']")).click();
				page_Wait();
				String ExamRegistration = dRDriver.findElement(
						By.xpath(".//*[@id='ExamManage_ExamRegButton']"))
						.getText();
				WebElement registeredlist = dRDriver
						.findElement(By
								.xpath(".//*[@id='content_0_pagecontrols_1_divApplication']/div[7]/h1"));

				String RegistrationStatus = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_pagecontrols_1_divApplication']/div[5]/div/div[1]/h1"))
						.getText();

				if (registeredlist.isDisplayed()) {
					logger.log("pass", "Cliked on " + '"' + ExamRegistration
							+ '"' + " link successfully " + '"' + RegistrationStatus
							+ '"' + "status of exam registration is displayed",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "registration   failed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			/*
			 * dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a"
			 * )).click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */

			redirectToHomePage(logger, et, dRDriver);

			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void VerifyAnnualSpecificNavigation(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("VerifyAnnualSpecificNavigation",
					"AnnualSpecific Test");
			/*
			 * String ParentwindowHandle = dRDriver.getWindowHandle(); Actions
			 * at=new Actions(dRDriver); at.keyDown(Keys.CONTROL).sendKeys("n");
			 * 
			 * at.perform();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); }
			 */

			// LoggerForHomepageDisplay(logger,dRDriver, et);
			
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");

			dRDriver.get("http://accscientificsession.acc.org/acc.aspx");
			page_Wait();

			try {
				String MyACCLink = dRDriver.findElement(
						By.xpath(".//*[@id='login_link']")).getText();
				String LogOutLink = dRDriver.findElement(
						By.xpath(".//*[@id='logout_btn']")).getText();
				if (MyACCLink.contains(MyACCLink)) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+ '"' + MyACCLink + '"' + " link " + '"'
									+ LogOutLink + '"' + " link exists", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Annual specific page is not displayed "
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

			page_Wait();
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='logout_btn']")).click();
			 * WebElement
			 * loginpage=dRDriver.findElement(By.xpath(".//*[@id='login_btn']"
			 * )); if (loginpage.isDisplayed()) { logger.log("pass",
			 * "Logged out " , false, dRDriver, reportingPath, et); } else {
			 * logger.log("fail", "Log Out  failed" + "\"" +
			 * "\" doesn't exists.", true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */
			/*
			 * dRDriver.close(); dRDriver.switchTo().window(ParentwindowHandle);
			 */
			/*
			 * page_Wait(); dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * );
			 */
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			page_Wait();
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void VerifyMediaSiteNavigation(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et,
			String run, Configuration configuration) throws Exception {

		try {
			et = logger.createTest("VerifyMediaSiteNavigation",
					"Mediasite Test");

			/*
			 * Actions at=new Actions(dRDriver);
			 * at.keyDown(Keys.CONTROL).sendKeys("t");
			 * 
			 * at.perform();
			 * 
			 * for(String winHandle : dRDriver.getWindowHandles()){
			 * dRDriver.switchTo().window(winHandle); } String windowHandle=
			 * dRDriver.getWindowHandle();
			 * 
			 * at.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
			 */
			System.out.println("Executing VerifyMediaSiteNavigation ");
			 LoggerForHomepageDisplayWithLogin(logger, dRDriver,et);
			 
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			page_Wait();

			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/MOD/2015/Familial-Hypercholesterolemia/FH-Identifying-Patients-and-Improving-Outcomes/FH-Identifying-Patients-and-Improving-Outcomes.aspx");
			page_Wait();

			String Mediasite = dRDriver
					.findElement(
							By.xpath(".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"))
					.getText();
			page_Wait();
			try {
				String StartProgram = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_StartLink']"))
						.getText();

				String Overview = dRDriver
						.findElement(
								By.xpath(".//*[@id='content_0_innercontent_0_ctl01_rptModules_hlModule_0']"))
						.getText();

				if (Mediasite
						.contains("Identification and Management of Patients with Familial Hypercholesterolemia")) {
					logger.log("pass",
							"Successfully Navigated to Media site page " + '"'
									+ Overview + '"' + " button and " + '"'
									+ StartProgram + '"' + " button exists",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", "Media site not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			page_Wait1();
			Thread.sleep(6000);
			/*
			 * dRDriver.findElement(By.xpath(
			 * ".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkBtnLogout']"
			 * )).click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */
			/*
			 * dRDriver.close(); dRDriver.switchTo().window(ParentwindowHandle);
			 */

			// at.keyDown(Keys.CONTROL).keyDown(Keys.SHIFT).sendKeys(Keys.TAB).build().perform();
			// at.release();

			// at.keyDown(Keys.CONTROL).sendKeys("w").perform();
			// dRDriver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL
			// +"w");

			// dRDriver.switchTo().defaultContent();
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");

			page_Wait();
			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait();
			 */
			/*
			 * dRDriver.findElement(By.xpath(
			 * ".//*[@id='nav-primary-holder']/div[3]/a")).click();
			 * 
			 * at.keyDown(Keys.CONTROL).sendKeys("w").perform();
			 */
			redirectToHomePage(logger, et, dRDriver);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void TestForCVQualityCheck(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et,
			String run, Configuration configuration) throws Exception {

		try {
			et = logger.createTest("VerifyCVQualityLoginPages", "NCDR Test");
			/*
			 * Actions at1=new Actions(dRDriver);
			 * at1.keyDown(Keys.CONTROL).sendKeys("t");
			 * 
			 * at1.perform();
			 * 
			 * 
			 * 
			 * at1.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
			 */

			// LoggerForHomepageDisplay(logger,dRDriver, et);
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			page_Wait();
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			// dRDriver.switchTo().window(windowHandle);
			dRDriver.get("http://cvquality.acc.org");
			Thread.sleep(5000);

			try {
				String NCDRTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_0']"))
						.getText();

				String InitiativesTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_1']"))
						.getText();

				String CommunicationsKitTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_2']"))
						.getText();
				String ClinicalToolTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_3']"))
						.getText();

				/*
				 * String LogOutTab = dRDriver.findElement(
				 * By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
				 * .getText();
				 */

				if (NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
				/* && LogOutTab.contains("LOGOUT") */) {
					logger.log("pass",
							"Successfully redirected to CV quality page and following tabs "
									+ '"' + NCDRTab + '"' + '"'
									+ InitiativesTab + '"' + " " + '"'
									+ CommunicationsKitTab + '"' + " " + '"'
									+ ClinicalToolTab + '"' + " exists", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "CVQuality page not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			Thread.sleep(5000);
			/*
			 * dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a"
			 * )).click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */

			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
			// dRDriver.switchTo().window(ParentwindowHandle);
			// at1.keyDown(Keys.CONTROL).sendKeys("w").perform();

			/*
			 * dRDriver.get(
			 * "https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending"
			 * ); page_Wait();
			 * 
			 * dRDriver.findElement(By.xpath(
			 * ".//*[@id='nav-primary-holder']/div[3]/a")).click();
			 */page_Wait();

			redirectToHomePage(logger, et, dRDriver);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void NCDRDashboardNavigation(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("NCDRDashboardNavigation", "NCDR Test");
			page_Wait();
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);


			page_Wait1();
			page_Wait();

			try {
				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
				.click();
		page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/div/ul/li[7]/a"))
						.click();

				page_Wait();

				String NCDRDashboard = dRDriver.findElement(
						By.xpath(".//*[@id='main-container']/article/form/h1"))
						.getText();
				if (NCDRDashboard.contains("NCDR Physician Dashboard")) {
					logger.log("pass",
							"NCDR dashboard page displayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "NCDR dashboard page not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			/*
			 * dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a"
			 * )).click(); WebElement loginpage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='myacc-holder']/div/div[2]/p/a")); if
			 * (loginpage.isDisplayed()) { logger.log("pass", "Logged out " ,
			 * false, dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "Log Out  failed" + "\"" + "\" doesn't exists.", true, dRDriver,
			 * reportingPath, et);
			 * 
			 * } logger.flush(); dRDriver.close();
			 */

			redirectToHomePage(logger, et, dRDriver);
			dRDriver.findElement(
					By.xpath(".//*[@id='nav-primary-holder']/div[3]/a"))
					.click();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static Properties loadProperties() {
		Properties prop = null;
		try {
			prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream stream = loader
					.getResourceAsStream("constants.properties");
			prop.load(stream);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return prop;
	}

	public static WebDriver OpenIEBrowser(WebDriver driver, Configuration conf)
			throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		String url = conf.getUrl();// ExcelUtils.getCellData(2, 1);
		System.setProperty("webdriver.ie.driver", /* Constant.Path_TestData */
				reportingPath + "IEDriverServer.exe");
		DesiredCapabilities ieCapabilities = DesiredCapabilities
				.internetExplorer();
		ieCapabilities
				.setCapability(
						InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
		ieCapabilities.setCapability(
				InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCapabilities.setCapability("nativeEvents", false);
		ieCapabilities.setCapability(
				InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
		/*
		 * InternetExplorerDriverService.Builder builder = new
		 * InternetExplorerDriverService.Builder();
		 * InternetExplorerDriverService srvc = builder.usingPort(5555)
		 * .withHost("127.0.0.1").build();
		 */

		// driver = new InternetExplorerDriver(srvc, ieCapabilities);
		driver = new InternetExplorerDriver(ieCapabilities);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static WebDriver OpenFFBrowser(WebDriver driver,
			Configuration configuration) throws Exception {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData +
		 * Constant.File_TestData reportingPath + "\\TestData.xlsx", //
		 * "C:\\Users\\E002226\\testData\\TestData.xlsx", "Config");
		 */
		// ExcelUtils.setExcelFile(
		// /* Constant.Path_TestData + Constant.File_TestData */reportingPath +
		// "\\TestData.xlsx", // "C:\\Users\\E002226\\testData\\TestData.xlsx",
		// "Config");
		String url = configuration.getUrl();
		System.out.println(url);// ExcelUtils.getCellData(2, 1);
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static WebDriver OpenChrmBrowser(WebDriver driver,
			Configuration configuration) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);
		System.setProperty("webdriver.chrome.driver",
		/* Constant.Path_TestData */reportingPath + "chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static WebDriver OpenSafariBrowser(WebDriver driver,
			Configuration configuration) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);
		System.setProperty("webdriver.safari.driver",
		/* Constant.Path_TestData */reportingPath + "safari.exe");
		driver = new SafariDriver();
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static void CloseBrowser(WebDriver dRDriver) {
		try {
			dRDriver.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void page_Wait() throws Exception {

		// Thread.sleep(2500);

		Thread.sleep(4000);
		// dRDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	}

	public static void page_Wait1() throws Exception {

		// Thread.sleep(2500);

		dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	public static void AlertForFIT(WebDriver dRDriver) {
		try {
			dRDriver.switchTo().alert().accept();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void ValidatingJACCBTSIndex(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("ValidatingJACCBTSIndex",
					"ValidatingJACCBTSIndex Test");

			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();
		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/div/span"))
						.click();
				page_Wait();
				String ArticleFilter = dRDriver
						.findElement(
								By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/span[2]"))
						.getText();
				String ArticelDisplayed = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[2]/div/span/span/span[3]"))
						.getText();

				if (ArticleFilter.equalsIgnoreCase(ArticelDisplayed)) {
					logger.log("pass",
							"JACC BTS article displayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC BTS article not displayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void ValidatingAddToLibraryFeatureForMembership(
			String reportPath, WebDriver dRDriver, String userName,
			String password, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) throws Exception {
		try {
			System.out
					.println("Executing ValidatingAddToLibraryFeatureForMembership");
			et = logger.createTest(
					"ValidatingAddToLibraryFeatureForMembership",
					"ValidatingAddToLibraryFeatureForMembership Test");

			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();
		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/div/span"))
						.click(); // selecting BTS journal
				page_Wait();
				page_Wait();
				WebElement checkBoxofBTS = dRDriver
						.findElement(By
								.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/div/span"));

				if (checkBoxofBTS.isSelected()) {
					logger.log("pass",
							"BTS articles are diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "BTS articles are  not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}

			logger.flush();
			page_Wait();
			
			try {
				String LibraryTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]"))
						.getText(); // clicking save to library

				if (LibraryTab.contains("Save to Library")) {
					logger.log(
							"pass",
							"Save to Library tab is shown for member Login ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Save to Library tab is not displayed for non member Login "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();
			page_Wait();
			String TagName="";
			try
			{
			dRDriver.findElement(
					By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/div/a"))
					.click(); // clicking save to library
			page_Wait();
			dRDriver.findElement(By.xpath(".//*[@id='tagInput']")).sendKeys(
					"Journal of the American College of Cardiology");
			page_Wait();
			TagName = dRDriver.findElement(
					By.xpath(".//*[@id='tagInput']")).getText();
			if (!TagName.isEmpty()) {
				logger.log("pass",
						"Clicked on save to library link and tag is added ",
						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail",
					"does not clicked on save to library"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
					reportingPath, et);

		}

		logger.flush();
		page_Wait();
		try
		{
			dRDriver.findElement(By.xpath(".//*[@id='addTags']/button"))
					.click();
			dRDriver.findElement(
					By.xpath(".//*[@id='addTags']/ul[2]/li[1]/button")).click();
			page_Wait();
			WebElement tagaddedpopup = dRDriver.findElement(
					By.xpath(".//*[@id='addTagsSuccessfull']/ul[2]/li/button"));
			
			if (tagaddedpopup.isDisplayed()) {
				logger.log("pass",
						"Clicked on save to library link and tag is added ",
						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail",
					"does not clicked on save to library"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
					reportingPath, et);

		}

		logger.flush();
		page_Wait();

			

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='addTagsSuccessfull']/ul[2]/li/button"))
						.click();
				page_Wait();

				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
						.click();
				page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			
			dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/div/ul/li[4]/a"))
						.click();
				page_Wait();
				
				String LibraryPage = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/div/article[1]"))
						.getText();

				if (LibraryPage.contains(TagName)) {
					logger.log(
							"pass",
							"BTS journal is displayed with tagname successfully in Library page",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();

				logger.log("fail", "BTS journal is not displayed with tagname"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void ValidatingLibraryForNonMember(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)
			throws Exception {

		try {
			et = logger.createTest("ValidatingLibraryForNonMember",
					"ValidatingLibraryForNonMember Test");
			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();
		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[1]/label/div/div/span"))
						.click(); // selecting BTS journal
				page_Wait();
				String LibraryTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]"))
						.getText(); // clicking save to library

				if (!LibraryTab.contains("Save to Library")) {
					logger.log(
							"pass",
							"Save to Library tab is hidden for non member Login ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Save to Library tab is displayed for non member Login "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			}

			logger.flush();
			page_Wait();

			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void VerifyingJACCBTSJournalArticle(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			et = logger.createTest("ValidatingJACCBTSArticle",
					"Validationgarticle Test");

			
			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			

			page_Wait();

			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();
				page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			
			try
			{
				
		
			dRDriver.findElement(
					By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/div/span"))
					.click(); // selecting BTS journal
			page_Wait();
			WebElement checkBoxofBTS = dRDriver
					.findElement(By
							.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/div/span"));

			if (checkBoxofBTS.isSelected()) {
				logger.log("pass",
						"BTS articles are diaplayed successfully ", false,
						dRDriver, reportingPath, et);
			}
			else
			{
				logger.log("pass",
						"BTS articles are diaplayed successfully ", false,
						dRDriver, reportingPath, et);
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "BTS articles are  not diaplayed " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);
		}

		logger.flush();
		page_Wait();
		
		try
		{
			 WebElement ArticleTitle = dRDriver
					.findElement(
							By.xpath("//div[@class='CoveoResultList']/div/article[1]/div/h1"));
			
		page_Wait();
		if (ArticleTitle.isDisplayed()) {
			logger.log(
					"pass",
					"ArticleTitle is diaplayed successfully for BTS article by selecting first article",
					false, dRDriver, reportingPath, et);
		} }
		catch(Exception e){
			logger.log("fail", "ArticleTitle is not diaplayed " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
		logger.flush();
		page_Wait();
		
		
		try
		{
			 WebElement Author = dRDriver
					.findElement(
							By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/p[2]"));
			
		page_Wait();
		if (Author.isDisplayed()) {
			logger.log(
					"pass",
					"Article Author  is diaplayed successfully for BTS article by selecting first article",
					false, dRDriver, reportingPath, et);
		} }
		catch(Exception e){
			logger.log("fail", "Article author is not diaplayed " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
		logger.flush();
		page_Wait();
		

		try
		{
			 WebElement DOI = dRDriver
					.findElement(
							By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/p[1]"));
			
		page_Wait();
		if (DOI.isDisplayed()) {
			logger.log(
					"pass",
					"Article Date of publication  is diaplayed successfully for BTS article by selecting first article",
					false, dRDriver, reportingPath, et);
		} }
		catch(Exception e){
			logger.log("fail", "Article date is not diaplayed " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}
		logger.flush();
		page_Wait();
		/*String Author="";
			try
			{
				String ArticleTitle = dRDriver
						.findElement(
								By.xpath("//div[@class='CoveoResultList']/div/article[1]/div/h1"))
						.getText();

				 Author = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/p[2]"))
						.getText();
				page_Wait();
			
			dRDriver.findElement(
					By.xpath("//div[@class='CoveoResultList']/div/article[1]/div/h1/a"))
					.click(); // selectiong first article
			page_Wait();

			String ArticleTitleMain = dRDriver.findElement(
					By.id("scm6MainContent_lblArticleTitle")).getText();
			
			String ArticleTitleMain = dRDriver.findElement(
					By.xpath(".//*[@id='node3318']/div/header/h1/div[1]")).getText();
			page_Wait();
			if (ArticleTitleMain.contains(ArticleTitle)) {
				logger.log(
						"pass",
						"ArticleTitle is diaplayed successfully for BTS article by selecting first article",
						false, dRDriver, reportingPath, et);
			} }
			catch(Exception e){
				logger.log("fail", "ArticleTitle is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			
			 * WebElement JournalTitle =
			 * dRDriver.findElement(By.xpath(".//*[@id='node11472--3']/div[1]/h2"
			 * ));
			 * 
			 * if (JournalTitle.isDisplayed()) { logger.log("pass",
			 * "JournalTitle is diaplayed successfully for BTS article" , false,
			 * dRDriver, reportingPath, et); } else { logger.log("fail",
			 * "JournalTitle is not diaplayed " + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 
			try
			{
			String AuthorMain = dRDriver
					.findElement(
							By.xpath(".//*[@id='scm6MainContent_dvAuthorSection']/span"))
					.getText();
			if (AuthorMain.contains(Author)) {
				logger.log("pass",
						"Author for BTS article is diaplayed successfully ",
						false, dRDriver, reportingPath, et);
			}
			}
			catch(Exception e) {
				logger.log("fail", "Author is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			page_Wait();try
			{

			WebElement DOI = dRDriver.findElement(By
					.xpath(".//*[@id='scm6MainContent_lblDoi']"));

			// WebElement PublishedDate =
			// dRDriver.findElement(By.xpath(".//*[@id='node11472--3']/div[1]/div[2]/span[2]"));

			if (DOI.isDisplayed()) {
				logger.log(
						"pass",
						"Published date for BTS article is diaplayed successfully ",
						false, dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "published date is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();*/

			page_Wait();
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	public static void SearchingArticleByJournLType(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		et = logger.createTest("SearchingArticleByJournalTypeForNONBTS",
				"SearchByjournalTypeTest");
		
		try {
			LoggerForHomepageDisplay(logger, dRDriver, et);

			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='nav-primary']/li[2]/a"))
				.click();

		page_Wait();
		dRDriver
		.findElement(By
				.xpath(".//*[@id='filter-holder']/div[3]/div[2]/ul/li[1]/label/div/div/span")).click();
		
				WebElement CheckBoxForJournaltype = dRDriver
						.findElement(By
								.xpath(".//*[@id='filter-holder']/div[3]/div[2]/ul/li[1]/label/div/div/span"));
				if (CheckBoxForJournaltype.isSelected()) {
					logger.log("pass",
							"JACC article is diaplayed successfully ", false,
							dRDriver, reportingPath, et);

				}else
				{
					logger.log("pass",
							"JACC article is diaplayed successfully ", false,
							dRDriver, reportingPath, et);

				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", "JACC article is not diaplayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			/*dRDriver.findElement(
					By.xpath(".//*[@id='filter-holder']/div[3]/div[2]/ul/li[1]/label/div/div/span"))
					.click();*/
			page_Wait();

			

			try {
				
				String FilterType = dRDriver
						.findElement(
								By.xpath(".//*[@id='filter-holder']/div[3]/div[2]/ul/li[1]/label/div/span[1]"))
						.getText();
				page_Wait();
				String JournalType = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/h2"))
						.getText();
				// WebElement PublishedDate =
				// dRDriver.findElement(By.xpath(".//*[@id='node11472--3']/div[1]/div[2]/span[2]"));

				if (FilterType.contains(JournalType)) {
					logger.log(
							"pass",
							"Published date for BTS article is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "published date is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			
			
			/*dRDriver.findElement(
					By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/h1/a"))
					.click();*/
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		}

		catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void SearchingArticleByKeywordForBTS(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			et = logger.createTest("SearchingArticleByKeywordForBTS",
					"SearchByKeyword Test");

			
			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();

		page_Wait();

				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			
			try
			{
			dRDriver.findElement(
					By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[1]/label/div/div/span"))
					.click(); // selecting BTS journal
			page_Wait();

			dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
					.click();
			dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
					.sendKeys("Implantable Devices");
			String searchText=dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).getAttribute("value");
			
			if (!searchText.isEmpty()) {
				logger.log("pass",
						"search text by keyword entered successfully",
						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "search text by keyword not entered  " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}

		logger.flush();
		page_Wait();
			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='globalSearchBoxButton']"))
				.click();
		page_Wait();
				String TitleElements = dRDriver.findElement(
						By.xpath(".//*[@id='search']/div/div[2]/div[6]/div"))
						.getText();
				if (TitleElements.contains("Implantable Devices")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		}

		catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void SearchingArticleByKeywordForNonBTS(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			et = logger.createTest("SearchingArticleByKeywordForNONBTS",
					"SearchByKeyword Test");

			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();

		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			
			try
			{
			dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
					.click();
			dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
					.sendKeys("Nonstatins");
             String searchText=dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']")).getAttribute("value");
			
			if (!searchText.isEmpty()) {
				logger.log("pass",
						"search text by keyword entered successfully",
						false, dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "search text by keyword not entered  " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);

		}

		logger.flush();
		page_Wait();                
			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='globalSearchBoxButton']"))
				.click();
		page_Wait();

				String TitleElements = dRDriver.findElement(
						By.xpath(".//*[@id='search']/div/div[2]/div[6]/div"))
						.getText();
				if (TitleElements.contains("Nonstatins")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			page_Wait();
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void SearchingArticleByJournalTitleForBTS(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			et = logger.createTest("SearchingArticlaByJournalTitle",
					"SearchByTitle Test");

			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			
			page_Wait();

			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();

				page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			

			String SearchTitle="";
			try
			{
				dRDriver.findElement(
						By.xpath(".//*[@id='filter-holder']/div[2]/div[2]/ul/li[6]/label/div/div/span"))
						.click(); // selecting BTS journal
				page_Wait();
			dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
					.click();
			dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
					.sendKeys(
							"PCSK9 Modulates the Secretion But Not the Cellular Uptake of Lipoprotein(a) Ex Vivo: An Effect Blunted by Alirocumab ");
			
				SearchTitle = dRDriver.findElement(
						By.xpath(".//*[@id='twotabsearchtextbox']")).getText();
				
				if (!SearchTitle.isEmpty()) {
					logger.log(
							"pass",
							"Search for BTS journal using Article title is entered ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail",
						"Search for BTS journal using Article title is not entered "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			try
			{
				dRDriver.findElement(
						By.xpath(".//*[@id='globalSearchBoxButton']")).click();
				page_Wait();

				String SearchResult = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.getText();

				if (SearchResult.contains(SearchTitle)) {
					logger.log(
							"pass",
							"Search for BTS journal using Article title is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail",
						"Search for BTS journal using Article title is not diaplayed "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		}
	catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void SearchingArticleByJournalTitleForNonBTS(
			String reportPath, WebDriver dRDriver, String userName,
			String password, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			et = logger.createTest("SearchingJournalTitle",
					"Search for Journal Test");

			try {
				WebElement Homepage = dRDriver.findElement(By
						.xpath(".//*[@id='nav-primary-holder']/div[3]/a"));
				if (Homepage.isDisplayed()) {
					logger.log("pass",
							"ACC Home page is diaplayed successfully ", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "ACC Home page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			try {

				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
						.click();

				page_Wait();

				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			page_Wait();
			String SearchTitle="";

			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
				.click();
		dRDriver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"))
				.sendKeys(
						"Persistent AF from the Onset (JACC: Clinical Electrophysiology)");
				 SearchTitle = dRDriver.findElement(
						By.xpath(".//*[@id='twotabsearchtextbox']")).getText();
				
				if (!SearchTitle.isEmpty()) {
					logger.log(
							"pass",
							"Search for NOnBTS journal using Article title is entered ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail",
						"Search for NOnBTS journal using Article title is not entered "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
				
			try
			{
				dRDriver.findElement(
						By.xpath(".//*[@id='globalSearchBoxButton']")).click();
				page_Wait();

				String SearchResult = dRDriver
						.findElement(
								By.xpath(".//*[@id='search']/div/div[2]/div[6]/div/article[1]/div/h1/a"))
						.getText();

				if (SearchResult.contains(SearchTitle)) {
					logger.log(
							"pass",
							"Search for Non BTS journal using Article title is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail",
						"Search for Non BTS journal using Article title is not diaplayed "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}
			logger.flush();

			page_Wait();
			// dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void ValidatingPurchaseLink(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		et = logger.createTest("ValidationPurchaseLink", "PurchaseLink Test");
		try {
			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
				.click();

		page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/div/ul/li[6]/a"))
						.click();
				page_Wait();
				String PurchasePage = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-masthead']/h1")).getText();

				if (PurchasePage.contains("My Purchase History")) {
					logger.log("pass",
							"Purchase History is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log("fail", "Purchase History is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			page_Wait();
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void ValidatingSSODisclosure(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			et = logger.createTest("ValidatingSSODisclosure",
					"ValidatingSSODisclosure Test");

			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			page_Wait();
			dRDriver.get("http://disclosures.acc.org/");
			page_Wait();

			System.out.println("Validating Disclosure link");

			try {
				String ManageYourDisclosures = dRDriver.findElement(
						By.xpath("html/body/div[1]/div/section/div[1]/div/a"))
						.getText();

				String MergeAccounts = dRDriver.findElement(
						By.xpath("html/body/div[1]/div/section/div[2]/div/a"))
						.getText();

				String Logout = dRDriver.findElement(
						By.xpath("html/body/header/div/div[2]/span/a[2]/span"))
						.getText();

				if (ManageYourDisclosures.contains("Manage Your Disclosures")
						&& MergeAccounts.contains("Merge Accounts")
						&& Logout.contains("Logout")) {
					logger.log("pass",
							"Navigated to annual disclosure page and " + '"' + Logout
							+ '"' + " " + '"' + ManageYourDisclosures + '"' + " "
							+ '"' + MergeAccounts + '"' + " exists", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Navigation to annual diclosure page failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			page_Wait();
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");

			page_Wait();
			// dRDriver.findElement(By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")).click();

			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void ValidatingCommiteeNominatioms(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			et = logger.createTest("ValidatingCommiteeNominatioms",
					"ValidatingCommiteeNominatioms Test");

		LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			page_Wait();
			dRDriver.get("https://services.acc.org/CommitteeNominations/");
			page_Wait();

			System.out.println("Validating Commitee Nominations link");

			try {
				String Signout = dRDriver
						.findElement(
								By.xpath("html/body/header/div/div/div/div/ul[2]/li[3]/a"))
						.getText();

				// String Search =
				// dRDriver.findElement(By.xpath(".//*[@id='globalHeader_divSearchSection']/div[1]/div[2]/div/label")).getText();
				if (Signout.contains("Sign out")) {
					logger.log("pass", "Commitee nomination page is displayed",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "Commitee nomination  page is not displayed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");

			page_Wait();
			// dRDriver.findElement(By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")).click();

			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void ValidatingOnlineMembershipApp(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			et = logger.createTest("ValidatingOnlineMembershipApp",
					"ValidatingOnlineMembershipApp Test");

			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			page_Wait();
			dRDriver.get("http://memberapp.acc.org/");
			page_Wait();

			System.out.println("Validating Online Membership applications ");

			try {
				String WelcomePage = dRDriver.findElement(
						By.xpath(".//*[@id='fullscreen-spacer']/div/div/h1"))
						.getText();
				WebElement Login = dRDriver
						.findElement(By
								.xpath(".//*[@id='fullscreen-spacer']/div/div/div[2]/div[1]/input"));

				WebElement StartApplication = dRDriver
						.findElement(By
								.xpath(".//*[@id='fullscreen-spacer']/div/div/div[2]/div[2]/input"));

				if (WelcomePage
						.contains("Welcome to ACC's Online Membership Application")
						&& Login.isDisplayed()
						&& StartApplication.isDisplayed()) {
					logger.log("pass",
							"Online Membership application is  opened", false,
							dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Online Membership application is not opened" + "\""
								+ "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='fullscreen-spacer']/div/div/div[2]/div[1]/input"))
						.click();
				page_Wait();
				WebElement StartNewApplication = dRDriver.findElement(By
						.xpath(".//*[@id='fullscreen-spacer']/div/div/input"));

				if (StartNewApplication.isDisplayed()) {
					logger.log(
							"pass",
							"Login for Online Membership application is  opened",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Login for Online Membership application is not opened"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();

			dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");

			page_Wait();
			// dRDriver.findElement(By.xpath(".//*[@id='nav-primary-holder']/div[3]/a")).click();

			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void ValidatingLibraryForNONBTSJournalMember(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {
			System.out
					.println("Executing ValidatingLibraryForNONBTSJournalMember");
			et = logger.createTest("ValidatingLibraryForNONBTSJournalMember",
					"ValidatingLibraryForNONBTSJournalMember Test");
			 LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

		
			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();
		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			page_Wait();
			String ArticleType = dRDriver
					.findElement(
							By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/h2"))
					.getText();

			if (!ArticleType
					.equalsIgnoreCase("JACC: Basic to Translational Science")) {
				
				try {
					String LibraryTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]"))
							.getText(); // clicking save to library

					if (LibraryTab.contains("Save to Library")) {
						logger.log(
								"pass",
								"Save to Library tab is shown for member Login "
								 ,false, dRDriver, reportingPath, et);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					logger.log("fail",
							"Save to Library tab is not displayed for non member Login "
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}

				logger.flush();
				page_Wait();
				String TagName="";
				try
				{
				dRDriver.findElement(
						By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/div/a"))
						.click();
				dRDriver.findElement(By.xpath(".//*[@id='tagInput']")).sendKeys(
						"Journal of the American College of Cardiology");
				page_Wait();
				TagName = dRDriver.findElement(
						By.xpath(".//*[@id='tagInput']")).getText();
				if (!TagName.isEmpty()) {
					logger.log("pass",
							"Clicked on save to library link and tag is added ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"does not clicked on save to library"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
		
				
			try
			{
				dRDriver.findElement(By.xpath(".//*[@id='addTags']/button"))
						.click();
				dRDriver.findElement(
						By.xpath(".//*[@id='addTags']/ul[2]/li[1]/button")).click();
				page_Wait();
				WebElement tagaddedpopup = dRDriver.findElement(
						By.xpath(".//*[@id='addTagsSuccessfull']/ul[2]/li/button"));
				
				if (tagaddedpopup.isDisplayed()) {
					logger.log("pass",
							"Clicked on save to library link and tag is added ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"does not clicked on save to library"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='addTagsSuccessfull']/ul[2]/li/button"))
						.click();
				page_Wait();

				dRDriver.findElement(By.xpath(".//*[@id='nav-myacc']/li[1]/span"))
						.click();
				page_Wait();
				WebElement dropdownMenu = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/div"));
				page_Wait();

				if (dropdownMenu.isDisplayed()) {
					logger.log("pass",
							"Clicked on My ACC and dropdown menu displayed ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Clicked on My ACC and dropdown menu not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
				
				page_Wait();

				
				try {

					dRDriver.findElement(
							By.xpath(".//*[@id='nav-myacc']/li[1]/div/ul/li[4]/a"))
							.click();
					page_Wait();
					String LibraryPage = dRDriver
							.findElement(
									By.xpath(".//*[@id='main-content']/div/div/article[1]"))
							.getText();

					page_Wait();
					if (LibraryPage.contains(TagName)) {
						logger.log(
								"pass",
								"Non BTS journal is displayed with tagname successfully in Library page ",
								false, dRDriver, reportingPath, et);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					logger.log("fail",
							"Non BTS journal is not displayed with tagname"
									+ "\"" + "\" doesn't exists.", true,
							dRDriver, reportingPath, et);

				}

			}
			logger.flush();

			page_Wait();
			redirectToHomePage(logger, et, dRDriver);
//			page_Wait();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void ValidatingLibraryForNONBTSJournalNonMember(
			String reportPath, WebDriver dRDriver, String userName,
			String password, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			et = logger.createTest(
					"ValidatingLibraryForNONBTSJournalNonMember",
					"ValidatingLibraryForNONBTSJournalNonMember Test");

		LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();
		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
			String ArticleType = dRDriver
					.findElement(
							By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/h2"))
					.getText();

			if (!ArticleType
					.equalsIgnoreCase("JACC: Basic to Translational Science")) {

				try {
					String LibraryTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]"))
							.getText(); // clicking save to library

					if (!LibraryTab.contains("Save to Library")) {
						logger.log(
								"pass",
								"Save to Library tab is hidden for non member Login ",
								false, dRDriver, reportingPath, et);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					logger.log("fail",
							"Save to Library tab is displayed for non member Login "
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
				}

				logger.flush();

				page_Wait();
			}
			page_Wait();
			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void VerfyingCalendarFacet(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			et = logger.createTest("VerfyingCalendarFacet",
					"VerfyingCalendarFacet Test");
			 LoggerForHomepageDisplay(logger, dRDriver, et);

			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();

		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='nav-primary']/li[3]/a"))
				.click();
		page_Wait();
				String Header = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Header.contains("Education and Meetings")) {
					logger.log(
							"pass",
							"Education and Meetings page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Education and Meetings page is not diaplayed " + "\""
								+ "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@class='main-content-inner']/table[1]//div[2]/a/input"))
						.click();
				page_Wait();

				String UpcomngMeeting = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (UpcomngMeeting.contains("Meetings")) {
					logger.log(
							"pass",
							"UpcommingMeetings page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "UpcommingMeeting page is not diaplayed "
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();
			page_Wait();
			
			
			dRDriver.findElement(By.xpath(".//*[@id='from-day-picker']"))
					.click();
			dRDriver.findElement(
					By.xpath(".//*[@id='ng-app']/div[2]/table[2]/tbody/tr[5]/td[5]"))
					.click();
			page_Wait();
			dRDriver.findElement(By.xpath(".//*[@id='to-day-picker']")).click();
			dRDriver.findElement(
					By.xpath(".//*[@id='ng-app']/div[3]/table[2]/tbody/tr[6]/td[3]"))
					.click();

			redirectToHomePage(logger, et, dRDriver);
			page_Wait();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void VerifyingClinicalTrialContent(String reportPath,
			WebDriver dRDriver, String userName, String password,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		/*
		 * List<WebElement> ClinicalTrial = dRDriver.findElements(By.xpath(
		 * ".//*[@id='latest-in-cardiology']/div/div/div/article/div[2]/h2"));
		 * if(ClinicalTrial.contains("Clinical Trial")) {
		 * 
		 * }
		 */

		et = logger.createTest("VerifyingClinicalTrialContent",
				"VerifyingClinicalTrialContent Test");
		try {
//			displayingJournalPage(logger, et);
                LoggerForHomepageDisplay(logger, dRDriver, et);

                page_Wait();
			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
				.click();

		page_Wait();
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();

			page_Wait();
		
			try
			{
			dRDriver.findElement(By.xpath(".//*[@id='nav-primary']/li[2]/a"))
					.click();

			page_Wait();

			dRDriver.findElement(
					By.xpath(".//*[@id='filter-holder']/div[3]/div[2]/ul/li[3]/label/div/div/span"))
					.click();

			String selectedele = dRDriver.findElement(
					By.xpath(".//*[@id='search']/div/div[2]/div[5]/div/article[1]/div[2]/h2")).getText();
			page_Wait();
			
		
			
			dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			if (selectedele.contains("Clinical")) {
				logger.log("pass",
						"Clinicaltrial articles are diaplayed successfully ", false,
						dRDriver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			logger.log("fail", "Clinicaltrial articles are  not diaplayed " + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);
		}

		logger.flush();
		page_Wait();

			
			try {
				dRDriver.findElement(
						By.xpath("//div[@class='CoveoResultList']/div/article[1]/div/h1/a"))
						.click();

				page_Wait();
				String UpdateDAte = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/article/div[1]/dl/dd[7]"))
						.getText();

				String OriginalDAte = dRDriver
						.findElement(
								By.xpath(".//*[@id='main-content']/div/article/div[1]/dl/dd[8]"))
						.getText();

				if (UpdateDAte.equals(OriginalDAte)) {
					logger.log("pass",
							"Updated Date and Original POsted date are same ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail",
						"Updated Date and Original POsted date are not same "
								+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);

			}

			logger.flush();

			redirectToHomePage(logger, et, dRDriver);
			page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void redirectToHomePage(com.gallop.Logger logger,
			ExtentTest et, WebDriver dRDriver) {

		try {

			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			page_Wait();
			WebElement HomePage = dRDriver.findElement(By
					.xpath(".//*[@id='nav-primary-holder']/div[3]"));
			// .//*[@id='nav-myacc']/li[1]/span

			if (HomePage.isDisplayed()) {
				System.out.println("Logging true");
				logger.log("pass", "Home page displayed for acc", false,
						dRDriver, reportingPath, et);
			} else {
				System.out.println("Failed to display homepage");
			}
			/*
			 * logger.log("pass",
			 * "Updated Date and Original POsted date are same ", false,
			 * dRDriver, reportingPath, et);
			 */

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();

			logger.log("fail",
					"Updated Date and Original POsted date are not same "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
					reportingPath, et);
		}

		logger.flush();
	}

	public static void LoggerForHomepageDisplay(com.gallop.Logger logger,
			WebDriver dRDriver, ExtentTest et) throws Exception {
		try {
			Thread.sleep(3000);
			
			System.out.println("Entered Logger For HomePage Display");
			page_Wait();
			WebElement HomePage = dRDriver.findElement(By
					.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"));
			// .//*[@id='nav-myacc']/li[1]/span
             page_Wait();
			if (HomePage.isDisplayed()) {
				System.out.println("Logging true");
				logger.log("pass", "Home page displayed for acc", false,
						dRDriver, reportingPath, et);
			} else {
				System.out.println("Failed to display homepage");
			}
		} catch (Exception e) {

			logger.log("fail", "Home page not displayed" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath, et);
		}
		logger.flush();
		page_Wait();
	}

	public static void LoggerForHomepageDisplayWithLogin(com.gallop.Logger logger,
			WebDriver dRDriver, ExtentTest et) throws Exception {
		try {
			page_Wait();
			dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			System.out.println("Entered Logger For HomePage Display");
			WebElement HomePage = dRDriver.findElement(By
					.xpath(".//*[@id='nav-myacc']/li[1]/span"));
			// .//*[@id='nav-myacc']/li[1]/span
             page_Wait();
//             dRDriver.manage().
			if (HomePage.isDisplayed()) {
				System.out.println("Logging true");
				logger.log("pass", "Home page displayed for acc", false,
						dRDriver, reportingPath, et);
			} else {
				System.out.println("Failed to display homepage");
			}
		} catch (Exception e) {

			logger.log("fail", "Home page not displayed" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath, et);
		}
		logger.flush();
		page_Wait();
	}

	public static void displayingJournalPage(com.gallop.Logger logger,
			ExtentTest et) {
		try {
			LoggerForHomepageDisplay(logger, dRDriver, et);
			dRDriver.findElement(By.xpath(".//*[@id='footer']/div[1]/h2[2]/a"))
					.click();

			page_Wait();

			try {
				String Title = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				if (Title.contains("JACC Journals")) {
					logger.log("pass",
							"JACC journal page is diaplayed successfully ",
							false, dRDriver, reportingPath, et);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
				logger.log("fail", "JACC journal page is not diaplayed " + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}

			logger.flush();
		}

		catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
