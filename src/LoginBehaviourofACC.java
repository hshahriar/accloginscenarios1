

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Utils;

import com.relevantcodes.extentreports.ExtentTest;

public class LoginBehaviourofACC {

	//private static Logger Log = Logger.getLogger(LogIn.class.getName());
	//static Logger LoggerParent = LogManager.getLogger("LoggerParent");
	//private static Reporter reporter = null;
	//static Properties properties = loadProperties();
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");
	private static String chromePath = Utils.loadProperty("chromePath");
	private static String ieDriver = Utils.loadProperty("ieDriver");
	
	public static String geckoDriverPath = "C:\\Pradeep Srivastava Backup\\Pradeep Backup\\geckodriver-v0.11.1-win64\\geckodriver.exe";
	static WebDriver dRDriver = null;
	static WebDriver driverFF=null;
	
	
	static WebDriver driverChrome=null;
	static WebDriver driverInternet=null;
	static DesiredCapabilities capabilities = null;
	static DesiredCapabilities capabilities1 = null;

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		
		System.out.println("Main metho Starts for execution");
		
		String destFilePath=null;
		com.gallop.Logger logger = new com.gallop.Logger();
		logger.createTestReport(reportPath + "\\testReports.html"/* "Q:\\QA Stuff\\Syam pithani\\NCDR\\NCDR\\WebContent\\WEB-INF\\views\\testReports.html" */);

		ExtentTest et = null;
	
		dRDriver = OpenChrmBrowser(dRDriver, capabilities);
		//dRDriver = OpenFFBrowser(dRDriver, capabilities);
		//dRDriver = OpenIEBrowser(dRDriver, capabilities);
		

	    //dRDriver = OpenFirefoxBrowser(dRDriver);
		
		//dRDriver=OpenIEBrowser(dRDriver);

		dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
		ScenarioForAcc.page_Wait();
		
		List<WebElement> loginPagePopUP = dRDriver.findElements(By.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
		ScenarioForAcc.page_Wait();
		if(loginPagePopUP.size()>0){
		loginPagePopUP.get(0).click();
		ScenarioForAcc.page_Wait();
		}
				
		 //Working fine!!!
		//  System.out.println("scenario1 started");
	    VerifyingBasicLogin(reportPath, dRDriver, logger, reportingPath, et);
	     
	    //  System.out.println("scenario2 started");
	     VerifyingBasicSSOConnection(reportPath, dRDriver, logger, destFilePath, et);
	    
	   //System.out.println("scenario3 started");
	    dRDriver = VerifyRememberMeDoesNotBreakAccesstoLLPProducts(reportPath, dRDriver, logger, destFilePath, et);
	     
	     //System.out.println("scenario4 started");
	     dRDriver = VerifyRememberMeFunctionlityonACC(reportPath, dRDriver, logger, destFilePath, et);
		
		System.out.println("scenario5 started");
		dRDriver = VerifyRememberMeFunctionalityIfUserVisitsLLP(reportPath, dRDriver, logger, destFilePath, et);
		
		//System.out.println("scenario6 started");
		dRDriver = VerifyRememberMeFunctionalityonQII(reportPath, dRDriver, logger, destFilePath, et);
		
		//System.out.println("scenario7 started");
		dRDriver = VerifyRememberMeFunctionalityonScientificSession(reportPath, dRDriver, logger, destFilePath, et);
	     
		dRDriver.close();
//		String url=DR_ACCMain.run;
		String run = null;
		
		destFilePath = Utils.createReportWithTimeStamp(run);
		
	}

	public static WebDriver OpenFFBrowser(WebDriver dRDriver, DesiredCapabilities capabilities) throws Exception 
	{
		System.setProperty("webdriver.gecko.driver", geckoDriverPath);

         capabilities = DesiredCapabilities.firefox();
         FirefoxProfile profile = new FirefoxProfile();

			capabilities.setCapability(FirefoxDriver.PROFILE, profile);	
		
		 dRDriver =  new FirefoxDriver();
		

		dRDriver.manage().window().maximize();
		
		driverFF=dRDriver;
		
		return dRDriver;
	}

	public static WebDriver OpenIEBrowser(WebDriver dRDriver, DesiredCapabilities capabilities) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		// String url = conf.getUrl();// ExcelUtils.getCellData(2, 1);
		System.setProperty("webdriver.ie.driver", /* Constant.Path_TestData */
				ieDriver + "IEDriverServer.exe");
		DesiredCapabilities ieCapabilities = DesiredCapabilities
				.internetExplorer();
		ieCapabilities
				.setCapability(
						InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
		//ieCapabilities.setCapability(
		//		InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		ieCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, false);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCapabilities.setCapability("nativeEvents", false);
		ieCapabilities.setCapability(
				InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
		/*
		 * InternetExplorerDriverService.Builder builder = new
		 * InternetExplorerDriverService.Builder();
		 * InternetExplorerDriverService srvc = builder.usingPort(5555)
		 * .withHost("127.0.0.1").build();
		 */

		// driver = new InternetExplorerDriver(srvc, ieCapabilities);
		dRDriver = new InternetExplorerDriver(ieCapabilities);
		dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		// dRDriver.get(url);
		dRDriver.manage().window().maximize();
		
		driverInternet = dRDriver;
		return dRDriver;
	}

	public static WebDriver OpenChrmBrowser(WebDriver dRDriver, DesiredCapabilities capabilities)
			throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		// String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);

		// dRDriver.get(url);
	

		System.setProperty("webdriver.chrome.driver",
chromePath + "chromedriver.exe");
				capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
//				options.addArguments("--incognito");
				options.addArguments("start-maximized");
				options.addArguments("user-data-dir=" + reportingPath);
				capabilities.setCapability("chrome.binary", reportingPath
						+ "chromedriver.exe");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				
//		capabilities=getCapabilities(capabilities);
	dRDriver = new ChromeDriver(capabilities);
		
//		dRDriver.manage().window().maximize();
		driverChrome=dRDriver;
		
		
		/*
		 * dRDriver = new ChromeDriver(); 
		 */
		return dRDriver;
	}

	public static WebDriver OpenFirefoxBrowser(WebDriver dRDriver)
			throws Exception {
		

				/*capabilities = DesiredCapabilities.firefox();
				FirefoxProfile options = new FirefoxProfile();
				
				options.setPreference("user-data-dir" , reportingPath);
				capabilities.setCapability(FirefoxDriver.PROFILE, options);
				
				
				dRDriver = new FirefoxDriver(options);*/
		/*capabilities=getCapabilitiesForFF(capabilities);
		
		

		dRDriver = new FirefoxDriver(capabilities);

		driverFF=dRDriver;*/
		ProfilesIni profile = new ProfilesIni();
		 
		FirefoxProfile myprofile = profile.getProfile("Testfirefox");
		//myprofile.setPreference("network.cookie.cookieBehavior", 0);
		//myprofile.setPreference("browser.cache.memory.enable",true);
		dRDriver = new FirefoxDriver(myprofile);
		
		/*
		 * dRDriver = new ChromeDriver(); 
		 */
		return dRDriver;
	}
	public static DesiredCapabilities getCapabilities(
			DesiredCapabilities capabilities) throws Exception {
		
		/* * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");*/
		 
		// String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);

		// dRDriver.get(url);

		if (capabilities != null) {
			return capabilities;
		}
		
	

		/*System.setProperty("webdriver.chrome.driver",
		 reportingPath + "chromedriver.exe");
		capabilities = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("test-type");
		options.addArguments("start-maximized");
		options.addArguments("user-data-dir=" + reportingPath);
		capabilities.setCapability("chrome.binary", reportingPath
				+ "chromedriver.exe");
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        return capabilities;
		
		
		
//		System.setProperty("webdriver.chrome.driver",
				/* Constant.Path_TestData 
	/*reportingPath + "chromedriver.exe");
		
		FirefoxProfile profile = new FirefoxProfile();

		
		DesiredCapabilities dc=DesiredCapabilities.firefox();
		FirefoxProfile profile1 = new FirefoxProfile();
		dc.setCapability(FirefoxDriver.PROFILE, profile);
		return dc;
		
		
				capabilities = DesiredCapabilities.firefox();
				Firefox options = new ChromeOptions();
				options.addArguments("test-type");
				options.addArguments("start-maximized");
				options.addArguments("user-data-dir=" + reportingPath);
				capabilities.setCapability("chrome.binary", reportingPath
						+ "chromedriver.exe");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		
	*/
          /* FirefoxProfile profile = new FirefoxProfile();

		
		DesiredCapabilities dc=DesiredCapabilities.firefox();
		FirefoxProfile profile1 = new FirefoxProfile();
		dc.setCapability(FirefoxDriver.PROFILE, profile1);
		return dc;
		*/
		
				/*capabilities = DesiredCapabilities.firefox();
				Firefox options = new ChromeOptions();
				options.addArguments("test-type");
				options.addArguments("start-maximized");
				options.addArguments("user-data-dir=" + reportingPath);
				capabilities.setCapability("chrome.binary", reportingPath
						+ "chromedriver.exe");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);*/
		  return capabilities;
	}
	
	
	public static DesiredCapabilities getCapabilitiesForFF(
			DesiredCapabilities capabilities) throws Exception {
		

		if (capabilities != null) {
			return capabilities;
		}
		
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("browser.cache.disk.enable", true);
		profile.setPreference("browser.cache.memory.enable", true);
		profile.setPreference("browser.cache.offline.enable", true);
		profile.setPreference("network.http.use-cache", true);

		DesiredCapabilities dc = DesiredCapabilities.firefox(); 
		dc.setCapability(FirefoxDriver.PROFILE, profile); 
		return capabilities;

	}

	public static void BasicLoginFeature(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			/*ScenarioForAcc.page_Wait();
			Thread.sleep(5000);*/

			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
			ScenarioForAcc.page_Wait();
			try
			{
				WebElement action1=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")); // login button validation
				if(action1.isDisplayed())
				{
					System.out.println("Element displayed");
					action1.click();
					Thread.sleep(2000);
					logger.log("pass", "login button exists and clicked on login button", false,
							dRDriver, reportingPath, et);
				}
			}
			catch (Exception e) 
			{

					//e.printStackTrace();
					logger.log("fail", "Login button is not clicked" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
			}
				logger.flush();
				//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ScenarioForAcc.page_Wait();
			

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
			try {
					String login = dRDriver.findElement(
							By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
	
					if (login.contains("Login")) 
					{
						Thread.sleep(1000);
						logger.log("pass", "Login page displayed for acc", false,
								dRDriver, reportingPath, et);
					} 
				} 
			catch (Exception e) 
			{
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
			}
			
			logger.flush();

			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
			System.out.println("login screen captured");
			
			try {
				
					/*String UserNameForMembership = properties
							.getProperty("UserNameForMembership");*/
				String UserNameForMembership=Utils.loadProperty("UserNameForMembership");
	
					dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
							UserNameForMembership); // this enters the username
	
					String username = dRDriver.findElement(
							By.xpath("//*[@id='UserName']")).getAttribute("value");
	
					System.out.println(username);
					ScenarioForAcc.page_Wait();
					if (!username.isEmpty()) 
					{
						logger.log("pass", "UserName " +'"'+username +'"'+" successfully entered", false,
								dRDriver, reportingPath, et);
					}
				} 
			catch (Exception e) 
			{
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
			}
			
			logger.flush();

			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
			

			try {
				
					//dRDriver.findElement(By.id("Password")).click(); //this will click on the password field
					ScenarioForAcc.page_Wait();
					String PasswordForMembership = Utils.loadProperty("PasswordForMembership");
	
					// driver.findElement(By.id("Password")).sendKeys(vPassword);
	
					dRDriver.findElement(By.id("Password")).sendKeys(
							PasswordForMembership); //this will enter the password data
					String password = dRDriver.findElement(By.id("Password"))
							.getAttribute("value");
					if (!password.isEmpty()) 
					{
						logger.log("pass", "Password "+'"'+ password +'"'+" successfully entered", false,
								dRDriver, reportingPath, et);
					} 
				} 
			catch (Exception e) 
			{
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
			}
			
			logger.flush();

			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
			// et=logger.createTest("password", "password entered");
			/*
			 * dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			 * .click(); ScenarioForAcc.page_Wait();
			 */
			

			try {
				
					WebElement signIn=dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"));
					// check remember me check
					String signInText=dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button")).getText();
					if(signIn.isDisplayed())
					{
						signIn.click();
						Thread.sleep(2000);
						logger.log("pass", "Clicked on"+'"'+signInText+'"'+"button", false,
								dRDriver, reportingPath, et);
						
					}
					System.out.println("Successfully logged in to ACC site");
					//dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					ScenarioForAcc.page_Wait();
					
					WebDriverWait wait = new WebDriverWait(dRDriver, 20);
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='nav-myacc']/li[1]/span")));
					
					WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
					// String
					// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
					if (logoforacc.isDisplayed()) 
					{
						logger.log("pass", "Successfully logged into ACC", false,
								dRDriver, reportingPath, et);
					} 
				} 
			catch (Exception e) 
			{
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
			}
			logger.flush();
			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();

			try 
			{
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();

				if (logoutLink.contains("Log Out")) {
					logger.log("pass","The"+'"'+logoutLink +'"'+" Link is displayed", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
					
					//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
					ScenarioForAcc.page_Wait();
			}
			logger.flush();
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();			
		}
	}
	public static void BasicLoginFeatureForEchoSAP7(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) 
	{

		try 
		{
				String urlForEchoSap7="http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx";
				if(!urlForEchoSap7.isEmpty())
				{
						dRDriver.get(urlForEchoSap7);
						logger.log("pass", "Accessed Echosap7 page", false,
								dRDriver, reportingPath, et);
				}
				//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ScenarioForAcc.page_Wait();
				try
				{
					WebElement Login = dRDriver.findElement(
							By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']"));
		
					if (Login.isDisplayed()) 
					{
						
						ScenarioForAcc.page_Wait();
						logger.log("pass", "Home page displayed for Echosap7", false,
								dRDriver, reportingPath, et);
					}
				} 
				catch (Exception e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Home page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
		
			logger.flush();
			ScenarioForAcc.page_Wait();
			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				
				System.out.println("driver" + dRDriver);
				System.out.println("path" + reportingPath);
				System.out.println("url captured");
				// driver.get("http://www.acc.org/" );
				ScenarioForAcc.page_Wait();
				try {
					dRDriver.findElement(
							By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']")).click();
					ScenarioForAcc.page_Wait();
					
					String login = dRDriver.findElement(
							By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
	
					if (login.contains("Login")) {
						logger.log("pass", "Login page displayed for Echosap7", false,
								dRDriver, reportingPath, et);
					}} catch (Exception e) {
						// TODO Auto-generated catch block
	//					e.printStackTrace();
						logger.log("fail", "Login page is not displayed" + "\""
								+ "\" doesn't exists.", true, dRDriver, reportingPath,
								et);
					}
				
				
				logger.flush();
	
				//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ScenarioForAcc.page_Wait();
				System.out.println("login screen captured");
				
				try {
					dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
					ScenarioForAcc.page_Wait();
					// driver.findElement(By.id("UserName")).clear();
	
					// driver.findElement(By.id("UserName")).sendKeys(vUsername);
					String UserNameForMembership = Utils.loadProperty("UserNameForMembership");
	
					dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
							UserNameForMembership);
	
					String username = dRDriver.findElement(
							By.xpath("//*[@id='UserName']")).getAttribute("value");
	
					System.out.println(username);
					ScenarioForAcc.page_Wait();
					if (!username.isEmpty()) {
						logger.log("pass", "UserName "+'"'+username +'"'+" successfully entered for Echosap7", false,
								dRDriver, reportingPath, et);
					}} catch (Exception e) {
						// TODO Auto-generated catch block
	//					e.printStackTrace();
						logger.log("fail", "username field empty" + "\""
								+ "\" doesn't exists.", true, dRDriver, reportingPath,
								et);
					}
			
				logger.flush();
	
				//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ScenarioForAcc.page_Wait();
	
				try {
					dRDriver.findElement(By.id("Password")).click();
					ScenarioForAcc.page_Wait();
					String PasswordForMembership = Utils.loadProperty("PasswordForMembership");
	
					// driver.findElement(By.id("Password")).sendKeys(vPassword);
	
					dRDriver.findElement(By.id("Password")).sendKeys(
							PasswordForMembership);
					String password = dRDriver.findElement(By.id("Password"))
							.getAttribute("value");
					if (!password.isEmpty()) {
						logger.log("pass", "Password "+'"'+password +'"'+" successfully entered for Echosap7", false,
								dRDriver, reportingPath, et);
					} } catch (Exception e) {
						// TODO Auto-generated catch block
	//					e.printStackTrace();
						logger.log("fail", "password field empty" + "\""
								+ "\" doesn't exists.", true, dRDriver, reportingPath,
								et);
					}
				logger.flush();
	
				// et=logger.createTest("password", "password entered");
				/*
				 * dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
				 * .click(); ScenarioForAcc.page_Wait();
				 */
				
	
				// et=logger.createTest("Login", "Login successful");
	
				System.out.println("Successfully logged in to ACC site");
				//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
				ScenarioForAcc.page_Wait();
	
				// page_Wait();
	
				// CapturingPopup(dRDriver);
				//dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	
				try 
				{
					WebElement loginClick=dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"));
					if(loginClick.isDisplayed())
					{
						loginClick.click();
						ScenarioForAcc.page_Wait();
						logger.log("pass", "Clicked on "+'"'+loginClick.getAttribute("value")+'"'+"button", false,
								dRDriver, reportingPath, et);
					}
					
					ScenarioForAcc.page_Wait();
					
					WebElement logoforacc = dRDriver.findElement(By
							.xpath(".//*[@id='nav-myacc']/li[1]/span"));
					// String
					// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
					if (logoforacc.isDisplayed()) 
					{
						logger.log("pass", "Successfully logged into ACC for Echosap7", false,
								dRDriver, reportingPath, et);
					}
				} 
				catch (Exception e) 
				{
						// TODO Auto-generated catch block
	//					e.printStackTrace();
						logger.log("fail", "Login failed for ACC" + "\""
								+ "\" doesn't exists.", true, dRDriver, reportingPath,
								et);
	
				}
				
				
				logger.flush();
				ScenarioForAcc.page_Wait();
				//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
					try 
					{
						String logoutLink = dRDriver.findElement(
								By.xpath(".//*[@id='header-personal']/p/a")).getText();
		
						if (logoutLink.contains("Log Out")) {
							logger.log("pass","The" +'"'+ logoutLink +'"'+" Link is displayed", false,
									dRDriver, reportingPath, et);
						} 
					} 
					catch (Exception e) 
					{
							// TODO Auto-generated catch block
		//					e.printStackTrace();
							logger.log("fail", "Login is not successfull" + "\""
									+ "\" doesn't exists.", true, dRDriver, reportingPath,
									et);
		
					}	
				
					logger.flush();
		} // try ending
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}//method ending

	public static void CapturingPopup(WebDriver dRDriver) {
		try {
			List<WebElement> popUpElements = dRDriver.findElements(
					By.xpath("//*[@id='fsrOverlay']/div/div/div/div/div/div[2]/div[1]/a"));
			if(popUpElements.size()>0){
				popUpElements.get(0).click();
				ScenarioForAcc.page_Wait();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void LogoutFeature(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {
			dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
			ScenarioForAcc.page_Wait();
			System.out.println(dRDriver.getCurrentUrl());
			try 
			{
				System.out.println("in logout!!");
				WebElement ele = dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a"));
				System.out.println("Logout text is #### "+ele.getText());
				
				
				JavascriptExecutor executor = (JavascriptExecutor)dRDriver;
				executor.executeScript("arguments[0].click();", ele);
				ScenarioForAcc.page_Wait();
				System.out.println("After Clicking logout!!");

				
				ScenarioForAcc.page_Wait();
		      System.out.println(dRDriver.getCurrentUrl());
				 WebElement Homepage = dRDriver.findElement(By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"));
				if (Homepage.isDisplayed()) 
				{
					logger.log("pass", "logged out  successfully and home page for ACC is displayed", false,
							dRDriver, reportingPath, et);
				} 
			} 
			catch (Exception e) 
			{
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "log out is not successful" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
			}
			logger.flush();
			
			
			List<WebElement> loginPagePopUP = dRDriver.findElements(By.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
			ScenarioForAcc.page_Wait();
			if(loginPagePopUP.size()>0){
			loginPagePopUP.get(0).click();
			ScenarioForAcc.page_Wait();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void LogoutFeatureForACCScientific(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {
			
			try{
				dRDriver.findElement(By.xpath(".//*[@id='logout_btn']"))
				.click();

		ScenarioForAcc.page_Wait();
				
			String LoginPage = dRDriver.findElement(
					By.xpath(".//*[@id='login_btn']"))
					.getText();

			
			ScenarioForAcc.page_Wait();
			if (LoginPage.contains("Login")) {
				logger.log("pass", "Logged out successfully for ACCScentific and  "+'"'+LoginPage +'"'+" tab exixts ForACCScientific", false,
						dRDriver, reportingPath, et);
			}	} catch (Exception e2) {
				// TODO Auto-generated catch block
//				e2.printStackTrace();
				logger.log("fail", "ACCScientific is not logged out" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
	
		logger.flush();
	
		ScenarioForAcc.page_Wait();


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void LogoutFeatureForJACCJournal(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {
			
			
			try {
				dRDriver.findElement(By.xpath(".//*[@id='globalHeader_lbSignOut']"))
				.click();

		ScenarioForAcc.page_Wait();
				 WebElement LogoForJACC = dRDriver.findElement(By.xpath(".//*[@id='siteLogo']"));
						
				if (LogoForJACC.isDisplayed()) {
					logger.log("pass", "Logged in successfully  ForJACCJournal", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Logged in not uccessfull  ForJACCJournal" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			
		
			logger.flush();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void RememberMeFeature(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			/*et = logger.createTest("ACCMemberLoginWith Remember Me Feature", "Login Test");
			Thread.sleep(4000);*/
			dRDriver.get("http://www.acc.org/" );
			ScenarioForAcc.page_Wait();
			try
			{
			WebElement action1=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
			if(action1.isDisplayed())
			{
				System.out.println("Element displayed");
				
				logger.log("pass", "Home page displayed and login button is clicked", false,
						dRDriver, reportingPath, et);
			}
			}catch (Exception e) {

					//e.printStackTrace();
					logger.log("fail", "Login button is not clicked" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
				logger.flush();
				ScenarioForAcc.page_Wait();
			

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			ScenarioForAcc.page_Wait();
			try
			{
				dRDriver.findElement(
						By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")).click();
				ScenarioForAcc.page_Wait();
			
			String login = dRDriver.findElement(
					By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

			if (login.contains("Login")) {
				logger.log("pass", "Login page displayed for acc", false,
						dRDriver, reportingPath, et);
			} }
			catch(Exception e)
			{
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			System.out.println("login screen captured");
			 try
            {
			        	dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
			        	ScenarioForAcc.page_Wait();
						// driver.findElement(By.id("UserName")).clear();

						// driver.findElement(By.id("UserName")).sendKeys(vUsername);
						String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

						dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
								UserNameForMembership);
			    
			String username = dRDriver.findElement(
					By.xpath("//*[@id='UserName']")).getAttribute("value");

			System.out.println(username);
			ScenarioForAcc.page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "UserName"+'"'+username +'"'+ "successfully entered", false,
						dRDriver, reportingPath, et);
			} }
            catch(Exception e){
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			
           try{
        	   dRDriver.findElement(By.id("Password")).click();
   			ScenarioForAcc.page_Wait();
   			String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

   			// driver.findElement(By.id("Password")).sendKeys(vPassword);

   			dRDriver.findElement(By.id("Password")).sendKeys(
   					PasswordForMembership);
           
			String password = dRDriver.findElement(By.id("Password"))
					.getAttribute("value");
			if (!password.isEmpty()) {
				logger.log("pass", "Password" +'"'+ password +'"'+" successfully entered", false,
						dRDriver, reportingPath, et);
			} }
           catch(Exception e ){
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

		ScenarioForAcc.page_Wait();
			
			try
			{
				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
				.click();

		ScenarioForAcc.page_Wait();
		dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"))
				.click();

		// et=logger.createTest("Login", "Login successful");
		//dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ScenarioForAcc.page_Wait();
		System.out.println("Successfully logged in to ACC site");

		// page_Wait();

		 CapturingPopup(dRDriver);
			WebElement logoforacc = dRDriver.findElement(By
					.xpath(".//*[@id='nav-myacc']/li[1]/span"));
			// String
			// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
			if (logoforacc.isDisplayed()) {
				logger.log("pass", "Successfully logged into ACC", false,
						dRDriver, reportingPath, et);
			} }
			catch(Exception e){
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			ScenarioForAcc.page_Wait();

			try
			{
			String logoutLink = dRDriver.findElement(
					By.xpath(".//*[@id='header-personal']/p/a")).getText();

			if (logoutLink.contains("Log Out")) {
				logger.log("pass","The" +'"'+ logoutLink +'"'+ " Link is displayed", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();

			ScenarioForAcc.page_Wait();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void RememberMeFeatureForNonMember(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			/*et = logger.createTest("ACCMemberLoginWith Remember Me Feature", "Login Test");
			Thread.sleep(4000);*/
			dRDriver.get("http://www.acc.org/" );
			ScenarioForAcc.page_Wait();
			
			List<WebElement> loginPagePopUP = dRDriver.findElements(By.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
			ScenarioForAcc.page_Wait();
			if(loginPagePopUP.size()>0){
			loginPagePopUP.get(0).click();
			ScenarioForAcc.page_Wait();
			}
			
			try
			{
			WebElement action1=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
			if(action1.isDisplayed())
			{
				System.out.println("Element displayed");
				
				logger.log("pass", "action performed", false,
						dRDriver, reportingPath, et);
			}
			}catch (Exception e) {

					//e.printStackTrace();
					logger.log("fail", "Login button is not clicked" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
				logger.flush();
				ScenarioForAcc.page_Wait();
			
			

			System.out.println("driver" + dRDriver);
			System.out.println("path" + reportingPath);
			System.out.println("url captured");
			// driver.get("http://www.acc.org/" );
			ScenarioForAcc.page_Wait();
			try
			{
				dRDriver.findElement(
						By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a")).click(); // click log in to my application
				ScenarioForAcc.page_Wait();
			
			String login = dRDriver.findElement(
					By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

			if (login.contains("Login")) {
				logger.log("pass", "Login page displayed for acc", false,
						dRDriver, reportingPath, et);
			} }
			catch(Exception e)
			{
				logger.log("fail", "Login page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			ScenarioForAcc.page_Wait();
			System.out.println("login screen captured");
			
            try
            {
            	dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
    			// driver.findElement(By.id("UserName")).clear();
            	ScenarioForAcc.page_Wait();
    			// driver.findElement(By.id("UserName")).sendKeys(vUsername);
    			String UserNameForMembership = Utils.loadProperty("UserNameForNonMembership");

    			dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
    					UserNameForMembership); // enters username data in username field
            	
			String username = dRDriver.findElement(
					By.xpath("//*[@id='UserName']")).getAttribute("value");

			System.out.println(username);
			ScenarioForAcc.page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "UserName"+'"'+username +'"'+ "successfully entered", false,
						dRDriver, reportingPath, et);
			} }
            catch(Exception e){
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			ScenarioForAcc.page_Wait();
			
           try{
        	   
        	   dRDriver.findElement(By.id("Password")).click();
   			ScenarioForAcc.page_Wait();
   			String PasswordForMembership = Utils.loadProperty("PasswordForNonMembership");

   			// driver.findElement(By.id("Password")).sendKeys(vPassword);

   			dRDriver.findElement(By.id("Password")).sendKeys(
   					PasswordForMembership);// enters the password info
   			System.out.println(PasswordForMembership);
			String password = dRDriver.findElement(By.id("Password"))
					.getAttribute("value");
			if (!password.isEmpty()) {
				logger.log("pass", "Password" +'"'+ password +'"'+" successfully entered", false,
						dRDriver, reportingPath, et);
			} }
           catch(Exception e ){
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			ScenarioForAcc.page_Wait();
			

			// page_Wait();

			
			try
			{
				
				dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
				.click();// remember me check

		ScenarioForAcc.page_Wait();
		dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"))
				.click(); // Click on login button

		// et=logger.createTest("Login", "Login successful");
		//dRDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		ScenarioForAcc.page_Wait();
		System.out.println("Successfully logged in to ACC site");
		
		 CapturingPopup(dRDriver);
		 ScenarioForAcc.page_Wait();
		 
			WebElement logoforacc = dRDriver.findElement(By
					.xpath(".//*[@id='nav-myacc']/li[1]/span"));
			// String
			// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
			if (logoforacc.isDisplayed()) {
				logger.log("pass", "Successfully logged into ACC", false,
						dRDriver, reportingPath, et);
			} }
			catch(Exception e){
				logger.log("fail", "Login failed for ACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			ScenarioForAcc.page_Wait();

			try
			{
			String logoutLink = dRDriver.findElement(
					By.xpath(".//*[@id='header-personal']/p/a")).getText();

			if (logoutLink.contains("Log Out")) {
				logger.log("pass","The" +'"'+ logoutLink +'"'+ " Link is displayed", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			ScenarioForAcc.page_Wait();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static void VerifyingEchoSap7WithoutLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)

	{
		try {
			/*et = logger.createTest("VerifyECHO SAP 7 Navigation",
					"VerifyECHO SAP 7 Navigation Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");*/
			/*ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			
			ScenarioForAcc.LoggerForHomepageDisplay(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			
			ScenarioForAcc.page_Wait();
			
			
			System.out.println("Validating Product: ECHOSap7 ");
			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx");
			
			
			ScenarioForAcc.page_Wait();
			

//			try {
				try {
					
					CapturingPopup(dRDriver);
					String EchoSap7 = dRDriver
							.findElement(
									By.xpath(".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"))
							.getText();
					String PurchaseNow = dRDriver
							.findElement(
									By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
							.getText();
					ScenarioForAcc.page_Wait();
					/*String LoginTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']"))
							.getText();*/
//					ScenarioForAcc.page_Wait();
					if (EchoSap7.contains("EchoSAP 7")
						) {
						logger.log("pass", " Result for EchoSap7  displayed and"
								+ PurchaseNow + " button exists Without Login", false, dRDriver,
								reportingPath, et);
					} } catch (Exception e) {
						// TODO Auto-generated catch block
//						e.printStackTrace();
						logger.log("fail", "Results fo Echo Sap7 is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
								reportingPath, et);

					}
				
				logger.flush();
				ScenarioForAcc.page_Wait();
			/*} catch (Exception e) {
				
				e.printStackTrace();
			}*/
			/*try
			{
				try {
					String MyACCTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_rptMyAcc_lnkName_0']"))
							.getText();
					ScenarioForAcc.page_Wait();
					if (EchoSap7.contains("EchoSAP 7")
							&& MyACCTab.contains("My ACC")) {
						logger.log("pass", " Result for EchoSap7  displayed and"
								+ PurchaseNow + " button exists with Login", false, dRDriver,
								reportingPath, et);
					} } catch (Exception e1) {
						// TODO Auto-generated catch block
//						e1.printStackTrace();
						logger.log("fail", "Results fo Echo Sap7 is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
								reportingPath, et);
						
//						LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

					}
				
				
				logger.flush();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			*/
		
		
			
		

		} catch (Exception e) {

			e.printStackTrace();
		}
		//newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);

	}
	
	public static void VerifyingEchoSap7WithLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et)

	{
		try {
			/*et = logger.createTest("VerifyECHO SAP 7 Navigation",
					"VerifyECHO SAP 7 Navigation Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");*/
			/*ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			
//			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			ScenarioForAcc.page_Wait();
//			newTabOpening(dRDriver);
			
			ScenarioForAcc.page_Wait();
			
			
			System.out.println("Validating Product: ECHOSap7 ");
			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx");
			
			

			/*try {
				try {
					
					
					
					String LoginTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']"))
							.getText();
//					ScenarioForAcc.page_Wait();
					if (EchoSap7.contains("EchoSAP 7")
						) {
						logger.log("pass", " Result for EchoSap7  displayed and"
								+ PurchaseNow + " button exists Without Login", false, dRDriver,
								reportingPath, et);
					} } catch (Exception e) {
						// TODO Auto-generated catch block
//						e.printStackTrace();
						logger.log("fail", "Results fo Echo Sap7 is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
								reportingPath, et);

					}
				
				logger.flush();
				
			}
			 catch (Exception e) {
				
				e.printStackTrace();
			}*/
			
				try {
					
					CapturingPopup(dRDriver);
					String EchoSap7 = dRDriver
							.findElement(
									By.xpath(".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"))
							.getText();
					String PurchaseNow = dRDriver
							.findElement(
									By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']")).getAttribute("value");
					ScenarioForAcc.page_Wait();
					String MyACCTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_rptMyAcc_lnkName_0']"))
							.getText();
					ScenarioForAcc.page_Wait();
					if (EchoSap7.contains("EchoSAP 7")
							&& MyACCTab.contains("My ACC")) {
						logger.log("pass", " Result for EchoSap7  displayed and"
								+'"'+ PurchaseNow +'"'+ " button exists with Login", false, dRDriver,
								reportingPath, et);
					} } catch (Exception e1) {
						// TODO Auto-generated catch block
//						e1.printStackTrace();
						logger.log("fail", "Results fo Echo Sap7 is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
								reportingPath, et);
						
//						LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

					}
				
				
				logger.flush();
				ScenarioForAcc.page_Wait();
			/*}
			catch(Exception e)
			{
				e.printStackTrace();
			}*/
			
		
		
			
		

		} catch (Exception e) {

			e.printStackTrace();
		}
System.out.println("going to close the opened tab");
	//	newTabClosing(dRDriver);
		System.out.println("After closing tab ab");
		
		
	}

	public static void VerifyCVQualityWithoutLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			/*et = logger.createTest("VerifyCVQualityLoginPages",
					"CVQUALITY Test");
*/
			//newTabOpening(dRDriver);
			
			dRDriver.get("http://cvquality.acc.org");
			ScenarioForAcc.page_Wait();
			
			try {
				String NCDRTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_0']"))
						.getText();

				String InitiativesTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_1']"))
						.getText();

				String CommunicationsKitTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_2']"))
						.getText();
				String ClinicalToolTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_3']"))
						.getText();

				
				String LoginTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"))
						.getText();

				if (NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
						&& LoginTab.contains("LOGIN")) {
					logger.log("pass",
							"Successfully redirected to CV quality page and following tabs "
									+'"'+ NCDRTab+'"' + " , " +'"' + InitiativesTab +'"' + " , "
									+'"' + CommunicationsKitTab +'"' + " , "
									+'"' + ClinicalToolTab +'"' + " exists without Logging in", false,
							dRDriver, reportingPath, et);
				}} catch(Exception e) {
					logger.log("fail", "CVQuality page not displayed " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				ScenarioForAcc.page_Wait();
				
			/*try{
			

				String LogoutTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
						.getText();

				if (NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
						&& LogoutTab.contains("LOGOUT")) {
					logger.log("pass",
							"Successfully redirected to CV quality page and following tabs "
									+'"' + NCDRTab +'"' + " , " +'"' + InitiativesTab +'"' + " , "
									+'"' + CommunicationsKitTab +'"' + " , "
									+'"' + ClinicalToolTab +'"' + " exists after Logging in", false,
							dRDriver, reportingPath, et);
				} }
			catch(Exception e){
					logger.log("fail", "CVQuality page not displayed " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			*/

		


		} catch (Exception e) {

			e.printStackTrace();
		}
		

		//newTabClosing(dRDriver);
	}
	
	public static void VerifyCVQualityWithLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			/*et = logger.createTest("VerifyCVQualityLoginPages",
					"CVQUALITY Test");
*/
			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			
			dRDriver.get("http://cvquality.acc.org");
			ScenarioForAcc.page_Wait();
			
			/*try {
				
				
				String LoginTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"))
						.getText();

				if (NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
						&& LoginTab.contains("LOGIN")) {
					logger.log("pass",
							"Successfully redirected to CV quality page and following tabs "
									+'"'+ NCDRTab+'"' + " , " +'"' + InitiativesTab +'"' + " , "
									+'"' + CommunicationsKitTab +'"' + " , "
									+'"' + ClinicalToolTab +'"' + " exists without Logging in", false,
							dRDriver, reportingPath, et);
				}} catch(Exception e) {
					logger.log("fail", "CVQuality page not displayed " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				ScenarioForAcc.page_Wait();
				
				*/
			ScenarioForAcc.page_Wait();
			try{
			

				/*String LogoutTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
						.getText();*/
						
				String NCDRTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_0']"))
						.getText();

				String InitiativesTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_1']"))
						.getText();

				String CommunicationsKitTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_2']"))
						.getText();
				String ClinicalToolTab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnk_3']"))
						.getText();

						WebElement LogoutTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"));

				if (NCDRTab.contains("NCDR")
						&& InitiativesTab.contains("INITIATIVES")
						&& CommunicationsKitTab
								.contains("QI COMMUNICATIONS KIT")
						&& ClinicalToolTab.contains("CLINICAL TOOLKITS")
						&& LogoutTab.isDisplayed()) {
					logger.log("pass",
							"Successfully redirected to CV quality page and following tabs "
									+'"' + NCDRTab +'"' + " , " +'"' + InitiativesTab +'"' + " , "
									+'"' + CommunicationsKitTab +'"' + " , "
									+'"' + ClinicalToolTab +'"' + " exists after Logging in", false,
							dRDriver, reportingPath, et);
				} }
			catch(Exception e){
					logger.log("fail", "CVQuality page not displayed " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			

			ScenarioForAcc.page_Wait();

			


		} 
		catch (Exception e) {

			e.printStackTrace();
		}
		//newTabClosing(dRDriver);
		
	}
	
	public static void logoutForCV(WebDriver dRDriver)
	{
	try {
		dRDriver
		.findElement(
				By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']")).click();
		ScenarioForAcc.page_Wait();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	public static void logoutForEchoSAP7(String reportPath, WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et)
	{
		try
		{ 
	       try {
		dRDriver
		.findElement(
				By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkBtnLogout']")).click();
		ScenarioForAcc.page_Wait();
		
		WebElement action1=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div[2]/p/a"));
		if(action1.isDisplayed())
		{
			System.out.println("Element displayed");
			
			logger.log("pass", "login button exists and clicked on login button", false,
					dRDriver, reportingPath, et);
		}
		}catch (Exception e) {

				//e.printStackTrace();
				logger.log("fail", "Login button is not clicked" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	public static void VerifyACCScientificSessionWithoutLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			/*et = logger.createTest("VerifyAnnualSpecificNavigation",
					"AnnualSpecific Test");*/

			//newTabOpening(dRDriver);

			dRDriver.get("http://accscientificsession.acc.org/acc.aspx");
			ScenarioForAcc.page_Wait(); 
			
			 
			
			

//			try {
				
				
				try
				{
					String RegisterNowLink = dRDriver.findElement(
							By.xpath(".//*[@id='header_nav']/ul/li[7]/a")).getText();
					
					String LoginLink = dRDriver.findElement(
							By.xpath(".//*[@id='login_btn']")).getText();
				
				if (LoginLink.contains("Login")
						&& RegisterNowLink.contains("Register Now")) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+'"' + LoginLink +'"' + " link " +'"' + RegisterNowLink
									+'"' + " link exists without Logging in", false, dRDriver,
							reportingPath, et);
				}
				}catch(Exception e) {
					logger.log("fail", "Annual specific page is not displayed "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				ScenarioForAcc.page_Wait();
			/*} catch (Exception e) {
				
				e.printStackTrace();
			}*/
			
			/*try
			{
				
				
				try{
					String LogoutLink = dRDriver.findElement(
							By.xpath(".//*[@id='logout_btn']")).getText();
			
				if (LogoutLink.contains("Logout")
						&& RegisterNowLink.contains("Register Now")) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+ LogoutLink + " link " + RegisterNowLink
									+ " link exists without Logging in", false, dRDriver,
							reportingPath, et);
				} 
			}catch(Exception e) {
					logger.log("fail", "Annual specific page is not displayed "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			}

			catch(Exception e)
			{
				e.printStackTrace();
			}*/
		
		


		} catch (Exception e) {

			e.printStackTrace();
		}

		
		//newTabClosing(dRDriver);
	}

	
	public static void VerifyACCScientificSessionWithLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			/*et = logger.createTest("VerifyAnnualSpecificNavigation",
					"AnnualSpecific Test");*/

//			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			ScenarioForAcc.page_Wait();
			//newTabOpening(dRDriver);

			dRDriver.get("http://accscientificsession.acc.org/acc.aspx");
			ScenarioForAcc.page_Wait(); 
			
			 
//			 dRDriver.get("http://accscientificsession.acc.org/acc.aspx");
			 
//			ScenarioForAcc.page_Wait();
			

			/*try {
				
				String LoginLink = dRDriver.findElement(
						By.xpath(".//*[@id='login_btn']")).getText();
				try
				{
				
				if (LoginLink.contains("Login")
						&& RegisterNowLink.contains("Register Now")) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+'"' + LoginLink +'"' + " link " +'"' + RegisterNowLink
									+'"' + " link exists without Logging in", false, dRDriver,
							reportingPath, et);
				}
				}catch(Exception e) {
					logger.log("fail", "Annual specific page is not displayed "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			} catch (Exception e) {
				
				e.printStackTrace();
			}*/
			
			/*try
			{
				*/
				
				try{
			
					String RegisterNowLink = dRDriver.findElement(
							By.xpath(".//*[@id='header_nav']/ul/li[7]/a")).getText();
					
					String LogoutLink = dRDriver.findElement(
							By.xpath(".//*[@id='logout_btn']")).getText();
				if (LogoutLink.contains("Logout")
						&& RegisterNowLink.contains("Register Now")) {
					logger.log("pass",
							"Successfully navigated to annual specific page and "
									+'"'+ LogoutLink +'"'+ " link " +'"'+ RegisterNowLink
									+'"'+ " link exists without Logging in", false, dRDriver,
							reportingPath, et);
				} 
			}catch(Exception e) {
					logger.log("fail", "Annual specific page is not displayed "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			

			/*}catch(Exception e)
			{
				e.printStackTrace();
			}*/
			ScenarioForAcc.page_Wait();
			
		


		} catch (Exception e) {

			e.printStackTrace();
		}
		
		//newTabClosing(dRDriver);
		

	}
	public static void VerifyJACCJournalWithoutLogin (String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {

			newTabOpening(dRDriver);
			ScenarioForAcc.page_Wait();
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			ScenarioForAcc.page_Wait();
			CapturingPopup(dRDriver);
             
             /* ScenarioForAcc.page_Wait();
              dRDriver.get("http://onlinejacc.org/journals.aspx");*/
              ScenarioForAcc.page_Wait();
			System.out.println("Validating JACC Journal link");

		
			try {
				String Search = dRDriver
						.findElement(
								By.xpath(".//*[@id='divUmbrellasiteHeader']/div[2]/div[1]/div[2]/div[1]/label"))
						.getText();

				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignIn']"))
						.getText();

				if (Login.contains("LOGIN") && Search.contains("SEARCH")) {
					logger.log("pass",
							"JACC Journal page displayed without Login session", false,
							dRDriver, reportingPath, et);
				} }
			catch(Exception e) {
					logger.log("fail", "JACC Journal page is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				
		/*} catch (Exception e) {
				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']"))
						.getText();

				if (Logout.contains("SIGN OUT") && Search.contains("SEARCH")) {
					logger.log("pass",
							"JACC Journal page displayed with Login", false,
							dRDriver, reportingPath, et);
				} else {
					logger.log("fail", "JACC Journal page is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				e.printStackTrace();
			}*/
			ScenarioForAcc.page_Wait();
		
		} catch (Exception e) {

			e.printStackTrace();
		}

		newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyJACCJournalWithLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {
			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);

			newTabOpening(dRDriver);
			ScenarioForAcc.page_Wait();
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			ScenarioForAcc.page_Wait();
			CapturingPopup(dRDriver);
             
             /* ScenarioForAcc.page_Wait();
              dRDriver.get("http://onlinejacc.org/journals.aspx");*/
              ScenarioForAcc.page_Wait();
			System.out.println("Validating JACC Journal link");

			
			/*try {
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignIn']"))
						.getText();

				if (Login.contains("LOGIN") && Search.contains("SEARCH")) {
					logger.log("pass",
							"JACC Journal page displayed without Login", false,
							dRDriver, reportingPath, et);
				} else {
					logger.log("fail", "JACC Journal page is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				}*/
			 try {
				 
				 String Search = dRDriver
							.findElement(
									By.xpath(".//*[@id='divUmbrellasiteHeader']/div[2]/div[1]/div[2]/div[1]/label"))
							.getText();

				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']"))
						.getText();

				if (Logout.contains("SIGN OUT") && Search.contains("SEARCH")) {
					logger.log("pass",
							"JACC Journal page displayed with Login", false,
							dRDriver, reportingPath, et);
				}
			 }catch(Exception e) {
					logger.log("fail", "JACC Journal page is not displayed"
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			/*}
			 catch(Exception e)catch (Exception e) {
				logger.log("fail", "Navigation to annual diclosure page failed"
						+ "\"" + "\" doesn't exists.", true, dRDriver,
						reportingPath, et);
			 {
				 e.printStackTrace();
			 }*/
			ScenarioForAcc.page_Wait();
		
		
		
		
		} catch(Exception e) {

			e.printStackTrace();
		}
		newTabClosing(dRDriver);
		
		redirectToHomePage(logger, et, dRDriver);
	}
	
	public static void VerifyACCSAP9WithoutLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			/*et = logger.createTest("VerifyACCSAP9Navigation",
					"VerifyACCSAP9Navigation Test");*/

			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			//dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			/*ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			ScenarioForAcc.page_Wait();
			
			
			
			dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			

			System.out.println("Validating ACCSAP 9 link");

			
			

			try {
				
				String Header = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				
				ScenarioForAcc.page_Wait();
				/*dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")).click();
				ScenarioForAcc.page_Wait();*/
				
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				if (Header.contains("ACCSAP 9")
						&& Login.contains("Log in to MyACC")) {
					logger.log("pass", "ACCSAP 9 page displayed without Login",
							false, dRDriver, reportingPath, et);
				}
			}catch(Exception e) {
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			/*} catch (Exception e) {
				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (Header.contains("ACCSAP 9") && Logout.contains("Log Out")) {
					logger.log("pass", "ACCSAP 9 page displayed after Login",
							false, dRDriver, reportingPath, et);
				} else {
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();}*/
			
			ScenarioForAcc.page_Wait();
			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");*/
			
			

		} catch (Exception e) {

			e.printStackTrace();
		}
		
		//newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);

	}
	
	public static void VerifyACCSAP9WithLogin(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {
		try {

			/*et = logger.createTest("VerifyACCSAP9Navigation",
					"VerifyACCSAP9Navigation Test");*/

			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			//dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			/*ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			ScenarioForAcc.page_Wait();
			
			
			
			dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
			

			System.out.println("Validating ACCSAP 9 link");

			
			

			/*try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")).click();
				ScenarioForAcc.page_Wait();
				
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				if (Header.contains("ACCSAP 9")
						&& Login.contains("Log in to MyACC")) {
					logger.log("pass", "ACCSAP 9 page displayed without Login",
							false, dRDriver, reportingPath, et);
				}
			}catch(Exception e) {
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();}*/
			
			 try {
				 
				 String Header = dRDriver.findElement(
							By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
					
					ScenarioForAcc.page_Wait();
				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (Header.contains("ACCSAP 9") && Logout.contains("Log Out")) {
					logger.log("pass", "ACCSAP 9 page displayed after Login",
							false, dRDriver, reportingPath, et);
				} 
			 }catch(Exception e) {
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
			
			ScenarioForAcc.page_Wait();
			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");*/
			
		

		} catch (Exception e) {

			e.printStackTrace();
		}

		//newTabClosing(dRDriver);
	
	}

	public static void RegistrationForMeetingFromEbiz(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {

			/*et = logger.createTest("RegistrationForaMeeting",
					"RegistrationForaMeeting Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			
			ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			
			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			ScenarioForAcc.page_Wait();
			dRDriver.get("https://www.acc.org/education-and-meetings/meetings/meeting-items/2016/03/17/09/04/2017-snowmass");
			ScenarioForAcc.page_Wait();

			System.out.println("Validating Registration link");

			
			/*try {
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				if (Header.contains("Cardiovascular Conference at Snowmass")
						&& Login.contains("Log in to MyACC")) {
					logger.log(
							"pass",
							"Registring for a meeting page displayed without Login",
							false, dRDriver, reportingPath, et);
				} else {
					logger.log("fail",
							"Registring for a meeting page is not displayed"
									+ "\"" + "\" doesn't exists.", true,
							dRDriver, reportingPath, et);

				}
				logger.flush();}*/
			 try {

				 
				 String Header = dRDriver.findElement(
							By.xpath(".//*[@id='main-content']/div/article/h1"))
							.getText();

				String Logout = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (Header.contains("Cardiovascular Conference at Snowmass")
						&& Logout.contains("Log Out")) {
					logger.log("pass", "ACCSAP 9 page displayed after Login",
							false, dRDriver, reportingPath, et);
				} 
			 }catch(Exception e) {
					logger.log("fail", "ACCSAP 9 page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
//			}
			ScenarioForAcc.page_Wait();

			
			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='accordion']/article[4]/div[1]/h1/a/span"))
						.click();
				ScenarioForAcc.page_Wait();
				String Register = dRDriver.findElement(
						By.xpath(".//*[@id='box3']/p[3]")).getText();
				if (Register.contains("click here")) {
					logger.log("pass", "Registration link is opened with Login",
							false, dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Registration link is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			
			logger.flush();

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");*/
			
			/*dRDriver.close();
		    dRDriver.switchTo().window(tabs2.get(0));*/
			

		} catch (Exception e) {

			e.printStackTrace();
		}

	
		//newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyACCSAP9OL(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {

			/*et = logger.createTest("Verifying ACCSAP 9 Link",
					"Verifying ACCSAP 9 Link Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			
			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			
			ScenarioForAcc.page_Wait();
			dRDriver.get("http://edu.acc.org/diweb/catalog/item/eid/ACCSAP9OL");
			ScenarioForAcc.page_Wait();

			System.out.println("Validating ACCSAP 9 link");

		 try {
			WebElement BuyNow = dRDriver.findElement(By.xpath(".//*[@id='id4']"));
						

				if (BuyNow.isDisplayed()) {
					logger.log("pass",
							"ACCSAP 9 link page displayed without Login", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e1) {
					// TODO Auto-generated catch block
//					e1.printStackTrace();
					logger.log("fail", "ACCSAP 9 link page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

		
		}
			logger.flush();
			ScenarioForAcc.page_Wait();
			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='id4']")).click();

				ScenarioForAcc.page_Wait();
				WebElement ContinueShopping = dRDriver
						.findElement(
								By.xpath(".//*[@id='main_0_ctl00_ButtonContinueShopping']"));
						

				WebElement Logout = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div/p/a"));
						
				if (Logout.isDisplayed()
						&& ContinueShopping.isDisplayed()) {
					logger.log("pass", "Shopping page is opened with Login", false,
							dRDriver, reportingPath, et);
				} 
			}catch(Exception e) {
				e.printStackTrace();
					logger.log("fail", "Shopping page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				ScenarioForAcc.page_Wait();
			/*} catch (Exception e) {
				String LoginHeader = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

				// String Login =
				// dRDriver.findElement(By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")).getText();
				if (LoginHeader.contains("Login")) {
					logger.log("pass", "Shopping page is not opened for opening without login", false,
							dRDriver, reportingPath, et);
				} else {
					logger.log("fail", "Login page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();}*/
			

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
*//*dRDriver.close();
dRDriver.switchTo().window(tabs2.get(0));*/
			
		} catch (Exception e) {

			e.printStackTrace();
		}

		//newTabClosing(dRDriver);
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyECCOA2(String reportPath, WebDriver dRDriver,
			com.gallop.Logger logger, String reportingPath, ExtentTest et) {

		try {

			/*et = logger.createTest("Verifying ECCOA2 Link",
					"Verifying ECCOA2 Link Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			
			ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			
			ScenarioForAcc.page_Wait();
			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2014/ECCOA2.aspx");
			ScenarioForAcc.page_Wait();

			System.out.println("Validating ECCOA2 link");
         
			
                
                	
                try{
                	
                	String StartProgram = dRDriver
    						.findElement(
    								By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_StartLink']"))
    						.getText();
                	
				if (StartProgram.contains("Start Program")) {
					logger.log("pass", "ECCOA2 link page displayed and "+'"'+ StartProgram +'"'+"is displayed" ,
							false, dRDriver, reportingPath, et);
				} } catch (Exception e1) {
					// TODO Auto-generated catch block
//					e1.printStackTrace();
					logger.log("fail", "ECCOA2 link page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
		
			logger.flush();
			ScenarioForAcc.page_Wait();
			
			
			
				
				try{
					dRDriver.findElement(
							By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_StartLink']"))
							.click();

					ScenarioForAcc.page_Wait();
					/*dRDriver.switchTo().frame(
							dRDriver.findElement(By.xpath(".//*[@id='form1']")));*/
					dRDriver.findElement(By.tagName("body")).sendKeys(Keys.ESCAPE);
					//dRDriver.switchTo().frame("_fsControlFrame");
					
//					dRDriver.switchTo().frame(dRDriver.findElement(By.id("form1")));
					
					ScenarioForAcc.page_Wait();
					
					if(dRDriver.findElements(By.xpath("//a[@id='fancybox-close']")).size()>0 && dRDriver.findElements(By.xpath("//a[@id='fancybox-close']")).get(0).isDisplayed()){
						dRDriver.findElements(By.xpath("//a[@id='fancybox-close']")).get(0).click();
						ScenarioForAcc.page_Wait();
					}
					
				 WebElement ActivcationPage = dRDriver.findElement(
						By.xpath(".//*[@id='header']"));

				/*WebElement ReqInformation = dRDriver
						.findElement(By
								.xpath(".//*[@id='content_0_divEulaInstruction']/span/strong"));*/
				if (ActivcationPage.isDisplayed()
						) {
					logger.log("pass",
							"Activation page is opened without Login", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					logger.log("fail", "Activation page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);
e.printStackTrace();
				}
				logger.flush();
				
         
				/*try{
					String LoginHeader = dRDriver.findElement(
							By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();

					String Login = dRDriver.findElement(
							By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
							.getText();
					if (Login.contains("Login")
							&& LoginHeader.contains("Log in to MyACC")) {
						logger.log("pass", "Login page is opened ", false,
								dRDriver, reportingPath, et);
					} } catch (Exception e1) {
						// TODO Auto-generated catch block
//						e1.printStackTrace();
						logger.log("fail", "Login page is not opened " + "\""
								+ "\" doesn't exists.", true, dRDriver,
								reportingPath, et);

					}
				
				
				logger.flush();*/
			
			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");
*//*dRDriver.findElement(By.xpath("//body")).sendKeys(
		Keys.CONTROL + "w");*/
			
	      
			/*dRDriver.close();
			dRDriver.switchTo().window(tabs2.get(0));
	*/ 
		} catch (Exception e) {

			e.printStackTrace();
		}
		//newTabClosing(dRDriver);
		
		redirectToHomePage(logger, et, dRDriver);

	}

	public static void VerifyingPurchaseLink(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*
			 * VerifyingEchoSap7(reportPath, dRDriver, logger, reportingPath,
			 * et); ScenarioForAcc.page_Wait();
			 */
			/*et = logger.createTest("VerifyECHO SAP 7 Navigation Without Login",
					"VerifyECHO SAP 7 Navigation Without Login Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			
			ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			ScenarioForAcc.page_Wait();
			//LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			
			//newTabOpening(dRDriver);
			ScenarioForAcc.page_Wait();

			dRDriver.get("http://education.acc.org/Lifelong-Learning-and-MOC/Certified-Learning/SAPs/2013/EchoSAP-7.aspx");

			ScenarioForAcc.page_Wait();
			System.out.println("Validating Product: ECHOSap7 ");
			

			/*try {
				String LoginTab = dRDriver
						.findElement(
								By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_lnkLogin']"))
						.getText();
				try {
					
					ScenarioForAcc.page_Wait();
					if (EchoSap7.contains("EchoSAP 7")
							&& LoginTab.contains("Log in to MyACC")) {
						logger.log("pass", " Result for EchoSap7  displayed and"
								+ PurchaseNow + " button exists", false, dRDriver,
								reportingPath, et);
					} } catch (Exception e) {
						// TODO Auto-generated catch block
//						e.printStackTrace();
						logger.log("fail", "Results fo Echo Sap7 is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
								reportingPath, et);

				}
				logger.flush();
			}
			 try {*/
				
			
				try {
					String EchoSap7 = dRDriver
							.findElement(
									By.xpath(".//*[@id='content_0_innercontent_0_pnlHeader']/div/div[1]"))
							.getText();
					String PurchaseNow = dRDriver
							.findElement(
									By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
							.getText();
					
					String MyACCTab = dRDriver
							.findElement(
									By.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_rptMyAcc_lnkName_0']"))
							.getText();
				
					
					ScenarioForAcc.page_Wait();
					if (EchoSap7.contains("EchoSAP 7")
							&& MyACCTab.contains("My ACC")) {
						logger.log("pass", " Result for EchoSap7  displayed and"
								+ PurchaseNow + " button exists", false, dRDriver,
								reportingPath, et);
					} } catch (Exception e1) {
						// TODO Auto-generated catch block
//						e1.printStackTrace();
						logger.log("fail", "Results fo Echo Sap7 is not displayed"
								+ "\"" + "\" doesn't exists.", true, dRDriver,
								reportingPath, et);

					}
				
		
				logger.flush();

				ScenarioForAcc.page_Wait();
//			}
			
			
//               try{
            	   try{
               
            		   dRDriver.findElement(
           					By.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnPurchaseLink']"))
           					.click();

           				
                          ScenarioForAcc.page_Wait();
				WebElement shoppingLink = dRDriver
						.findElement(By
								.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnContinueShopping']"));
				WebElement GotoCart = dRDriver
						.findElement(By
								.xpath(".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_btnGotoCart']"));

				if (shoppingLink.isDisplayed() && GotoCart.isDisplayed()) {
					logger.log("pass",
							"Shopping cart page is opened without Login",
							false, dRDriver, reportingPath, et);
				}} catch (Exception e) {
					logger.log("fail", "Shopping cart page is not opened "
							+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
              /* }
			catch(Exception e)
			{
				try{
				String LoginHeader = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1")).getText();
				ScenarioForAcc.page_Wait();
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
						.getText();
				if (Login.contains("Login")
						&& LoginHeader.contains("Log in to MyACC")) {
					logger.log("pass", "Login page is opened ", false,
							dRDriver, reportingPath, et);
				}}
				catch(Exception e1){
					logger.log("fail", "Login page is not opened " + "\""
							+ "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
				logger.flush();
				
				
			
			
			}*/
               
             /*  dRDriver.close();
   			dRDriver.switchTo().window(tabs2.get(0));*/
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//newTabClosing(dRDriver);
		
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyingEbizforMeetingAndProductId(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {

			/*et = logger.createTest("Verifying Ebiz for meeting Link",
					"Verifying Ebiz meeting and product Id Link Test");*/

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
			
			ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
			dRDriver.switchTo().window(tabs2.get(1));*/
			
//			LoggerForHomepageDisplayWithLogin(logger, dRDriver, et);
			//newTabOpening(dRDriver);
			
			ScenarioForAcc.page_Wait();
			dRDriver.get("https://ebiz.acc.org/Meetings?ProductId=41118240");
			ScenarioForAcc.page_Wait();

			System.out.println("Validating Ebiz meeting and product Id link");

			try {
				 WebElement Header = dRDriver
						.findElement(
								By.xpath(".//*[@id='main_0_ctl00_cMtgRegWizContactInfo_MtgRegWizHeader_lblMtgName']"));
						
				 String Logout = dRDriver
							.findElement(
									By.xpath(".//*[@id='main_0_ctl00_cMtgRegWizContactInfo_MtgRegWizHeader_lblMtgName']")).getText();

				if (Header.isDisplayed()) {
					logger.log("pass",
							"Ebiz meeting and product Id link page displayed with Login session and"+ '"'+ Logout +'"'+"link exists ",
							false, dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail",
							"Ebiz meeting and product Id link is not displayed"
									+ "\"" + "\" doesn't exists.", true, dRDriver,
							reportingPath, et);

				}
			
			
			logger.flush();
			/*
			 * ScenarioForAcc.page_Wait(); dRDriver.findElement(By.xpath(
			 * ".//*[@id='content_0_innercontent_0_innercontent_0_maincolumn_0_StartLink']"
			 * )).click();
			 * 
			 * ScenarioForAcc.page_Wait();
			 * 
			 * String ActivcationPage=dRDriver.findElement(By.xpath(
			 * ".//*[@id='header']/div")).getText();
			 * 
			 * WebElement ReqInformation = dRDriver.findElement(By.xpath(
			 * ".//*[@id='content_0_divEulaInstruction']/span/strong")); if
			 * (ActivcationPage.contains("Activity") &&
			 * ReqInformation.isDisplayed()) { logger.log("pass",
			 * "Activation page is opened without Login" , false, dRDriver,
			 * reportingPath, et); } else { logger.log("fail",
			 * "Activation page is not opened " + "\"" + "\" doesn't exists.",
			 * true, dRDriver, reportingPath, et);
			 * 
			 * } logger.flush();
			 */

			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "w");*/
			
			/*dRDriver.close();
			dRDriver.switchTo().window(tabs2.get(0));
*/
			
			ScenarioForAcc.page_Wait();
		} catch (Exception e) {

			e.printStackTrace();
		}

		//newTabClosing(dRDriver);
		
		redirectToHomePage(logger, et, dRDriver);
	}

	public static void VerifyingBasicLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et)
	{
		try {
			et = logger.createTest("VerifyingBasicLogin", "VerifyingBasicLogin Test");
			System.out.println("Scenario1");
			
			
			System.out.println("drDriver is "+dRDriver);
			
			BasicLoginFeature(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

			ScenarioForAcc.page_Wait();
           BasicLoginFeatureForEchoSAP7(reportPath, dRDriver, logger, reportingPath, et);
           
           VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath,
					et);
			
           LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
			/*VerifyingEchoSap7WithoutLogin(reportPath, dRDriver, logger, reportingPath,
					et);*/
            verifyLoginForCVQuality(reportPath, dRDriver, logger, reportingPath, et);
			
            VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath, et);
            
            LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

            VerifyCVQualityWithoutLogin(reportPath, dRDriver, logger, reportingPath, et);
			
			 verifyLoginForACCScientific(reportPath, dRDriver, logger, reportingPath, et);
			 
			 VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
						reportingPath, et);
			 
			 LogoutFeatureForACCScientific(reportPath, dRDriver, logger, reportingPath, et);

			/* verifyLoginForJACCJournal(reportPath, dRDriver, logger, reportingPath, et);
			 VerifyJACCJournalWithLogin(reportPath, dRDriver, logger, reportingPath,
						et);
			 LogoutFeatureForJACCJournal(reportPath, dRDriver, logger, reportingPath, et);*/


			verifyLoginForACCSAP9(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait();
			VerifyACCSAP9WithLogin(reportPath, dRDriver, logger, reportingPath, et);
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

			
			BasicLoginFeature(reportPath, dRDriver, logger, reportingPath, et);
			verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait();
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void VerifyingBasicSSOConnection(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			System.out.println("Scenario2");
			
			et = logger.createTest("VerifyingBasicSSOConnection", "VerifyingBasicSSOConnection Test");
			
			BasicLoginFeature(reportPath, dRDriver, logger, reportingPath, et);

			ScenarioForAcc.page_Wait();
			
			
			
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath,
					et);
			
			verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
			
			VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath, et);

			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);

		/*VerifyJACCJournalWithLogin(reportPath, dRDriver, logger, reportingPath,
					et);*/

			VerifyACCSAP9WithLogin(reportPath, dRDriver, logger, reportingPath, et);

			RegistrationForMeetingFromEbiz(reportPath, dRDriver, logger,
					reportingPath, et);

			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static WebDriver VerifyRememberMeFunctionlity(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		try {
			ScenarioForAcc.page_Wait();
			RememberMeFeatureForNonMember(reportingPath, dRDriver, logger, reportingPath,
					et);
			/*et = logger.createTest("Verifying RememberMe Functionality",
					"Verifying RememberMe Functionality");*/
			/*dRDriver.findElement(By.xpath("//body")).sendKeys(
					Keys.CONTROL + "t");
*/
			ScenarioForAcc.page_Wait();
			dRDriver.get("http://author.education.acc.org/sitecore/login");
			ScenarioForAcc.page_Wait();

			
			
			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");

			/*String logoutLink = dRDriver.findElement(
					By.xpath(".//*[@id='header-personal']/p/a")).getText();

			if (logoutLink.contains("Log Out")) {
				logger.log("pass", logoutLink
						+ " Link is displayed and user still logged in", false,
						dRDriver, reportingPath, et);
			} else {
				logger.log("fail", "Login is not successfull" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();*/
			
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Login session exits after navigating to other website", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				
			}
			logger.flush();

			ScenarioForAcc.page_Wait();
//			Set<Cookie> cookies1= dRDriver.manage().getCookies();
			//dRDriver.manage().timeouts().implicitlyWait(30000, TimeUnit.SECONDS);
			ScenarioForAcc.page_Wait();
			
			Set<Cookie> allCookies = null;
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				allCookies = dRDriver.manage().getCookies();
			}
			
			//Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();

			
//			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
//			dRDriver = OpenFFBrowser(dRDriver, capabilities);
			/*dRDriver= new FirefoxDriver();
			for (Cookie cookie : cookies1) {
				dRDriver.manage().addCookie(cookie);
			}*/
			
			
			
			if(dRDriver.equals(driverFF))
			{
				dRDriver=OpenFirefoxBrowser(dRDriver);
			}
			else if(dRDriver.equals(driverChrome))
			{
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else if(dRDriver.equals(driverInternet)){
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}
			
			
			dRDriver.get("http://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			dRDriver.manage().window().maximize();
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				for(Cookie cookie : allCookies) {
				dRDriver.manage().addCookie(cookie);
	        }
			}
			
			/*for(Cookie cookie : allCookies) {
				dRDriver.manage().addCookie(cookie);
	        }*/
			//cookieManager();
			Thread.sleep(2000);
			dRDriver.navigate().refresh();
//			dRDriver.manage().window().maximize();

//			String ChildWindow = dRDriver.getWindowHandle();

			// String
			// logoutLink=dRDriver.findElement(By.xpath(".//*[@id='header-personal']/p/a")).getText();
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Login session exits after nclosing and reopening the browser", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				
			}
			logger.flush();

			// dRDriver.switchTo().window(ChildWindow);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static WebDriver VerifyRememberMeFunctionlityonACC(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		
		et = logger.createTest("VerifyRememberMeFunctionlityonACC",
				"VerifyRememberMeFunctionlityonACC");
		dRDriver = VerifyRememberMeFunctionlity(reportPath, dRDriver,
				logger, reportingPath, et);

		VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath, et);
		VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath, et);

//		VerifyJACCJournalWithLogin(reportPath, dRDriver, logger, reportingPath, et);
		VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
		VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger, reportingPath,
				et);
		verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
		VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger, reportingPath, et);
		LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
		return dRDriver;

	}

	public static WebDriver VerifyRememberMeFunctionalityIfUserVisitsLLP(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {
			System.out.println("Scenario3");
			
			et = logger.createTest("VerifyRememberMeFunctionalityIfUserVisitsLLP", "VerifyRememberMeFunctionalityIfUserVisitsLLP Test");
			
			RememberMeFeature(reportingPath, dRDriver, logger, reportingPath,
					et);

			 VerifyingPurchaseLink(reportingPath, dRDriver, logger,
			 reportingPath, et);

			// ScenarioForAcc.page_Wait();
			ScenarioForAcc.page_Wait();
			dRDriver.get("http://author.education.acc.org/sitecore/login");
			ScenarioForAcc.page_Wait();

			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");

			ScenarioForAcc.page_Wait();
			
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Login session exits after navigating to other website", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				
			}
			logger.flush();
			
			Set<Cookie> allCookies = null;
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				allCookies = dRDriver.manage().getCookies();
			}
			
			//Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
			ScenarioForAcc.page_Wait();
		//dRDriver = OpenChrmBrowser(dRDriver,  capabilities);
			
			
			
			if(dRDriver.equals(driverFF))
			{
				dRDriver=OpenFirefoxBrowser(dRDriver);
			}
			else if(dRDriver.equals(driverChrome))
			{
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			}else if(dRDriver.equals(driverInternet)){
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}
			
			dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
			dRDriver.manage().window().maximize();
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				for(Cookie cookie : allCookies) {
					dRDriver.manage().addCookie(cookie);
		        }
			}
			
			
			//cookieManager();
			Thread.sleep(2000);
			dRDriver.navigate().refresh();
			
			ScenarioForAcc.page_Wait();
			
			
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Login session exits after closing and reopening the browser", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

	
			}
			logger.flush();
			
          VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
//			VerifyJACCJournalWithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
			verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
			VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger, reportingPath, et);
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;
	}

	public static WebDriver VerifyRememberMeDoesNotBreakAccesstoLLPProducts(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et)

	{
		
		et = logger.createTest("VerifyRememberMeDoesNotBreakAccesstoLLPProducts",
				"VerifyRememberMeDoesNotBreakAccesstoLLPProducts");
		
		dRDriver = VerifyRememberMeFunctionlity(reportPath, dRDriver, logger,
				reportingPath, et);

		VerifyECCOA2(reportPath, dRDriver, logger, reportingPath, et);

      	VerifyingPurchaseLink(reportPath, dRDriver, logger, reportingPath, et);
		LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
		return dRDriver; 
	}

	public static WebDriver VerifyRememberMeFunctionalityonFederatedLogin(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {
		try {

			/*et = logger.createTest("VerifyingFederatedLogin",
					"VerifyingFederatedLogin Test");*/
			dRDriver.get("http://auth.acc.org/ACCFederatedLogin/Login?SP=https://sp.silverchair.com/jacc/production/shibboleth&src=Silverchair&targeturl=/journals.aspx");

			ScenarioForAcc.page_Wait();
			ScenarioForAcc.page_Wait();

			try
			{
			// dRDriver.findElement(By.xpath(".//*[@id='globalHeader_lbSignIn']")).click();
			dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
			ScenarioForAcc.page_Wait();
			// driver.findElement(By.id("UserName")).clear();

			// driver.findElement(By.id("UserName")).sendKeys(vUsername);
			String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

			dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
					UserNameForMembership);

			String username = dRDriver.findElement(
					By.xpath("//*[@id='UserName']")).getAttribute("value");

			System.out.println(username);
			ScenarioForAcc.page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "UserName"+'"'+ username +'"'+ "successfully entered", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			ScenarioForAcc.page_Wait();
			try
			{
			dRDriver.findElement(By.id("Password")).click();
			ScenarioForAcc.page_Wait();
			String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

			// driver.findElement(By.id("Password")).sendKeys(vPassword);

			dRDriver.findElement(By.id("Password")).sendKeys(
					PasswordForMembership);

			String password = dRDriver.findElement(By.id("Password"))
					.getAttribute("value");
			if (!password.isEmpty()) {
				logger.log("pass", "Password"+'"'+ password+'"'+" successfully entered", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			

			// page_Wait();

			// CapturingPopup(dRDriver);
		
			ScenarioForAcc.page_Wait();
			try
			{
			dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			.click();

	ScenarioForAcc.page_Wait();
	dRDriver.findElement(By.xpath(".//*[@id='formLogin']/input[3]"))
			.click();

	// et=logger.createTest("Login", "Login successful");
	ScenarioForAcc.page_Wait();
	System.out.println("Successfully logged in to ACC site");
	ScenarioForAcc.page_Wait();
	AlertForFederatedLogin();
			WebElement logoforJacc = dRDriver.findElement(By
					.xpath(".//*[@id='siteLogo']"));
			// String
			// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
			if (logoforJacc.isDisplayed()) {
				logger.log("pass", "Successfully logged into JACC", false,
						dRDriver, reportingPath, et);
			} }
			catch(Exception e){
				logger.log("fail", "Login failed for JACC" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			}
			logger.flush();
			ScenarioForAcc.page_Wait();

			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']")).getText();

				if (logoutLink.contains("SIGN OUT")) {
					logger.log("pass", logoutLink + " Link is displayed", false,
							dRDriver, reportingPath, et);
				} 	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
		
			
			logger.flush();

			ScenarioForAcc.page_Wait();
			dRDriver.get("http://author.education.acc.org/sitecore/login");

			ScenarioForAcc.page_Wait();
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			ScenarioForAcc.page_Wait();
			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']")).getText();

				if (logoutLink.contains("SIGN OUT")) {
					logger.log("pass","The "+'"'+ logoutLink +'"'+ " Link is displayed", false,
							dRDriver, reportingPath, et);
				} 	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Login is not successfull after navigating to other website" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
		
			
			logger.flush();
			ScenarioForAcc.page_Wait();
			dRDriver.close();
			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			dRDriver.get("http://onlinejacc.org/journals.aspx");
			try {
				String logoutLink = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignOut']")).getText();

				if (logoutLink.contains("SIGN OUT")) {
					logger.log("pass","The" +'"'+ logoutLink+'"'+ " Link is displayed after closing and reopening the browser", false,
							dRDriver, reportingPath, et);
				} 	} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login is not successfull" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
		
			
			logger.flush();
			ScenarioForAcc.page_Wait();
			dRDriver.close();
			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			dRDriver.get("http://www.acc.org");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static WebDriver scenariosForFederatedLogin(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {
		et = logger.createTest("scenariosForFederatedLogin", "scenariosForFederatedLogin Test");
		
		dRDriver = VerifyRememberMeFunctionalityonFederatedLogin(reportingPath,
				dRDriver, logger, reportingPath, et);
		
		VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath, et);
		VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath, et);
		VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger, reportingPath,
				et);
		verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
		VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
		
		VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger, reportingPath, et);
     LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);
	return dRDriver;
	}

	public static WebDriver VerifyRememberMeFunctionalityonScientificSession(
			String reportPath, WebDriver dRDriver, com.gallop.Logger logger,
			String reportingPath, ExtentTest et) {

		try {
			
			et = logger.createTest("VerifyRememberMeFunctionalityonScientificSession", "Login Test");
			
			dRDriver.get("https://accscientificsession.acc.org/");

			ScenarioForAcc.page_Wait();
			try {
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='login_btn']"));
				 WebElement MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='header_nav']/ul/li[7]/a"));
				 
				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

			
		
			}
			logger.flush();
			ScenarioForAcc.page_Wait();

//			 dRDriver.findElement(By.xpath(".//*[@id='login_btn']")).click();
			try
			{
			dRDriver.findElement(By.xpath(".//*[@id='username']")).click();
			// driver.findElement(By.id("UserName")).clear();
			ScenarioForAcc.page_Wait();
			// driver.findElement(By.id("UserName")).sendKeys(vUsername);
			String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

			dRDriver.findElement(By.xpath(".//*[@id='username']")).sendKeys(
					UserNameForMembership);

			String username = dRDriver.findElement(
					By.xpath(".//*[@id='username']")).getAttribute("value");

			System.out.println(username);
			ScenarioForAcc.page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "UserName" +'"'+ username+'"'+" successfully entered", false,
						dRDriver, reportingPath, et);
			} }
			catch(Exception e){
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();

			ScenarioForAcc.page_Wait();
			
			try
			{
			dRDriver.findElement(By.id("password")).click();
			ScenarioForAcc.page_Wait();
			String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

			// driver.findElement(By.id("Password")).sendKeys(vPassword);

			dRDriver.findElement(By.id("password")).sendKeys(
					PasswordForMembership);

			String password = dRDriver.findElement(By.id("password"))
					.getAttribute("value");
			if (!password.isEmpty()) {
				logger.log("pass", "Password"+'"'+ password+'"'+" successfully entered", false,
						dRDriver, reportingPath, et);
			} 
			}catch(Exception e) {
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
			logger.flush();
			ScenarioForAcc.page_Wait();

			// CapturingPopup(dRDriver);
			try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='login_form']/div/div[4]/label"))
						.click();

				ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath(".//*[@id='login_btn']")).click();
				ScenarioForAcc.page_Wait();
				// et=logger.createTest("Login", "Login successful");

				System.out.println("Successfully logged in to ACC site");

				ScenarioForAcc.page_Wait();
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='logout_btn']"));
				 WebElement MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='login_link']"));
				 
				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

			
		
			}
			logger.flush();
			ScenarioForAcc.page_Wait();

			dRDriver.get("http://author.education.acc.org/sitecore/login");

			ScenarioForAcc.page_Wait();
			dRDriver.get("https://accscientificsession.acc.org/");
			ScenarioForAcc.page_Wait();

			
			try {
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='logout_btn']"));
				 WebElement MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='login_link']"));
				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC after navigating to other website", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

			
		
			}
			logger.flush();
			dRDriver.get("https://accscientificsession.acc.org/");
			ScenarioForAcc.page_Wait();
			///######
			
			Set<Cookie> allCookies = null;
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				allCookies = dRDriver.manage().getCookies();
			}
			
			//Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
			
			if(dRDriver.equals(driverFF))
			{
				dRDriver=OpenFirefoxBrowser(dRDriver);
			}
			else if(dRDriver.equals(driverChrome))
			{
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else if(dRDriver.equals(driverInternet)){
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}
			
			//dRDriver = OpenChrmBrowser(dRDriver, capabilities);
//			dRDriver = OpenFFBrowser(dRDriver, capabilities);
			dRDriver.get("https://accscientificsession.acc.org/");
			dRDriver.manage().window().maximize();
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				for(Cookie cookie : allCookies) {
					dRDriver.manage().addCookie(cookie);
		        }
			}
			
			
			//cookieManager();
			Thread.sleep(2000);
			dRDriver.navigate().refresh();
			
			try {
				WebElement logout = dRDriver.findElement(By
						.xpath(".//*[@id='logout_btn']"));
				 WebElement MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='login_link']"));
				if (logout.isDisplayed() && MyAcc.isDisplayed()) {
					logger.log("pass", "Successfully logged into ACC after closing and reopening the browser", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

			
		
			}
			logger.flush();
			dRDriver.get("http://www.acc.org");
			ScenarioForAcc.page_Wait();
			
			allCookies = null;
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				allCookies = dRDriver.manage().getCookies();
			}
			
			//allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
//			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			
			if(dRDriver.equals(driverFF))
			{
				dRDriver=OpenFirefoxBrowser(dRDriver);
			}
			else if(dRDriver.equals(driverChrome))
			{
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else if(dRDriver.equals(driverInternet)){
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}
			
			//dRDriver = OpenFFBrowser(dRDriver,capabilities);
			dRDriver.get("http://www.acc.org");
			dRDriver.manage().window().maximize();
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				for(Cookie cookie : allCookies) {
					dRDriver.manage().addCookie(cookie);
		        }
			}
			
			
			//cookieManager();
			Thread.sleep(2000);
			dRDriver.navigate().refresh();
			
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Login session exits after closing and reopening the browser of other website", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				
			}
			logger.flush();
			ScenarioForAcc.page_Wait();

			VerifyCVQualityWithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
			VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
			VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger, reportingPath, et);
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static WebDriver VerifyRememberMeFunctionalityonQII(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			et = logger.createTest("VerifyRememberMeFunctionalityonQII", "Login Test");
			
			dRDriver.get("http://cvquality.acc.org/Login");
			
			ScenarioForAcc.page_Wait();

			try {
				
				 WebElement LogIn = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"));
				if (LogIn.isDisplayed()) {
					logger.log("pass", "Homepage displayed for CVQuality", false,
							dRDriver, reportingPath, et);
				} 	} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Homepage not displayed for CVQuality" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
		
			logger.flush();
			
			ScenarioForAcc.page_Wait();

			

			try {
				dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"))
						.click();
				ScenarioForAcc.page_Wait();

				dRDriver.findElement(By.xpath(".//*[@id='content_0_txtUsername']"))
						.click();
				// driver.findElement(By.id("UserName")).clear();
				ScenarioForAcc.page_Wait();
				// driver.findElement(By.id("UserName")).sendKeys(vUsername);
				String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

				dRDriver.findElement(By.xpath(".//*[@id='content_0_txtUsername']"))
						.sendKeys(UserNameForMembership);
				
				String username = dRDriver.findElement(
						By.xpath(".//*[@id='content_0_txtUsername']"))
						.getAttribute("value");

				System.out.println(username);
				ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName"+'"'+ username+'"'+" successfully entered", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			
		
			logger.flush();

			ScenarioForAcc.page_Wait();
			try {
				dRDriver.findElement(By.xpath(".//*[@id='content_0_txtPassword']"))
				.click();
		ScenarioForAcc.page_Wait();
		String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

		// driver.findElement(By.id("Password")).sendKeys(vPassword);

		dRDriver.findElement(By.xpath(".//*[@id='content_0_txtPassword']"))
				.sendKeys(PasswordForMembership);

				String password = dRDriver.findElement(
						By.xpath(".//*[@id='content_0_txtPassword']"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password"+'"'+ password+'"'+" successfully entered", false,
							dRDriver, reportingPath, et);
				}} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();

			ScenarioForAcc.page_Wait();

			// page_Wait();

			// CapturingPopup(dRDriver);
			try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='content_0_chbxRememberMe']")).click();

				ScenarioForAcc.page_Wait();
				dRDriver.findElement(By.xpath(".//*[@id='content_0_btnLogin']"))
						.click();

				ScenarioForAcc.page_Wait();
				// et=logger.createTest("Login", "Login successful");

				System.out.println("Successfully logged in to CV Quality site");
				String MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/span")).getText();
				String LogOut = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (LogOut.contains("Log Out") && MyAcc.contains("My ACC")) {
					logger.log("pass", "Successfully logged into ACC", false,
							dRDriver, reportingPath, et);
				} 	} catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
		
			logger.flush();
			
//			verifyLoginForCVQuality(reportPath, dRDriver, logger, reportingPath, et);
			ScenarioForAcc.page_Wait();
			ScenarioForAcc.page_Wait();

			dRDriver.get("http://cvquality.acc.org/Login");

			try {
				String LogOutLink = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
						.getText();

				if (LogOutLink.contains("LOGOUT")) {
					logger.log("pass", "CV Quality page displayed", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "CV Quality page displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			
			
			logger.flush();
			ScenarioForAcc.page_Wait();

			dRDriver.get("http://author.education.acc.org/sitecore/login");

			ScenarioForAcc.page_Wait();
			dRDriver.get("http://cvquality.acc.org/Login");
			ScenarioForAcc.page_Wait();

			try {
				String LogOutLink = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
						.getText();

				if (LogOutLink.contains("LOGOUT")) {
					logger.log("pass", "CV Quality page displayed and loggin session continues after navigatng to other website", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "CV Quality page displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			
			
			logger.flush();
			ScenarioForAcc.page_Wait();
			///////////////////
			
			Set<Cookie> allCookies = null;
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				allCookies = dRDriver.manage().getCookies();
			}
			
			//Set<Cookie> allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
	//		dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			
			if(dRDriver.equals(driverFF))
			{
				dRDriver=OpenFirefoxBrowser(dRDriver);
			}
			else if(dRDriver.equals(driverChrome))
			{
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else if(dRDriver.equals(driverInternet)){
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}
			
			dRDriver.get("http://cvquality.acc.org/Login");
			dRDriver.manage().window().maximize();
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				for(Cookie cookie : allCookies) {
					dRDriver.manage().addCookie(cookie);
		        }
			}
			
			
			//cookieManager();
			Thread.sleep(2000);
			dRDriver.navigate().refresh();
			
			/////
			
			
			
			
			try {
				String LogOutLink = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_lnkBtnLogout']"))
						.getText();

				if (LogOutLink.contains("LOGOUT")) {
					logger.log("pass", "CV Quality page displayed and loggin session continues after closing and reopening the browser", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "CV Quality page displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			dRDriver.get("http://www.acc.org");
			
			logger.flush();
			ScenarioForAcc.page_Wait();
			
			allCookies = null;
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				allCookies = dRDriver.manage().getCookies();
			}
			
			//allCookies = dRDriver.manage().getCookies();
			dRDriver.close();
//			dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			//dRDriver = OpenFFBrowser(dRDriver, capabilities);
			
			if(dRDriver.equals(driverFF))
			{
				dRDriver=OpenFirefoxBrowser(dRDriver);
			}
			else if(dRDriver.equals(driverChrome))
			{
				dRDriver = OpenChrmBrowser(dRDriver, capabilities);
			} else if(dRDriver.equals(driverInternet)){
				dRDriver = OpenIEBrowser(dRDriver, capabilities);
			}
			
			dRDriver.get("http://www.acc.org");
			dRDriver.manage().window().maximize();
			
			if(dRDriver.equals(driverFF) || dRDriver.equals(driverChrome))
			{
				for(Cookie cookie : allCookies) {
					dRDriver.manage().addCookie(cookie);
		        }
			}
			
			
			//cookieManager();
			Thread.sleep(2000);
			dRDriver.navigate().refresh();
			
			try {
				WebElement logoforacc = dRDriver.findElement(By
						.xpath(".//*[@id='nav-myacc']/li[1]/span"));
				// String
				// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
				if (logoforacc.isDisplayed()) {
					logger.log("pass", "Login session exits after closing and reopening the browser of other website", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				
			}
			logger.flush();
			ScenarioForAcc.page_Wait();
			

//			 VerifyCVQuality(reportPath, dRDriver, logger, reportingPath, et);
			VerifyingEchoSap7WithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyACCScientificSessionWithLogin(reportPath, dRDriver, logger,
					reportingPath, et);
			verifyingDisclosureLink(reportPath, dRDriver, logger, reportingPath, et);
			VerifyACCSAP9OL(reportPath, dRDriver, logger, reportingPath, et);
//			VerifyJACCJournalWithLogin(reportPath, dRDriver, logger, reportingPath, et);
			VerifyingEbizforMeetingAndProductId(reportPath, dRDriver, logger, reportingPath, et);
			LogoutFeature(reportPath, dRDriver, logger, reportingPath, et);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dRDriver;

	}

	public static Properties loadProperties() {
		Properties prop = null;
		try {
			prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream stream = loader
					.getResourceAsStream("constants.properties");
			prop.load(stream);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return prop;
	}
	
	public static void verifyLoginForCVQuality(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			
			
			dRDriver.get("http://cvquality.acc.org/Login");

			ScenarioForAcc.page_Wait();
			
			try {
				String HomePageLogintab = dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"))
						.getText();

				
				ScenarioForAcc.page_Wait();
				if (HomePageLogintab.contains("LOGIN")) {
					logger.log("pass", "Home page displayed and "+'"'+HomePageLogintab+'"' +" tab exists ForCVQuality", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "Home page is not displayed ofr CVquality" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();
			ScenarioForAcc.page_Wait();
			
			
			
			try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='msTopNav_rptPrimary_hypLnkLogin']"))
						.click();
				ScenarioForAcc.page_Wait();
				
				String Login = dRDriver.findElement(
						By.xpath(".//*[@id='content_0_pnlLogin']/div/h1"))
						.getText();

				
				ScenarioForAcc.page_Wait();
				if (Login.contains("Login")) {
					logger.log("pass", "Login page displayed and "+'"'+Login+'"' +" Header exists ForCVQuality", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "Login page is not displayed ofr CVquality" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();

			ScenarioForAcc.page_Wait();

			

			try {
				dRDriver.findElement(By.xpath(".//*[@id='content_0_txtUsername']"))
				.click();
		// driver.findElement(By.id("UserName")).clear();
				ScenarioForAcc.page_Wait();
		// driver.findElement(By.id("UserName")).sendKeys(vUsername);
		String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

		dRDriver.findElement(By.xpath(".//*[@id='content_0_txtUsername']"))
				.sendKeys(UserNameForMembership);
				String username = dRDriver.findElement(
						By.xpath(".//*[@id='content_0_txtUsername']"))
						.getAttribute("value");

				System.out.println(username);
				ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName "+'"'+username+'"' +" successfully entered ForCVQuality", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();

			ScenarioForAcc.page_Wait();

			try {
				dRDriver.findElement(By.xpath(".//*[@id='content_0_txtPassword']"))
				.click();
		ScenarioForAcc.page_Wait();
		String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

		// driver.findElement(By.id("Password")).sendKeys(vPassword);

		dRDriver.findElement(By.xpath(".//*[@id='content_0_txtPassword']"))
				.sendKeys(PasswordForMembership);
				String password = dRDriver.findElement(
						By.xpath(".//*[@id='content_0_txtPassword']"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password "+'"' + password+'"' +" successfully entered ForCVQuality", false,
							dRDriver, reportingPath, et);
				}} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			
			
			logger.flush();
			ScenarioForAcc.page_Wait();
//			dRDriver.findElement(
//					By.xpath(".//*[@id='content_0_chbxRememberMe']")).click();

			ScenarioForAcc.page_Wait();
			

			// page_Wait();

			// CapturingPopup(dRDriver);
			try {
				dRDriver.findElement(By.xpath(".//*[@id='content_0_btnLogin']"))
				.click();

				ScenarioForAcc.page_Wait();
		// et=logger.createTest("Login", "Login successful");

		System.out.println("Successfully logged in to CV Quality site ForCVQuality");
				String MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='nav-myacc']/li[1]/span")).getText();
				String LogOut = dRDriver.findElement(
						By.xpath(".//*[@id='header-personal']/p/a")).getText();
				if (LogOut.contains("Log Out") && MyAcc.contains("My ACC")) {
					logger.log("pass", "Successfully logged into ACC ForCVQuality", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			
			
			logger.flush();
			ScenarioForAcc.page_Wait();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void verifyLoginForACCScientific(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et) {

		try {
			
			
			dRDriver.get("https://accscientificsession.acc.org/");

			ScenarioForAcc.page_Wait();

			List<WebElement> loginPagePopUP = dRDriver.findElements(By.xpath("//a[@class='acsCloseButton acsAbandonButton ']"));
			ScenarioForAcc.page_Wait();
			if(loginPagePopUP.size()>0){
			loginPagePopUP.get(0).click();
			ScenarioForAcc.page_Wait();
			}
			
			/**/try {
				String LoginPage = dRDriver.findElement(
						By.xpath(".//*[@id='login_btn']"))
						.getText();

				
				ScenarioForAcc.page_Wait();
				if (LoginPage.contains("Login")) {
					logger.log("pass", "Login page for ACCScentific is displayed and  "+'"'+LoginPage +'"'+" tab exixts ForACCScientific", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "Login page for ACCScientific page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();
		
			ScenarioForAcc.page_Wait();
			

			try {
				
				dRDriver.findElement(By.xpath(".//*[@id='username']"))
				.click();
		// driver.findElement(By.id("UserName")).clear();
				ScenarioForAcc.page_Wait();
		// driver.findElement(By.id("UserName")).sendKeys(vUsername);
		String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

		dRDriver.findElement(By.xpath(".//*[@id='username']"))
				.sendKeys(UserNameForMembership);
				String username = dRDriver.findElement(
						By.xpath(".//*[@id='username']"))
						.getAttribute("value");

				System.out.println(username);
				ScenarioForAcc.page_Wait();
				if (!username.isEmpty()) {
					logger.log("pass", "UserName "+'"'+username +'"'+" successfully entered ForACCScientific", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "username field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();

			ScenarioForAcc.page_Wait();

			try {
				
				dRDriver.findElement(By.xpath(".//*[@id='password']"))
				.click();
		ScenarioForAcc.page_Wait();
		String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

		// driver.findElement(By.id("Password")).sendKeys(vPassword);

		dRDriver.findElement(By.xpath(".//*[@id='password']"))
				.sendKeys(PasswordForMembership);
				String password = dRDriver.findElement(
						By.xpath(".//*[@id='password']"))
						.getAttribute("value");
				if (!password.isEmpty()) {
					logger.log("pass", "Password "+'"'+ password +'"'+"successfully entered ForACCScientific", false,
							dRDriver, reportingPath, et);
				}} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					logger.log("fail", "password field empty" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			
			
			logger.flush();
			ScenarioForAcc.page_Wait();
			
			// page_Wait();

			// CapturingPopup(dRDriver);
			try {
				
				dRDriver.findElement(
						By.xpath(".//*[@id='login_btn']"))
						.click();

				ScenarioForAcc.page_Wait();
				WebDriverWait wait = new WebDriverWait(dRDriver, 10);
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(".//*[@id='logout_btn']")));
				// et=logger.createTest("Login", "Login successful");

				System.out.println("Successfully logged in to acc scientific");

				String MyAcc = dRDriver.findElement(
						By.xpath(".//*[@id='login_link']")).getText();
				String LogOut = dRDriver.findElement(
						By.xpath(".//*[@id='logout_btn']")).getText();
				if (LogOut.contains("Logout") && MyAcc.contains("My ACC")) {
					logger.log("pass", "Successfully logged into ACC ForACCScientific", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.log("fail", "Login failed for ACC" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);

				}
			
			
			logger.flush();
			ScenarioForAcc.page_Wait();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
public static void verifyLoginForJACCJournal(String reportPath,
			WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
			ExtentTest et)
{
	 try {
		 
		 dRDriver.get("http://onlinejacc.org/journals.aspx");
		 ScenarioForAcc.page_Wait();
		 
		 try {
				String HomepageforLogin = dRDriver.findElement(
						By.xpath(".//*[@id='globalHeader_lbSignIn']"))
						.getText();

				
				ScenarioForAcc.page_Wait();
				if (HomepageforLogin.contains("LOGIN")) {
					logger.log("pass", "Home page for JACCJpurnal is displayed and  "+'"'+HomepageforLogin +'"'+" tab exixts ForJACCJournal", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "Home page for JACCJournal page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();
		
			ScenarioForAcc.page_Wait();
		 
		
		
		 try {
			 dRDriver.findElement(By.xpath(".//*[@id='globalHeader_lbSignIn']")).click();
				ScenarioForAcc.page_Wait();
				
				String LoginPage = dRDriver.findElement(
						By.xpath(".//*[@id='lvl2-masthead']/h1"))
						.getText();

				
				ScenarioForAcc.page_Wait();
				if (LoginPage.contains("Login")) {
					logger.log("pass", "Login page for JACCJournal is displayed and  "+'"'+LoginPage +'"'+" Header exixts For JACCJournal", false,
							dRDriver, reportingPath, et);
				}	} catch (Exception e2) {
					// TODO Auto-generated catch block
//					e2.printStackTrace();
					logger.log("fail", "Login page for JACCJournal page is not displayed" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
		
			logger.flush();
		
			ScenarioForAcc.page_Wait();
		
		

		try {
			dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
			// driver.findElement(By.id("UserName")).clear();
			ScenarioForAcc.page_Wait();
			// driver.findElement(By.id("UserName")).sendKeys(vUsername);
			String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

			dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
					UserNameForMembership);
			String username = dRDriver.findElement(
					By.xpath("//*[@id='UserName']")).getAttribute("value");

			System.out.println(username);
			ScenarioForAcc.page_Wait();
			if (!username.isEmpty()) {
				logger.log("pass", "UserName "+'"'+ username+'"'+" successfully entered", false,
						dRDriver, reportingPath, et);
			} } catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", "username field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
	
		logger.flush();

		ScenarioForAcc.page_Wait();

		try {
			
			dRDriver.findElement(By.id("Password")).click();
			ScenarioForAcc.page_Wait();
			String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

		
			dRDriver.findElement(By.id("Password")).sendKeys(
					PasswordForMembership);
			String password = dRDriver.findElement(By.id("Password"))
					.getAttribute("value");
			if (!password.isEmpty()) {
				logger.log("pass", "Password "+'"'+password +'"'+" successfully entered ForJACCJournal", false,
						dRDriver, reportingPath, et);
			} } catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", "password field empty" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
		
	
		logger.flush();

		/*dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
				.click();*/

		ScenarioForAcc.page_Wait();
		
		 try {
			 dRDriver.findElement(By.xpath(".//*[@id='formLogin']/input[3]"))
				.click();
			 ScenarioForAcc.page_Wait();
		// et=logger.createTest("Login", "Login successful");

		System.out.println("Successfully logged in to ForJACCJournal");

		// page_Wait();

		 CapturingPopup(dRDriver);
		 
				 WebElement LogoForJACC = dRDriver.findElement(By.xpath(".//*[@id='siteLogo']"));
						
				if (LogoForJACC.isDisplayed()) {
					logger.log("pass", "Logged in successfully  ForJACCJournal", false,
							dRDriver, reportingPath, et);
				} } catch (Exception e) {
					// TODO Auto-generated catch block
//					e.printStackTrace();
					logger.log("fail", "Logged in not uccessfull  ForJACCJournal" + "\""
							+ "\" doesn't exists.", true, dRDriver, reportingPath,
							et);
				}
			
		
			logger.flush();
			ScenarioForAcc.page_Wait();
		 
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public static void verifyLoginForACCSAP9(String reportPath,
		WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
		ExtentTest et)
{
 try {
	 
	 dRDriver.get("https://www.acc.org/education-and-meetings/products-and-resources/accsap9-adult-clinical-cardiology-self-assessment-program");
	 ScenarioForAcc.page_Wait();
	 
	 try {
			String HomepageforLogin = dRDriver.findElement(
					By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a"))
					.getText();

			
			ScenarioForAcc.page_Wait();
			if (HomepageforLogin.contains("Log in to MyACC")) {
				logger.log("pass", "Home page for ACCSAP9 is displayed and  "+'"'+HomepageforLogin +'"'+" tab exixts For ACCSAP9", false,
						dRDriver, reportingPath, et);
			}	} catch (Exception e2) {
				// TODO Auto-generated catch block
//				e2.printStackTrace();
				logger.log("fail", "Home page for ACCSAP9 page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
	
		logger.flush();
	
		ScenarioForAcc.page_Wait();
		 CapturingPopup(dRDriver);
		 ScenarioForAcc.page_Wait();
	
	
	 try {
		 dRDriver.findElement(By.xpath(".//*[@id='myacc-holder']/div/div[2]/p/a")).click();
			ScenarioForAcc.page_Wait();
			
			String LoginPage = dRDriver.findElement(
					By.xpath(".//*[@id='lvl2-masthead']/h1"))
					.getText();

			
			ScenarioForAcc.page_Wait();
			if (LoginPage.contains("Login")) {
				logger.log("pass", "Login page for ACCSAP9 is displayed and  "+'"'+LoginPage +'"'+" header exixts For ACCSAP9", false,
						dRDriver, reportingPath, et);
			}	} catch (Exception e2) {
				// TODO Auto-generated catch block
//				e2.printStackTrace();
				logger.log("fail", "Login page for ACCSAP9 page is not displayed" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);
			}
	
		logger.flush();
	
		ScenarioForAcc.page_Wait();
	 
	
	
	

	try {
		
		dRDriver.findElement(By.xpath("//*[@id='UserName']")).click();
		// driver.findElement(By.id("UserName")).clear();
		ScenarioForAcc.page_Wait();
		// driver.findElement(By.id("UserName")).sendKeys(vUsername);
		String UserNameForMembership = Utils.loadProperty("UserNameForMembership");

		dRDriver.findElement(By.xpath("//*[@id='UserName']")).sendKeys(
				UserNameForMembership);
		String username = dRDriver.findElement(
				By.xpath("//*[@id='UserName']")).getAttribute("value");

		System.out.println(username);
		ScenarioForAcc.page_Wait();
		
		if (!username.isEmpty()) {
			logger.log("pass", "UserName "+'"'+username +'"'+" successfully entered ForACCSAP9", false,
					dRDriver, reportingPath, et);
		} } catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			logger.log("fail", "username field empty" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);
		}

	logger.flush();
ScenarioForAcc.page_Wait();
	

	try {
		
		dRDriver.findElement(By.id("Password")).click();
		ScenarioForAcc.page_Wait();
		String PasswordForMembership = Utils.loadProperty("PasswordForMembership");

		// driver.findElement(By.id("Password")).sendKeys(vPassword);

		dRDriver.findElement(By.id("Password")).sendKeys(
				PasswordForMembership);
		String password = dRDriver.findElement(By.id("Password"))
				.getAttribute("value");
		if (!password.isEmpty()) {
			logger.log("pass", "Password "+'"'+password +'"'+" successfully entered ForACCSAP9", false,
					dRDriver, reportingPath, et);
		} } catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			logger.log("fail", "password field empty" + "\""
					+ "\" doesn't exists.", true, dRDriver, reportingPath,
					et);
		}
	

	logger.flush();

	/*dRDriver.findElement(By.xpath(".//*[@id='formLogin']/p[4]/label"))
			.click();*/

	ScenarioForAcc.page_Wait();
	
	 
	 try {
		 
		 dRDriver.findElement(By.xpath(".//*[@id='formLogin']/button"))
			.click();

	// et=logger.createTest("Login", "Login successful");
	ScenarioForAcc.page_Wait();
	
	

	System.out.println("Successfully logged in to ACC site ForACCSAP9");

	// page_Wait();

	 CapturingPopup(dRDriver);
	 ScenarioForAcc.page_Wait();
			WebElement logoforacc = dRDriver.findElement(By
					.xpath(".//*[@id='nav-myacc']/li[1]/span"));
			// String
			// header=dRDriver.findElement(By.xpath("//*[@id='myacc-holder']/div/div/p/a")).getText();
			if (logoforacc.isDisplayed()) {
				logger.log("pass", "Successfully logged into ACCSAP9", false,
						dRDriver, reportingPath, et);
			} } catch (Exception e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
				logger.log("fail", "Login failed for ACCSAP9" + "\""
						+ "\" doesn't exists.", true, dRDriver, reportingPath,
						et);

			
		}
		logger.flush();
		ScenarioForAcc.page_Wait();
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
}

public static void verifyingDisclosureLink(String reportPath,
		WebDriver dRDriver, com.gallop.Logger logger, String reportingPath,
		ExtentTest et)
{
	try
	{
		//newTabOpening(dRDriver);
		
	dRDriver.get("http://disclosures.acc.org");
	ScenarioForAcc.page_Wait();
	
	try {
		String ManageYourDisclosures = dRDriver.findElement(
				By.xpath("html/body/div[1]/div/section/div[1]/div/a"))
				.getText();

		String MergeAccounts = dRDriver.findElement(
				By.xpath("html/body/div[1]/div/section/div[2]/div/a"))
				.getText();

		String Logout = dRDriver.findElement(
				By.xpath("html/body/header/div/div[2]/span/a[2]/span"))
				.getText();

		if (ManageYourDisclosures.contains("Manage Your Disclosures")
				&& MergeAccounts.contains("Merge Accounts")
				&& Logout.contains("Logout")) {
			logger.log("pass",
					"Navigated to annual disclosure page and " + '"' + Logout
					+ '"' +" " + ManageYourDisclosures+ '"' + " "
					+ '"' + MergeAccounts + '"' + " exists", false,
					dRDriver, reportingPath, et);
		}
	} catch (Exception e) {
		logger.log("fail", "Navigation to annual diclosure page failed"
				+ "\"" + "\" doesn't exists.", true, dRDriver,
				reportingPath, et);

	}
	logger.flush();
	ScenarioForAcc.page_Wait();
         
	
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	// newTabClosing(dRDriver);
}

public static void newTabOpening(WebDriver dRDriver) throws InterruptedException
{
	/*driverFF=dRDriver;
	driverChrome=dRDriver;*/
	
	/*WebElement body = dRDriver.findElement(By.tagName("body"));
    body.sendKeys(Keys.CONTROL + "t");*/
	
	if(dRDriver.equals(driverFF))
	{
		WebElement body = dRDriver.findElement(By.tagName("body"));
	    body.sendKeys(Keys.CONTROL + "t");
		ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
		System.out.println("Firefox##################################Number of windows opened are : "+tabs2.size());
		System.out.println(dRDriver.getCurrentUrl());
	//dRDriver.switchTo().window(tabs2.get(1));
	}
	else if(dRDriver.equals(driverChrome))
	{
		//String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,"t");
		WebElement body = dRDriver.findElement(By.tagName("body"));
	    //body.sendKeys(selectLinkOpeninNewTab);
		body.sendKeys(Keys.CONTROL + "t");
		ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
		System.out.println("Chrome##################################Number of windows opened are : "+tabs2.size());
		System.out.println(dRDriver.getCurrentUrl());
		Thread.sleep(1000);
		dRDriver.switchTo().window(tabs2.get(1));
	}
}

public static void newTabClosing(WebDriver dRDriver)
{
	/*driverFF=dRDriver;
	driverChrome=dRDriver;*/
	if(dRDriver.equals(driverFF))
	{
		System.out.println("in ff closing tab");
		dRDriver.findElement(By.xpath("//body")).sendKeys(
				Keys.CONTROL + "w");
		System.out.println("tab is closed");
		dRDriver.switchTo().defaultContent();
			
	}
	else if(dRDriver.equals(driverChrome))
	{
		ArrayList<String> tabs2 = new ArrayList<String> (dRDriver.getWindowHandles());
		dRDriver.close();
	    dRDriver.switchTo().window(tabs2.get(0));
	}
}
public static void LoggerForHomepageDisplayWithLogin(com.gallop.Logger logger,
		WebDriver dRDriver, ExtentTest et) throws Exception {
	try {
		ScenarioForAcc.page_Wait();
		//dRDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		System.out.println(dRDriver.getCurrentUrl());
		System.out.println("Entered Logger For HomePage Display");
		List<WebElement> HomePage1 = dRDriver.findElements(By
				.xpath(".//*[@id='AccLlpHeader_accMainNavigation_navMyAcc_rptMyAcc_liHeader_0']"));
		List <WebElement> HomePage2 = dRDriver.findElements(By
				.xpath(".//*[@id='nav-myacc']/li[1]/span"));///////////////////////////////////
		// .//*[@id='nav-myacc']/li[1]/span
		
		List<WebElement> HomePage3 = dRDriver.findElements(By.xpath(".//*[@id='msTopNav_rptNav_hypLnk_0']"));
		
         ScenarioForAcc.page_Wait();
//         dRDriver.manage().
		if (HomePage1.get(0).isDisplayed() || HomePage2.get(0).isDisplayed() || HomePage3.get(0).isDisplayed()) {
			System.out.println("Logging true");
			logger.log("pass", "Home page displayed for acc", false,
					dRDriver, reportingPath, et);
		} else {
			System.out.println("Failed to display homepage");
		}
	} catch (Exception e) {

		logger.log("fail", "Home page not displayed" + "\""
				+ "\" doesn't exists.", true, dRDriver, reportingPath, et);
	}
	logger.flush();
	ScenarioForAcc.page_Wait();
}

public static void redirectToHomePage(com.gallop.Logger logger,
		ExtentTest et, WebDriver dRDriver) {

	try {

		dRDriver.get("https://www.acc.org/#sort=%40fwhatstrendingscore86069%20descending");
		ScenarioForAcc.page_Wait();
		WebElement HomePage = dRDriver.findElement(By
				.xpath(".//*[@id='nav-primary-holder']/div[3]"));
		// .//*[@id='nav-myacc']/li[1]/span

		if (HomePage.isDisplayed()) {
			System.out.println("Logging true");
			logger.log("pass", "Home page displayed for acc", false,
					dRDriver, reportingPath, et);
		} else {
			System.out.println("Failed to display homepage");
		}
		/*
		 * logger.log("pass",
		 * "Updated Date and Original POsted date are same ", false,
		 * dRDriver, reportingPath, et);
		 */

	} catch (Exception e) {
		// TODO Auto-generated catch block
		// e.printStackTrace();

		logger.log("fail",
				"Updated Date and Original POsted date are not same "
						+ "\"" + "\" doesn't exists.", true, dRDriver,
				reportingPath, et);
	}

	logger.flush();
}

public static void AlertForFederatedLogin()
{
	try {
		dRDriver.switchTo().alert().accept();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

}

