package utility;


import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class emailReport {

public static void sendEmail() throws Exception { 

    final String username = "accautomationdc@gmail.com";
    final String password = "Ncdr@automation";

    Properties props = new Properties();
    props.put("mail.smtp.auth", true);
    props.put("mail.smtp.starttls.enable", true);
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "465");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.SocketFactory");
    props.put("mail.smtp.ssl.enable", true);
    props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

    Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

    try {
    	Date dt =new Date();
    	ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");
    	String EmailList = ExcelUtils.getCellData(16, 1);
    	
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("accautomationdc@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(EmailList.trim()));
        //InternetAddress.parse("sbarua@acc.org,akopleff@acc.org,iprattip@acc.org"));
        message.setSubject("NCDR disaster recovery report --"+dt);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText("Welcome to NCDR recovery testing!!!");
        //message.setText("This test message from Selenium PFA");
      
        
        //Handle attachment 1
        MimeBodyPart attachment1 = new MimeBodyPart();
        attachment1 = new MimeBodyPart();
        String file1 = Constant.Report_Path;//Constant.Path_TestData+Constant.File_TestData;
        
        //Handle attachment 2
        MimeBodyPart attachment2 = new MimeBodyPart();
        attachment2 = new MimeBodyPart();
        String file2 = Constant.logFilePath;
               
        //String fileName = "test.txt";
        //DataSource source1 = new FileDataSource(file1);
        //messageBodyPart1.setDataHandler(new DataHandler(source1));
        //messageBodyPart1.setFileName(file1);
        attachment1.attachFile(file1);
        
       // DataSource source2 = new FileDataSource(file2);
       // messageBodyPart2.setDataHandler(new DataHandler(source2));
        attachment2.attachFile(file2);
        
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        multipart.addBodyPart(attachment1);
        multipart.addBodyPart(attachment2);
        
        message.setContent(multipart);
        System.out.println(message);
        System.out.println("Email sending...");

        Transport.send(message);
        

        System.out.println("Done");

    } catch (MessagingException e) {
        e.printStackTrace();
        System.out.println(e.toString());
    }catch (Exception e) {
        e.printStackTrace();
        System.out.println(e.toString());
    }
  }

public static void sendEmail(String receipients,String filePath) throws Exception { 

    final String username = "shyampacer@gmail.com";//"accautomationdc@gmail.com";
    final String password = "1988zerofive12";//"Ncdr@automation";

    Properties props = new Properties();
    props.put("mail.smtp.auth", true);
    props.put("mail.smtp.starttls.enable", true);
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "465");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class", "javax.net.SocketFactory");
    props.put("mail.smtp.ssl.enable", true);
    props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

    Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

    try {
    	Date dt =new Date();
    	/*ExcelUtils.setExcelFile(Constant.Path_TestData + Constant.File_TestData,"Config");*/
    	String EmailList = "shyampacer@gmail.com";//ExcelUtils.getCellData(16, 1);
    	if(receipients!=null && !receipients.isEmpty()){
    		EmailList=EmailList+","+receipients;
    	}
    	
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("shyampacer@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(EmailList.trim()));
        //InternetAddress.parse("sbarua@acc.org,akopleff@acc.org,iprattip@acc.org"));
        message.setSubject("NCDR disaster recovery report --"+dt);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setText("Welcome to NCDR recovery testing!!!");
        //message.setText("This test message from Selenium PFA");
      
        
        //Handle attachment 1
        MimeBodyPart attachment1 = new MimeBodyPart();
        attachment1 = new MimeBodyPart();
        String file1 = filePath;//Constant.Report_Path;//Constant.Path_TestData+Constant.File_TestData;
        
        //Handle attachment 2
        MimeBodyPart attachment2 = new MimeBodyPart();
        attachment2 = new MimeBodyPart();
        String file2 = Constant.logFilePath;
               
        //String fileName = "test.txt";
        //DataSource source1 = new FileDataSource(file1);
        //messageBodyPart1.setDataHandler(new DataHandler(source1));
        //messageBodyPart1.setFileName(file1);
        attachment1.attachFile(file1);
        
       // DataSource source2 = new FileDataSource(file2);
       // messageBodyPart2.setDataHandler(new DataHandler(source2));
        //attachment2.attachFile(file2);
        
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
        multipart.addBodyPart(attachment1);
      //  multipart.addBodyPart(attachment2);
        
        message.setContent(multipart);
        System.out.println(message);
        System.out.println("Email sending...");

        Transport.send(message);
        

        System.out.println("Done");

    } catch (MessagingException e) {
        e.printStackTrace();
        System.out.println(e.toString());
    }catch (Exception e) {
        e.printStackTrace();
        System.out.println(e.toString());
    }
  }
}
