package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gallop.Logger;
import com.relevantcodes.extentreports.ExtentTest;

import pageObjects.AFibRegPage;
import pageObjects.ActionRegPage;
import pageObjects.CathPCIRegPage;
import pageObjects.DiabetesRegPage;
import pageObjects.ICDRegPage;
import pageObjects.ImpactRegPage;
import pageObjects.LaaoRegPage;
import pageObjects.PVIRegPage;
import pageObjects.PinnacleRegPage;
import pageObjects.TVTRegPage;
import appModule.ActionDQR;
import appModule.ActionReg;
import appModule.CathPCIDQR;
import appModule.Configuration;
import appModule.ICDDQR;
import appModule.ImpactDQR;
import appModule.LAAODQR;
import appModule.PVIDQR;
import appModule.TVTDQR;
import beans.Tab;
import beans.Registry;

public class Utils {

	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");

	public static Map<String, List<Tab>> getTabsFromRegistry(
			List<Registry> registries) {

		List<Tab> tabs = new ArrayList<Tab>();
		Map<String, List<Tab>> registryTabsMap = new HashMap<String, List<Tab>>();
		for (Registry registry : registries) {
			String header = registry.getHeaders();
			if (header != null && !header.isEmpty()) {
				String[] headersBuffer = header.split("\\,");
				for (String head : headersBuffer) {
					Tab tab2 = new Tab();
					String[] split = head.split("\\#");
					tab2.setId(split[0]);
					tab2.setTabName(split[1]);
					tabs.add(tab2);
					if (registryTabsMap.containsKey(registry.getRegistryName())) {
						List<Tab> tabs1 = registryTabsMap.get(registry
								.getRegistryName());
						tabs1.add(tab2);
						registryTabsMap.put(registry.getRegistryName(), tabs1);

					} else {
						List<Tab> tabs1 = new ArrayList<Tab>();
						tabs1.add(tab2);
						registryTabsMap.put(registry.getRegistryName(), tabs1);
					}
				}
			}

		}

		return registryTabsMap;
	}

	public static Map<String, List<Tab>> getTabsAndChildrenFromRegistry(
			List<Registry> registries) {

		List<Tab> tabs = new ArrayList<Tab>();
		Map<String, List<Tab>> registryTabsMap = new HashMap<String, List<Tab>>();
		for (Registry registry : registries) {
			String header = registry.getHeaders();
			if (header != null && !header.isEmpty()) {
				String[] leftMenuItems = header.split("###");
				for (String ledtMenuItem : leftMenuItems) {
					String[] parentAndChild = ledtMenuItem.split("##");
					if (parentAndChild != null && parentAndChild.length > 0) {
						Tab tab = new Tab();
						List<Tab> childTabs = new ArrayList<>();
						String parentTabId = parentAndChild[0].split("#")[0];
						String parentTabName = parentAndChild[1].split("#")[0];
						tab.setId(parentTabId);
						tab.setTabName(parentTabName);
						String[] headersBuffer = parentAndChild[1].split("\\,");
						for (String head : headersBuffer) {
							Tab tab2 = new Tab();
							String[] split = head.split("#");
							tab2.setId(split[0]);
							tab2.setTabName(split[1]);
							childTabs.add(tab2);
						}
						tab.setChildTabs(childTabs);
						if (registryTabsMap.containsKey(registry
								.getRegistryName())) {
							List<Tab> tabs1 = registryTabsMap.get(registry
									.getRegistryName());
							tabs1.add(tab);
							registryTabsMap.put(registry.getRegistryName(),
									tabs1);

						} else {
							List<Tab> tabs1 = new ArrayList<Tab>();
							tabs1.add(tab);
							registryTabsMap.put(registry.getRegistryName(),
									tabs1);
						}
					} else if (ledtMenuItem != null && !ledtMenuItem.isEmpty()) {
						String[] headersBuffer = ledtMenuItem.split("\\,");
						for (String head : headersBuffer) {
							Tab tab2 = new Tab();
							String[] split = head.split("#");
							tab2.setId(split[0]);
							tab2.setTabName(split[1]);
							tabs.add(tab2);
							if (registryTabsMap.containsKey(registry
									.getRegistryName())) {
								List<Tab> tabs1 = registryTabsMap.get(registry
										.getRegistryName());
								tabs1.add(tab2);
								registryTabsMap.put(registry.getRegistryName(),
										tabs1);

							} else {
								List<Tab> tabs1 = new ArrayList<Tab>();
								tabs1.add(tab2);
								registryTabsMap.put(registry.getRegistryName(),
										tabs1);
							}
						}
					}
				}
			}

		}

		return registryTabsMap;
	}

	public static void checkForUsernameAndLogoutLink(WebDriver driver,
			com.gallop.Logger logger, ExtentTest et) {
		String sUserName = null;
		try {
			XSSFSheet ExcelWSheet=ExcelUtils.setExcelFile2("Config");

			sUserName = ExcelUtils.getCellData(1, 2,ExcelWSheet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (sUserName != null) {

		} else {
			sUserName = "username";
		}
		String xpath = ".//*[@id='BreadcrumbNav']/ul[2]/li[1]/a";
		WebElement element = driver.findElement(By.xpath(xpath));
		if (element != null && !element.getText().isEmpty()) {
			logger.log("pass", "Username " + "\"" + sUserName + "\" exists!!",
					false, driver, "", et);
		} else {
			logger.log("fail", "Username " + "\"" + sUserName
					+ "\" doesn't exists.", true, driver, reportingPath, et);

		}

		String logoutxpath = "//*[@id='BreadcrumbNav']/ul[2]/li[3]/a";
		WebElement logoutelement = driver.findElement(By.xpath(logoutxpath));
		if (logoutelement != null && !logoutelement.getText().isEmpty()) {
			logger.log("pass", "\"Logout\" link exists", false, driver, "", et);
		} else {
			logger.log("fail", "\"Logout\" link doesn't exists.", true, driver,
					reportingPath, et);
		}

	}

	public static void checkForRegistriesHeaderAndAnnouncementsHeader(
			WebDriver driver, com.gallop.Logger logger, ExtentTest et) {

		String xpath = "//*[@id='BreadcrumbNav']/ul[1]/li[1]/a";
		WebElement element = driver.findElement(By.xpath(xpath));
		if (element != null && !element.getText().isEmpty()) {
			logger.log("pass", "Registry Header \"" + element.getText()
					+ "\" exists", false, driver, "", et);
		} else {
			logger.log("fail", "Registry Header doesn't exists.", true, driver,
					reportingPath, et);

		}

		String announcementsXpath = "//*[@id='BreadcrumbNav']/ul[1]/li[3]/a";
		WebElement announcementsElement = driver.findElement(By
				.xpath(announcementsXpath));
		if (announcementsElement != null
				&& !announcementsElement.getText().isEmpty()) {
			logger.log("pass", "Header \"" + announcementsElement.getText()
					+ "\" exists", false, driver, "", et);
		} else {
			logger.log("fail", "Announcements Header doesn't exists.", true,
					driver, reportingPath, et);
		}

	}

	public static void checkForLinks(WebDriver driver,
			com.gallop.Logger logger, String Links, ExtentTest et) {

		if (Links != null && !Links.isEmpty()) {
			String[] linksBuffer = Links.split("\\,");
			for (String string : linksBuffer) {

				try {
					WebElement element = driver
							.findElement(By.xpath("//span[contains(text(), '"
									+ string + "')]"));
					if (element != null && !element.getText().isEmpty()) {
						logger.log("pass", "Menu item \"" + string
								+ "\" exists", false, driver, "", et);
					} else {
						logger.log("fail", "Menu item \"" + string
								+ "\" doesn't exists.", true, driver,
								reportingPath, et);

					}
				} catch (Exception e) {
					logger.log("fail", "Menu item \"" + string
							+ "\" doesn't exists.", true, driver,
							reportingPath, et);
				}
			}
		}

	}

	public static void checkSwithRegisteries(WebDriver driver,
			com.gallop.Logger logger, ExtentTest et) {
		int count = 0;
		String[] registries = { "ACTION Registry - GWTG", "CathPCI Registry",
				"Diabetes Collaborative Registry", "IMPACT Registry",
				"LAAO Registry", "PINNACLE Registry", "PVI Registry",
				"STS/ACC TVT Registry", "ICD Registry",
				"AFib Ablation Registry" };
		List<WebElement> wes = driver.findElements(By
				.xpath("//div[contains(@class, 'link')]"));

		for (WebElement webElement : wes) {
			for (String registry : registries) {
				/*
				 * System.out.println("webElement.getAttribute(innerHTML):" +
				 * webElement.getAttribute("innerHTML"));
				 */
				if (webElement.getAttribute("innerHTML").contains(registry)) {
					count++;
					break;
				}
			}

		}

		if (count >= 10) {
			logger.log(
					"pass",
					"All 10 registry options exists in switch registry dropdown",
					false, driver, "", et);
		} else {
			/*
			 * logger.log( "fail",
			 * "All 10 registry options doesn't exists in switch registry dropdown."
			 * , true, driver, reportingPath,et);
			 */
			ActionRegPage actionRegPage = new ActionRegPage();
			actionRegPage.ActionRegPageMethod(driver, "ActionRegistry").click();
			wes = driver.findElements(By
					.xpath("//div[contains(@class, 'link')]"));
			count = 0;
			for (WebElement webElement : wes) {
				for (String registry : registries) {
					/*
					 * System.out.println("webElement.getAttribute(innerHTML):"
					 * + webElement.getAttribute("innerHTML"));
					 */
					if (webElement.getAttribute("innerHTML").contains(registry)) {
						count++;
						break;
					}
				}

			}

			if (count >= 10) {
				logger.log(
						"pass",
						"All 10 registry options exists in switch registry dropdown",
						false, driver, "", et);
			}
		}

		try {
			/*
			 * driver.findElement(By.xpath("//*[@id='switch-registry']")).click()
			 * ;
			 * 
			 * driver.findElement(By.xpath("//*[@id='ACTION_NAME']")).click();
			 * logger.log("pass", "\"ACTION Registry\" option selected", false,
			 * driver, ""); if
			 * (driver.getPageSource().contains("ACTION Registry")) {
			 * 
			 * logger.log("pass", " \"ACTION Registry\" header exists", false,
			 * driver, ""); }
			 */

			int randomIndex = new Random().nextInt(registries.length);
			/* System.out.println(randomIndex); */

			String randomRegistryName = registries[randomIndex];

			if (randomRegistryName.contains("ACTION")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='ACTION_NAME']")).click();
				logger.log("pass", "\"ACTION Registry\" option selected",
						false, driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("ACTION Registry")) {

					logger.log("pass", " \"ACTION Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("CathPCI")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='CATHPCI_NAME']")).click();
				logger.log("pass", "\"CathPCI Registry\" option selected",
						false, driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("CathPCI Registry")) {

					logger.log("pass", " \"CathPCI Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("Diabetes")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='DIABETES_NAME']"))
						.click();
				logger.log("pass", "\"Diabetes Registry\" option selected",
						false, driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("Diabetes Collaborative")) {

					logger.log(
							"pass",
							" \"Diabetes Collaborative Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("IMPACT")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='IMPACT_NAME']")).click();
				logger.log("pass", "\"Impact Registry\" option selected",
						false, driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("IMPACT Registry")) {

					logger.log("pass", " \"IMPACT Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("LAAO")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='LAAO_NAME']")).click();
				logger.log("pass", "\"LAAO Registry\" option selected", false,
						driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("LAAO Registry")) {

					logger.log("pass", " \"LAAO Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("PINNACLE")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='PINNACLE_NAME']"))
						.click();
				logger.log("pass", "\"PINNACLE Registry\" option selected",
						false, driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("PINNACLE Registry")) {

					logger.log("pass", " \"PINNACLE Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("PVI")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='PVI_NAME']")).click();
				logger.log("pass", "\"PVI Registry\" option selected", false,
						driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("PVI Registry")) {

					logger.log("pass", " \"PVI Registry\" header exists",
							false, driver, "", et);
				}
			} else if (randomRegistryName.contains("TVT")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='TVT_NAME']")).click();
				logger.log("pass", "\"TVT Registry\" option selected", false,
						driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("TVT Registry")) {

					logger.log("pass", " \"TVT Registry\" header exists",
							false, driver, "", et);
				} else {
					logger.log("fail", " \"TVT Registry\" header"
							+ " doesn't exists.", true, driver, reportingPath,
							et);
				}
			} else if (randomRegistryName.contains("ICD")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='ICD_NAME']")).click();
				logger.log("pass", "\"ICD Registry\" option selected", false,
						driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("ICD Registry")) {

					logger.log("pass", " \"ICD Registry\" header exists",
							false, driver, "", et);
				} else {
					logger.log("fail", " \"ICD Registry\" header"
							+ " doesn't exists.", true, driver, reportingPath,
							et);
				}
			} else if (randomRegistryName.contains("AFib")) {
				driver.findElement(By.xpath("//*[@id='switch-registry']"))
						.click();

				driver.findElement(By.xpath("//*[@id='AFIB_NAME']")).click();
				logger.log("pass", "\"AFIB Registry\" option selected", false,
						driver, "", et);
				Utils.page_wait(driver);
				if (driver.getPageSource().contains("AFib Ablation")) {

					logger.log("pass", " \"AFIB Registry\" header exists",
							false, driver, "", et);
				} else {
					logger.log("fail", " \"AFIB Registry\" header"
							+ " doesn't exists.", true, driver, reportingPath,
							et);
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static WebElement getWebElement(WebDriver driver, String type,
			String registry) {
		WebElement webElement = null;
		if (registry.equalsIgnoreCase("ActionRegistry")) {
			ActionRegPage ActionRegPageObj = new ActionRegPage();
			try {
				webElement = ActionRegPageObj.ActionRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("CathPCIRegistry")) {
			CathPCIRegPage cathPCIRegPage = new CathPCIRegPage();
			try {
				webElement = cathPCIRegPage.CathPCIRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("ImpactRegistry")) {
			ImpactRegPage impactRegPage = new ImpactRegPage();
			try {
				webElement = impactRegPage.ImpactRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("ICDRegistry")) {
			ICDRegPage icdRegPage = new ICDRegPage();
			try {
				webElement = icdRegPage.ICDRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("PVIRegistry")) {
			PVIRegPage pviRegPage = new PVIRegPage();
			try {
				webElement = pviRegPage.PVIRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("TVTRegistry")) {
			TVTRegPage tvtRegPage = new TVTRegPage();
			try {
				webElement = tvtRegPage.TVTRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("DiabetesRegistry")) {
			DiabetesRegPage diabetesRegPage = new DiabetesRegPage();
			try {
				webElement = diabetesRegPage.ActionRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("PinnacleRegistry")) {
			PinnacleRegPage pinnacleRegPage = new PinnacleRegPage();
			try {
				webElement = pinnacleRegPage.ActionRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (registry.equalsIgnoreCase("LaaoRegistry")) {
			LaaoRegPage laaoRegPage = new LaaoRegPage();
			try {
				webElement = laaoRegPage.ActionRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if (registry.equalsIgnoreCase("AfibRegistry")) {
			AFibRegPage afibRegPage = new AFibRegPage();
			try {
				webElement = afibRegPage.AFibRegPageMethod(driver, type);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return webElement;
	}

	public static Boolean checkIfTextExists(WebDriver driver, String text,
			com.gallop.Logger logger, ExtentTest et) {
		try {
			// Thread.sleep(2000);
			// page_wait();
			page_wait(driver);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (driver.getPageSource().contains(text)) {
			logger.log("pass", "Menu header \"" + text + "\"" + " exists",
					false, driver, "", et);
			logger.flush();
			return true;
		} else {
			logger.log("fail", "Menu header \"" + text + "\""
					+ " doesn't exists.", true, driver, reportingPath, et);
			logger.flush();
			return false;
		}

	}

	public static Boolean checkIfTextExistsInsideMenu(WebDriver driver,
			String text, com.gallop.Logger logger, String menuName,
			ExtentTest et) {
		try {
			// Thread.sleep(2000);
			// page_wait();
			page_wait(driver);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (driver.getPageSource().contains(text)) {
			logger.log("pass", "Under menu header \"" + menuName
					+ "  \" validated text " + "\"" + text + "\"" + " ", false,
					driver, "", et);
			logger.flush();
			return true;
		} else {
			logger.log("fail", "Under menu header \"" + menuName + "\""
					+ " text \"" + text + "\"" + " doesn't exists.", true,
					driver, reportingPath, et);
			logger.flush();
			return false;
		}

	}

	public static void uploadData(WebDriver driver, String registry,
			Logger logger, ExtentTest et) {
		try {
			if (registry.equalsIgnoreCase("ActionRegistry")) {
				ActionDQR.Execute(driver, logger, et);
			} else if (registry.equalsIgnoreCase("CathPCIRegistry")) {
				CathPCIDQR.Execute(driver, logger, et);
			} else if (registry.equalsIgnoreCase("ImpactRegistry")) {
				ImpactDQR.Execute(driver, logger, et);
			} else if (registry.equalsIgnoreCase("ICDRegistry")) {
				ICDDQR.Execute(driver, logger, et);
			} else if (registry.equalsIgnoreCase("PVIRegistry")) {
				PVIDQR.Execute(driver, logger, et);
			} else if (registry.equalsIgnoreCase("TVTRegistry")) {
				TVTDQR.Execute(driver, logger, et);
			} else if (registry.equalsIgnoreCase("LAAORegistry")) {
				LAAODQR.Execute(driver, logger, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<Registry> getRegistries() {
		List<Registry> registries = new ArrayList<Registry>();
		String[] registriesArray = { "ActionRegistry", "CathPCIRegistry",
				"DiabetesRegistry", "ImpactRegistry", "LaaoRegistry",
				"PinnacleRegistry", "PVIRegistry", "TVTRegistry",
				"ICDRegistry", "AFIBRegistry" };

		for (String string : registriesArray) {
			Registry registry = new Registry();
			registry.setRegistryName(string);
			registry.setRegistryWelcomeLabel(string);
			registries.add(registry);

		}
		return registries;
	}

	public static void checkIfElementExists(WebDriver driver, String element,
			Logger logger, ExtentTest et) {

		String xpath = null, id = null;
		WebElement webElement = null;

		try {
			// Thread.sleep(2000);
			Utils.page_long_wait();
			if (element.equalsIgnoreCase("Additional Reports")) {
				id = "cm70F20D038D786C1DA9DEF79858256164i36980CA5CE284ECB83498C0AE9F72604i19DF34B5E5794201AF3D6648866D9376tab_anchor";
				webElement = driver.findElement(By.id(id));

			} else if (element.equalsIgnoreCase("Filter Panel")) {
				xpath = "//*[@id='rtcm7B3F4CB0B99C1AE5A36C50B338B9F1DEframe1i91CADDF456BF47F4A27540F80C418A4FrvCanvas']/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td[1]/span";
				webElement = driver.findElement(By.xpath(xpath));
			}

			if (webElement != null && webElement.isDisplayed()) {
				logger.log("pass", "Menu element \"" + element + "\""
						+ " exists", false, driver, "", et);
				logger.flush();
			} else {
				logger.log("fail", "Menu element \"" + element + "\""
						+ " doesn't exists.", true, driver, reportingPath, et);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String createReportWithTimeStamp(String browserName) {
		String destFilePath = reportPath + "\\testReports_" + browserName + "_"
				+ getCurrentTimeStamp() + ".html";
		String sourceFilePath = reportPath + "\\testReports.html";// "Q:\\QA
																	// Stuff\\Syam
																	// pithani\\NCDR\\NCDR\\WebContent\\WEB-INF\\views\\testReports.html";//"Q:\\QA
																	// Stuff\\Syam
																	// pithani\\accautomation\\accicd\\automationACC\\test-output\\ExtentReports.html";

		try {
			File source = new File(sourceFilePath);
			File dest = new File(destFilePath);

			InputStream input = null;
			OutputStream output = null;
			try {
				if (!dest.exists()) {
					dest.createNewFile();
				}
				input = new FileInputStream(source);
				output = new FileOutputStream(dest);
				byte[] buf = new byte[1024];
				int bytesRead;
				while ((bytesRead = input.read(buf)) > 0) {
					output.write(buf, 0, bytesRead);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				input.close();
				output.close();
			}
			return destFilePath;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static String getCurrentTimeStamp() {
		java.util.Date today = new java.util.Date();
		String timeStamp = new java.sql.Timestamp(today.getTime()).toString();
		timeStamp = timeStamp.replaceAll(" ", "_");
		timeStamp = timeStamp.replaceAll("\\.", "_");
		timeStamp = timeStamp.replaceAll("\\:", "_");
		return timeStamp;
	}

	public static void close_Browser(WebDriver driver) {

		try {
			driver.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String loadProperty(String name) {
		Properties prop = new Properties();
		InputStream input = null;
		String value = null;
		try {

			// input = new FileInputStream("constants.properties");

			// load a properties file
			// prop.load(input);
			prop.load(Utils.class.getClassLoader().getResourceAsStream(
					"constants.properties"));
			value = prop.getProperty(name);
			// get the property value and print it out

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}

	public static void page_wait() {
		try {
			Thread.sleep(10);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void page_long_wait() {
		try {
			Thread.sleep(100);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void page_long_wait1() {
		try {
			Thread.sleep(2000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void page_wait(WebDriver driver) {
		String browserName = "";
		try {
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			browserName = cap.getBrowserName().toLowerCase();
			System.out.println(browserName);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (browserName.equalsIgnoreCase("chrome")
				|| browserName.equalsIgnoreCase("firefox")) {
			System.out.println("Implicit wait for chrome/firefox 4 secs");
			driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		} else {
			System.out.println("Implicit wait for IE 10 secs");
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			/*try {
				driver.wait(10*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			/*try {
				Thread.sleep(6000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}

	public static WebDriver OpenFFBrowser(WebDriver driver,
			Configuration configuration) throws Exception {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData +
		 * Constant.File_TestData reportingPath + "\\TestData.xlsx", //
		 * "C:\\Users\\E002226\\testData\\TestData.xlsx", "Config");
		 */
		// ExcelUtils.setExcelFile(
		// /* Constant.Path_TestData + Constant.File_TestData */reportingPath +
		// "\\TestData.xlsx", // "C:\\Users\\E002226\\testData\\TestData.xlsx",
		// "Config");
		String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static WebDriver OpenIEBrowser(WebDriver driver, Configuration conf)
			throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		String url = conf.getUrl();// ExcelUtils.getCellData(2, 1);
		System.setProperty("webdriver.ie.driver", /* Constant.Path_TestData */
				reportingPath + "IEDriverServer.exe");
		DesiredCapabilities ieCapabilities = DesiredCapabilities
				.internetExplorer();
		ieCapabilities
				.setCapability(
						InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
		ieCapabilities.setCapability(
				InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		ieCapabilities.setCapability("nativeEvents", false);
		ieCapabilities.setCapability(
				InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
		/*
		 * InternetExplorerDriverService.Builder builder = new
		 * InternetExplorerDriverService.Builder();
		 * InternetExplorerDriverService srvc =
		 * builder.usingPort(4444).withHost("127.0.0.1").build();
		 */

		// driver = new InternetExplorerDriver(srvc, ieCapabilities);
		driver = new InternetExplorerDriver(ieCapabilities);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static WebDriver OpenChrmBrowser(WebDriver driver,
			Configuration configuration) throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);
		System.setProperty("webdriver.chrome.driver",
		/* Constant.Path_TestData */reportingPath + "chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		return driver;
	}

	public static void selectValue(WebDriver driver, String id, String value) {
		try {
			Select listbox = new Select(driver.findElement(By.id(id)));
			listbox.selectByVisibleText(value);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void page_wait_DQR(WebDriver driver, Long noOfSecs) {
		String browserName = "";
		try {
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			browserName = cap.getBrowserName().toLowerCase();
			System.out.println(browserName);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (browserName.equalsIgnoreCase("chrome")
				|| browserName.equalsIgnoreCase("firefox")) {
			driver.manage().timeouts()
					.implicitlyWait(noOfSecs, TimeUnit.SECONDS);
		} else {
			System.out.println("implicit wait 3 times for IE");
			driver.manage().timeouts()
					.implicitlyWait(noOfSecs*3, TimeUnit.SECONDS);
			/*try {
				driver.wait(noOfSecs*3*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			/*try {
				Thread.sleep(noOfSecs*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}
	
	public static void page_explicit_wait(WebDriver driver, Long noOfSecs,String element) {
		String browserName = "";
		try {
			Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
			browserName = cap.getBrowserName().toLowerCase();
			System.out.println(browserName);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String xpath="";
		if(element.equalsIgnoreCase("Registry Information")){
			xpath="//*[@id='tabs-1']/div[1]/div/h4";
		}else{
			xpath="//*[@id='list-target']/section[1]/h2";
		}
		if (browserName.equalsIgnoreCase("chrome")
				|| browserName.equalsIgnoreCase("firefox")) {
			WebElement myDynamicElement = (new WebDriverWait(driver, noOfSecs))
					  .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
		
		} else {
			/*driver.manage().timeouts()
					.implicitlyWait(noOfSecs*3, TimeUnit.SECONDS);*/
			WebElement myDynamicElement = (new WebDriverWait(driver, noOfSecs))
					  .until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
			/*try {
				driver.wait(noOfSecs*3*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			/*try {
				Thread.sleep(noOfSecs*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}

}
