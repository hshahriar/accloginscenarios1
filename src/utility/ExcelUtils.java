package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import beans.Registry;
import beans.TestDataForACC;

public class ExcelUtils {

	
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;
	static Properties properties = loadProperties();
	private static String reportingPath = Utils.loadProperty("reportingPath");
	private static String reportPath = Utils.loadProperty("reportPath");
	public static String username;

	// This method is to set the File path and to open the Excel file, Pass
	// Excel Path and Sheetname as Arguments to this method

	public static void setExcelFile(String Path, String SheetName)
			throws Exception {
		try {
			// Open the Excel file
			Path = "Q:\\QA Stuff\\Harish Kumar K\\testData\\TestData.xlsx";
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
		} catch (Exception e) {
			throw (e);
		}
	}// Method
	
	public static XSSFSheet setExcelFile1(String Path, String SheetName)
			throws Exception {
		try {
			// Open the Excel file
			Path = "Q:\\QA Stuff\\Harish Kumar K\\testData\\TestData.xlsx";
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			XSSFSheet ExcelWSheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			return ExcelWSheet;
		} catch (Exception e) {
			throw (e);
		}
	}// Method
	
	public static XSSFSheet setExcelFile2(String SheetName)
			throws Exception {
		try {
			// Open the Excel file
			String Path = reportingPath+"TestData.xlsx";
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			XSSFSheet ExcelWSheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			return ExcelWSheet;
		} catch (Exception e) {
			throw (e);
		}
	}

	// This method is to read the test data from the Excel cell, in this we are
	// passing parameters as Row num and Col num

	public static String getCellData(int RowNum, int ColNum) throws Exception {
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			return "";
		}
	}// Method
	
	public static String getCellData(int RowNum, int ColNum,XSSFSheet ExcelWSheet) throws Exception {
		try {
			XSSFCell Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			return "";
		}
	}// Method

	// **********************************************************************************************

	// This method is to write in the Excel cell, Row num and Col num are the
	// parameters

	public static void setCellDataPass(String Result, int RowNum, int ColNum)
			throws Exception {
		try {
			XSSFCellStyle my_style = ExcelWBook.createCellStyle();
			Row = ExcelWSheet.getRow(RowNum);
			Cell = Row.getCell(ColNum,
					org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
			// Cell = Row.getCell(ColNum,
			// org.apache.poi.ss.usermodel.Row.RETURN_NULL_AND_BLANK);
			my_style.setFillPattern(XSSFCellStyle.ALIGN_LEFT);
			my_style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
			my_style.setFillBackgroundColor(IndexedColors.SKY_BLUE.getIndex());
			if (Cell == null) {
				Cell = Row.createCell(ColNum);
				Cell.setCellValue(Result);
				Cell.setCellStyle(my_style);
			} else {
				Cell.setCellValue(Result);
				Cell.setCellStyle(my_style);
			}
			// Constant variables Test Data path and Test Data file name
			FileOutputStream fileOut = new FileOutputStream(
					"Q:\\QA Stuff\\Harish Kumar K\\testData\\TestData.xlsx"/*
																		 * Constant.
																		 * Path_TestData
																		 * +
																		 * Constant
																		 * .
																		 * File_TestData
																		 */);
			ExcelWBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			throw (e);
		}
	} // Method

	// //

	public static void setCellDataFail(String Result, int RowNum, int ColNum)
			throws Exception {
		try {
			XSSFCellStyle my_style = ExcelWBook.createCellStyle();
			Row = ExcelWSheet.getRow(RowNum);
			Cell = Row.getCell(ColNum,
					org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
			// Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
			my_style.setFillPattern(XSSFCellStyle.ALIGN_LEFT);
			my_style.setFillForegroundColor(IndexedColors.RED.getIndex());
			my_style.setFillBackgroundColor(IndexedColors.RED.getIndex());
			if (Cell == null) {
				Cell = Row.createCell(ColNum);
				Cell.setCellValue(Result);
				Cell.setCellStyle(my_style);
			} else {
				Cell.setCellValue(Result);
				Cell.setCellStyle(my_style);
			}
			// Constant variables Test Data path and Test Data file name
			FileOutputStream fileOut = new FileOutputStream(
					Constant.Path_TestData + Constant.File_TestData);
			ExcelWBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			throw (e);
		}
	}// Method

	public static List<Registry> getRegisteries() {
		Long time=System.currentTimeMillis();

		List<Registry> registries = new ArrayList<>();
		try {
			// Open the Excel file
			
			String SheetName = "Registeries";
			String excelFilePath1 = properties.getProperty("ScenarioExcelPathforNCDR"); // //
			FileInputStream inputStream1 = new FileInputStream(new File(
					excelFilePath1));
			
			// Access the required test data sheet
			XSSFWorkbook ExcelWBook = new XSSFWorkbook(inputStream1);
			XSSFSheet sheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			int rowCount = sheet.getLastRowNum();
			System.out.println("rowcount:"+rowCount);
//			for (int i = rowCount; i > 0; i--) {
			for (int i = 1; i <= rowCount; i++) {
				Registry registry = new Registry();
				Row row = sheet.getRow(i);
				String registryName = null;
				if (row.getCell(0) != null && row.getCell(0).getCellType() == 0) {
					registryName = row.getCell(0).getNumericCellValue() + "";
				} else if (row.getCell(0) != null
						&& row.getCell(0).getCellType() == 1) {
					registryName = row.getCell(0).getStringCellValue();
				}
				if (registryName != null) {
					registry.setRegistryName(registryName);
				}
				String execute = null;
				if (row.getCell(1) != null && row.getCell(1).getCellType() == 0) {
					execute = row.getCell(1).getNumericCellValue() + "";
				} else if (row.getCell(1) != null
						&& row.getCell(1).getCellType() == 1) {
					execute = row.getCell(1).getStringCellValue();
				}
				if (execute != null) {
					registry.setExecuteRegistry(execute);
				}
				String welcomeLabel = null;
				if (row.getCell(2) != null && row.getCell(2).getCellType() == 0) {
					welcomeLabel = row.getCell(2).getNumericCellValue() + "";
				} else if (row.getCell(2) != null
						&& row.getCell(2).getCellType() == 1) {
					welcomeLabel = row.getCell(2).getStringCellValue();
				}
				if (welcomeLabel != null) {
					registry.setRegistryWelcomeLabel(welcomeLabel);
				}

				String welcomeLabelXpath = null;
				if (row.getCell(3) != null && row.getCell(3).getCellType() == 0) {
					welcomeLabelXpath = row.getCell(3).getNumericCellValue()
							+ "";
				} else if (row.getCell(3) != null
						&& row.getCell(3).getCellType() == 1) {
					welcomeLabelXpath = row.getCell(3).getStringCellValue();
				}
				if (welcomeLabelXpath != null) {
					registry.setRegistryWelcomeLabelXpath(welcomeLabelXpath);
				}
				
				String headers = null;
				if (row.getCell(4) != null && row.getCell(4).getCellType() == 0) {
					headers = row.getCell(4).getNumericCellValue()
							+ "";
				} else if (row.getCell(4) != null
						&& row.getCell(4).getCellType() == 1) {
					headers = row.getCell(4).getStringCellValue();
				}
				if (headers != null) {
					registry.setHeaders(headers);
				}

				registries.add(registry);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Time taken for registeries:"+(System.currentTimeMillis()-time)+" msecs");
		return registries;
	}
	
	public static List<TestDataForACC> getRegisteriesForAcc() {
		
	
		List<TestDataForACC> testDataforACC = new ArrayList<>();
		try {
			// Open the Excel file
			String excelFilePath1 = properties.getProperty("ScenarioExcelPath"); // //
			FileInputStream inputStream1 = new FileInputStream(new File(
					excelFilePath1));
			
			// Access the required test data sheet
			String testscenario = "";
			String username="";
			String password ="";
			XSSFWorkbook ExcelWBook;
			
				ExcelWBook = new XSSFWorkbook(inputStream1);
			
			// Access the required test data sheet
			String SheetName="Sheet2";
			XSSFSheet sheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			int rowCount = sheet.getLastRowNum();
			System.out.println("rowcount:"+rowCount);
//			for (int i = rowCount; i > 0; i--) {
			
			
			for (int i = 1; i <= rowCount; i++) {
				
				TestDataForACC testdata = new TestDataForACC();
				
				Row row = sheet.getRow(i);
				
				String testcase = null;
				if (row.getCell(0) != null && row.getCell(0).getCellType() == 0) {
					testcase = row.getCell(0).getNumericCellValue() + "";
				} else if (row.getCell(0) != null
						&& row.getCell(0).getCellType() == 1) {
					testcase = row.getCell(0).getStringCellValue();
				}
				if (testcase != null) {
					
					
					testdata.setTestCase(testcase);
				}
				
				String currenttestscenario1 = null;
				
				String testscenario1 = null;
				
				String currenttestscenario = null;
				
				if (row.getCell(1) != null && row.getCell(1).getCellType() == 0) {
					currenttestscenario = row.getCell(1).getNumericCellValue() + "";
				} else if (row.getCell(1) != null
						&& row.getCell(1).getCellType() == 1) {
					currenttestscenario = row.getCell(1).getStringCellValue();
				}
				if (currenttestscenario != null && !currenttestscenario.isEmpty()) {
					
					currenttestscenario1=currenttestscenario+"_"+testcase;
					testdata.setTestScenario(currenttestscenario1);
					
					testscenario=currenttestscenario;
				}
				else if(currenttestscenario == null || currenttestscenario.isEmpty())
				{
					testscenario1=testscenario+"_"+testcase;
					
					testdata.setTestScenario(testscenario1);
					
				}
				
				// Logic to get username and password
				
	String currentusername1 = null;
				
				String username1 = null;
				
				String currentusername = null;
				
				if (row.getCell(2) != null && row.getCell(2).getCellType() == 0) {
					currentusername = row.getCell(2).getNumericCellValue() + "";
				} else if (row.getCell(2) != null
						&& row.getCell(2).getCellType() == 1) {
					currentusername = row.getCell(2).getStringCellValue();
				}
				if (currentusername != null && !currentusername.isEmpty()) {
					
					currentusername1=currentusername+"_"+testcase;
					testdata.setUserName(currentusername1);
					
					username=currentusername;
				}
				else if(currentusername == null || currentusername.isEmpty())
				{
					username1=username+"_"+testcase;
					
					testdata.setUserName(username1);
					
				}
				//end of Logic
				
				
		/*		if (row.getCell(2) != null && row.getCell(2).getCellType() == 0) {
					username = row.getCell(2).getNumericCellValue() + "";
				} else if (row.getCell(2) != null
						&& row.getCell(2).getCellType() == 1) {
					username = row.getCell(2).getStringCellValue();
				}
				if (username != null) {
					testdata.setUserName(username);
				}
				*/
				
				// Logic to read password
				
				
String currentpassword1 = null;
				
				String password1 = null;
				
				String currentpassword = null;
				
				if (row.getCell(3) != null && row.getCell(3).getCellType() == 0) {
					currentpassword = row.getCell(3).getNumericCellValue() + "";
				} else if (row.getCell(3) != null
						&& row.getCell(3).getCellType() == 1) {
					currentpassword = row.getCell(3).getStringCellValue();
				}
				if (currentpassword != null && !currentpassword.isEmpty()) {
					
					currentpassword1=currentpassword+"_"+testcase;
					testdata.setPassword(currentpassword1);
					
					password=currentpassword;
				}
				else if(currentpassword == null || currentpassword.isEmpty())
				{
					password1=password+"_"+testcase;
					
					testdata.setPassword(password1);
					
				}
				
				// End of logic
		/*		if (row.getCell(3) != null && row.getCell(3).getCellType() == 0) {
					password = row.getCell(3).getNumericCellValue() + "";
				} else if (row.getCell(3) != null
						&& row.getCell(3).getCellType() == 1) {
					password = row.getCell(3).getStringCellValue();
				}
				if (password != null) {
					testdata.setPassword(password);
				} */
				testDataforACC.add(testdata);
			//	System.out.println("Get testdata "+testdata);
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return testDataforACC;
	
	}
	
	/*public static List<String> getRegisteriesForAcc() {
		Long time=System.currentTimeMillis();

		List<String> registriesAcc = new ArrayList<>();
		try {
			// Open the Excel file
			String excelFilePath1 = properties.getProperty("ScenarioExcelPath"); // //
			FileInputStream inputStream1 = new FileInputStream(new File(
					excelFilePath1));
			
			// Access the required test data sheet
			XSSFWorkbook ExcelWBook = new XSSFWorkbook(inputStream1);
			// Access the required test data sheet
			String SheetName="Sheet2";
			XSSFSheet sheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			int rowCount = sheet.getLastRowNum();
			System.out.println("rowcount:"+rowCount);
//			for (int i = rowCount; i > 0; i--) {
			for (int i = 1; i <= rowCount; i++) {
				String registry = "";
				Row row = sheet.getRow(i);
				String registryName = null;
				if (row.getCell(0) != null && row.getCell(0).getCellType() == 0 ) {
					registryName = row.getCell(0).getNumericCellValue() + "";
				} else if (row.getCell(0) != null
						&& row.getCell(0).getCellType() == 1) {
					registryName = row.getCell(0).getStringCellValue();
				}
				String category= "" ;
				if (row.getCell(1) != null && row.getCell(1).getCellType() == 0 ) {
					category = row.getCell(1).getNumericCellValue() + "";
				} else if (row.getCell(1) != null
						&& row.getCell(1).getCellType() == 1) {
					category = row.getCell(1).getStringCellValue();
				}
				if (registryName != null && category !=null) {
					registry=registryName +"_" + category;
				}
				

				registriesAcc.add(registry);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Time taken for registeries:"+(System.currentTimeMillis()-time)+" msecs");
		return registriesAcc;
	}*/
	
	public static List<String> getRegisteriesforNCDR() {
		Long time=System.currentTimeMillis();

		List<String> registriesAcc = new ArrayList<>();
		try {
			// Open the Excel file
			String excelFilePath1 = properties.getProperty("ScenarioExcelPathforNCDR"); // //
			FileInputStream inputStream1 = new FileInputStream(new File(
					excelFilePath1));
			
			// Access the required test data sheet
			XSSFWorkbook ExcelWBook = new XSSFWorkbook(inputStream1);
			// Access the required test data sheet
			String SheetName="Registeries";
			XSSFSheet sheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			int rowCount = sheet.getLastRowNum();
			System.out.println("rowcount:"+rowCount);
//			for (int i = rowCount; i > 0; i--) {
			for (int i = 1; i <= rowCount; i++) {
				String registry = "";
				Row row = sheet.getRow(i);
				String registryName = null;
				if (row.getCell(0) != null && row.getCell(0).getCellType() == 0) {
					registryName = row.getCell(0).getNumericCellValue() + "";
				} else if (row.getCell(0) != null
						&& row.getCell(0).getCellType() == 1) {
					registryName = row.getCell(0).getStringCellValue();
				}
				if (registryName != null) {
					registry=registryName;
				}
				

				registriesAcc.add(registry);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Time taken for registeries:"+(System.currentTimeMillis()-time)+" msecs");
		return registriesAcc;
	}
	
	
	
	public static Map<String, Registry> getRegistryMap() {
        Long time=System.currentTimeMillis();
		List<Registry> registries = new ArrayList<>();
		Map<String, Registry> registryMap=new LinkedHashMap<String, Registry>();
		try {
			// Open the Excel file
			String Path = "Q:\\QA Stuff\\Harish Kumar K\\testData\\TestData.xlsx";
			String SheetName = "Registeries";
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Access the required test data sheet
			XSSFWorkbook ExcelWBook = new XSSFWorkbook(ExcelFile);
			XSSFSheet sheet = ExcelWBook.getSheet(SheetName); // /SheetName
			System.out.println(SheetName);
			int rowCount = sheet.getLastRowNum();
			//for (int i = rowCount; i > 0; i--) {
				for (int i = 1; i < rowCount; i++) {	
				Registry registry = new Registry();
				Row row = sheet.getRow(i);
				String registryName = null;
				if (row.getCell(0) != null && row.getCell(0).getCellType() == 0) {
					registryName = row.getCell(0).getNumericCellValue() + "";
				} else if (row.getCell(0) != null
						&& row.getCell(0).getCellType() == 1) {
					registryName = row.getCell(0).getStringCellValue();
				}
				if (registryName != null) {
					registry.setRegistryName(registryName);
				}
				String execute = null;
				if (row.getCell(1) != null && row.getCell(1).getCellType() == 0) {
					execute = row.getCell(1).getNumericCellValue() + "";
				} else if (row.getCell(1) != null
						&& row.getCell(1).getCellType() == 1) {
					execute = row.getCell(1).getStringCellValue();
				}
				if (execute != null) {
					registry.setExecuteRegistry(execute);
				}
				String welcomeLabel = null;
				if (row.getCell(2) != null && row.getCell(2).getCellType() == 0) {
					welcomeLabel = row.getCell(2).getNumericCellValue() + "";
				} else if (row.getCell(2) != null
						&& row.getCell(2).getCellType() == 1) {
					welcomeLabel = row.getCell(2).getStringCellValue();
				}
				if (welcomeLabel != null) {
					registry.setRegistryWelcomeLabel(welcomeLabel);
				}

				String welcomeLabelXpath = null;
				if (row.getCell(3) != null && row.getCell(3).getCellType() == 0) {
					welcomeLabelXpath = row.getCell(3).getNumericCellValue()
							+ "";
				} else if (row.getCell(3) != null
						&& row.getCell(3).getCellType() == 1) {
					welcomeLabelXpath = row.getCell(3).getStringCellValue();
				}
				if (welcomeLabelXpath != null) {
					registry.setRegistryWelcomeLabelXpath(welcomeLabelXpath);
				}
				
				String headers = null;
				if (row.getCell(4) != null && row.getCell(4).getCellType() == 0) {
					headers = row.getCell(4).getNumericCellValue()
							+ "";
				} else if (row.getCell(4) != null
						&& row.getCell(4).getCellType() == 1) {
					headers = row.getCell(4).getStringCellValue();
				}
				if (headers != null) {
					registry.setHeaders(headers);
				}
				
				String links = null;
				if (row.getCell(5) != null && row.getCell(5).getCellType() == 0) {
					links = row.getCell(5).getNumericCellValue()
							+ "";
				} else if (row.getCell(5) != null
						&& row.getCell(5).getCellType() == 1) {
					links = row.getCell(5).getStringCellValue();
				}
				if (links != null) {
					registry.setLinks(links);
				}

				registries.add(registry);
				registryMap.put(registryName, registry);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Time taken for registrymap:"+(System.currentTimeMillis()-time)+" msecs");
		return registryMap;
	}
	public static Properties loadProperties() {
		Properties prop = null;
		try {
			prop = new Properties();
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream stream = loader
					.getResourceAsStream("constants.properties");
			prop.load(stream);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return prop;
	}
}// Class