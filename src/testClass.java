import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import utility.Utils;


public class testClass {
	static WebDriver driverChrome=null;
	static WebDriver dRDriver=null;
	static DesiredCapabilities capabilities = null;
	public static void main(String[] args) throws Exception {
		/*WebDriver driver = new FirefoxDriver();
		driver.get("http://www.google.com");
		WebElement body = driver.findElement(By.tagName("body"));
        body.sendKeys(Keys.CONTROL + "t");
        driver.get("http://www.facebook.com");*/
		dRDriver = OpenChrmBrowser(dRDriver, capabilities);
		dRDriver.get("http://www.acc.org/#sort=%40fcommonsortdate86069%20descending");
		//String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,"t");
		Thread.sleep(5000);
		dRDriver.findElement(By.id("ng-app")).sendKeys(Keys.CONTROL +"t");
		dRDriver.get("https://www.google.com");
	}
	public static WebDriver OpenChrmBrowser(WebDriver dRDriver, DesiredCapabilities capabilities)
			throws Exception {
		/*
		 * ExcelUtils.setExcelFile( Constant.Path_TestData reportingPath +
		 * Constant.File_TestData, "Config");
		 */
		// String url = configuration.getUrl();// ExcelUtils.getCellData(2, 1);

		// dRDriver.get(url);
	String chromePath=Utils.loadProperty("chromePath");
	String reportingPath = Utils.loadProperty("reportingPath");
		System.setProperty("webdriver.chrome.driver",
				chromePath + "chromedriver.exe");
				capabilities = DesiredCapabilities.chrome();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("test-type");
//				options.addArguments("--incognito");
				options.addArguments("start-maximized");
				options.addArguments("user-data-dir=" + chromePath);
				capabilities.setCapability("chrome.binary", chromePath
						+ "chromedriver.exe");
				capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				
//		capabilities=getCapabilities(capabilities);
	dRDriver = new ChromeDriver(capabilities);
		
//		dRDriver.manage().window().maximize();
		driverChrome=dRDriver;
		
		
		/*
		 * dRDriver = new ChromeDriver(); 
		 */
		return dRDriver;
	}
}
